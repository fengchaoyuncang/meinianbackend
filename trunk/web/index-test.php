<?php
/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */
date_default_timezone_set('Asia/Shanghai');
// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/test.php';
define('YII_ROOT',dirname(__FILE__));

// remove the following line when in production mode
// defined('YII_DEBUG') or define('YII_DEBUG',true);
//defined('YII_DEBUG') or define('YII_DEBUG',true);
require_once($yii);
require_once(dirname(__FILE__). '/protected/components/ApnsPHP/Autoload.php');

Yii::registerAutoloader('ApnsPHP_Autoload', true);
Yii::createWebApplication($config)->run();
