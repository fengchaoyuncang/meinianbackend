<?php

/**
 * 1. 确定没有患病的病
 * 2. 选择要检查的病
 * 	2.1是否已经问卷调查过
 * 	2.1 查找出体检信息并保存 memecha[tijian]中
 * 	2.3 是否有需要调差的诱因
 * 2. 查找需要做问卷调查的诱因（体检表字段）
 * 	3.1 诱因list
 * 3. 组合问卷list保存在session中
 * 4. 每一次问卷对应的诱因进行保存 保存到memecha[tijian]
 * 5. memecha[tjian] * 权重  （权重表）
 */
//require_once 'QuestionController.php';

class DiseaseController extends Controller {

    /**
     * 风险评估入口，根据疾病做相应处理
     * @param $disid 疾病种类，默认高血压
     */
    public $layout = "\\layouts\analysis";
    private $diseaseService = null;
    function __construct($id) {
        parent::__construct($id);
        $this->diseaseService = new DiseaseService();
    }

    /**
     * $user
     * Enter description here ...
     * @param unknown_type $disid
     * @param unknown_type $user
     */
    public function actionAnalysis($userid=0,$disid=0) {//$user = array('id' => 2, 'username' => 'test')
        if(!isset($_GET["uid"])||!isset($_GET["disid"])){
            echo '非法请求';
            return;
        }
        $disid = $_GET["disid"];
        $userid = $_GET["uid"];
        $dis = new Disease($disid); //$items中包含了所有的该疾病的诱因

        $items = $dis->getElements();

        if (!count($items)) {
            //错误，该疾病没有相关的诱因
//            JSONHelper::echoJSON(false,  JSONHelper::ERROR_INTERNAL);
            echo '内部错误';
            return;
        }

        $userinfo = Disease::getUserInfo($userid);

        $ques = array();
        foreach ($items as $key => $element_name) {
            $col = $element_name;
            //用户所有信息列默认为空，这里判断如果为空则表示信息不全，需要提供问题
            if ($userinfo->$col === NULL) {
                $ret = EvaluateQuestionElement::model()->find("element_name=:name", array(":name" => $element_name));
                if (!empty($ret)) {
                    array_push($ques, $ret->question_id);
                }
            }
        }

        if (count($ques)) {
            /* //保存所有需要问的问题ID
              Yii::app()->session["disid"] = $disid;
              Yii::app()->session["ques"] = array_values(array_unique($ques));
              //记录下需要问的问题，跳转到问题页面
              //                var_dump($ques); */

            $ques = array_values(array_unique($ques));
            $ques_str = implode(",", $ques);
            $this->redirect($this->createUrl("question/question",array(
                "ques_str"=>$ques_str,"disid"=>$disid,"uid"=>$userid
            )));
        } else {
            //直接风险评估
            /* Yii::app()->session["disid"] = $disid; */
            $ret = $dis->assess($userid);
            $resolve = $dis->getAdvice($userid);
            $dis_detail = EvaluateDiseaseType::model()->findByPk($disid);
            $suggestion_ret = EvaluateMultiSuggestion::model()->findByAttributes(array("diseaseid" => $disid));

            $nutrition_program = EvaluateNutritionProgram::model()->findByAttributes(array("diseaseid" => $disid));

            $this->render("assess", array("result" => $ret, "disdetail" => $dis_detail, "resolve" => $resolve, "suggestion" => $suggestion_ret, "nutritionsuggestion" => $nutrition_program,"uid" => $userid,"disid" => $disid));
        }
    }

    private function getDisType($disid) {
        $type = "";
        switch ($disid) {
            case 1:
                $type = "高血压";
                break;
            case 2:
                $type = "冠心病";
                break;
            case 3:
                $type = "脑卒中";
                break;
            case 4:
                $type = "糖尿病";
                break;
            default:
                $type = "未知";
                break;
        }
        return $type;
    }

    /**
     * 营养方案
     */
    public function actionNutritionlist(){
        $result = $this->diseaseService->getNutritionList($_GET);
        if (is_int($result)) {
            JSONHelper::echoJSON(false, $result);
        }
        JSONHelper::echoJSON(true, $result);
    }
    /**
     * 营养方案项目详情
     */
    public function actionNutritionItem(){
        $result = $this->diseaseService->getNutritionItem($_GET);
        if (is_int($result)) {
            echo "错误的请求";
            return;
        }
        $this->render("nutrition_item",array("item"=>$result));
    }

    /**
     * return to the index page from result page
     * Enter description here ...
     */
    public function actionListOut() {        
        $result = $this->diseaseService->getList();

        if (is_int($result)) {
            JSONHelper::echoJSON(false, $result);
        }
        JSONHelper::echoJSON(true, $result);
    }

    /**
     * remove the old question answer,
     * Enter description here ...
     */
    public function actionQuestionAgain(){
    	if (isset($_GET['uid'])){
    		$userid = $_GET['uid'];
    	}else{
    		return 0;
    	}
    	if (isset($_GET['disid'])){
    		$disid = $_GET['disid'];
    	}else{
    		return 0;
    	}
    	
    	$ret = Disease::deleteUserInfo($userid);
    	
    	$this->actionAnalysis($userid,$disid);
    }
}