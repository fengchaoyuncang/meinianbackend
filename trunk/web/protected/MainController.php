<?php

class MainController extends Controller {

    public $layout = "//layouts/main-website";
    private $diseaseService = null;

    function __construct($id) {
        parent::__construct($id);
        $this->diseaseService = new DiseaseService();
    }

    public function actionIndex($status = false) {
        if (isset(Yii::app()->session['user_n_p'])) {
            $status = true;
        }
        $this->render("index", array("login_status" => $status));
    }

    public function actionCancellation() {
        unset(Yii::app()->session['user_n_p']);
        unset(Yii::app()->session['user']);
        $this->render("index", array("login_status" => false));
    }

    public function actionError() {
        $this->render("error");
    }

    /*
     * 列出体检机构列表
     * @param 无
     * return 数据集
     */

    public function actionCenterList() {        
         $B2bSubCenter = new B2bSubCenter();
        //获取所有城市名称
         $cities = $B2bSubCenter->getCities();
         array_unshift($cities, "全部");
//           print_r($cities);exit;
        //获取所有体检机构        
        $rets = $B2bSubCenter->getSubcenterEachPage();
        $this->render('sub_center_list', array(
            'rets' => $rets,
            'cities'=>$cities,
        ));
    }
    
    /*
     * 获取属于某个城市的体检中心
     * @param $city 城市
     * return 体检中心数据集
     */
    public function actionCenterToCity(){
         $B2bSubCenter = new B2bSubCenter();
        
        //获取所有城市名称
         $cities = $B2bSubCenter->getCities();
         array_unshift($cities, "全部");
       
        
        //获取属于某个城市的体检中心
        $rets = $B2bSubCenter->getSubcenterToCityEachPage();
        $this->render('sub_center_list',array(
            'rets'=>$rets,
            'cities'=>$cities,
        ));
    }
    
    /*
     * 列出套餐列表
     * @param 无
     * return 数据集
     */

    public function actionPackageList() {
        $package = new B2bPackage();
        $rets = $package->getPackageEachPage();
        $this->render('package_list', array(
            'rets' => $rets,
        ));
    }


    /**
     * 营养方案
     */
    public function actionNutritionlist() {
        $result = $this->diseaseService->getNutritionList($_GET);
        $this->render("nutrition_list",array("result"=>$result));
    }

}