<style>
    #health_left {
        padding: 45px 30px 45px;
        line-height: 24px;
        color: #646464;
        font-size: 14px;
        width: 700px;
        float: left;
        border: 1px solid #c8dfff;
        margin: 0 10px 320px 0;
        text-indent: 24px;
    }
    #health_right {
        width: 260px;
        float: left;
        border: 1px solid #c8dfff;
        padding: 15px;
    }
</style>
<div id="main">
    <div id="health_left">
        <div id="health_left_title"><?=$infopaper_ret['title']?></div>
        <div id="health_sub_title"></div>
		    <?php
				echo $infopaper_ret['content'];
			?>
    </div>


    </div>
    <div id="health_right">
        <div id="health_right_top">
            <div id="health_arrows"></div>
            <div id="health_right_title">最新健康咨询</div>
        </div>
        <div id="healt_right_body">
            <ol>
				<?php $pagenum = 1;
				foreach ($infopaper_list->getData() as $papertitle){?>
                <li><span class="item_num"><?=$pagenum++?></span><a href="index.php?r=infoPapers/getContent/paperid/<?=$papertitle->id?>"><?=$papertitle->title?></a></li>
                <?php }?>
            </ol>
            <?php $this->widget('CLinkPager', array(
				        'header' => '',
				        'firstPageLabel' => '首页',
				        'lastPageLabel' => '末页',
				        'prevPageLabel' => '上一页',
				        'nextPageLabel' => '下一页',
				        'pages' => $infopaper_list->getPagination(),
				        'maxButtonCount' => 0
            ));?>
        </div>                
    </div>
<script type="text/javascript">
//function getcontent(){
//	$.post("<?=Yii::app()->createUrl("infoPapers/getContent")?>",
//			{
//				paperid:
//			}
//			);
//}
</script>