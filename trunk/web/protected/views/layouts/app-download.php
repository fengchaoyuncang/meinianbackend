<html>
<head>
	<meta charset="utf-8">
	<title>郁金香云健康</title>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/css_reset.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/stand_module.css">	
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/index.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/package.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/center.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/image_slider.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/medical_guide.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/center_details.css">
        
<!--	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/pngopacity.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/roll.js"></script>-->
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/input_nature.js"></script>
         <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/bxCarousel.js"> </script> 
        <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
</head>
<body>
    <?php echo $content; ?>
</body>
</html>