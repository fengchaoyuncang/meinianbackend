<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>郁金香云健康-注册</title>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/css_reset.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/stand_module.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/odd_css/enroll.css">
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/pngopacity.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/input_nature.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/cache_merge.js"></script>
        <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
</head>
<body>
		 <?php echo $content; ?>
		 <div class="bottom" id="bottom_">
			<ul class="bottom-border">
				<li class="bottom-line bottom-line-text-external">
					<ul class="bottom-line-text">
						<li>
							<a href="#">联系我们</a>|
							<a href="#">商务合作</a>|
							<a href="#">商务合作</a>|
							<a href="#">商务合作</a>|
							<a href="#">商务合作</a>
						</li>
						<li>版权所有:www.yixyun.com</li>
						<li>公司地址:北京市海淀区被理工科技大厦</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>
