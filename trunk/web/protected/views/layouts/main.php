<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="zh-CN" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <?php Yii::app()->bootstrap->register(); ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <div id="mainmenu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => '主页', 'url' => array('/site/index')),
                        array('label' => '体检项', 'url' => array('/examItem')),
                        array('label' => '体检指标', 'url' => array('/indicator')),
                        array('label' => '资讯', 'url' => array('/web/papers')),
                        array('label' => '推送', 'url' => array('/web/push')),
                        array('label' => '关于', 'url' => array('/site/page', 'view' => 'about')),
                        array('label' => '联系我们', 'url' => array('/site/contact')),
                        array('label' => '登录', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => '退出 (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                    ),
                ));
                ?>
            </div><!-- mainmenu -->
            <div id="sub_mainmenu">
                <?php
//                $this->widget('zii.widgets.CMenu', array(
//                    'items' => array(
//                        array('label' => '新预约订单', 'url' => array('/center/order/new')),
//                        array('label' => '预约成功订单', 'url' => array('/center/order/succeed')),
//                        array('label' => '已经体检的订单', 'url' => array('/center/order/checked')),
//                        array('label' => '已经取消的订单', 'url' => array('/center/order/canceled')),
//                        array('label' => '取得体检报告的订单', 'url' => array('/center/order/reported')),                                               
//                    ),
//                ));
                ?>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'homeLink' => CHtml::link('首页', Yii::app()->homeUrl),
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

		
            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; 2010-<?php
            date_default_timezone_set('Asia/Shanghai');
            echo date('Y');
            ?> by 郁金香云健康<br/>
                All Rights Reserved.<br/>
                <?php //echo Yii::powered(); ?>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
