<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="zh-CN"/>

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>
    <?php Yii::app()->bootstrap->register(); ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
    </div>
    <!-- header -->

    <div id="mainmenu">
        <?php
        $this->widget('bootstrap.widgets.TbMenu', array(
            'type' => 'pills',
            'items' => array(
                array('label' => '体检中心管理', 'items' => array(
//                            array('label'=>'品牌管理', 'url'=>array('admin/brand/admin')),
                    array('label' => '子体检中心管理', 'url' => array('admin/subcenter/admin')),
                    array('label' => '不可预约日期管理', 'url' => array('admin/b2bCenterNotpermittedDay/admin')),

                )),
                array('label' => '体检管理', 'items' => array(
                    array('label' => '体检项目', 'url' => array('admin/examItem/admin')),
                    array('label' => '体检指标', 'url' => array('admin/indicator/admin'))
                )),
                array('label' => '套餐管理', 'url' => array('admin/package/admin')),
//                        array('label' => '体检项目管理', 'items' => array(
//                        	array('label' => '小项目管理', 'url' => array('admin/basicexamitem')),
//                        	array('label' => '大项目管理', 'url' => array('admin/basicexamitemclassify')),
//                        )),
//                        array('label' => '公司管理', 'url' => array('admin/company/admin')),
                array('label' => '预约管理', "items" => array(
                    array('label' => '非团购预约管理', 'url' => array('admin/b2bTicket/admin')),
                    array('label' => '团购预约管理', 'url' => array('admin/b2bTicketFcode/admin')),
                    array('label' => '微信预约管理', 'url' => array('admin/yuyue/admin'))
                )),
                array('label' => '团购公司管理', 'url' => array('admin/b2bTuangouCompany/admin')),
                array('label' => '购买管理', 'url' => array('admin/b2cPurchaseRecords/admin')),
                array('label' => '用户管理', 'url' => array('admin/b2bUser/admin')),
//                        array('label' => '账户管理', 'items' => array(
//                            array('label'=>'美年帐号导入', 'url'=>array('admin/extuser/import')),
//                            array('label'=>'美年帐号管理', 'url'=>array('admin/extuser/admin')),
//                            array('label'=>'管理员账号', 'url'=>array('admin/user/index')),
//                        )),
                array('label' => '登录', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => '退出 (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
            ),
        ));
        ?>
    </div>
    <!-- mainmenu -->
    <?php if (isset($this->breadcrumbs)): ?>
        <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
//                    'homeLink' => CHtml::link('首页', Yii::app()->homeUrl),
           			'homeLink' => CHtml::link('首页', 'index.php?r=site/index'),
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
    <?php endif ?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; 2010-<?php date_default_timezone_set('Asia/Shanghai');
        echo date('Y');
        ?> 美年大健康.<br/>
        All Rights Reserved.<br/>

        <?php // echo Yii::powered(); ?>

    </div>
    <!-- footer -->

</div>
<!-- page -->

</body>
</html>
