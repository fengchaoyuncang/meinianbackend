<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>郁金香云健康</title>
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]>  
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>  
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/css_reset.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/stand_module.css">	
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/index.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/package.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/center.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/image_slider.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/medical_guide.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/center_details.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/appstyle.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/wait.css">

        
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/jquery/cookie/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/input_nature.js"></script>
         <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/bxCarousel.js"> </script> 
         	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/arrow27.js"></script>
        <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
</head>
<!--[if IE 6]>
		<script src="js/DD_belatedPNG.js"></script>
		<script> DD_belatedPNG.fix('*');</script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="js/pngopacity.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('*');
	</script> 
<![endif]-->
<body>
	<div class="hea">

<script>
	$(document).ready(function(){
		$(".navbar-line a").each(function(index){
		    $(this).attr('id',"MainMenu"+index);
		});
		if(!$.cookie("menu_index")){
			$.cookie("menu_index","MainMenu0");
		}
		$(".navbar-line a").each(function(){
			
			 if($(this).attr('id')==$.cookie("menu_index")){
			    $(this).addClass("selected");
			 }else{
			    $(this).removeClass("selected");
			 }
	    });
		$(".navbar-line a").click(function(){
             $.cookie("menu_index",$(this).attr('id'));
		});
	});
</script>
<div class="header">
<div class="header-inner clear">
<h2 class="logo left"><a href="#" title="郁金香云健康"></a></h2>
<div class="operate right clear">
<div class="landing right">
	<?php if(Yii::app()->user->getIsGuest() === true){?>
	<a href="index.php?r=login/pageregister">登陆</a> 
	<span class="landing-border left"></span> <a href="index.php?r=login/pageenroll">注册</a></div>
	<?php }else{?>
		<a href="index.php?r=main/cancellation">退出登录</a></div>
	<?php }?>
<ul class="navbar right clear">
	<li class="navbar-line navbar-line-bottom"><a href="index.php?r=main/index">首页</a></li>
	<li class="navbar-line"><a href=<?php 
										if(Yii::app()->user->getIsGuest() === true){
											echo "index.php?r=login/pageregister";
										}else{
											echo "index.php?r=package/yuyue";
										}?>>体检预约</a></li>
<!--	company/manager/index-->
	<li class="navbar-line"><a href="index.php?r=main/centerlist">体检机构</a></li>
	<li class="navbar-line"><a href="index.php?r=main/medicalguide">体检须知</a></li>
	<li class="navbar-line"><a href="index.php?r=main/packagelist">体检套餐</a></li>
</ul>
</div>
</div>
</div>
     <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'homeLink' => CHtml::link('首页', Yii::app()->homeUrl),
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>
		 <?php echo $content; ?>

		 

<noscript><a href="http://li-nks.com">Links Directory</a><br /><a href="http://www.scrolltotop.com">Scroll to Top</a></noscript>


		 <div class="bottom">
		 
<ul class="bottom-border">
	<li class="bottom-line">
	<h3 class="bottom-line-title">合作体检联盟</h3>
	</li>
	<li class="bottom-line bottom-line-img-external">
	<ul class="bottom-line-img clear">
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/bottom_img.gif"> </a></li>
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/aikang.jpg"> </a></li>
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/meinian.jpg"> </a></li>
	</ul>
	</li>
	<li class="bottom-line bottom-line-text-external">
	<ul class="bottom-line-text">
		<li>
			<a href="#">联系我们</a>|
			<a href="index.php?r=main/mobiledownload">手机应用</a>| 
			<a href="#">免责声明</a>
		</li>
		<li>版权所有:www.yjxtj.com</li>
		<li>公司地址:北京市海淀区北理工科技大厦</li>
	</ul>
	</li>
</ul>
</div>
<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/md5.js"></script>
</body>
</html>
