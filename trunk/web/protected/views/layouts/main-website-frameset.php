
<!DOCTYPE html>
<?php $menuurl = Yii::app()->createUrl('/package/package_menu');?>
<?php $contenturl = Yii::app()->createUrl('/package/yuyue');?>
<html>
<head>
	<meta charset="utf-8">
	<title>郁金香云健康</title>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/css_reset.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/stand_module.css">	
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/index.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/package.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/center.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/image_slider.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/medical_guide.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/center_details.css">
        
<!--	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/pngopacity.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/roll.js"></script>-->
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/input_nature.js"></script>
         <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/bxCarousel.js"> </script> 
        <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
</head>
<!--[if IE 6]>
		<script src="js/DD_belatedPNG.js"></script>
		<script> DD_belatedPNG.fix('*');</script>
	<![endif]-->
	<!--[if lt IE 7]>
	<script src="js/pngopacity.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('*');
	</script> 
<![endif]-->


	<div class="hea">
<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		)); ?><!-- breadcrumbs -->
<?php endif?>
<script>
</script>
<div class="header">
<div class="header-inner clear">
<h2 class="logo left"><a href="#" title="郁金香云健康"></a></h2>
<div class="operate right clear">
<div class="landing right">
	<?php if(Yii::app()->user->getIsGuest() === true){?>
	<a href="index.php?r=login/pageregister">登陆</a> 
	<?php }else{?>
		<a href="index.php?r=main/cancellation">退出登录</a>
	<?php }?>
<span class="landing-border left"></span> <a href="index.php?r=login/pageenroll">注册</a></div>
<ul class="navbar right clear">
	<li class="navbar-line navbar-line-bottom"><a href="index.php?r=main/index">首页</a></li>
	<li class="navbar-line"><a href="index.php?r=package/index">体检预约</a></li>
<!--	company/manager/index-->
	<li class="navbar-line"><a href="index.php?r=main/centerlist">体检机构</a></li>
	<li class="navbar-line"><a href="index.php?r=main/medicalguide">体检须知</a></li>
	<li class="navbar-line"><a href="index.php?r=main/packagelist">体检套餐</a></li>
</ul>
</div>
</div>
</div>
    <frameset rows="60px,*" border='0' framespacing='0'frameborder='0'>
		<frame  src="<?=$menuurl?>">
		<frame src="<?=$contenturl?>" name="mainframe">                   
</frameset>

		 <?php // secho $content; ?>




		 <div class="bottom">
<ul class="bottom-border">
	<li class="bottom-line">
	<h3 class="bottom-line-title">合作体检联盟</h3>
	</li>
	<li class="bottom-line bottom-line-img-external">
	<ul class="bottom-line-img clear">
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/bottom_img.gif"> </a></li>
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/aikang.jpg"> </a></li>
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/meinian.jpg"> </a></li>
		<li><a href="#"> <img src="<?=Yii::app()->baseUrl?>/images/front/guobin.jpg"> </a></li>
	</ul>
	</li>
	<li class="bottom-line bottom-line-text-external">
	<ul class="bottom-line-text">
		<li><a href="#">联系我们</a>| <a href="#">商务合作</a>| <a href="#">商务合作</a>|
		<a href="#">商务合作</a>| <a href="#">商务合作</a></li>
		<li>版权所有:www.pocdoc.cn</li>
		<li>公司地址:北京市海淀区北理工科技大厦</li>
	</ul>
	</li>
</ul>
</div>
<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/md5.js"></script>

</html>
