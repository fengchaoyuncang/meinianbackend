<?php
/* @var $this PapersController */
/* @var $data Papers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
    <?php echo CHtml::encode($data->type); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('related_item_id')); ?>:</b>
    <?php echo CHtml::encode($data->related_item_id); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('related_indicator_id')); ?>:</b>
    <?php echo CHtml::encode($data->related_indicator_id); ?>
    <br />
    
	<b><?php echo CHtml::encode($data->getAttributeLabel('abstract')); ?>:</b>
	<?php echo CHtml::encode($data->abstract); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('symptom')); ?>:</b>
    <?php echo CHtml::encode($data->symptom); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('examination')); ?>:</b>
    <?php echo CHtml::encode($data->examination); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('treatment')); ?>:</b>
	<?php echo CHtml::encode($data->treatment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suggestion')); ?>:</b>
	<?php echo CHtml::encode($data->suggestion); ?>
	<br />
</div>