<?php
/* @var $this PapersController */
/* @var $model Papers */

$this->breadcrumbs=array(
	'资讯'=>array('index'),
	'创建',
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>创建资讯</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, "allItems"=>$allItems, "allIndicators"=>$allIndicators)); ?>