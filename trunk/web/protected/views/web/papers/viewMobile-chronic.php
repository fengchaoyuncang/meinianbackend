<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php Yii::app()->clientScript->registerScriptFile('/js/jquery/mobile/jquery.mobile-1.3.0.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile('/js/jquery/mobile/jquery.mobile-1.3.0.min.css'); ?>
<?php Yii::app()->clientScript->registerCss('papers-viewMobile-bg', "
    body {
        /*background-color : #edf5db;*/
    }
"); ?>

<!-- content -->
<div data-role="content">
    <div data-role="collapsible-set" data-inset="false">
        <div data-role="collapsible" data-collapsed="false">
            <h3><?php echo $model->title; ?></h3>
            <div><?php echo $model->content; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>症状</h3>
            <div><?php echo $model->symptom; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>检查</h3>
            <div><?php echo $model->examination; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>治疗</h3>
            <div><?php echo $model->treatment; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>专家建议</h3>
            <div><?php echo $model->suggestion; ?></div>
        </div>

        <div>
            <a href="wtai://wp/mc;4000988855" data-role="button">专家电话资询: 400-098-8855</a>
            <?php echo CHtml::link('百度百科一下' . $model->title,
                'http://wapbaike.baidu.com/search?word=' . $model->title,
                array('data-role' => 'button')); ?>
        </div>
    </div>
</div>
