<?php
/* @var $this PapersController */
/* @var $model Papers */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID号'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'标题'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'类型'); ?>
        <?php echo $form->textField($model,'type'); ?>
    </div>

    <div class="row">
		<?php echo $form->label($model,'摘要'); ?>
		<?php echo $form->textArea($model,'abstract',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'正文'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('搜索'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'症状'); ?>
        <?php echo $form->textArea($model,'symptom',array('rows'=>6, 'cols'=>50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'检查'); ?>
        <?php echo $form->textArea($model,'examination',array('rows'=>6, 'cols'=>50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'治疗'); ?>
        <?php echo $form->textArea($model,'treatment',array('rows'=>6, 'cols'=>50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'专家建议'); ?>
        <?php echo $form->textArea($model,'suggestion',array('rows'=>6, 'cols'=>50)); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->