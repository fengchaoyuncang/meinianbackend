<?php
/* @var $this PapersController */
/* @var $model Papers */

$this->breadcrumbs=array(
	'资讯'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>查看资讯文章，ID号为 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'abstract',
		'content',
        'type',
        'related_item_id',
        'related_indicator_id',
        'symptom',
        'examination',
        'treatment',
        'suggestion',
	),
)); ?>
