<?php
/* @var $this PapersController */
/* @var $model Papers */
/* @var $form CActiveForm */
?>

<!-- jquery -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<!-- jquery te -->
<?php Yii::app()->clientScript->registerScriptFile('/js/jquery/te/jquery-te-1.3.3.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile('/js/jquery/te/jquery-te-1.3.3.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile('/js/papers/chronic.js'); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'papers-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有<span class="required">*</span> 的域是必填项。</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'标题'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
	
    <div class="row">
        <?php echo $form->labelEx($model,'类型（2:慢性病防治， 3：保健常识）'); ?>
        <?php echo $form->dropDownList($model,'type', array('2' => '慢性病防治', '3' => '保健常识')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>
	
    <div class="row">
        <?php echo $form->labelEx($model,'关联体检项'); ?>
        <?php $allItems['-1']='请选择'; ksort($allItems);
              echo $form->dropDownList($model, 'related_item_id', $allItems);?>
        <?php echo $form->error($model,'related_item_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'关联者体检指标'); ?>
        <?php $allIndicators['-1']='请选择'; ksort($allIndicators);
              echo $form->dropDownList($model, 'related_indicator_id', $allIndicators);?>
        <?php echo $form->error($model,'related_indicator_id'); ?>
    </div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'摘要'); ?>
		<?php echo $form->textArea($model,'abstract',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'abstract'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'正文'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

    <div class="row chronic">
        <?php echo $form->labelEx($model,'symptom'); ?>
        <?php echo $form->textArea($model,'symptom',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'symptom'); ?>
    </div>

    <div class="row chronic">
        <?php echo $form->labelEx($model,'examination'); ?>
        <?php echo $form->textArea($model,'examination',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'examination'); ?>
    </div>

    <div class="row chronic">
        <?php echo $form->labelEx($model,'treatment'); ?>
        <?php echo $form->textArea($model,'treatment',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'treatment'); ?>
    </div>

    <div class="row chronic">
        <?php echo $form->labelEx($model,'suggestion'); ?>
        <?php echo $form->textArea($model,'suggestion',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'suggestion'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->