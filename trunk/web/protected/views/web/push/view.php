<?php
/* @var $this SiteController */
/* @var $model PushForm */
/* @var $papers papers array('id' => 'title') */
/* @var $indicators indicator array('id'=>'name');*/
/* @var $items exam item array('id'=>'name');*/
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - 推送';
$this->breadcrumbs=array(
	'推送',
);
?>

<h1>推送</h1>

<p>请选择需要推送的文章标题名，以及他所关联的体检项目/体检指标名:</p>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'push-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">带有<span class="required">*</span> 号的是必填项。其中体检项目和体检指标，可以仅选择其中一个，也可以都选择。如果都选，则订阅这两个主题的人都会收到该次推送（同一个人仅会收到一次）。平台也是，可以都选，也可以只选择一个。</p>
	
	<div class="row">
		<?php echo $form->labelEx($model, '选择文章<span class="required">*</span>'); ?>
		<?php $papers['choose']='请选择'; 
			  ksort($papers);
			  echo $form->dropDownList($model, 'paper_id', $papers);?>
		<?php echo $form->error($model, 'paper_id');?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, '选择相关的体检项<span class="required">*</span>'); ?>
		<?php $items['choose']='请选择'; 
			ksort($items);
			echo $form->dropDownList($model, 'item_id', $items);?>
		<?php echo $form->error($model, 'item_id');?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model, '选择相关的体检指标<span class="required">*</span>'); ?>
		<?php $indicators['choose']='请选择'; 
			ksort($indicators);
			echo $form->dropDownList($model, 'indicator_id', $indicators);?>
		<?php echo $form->error($model, 'indicator_id');?>
	</div>

	<div>
		<?php echo $form->labelEx($model, '选择推送的手机平台'); ?>
		<?php echo $form->checkBox($model, 'android', array('checked'=>'checked', 'value'=>'1', 'uncheckValue'=>'0')) ?>
		<?php echo $form->labelEx($model, 'ANDROID(安卓)'); ?>
   		<?php echo $form->error($model, 'android');?>
		<?php echo $form->checkBox($model, 'ios', array('checked'=>'checked', 'value'=>'1', 'uncheckValue'=>'0')) ?>
		<?php echo $form->labelEx($model, 'IOS(苹果)'); ?>
   		<?php echo $form->error($model, 'ios');?>
	</div>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('推送'); ?>
	</div>

	

<?php $this->endWidget(); ?>
</div><!-- form -->
