<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/package_list.css" media="screen" />
<style>
  #package_banner {
    margin: 20px auto 0;
  }
  .package-wrapper li {
    width: 900px;
    height: 250px;
    margin: 30px auto 0;
  }
  .package-wrapper li img {
    float: left;
    padding: 10px;
  }
  .price-wrapper {
    width: 70%;
    height: 250px;
    float: left;
    border-top: 1px dashed #999;
    border-bottom: 1px dashed #999;
    position: relative;
  }
  .price-wrapper h1 {
    font-size: 1.4em;
    float: left;
    margin-top: 50px;
    text-indent: 20px;
  }
  .price-wrapper p {
    float: left;
    clear: left;
    font-size: .8em;
    border: 1px dashed #FDE3CC;
    background: #FFFDF6;
    padding: 4px;
    margin: 8px 0 10px 10px;
  }
  .price-wrapper div {
    width: 95%;
    height: 62px;
    clear: left;
    position: relative;
    left: -10px;
    background: #21D492;
  }
  .price-wrapper .price {
    color: white;
    line-height: 2.2em;
    font-size: 1.8em;
    padding-left: 40px;
    font-weight: bolder;
  }
  .price-wrapper .del-price {
    height: 2.2em;
    display: inline-block;
    color: white;
    text-decoration: line-through;
    padding: 20px 50px 0 30px;
    border-right: 1px dotted #ccc;
  }
  .price-wrapper a.purchase {
    cursor: pointer;
    line-height: 2em;
    font-size: 1.2em;
    border-radius: 4px;
    background: white;
    padding: 6px 12px;
    position: relative;
    top: -4px;
    margin-left: 30px;
    color: #21D492;
  }
  .price-wrapper div.blue {
    background: #00A6EA;
  }
  .price-wrapper div.yellow {
    background: #FDA72E;
  }
  .price-wrapper a.blue {
    color: #00A6EA;
  }
  .price-wrapper a.yellow {
    color: #FDA72E;
  }
  div.white-tri {
    width:0px; 
    height:0px; 
    border:31px solid; 
    position: relative;
    left: 526px;
    top: -62px;
    border-color:transparent white transparent transparent;
  }
</style>
<div id="package_banner">
  <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/front/package_banner.png"/> 
</div>
<div id="content">   
  <ul class="package-wrapper">
    <li>
      <img src="<?=Yii::app()->baseUrl?>/images/front/package_01.jpg" alt="遂心卡"/>
      <div class="price-wrapper">
        <h1>遂心卡</h1>
        <p>本套餐适合各个年龄段的人群进行普检</p>
        <div>
          <span class="price">￥260.00</span>
          <span class="del-price">￥377.00</span>
          <a href="<?=$this->createUrl("package/view",array("pname"=>"suixinka"))?>" class="purchase">马上抢</a>
        </div>
        <div class="white-tri"></div>
      </div>
    </li>
    <li>
      <img src="<?=Yii::app()->baseUrl?>/images/front/package_02.jpg" alt="顺心卡"/>
      <div class="price-wrapper">
        <h1>顺心卡</h1>
        <p>本套餐适合各个年龄段的人群进行普检</p>
        <div class="blue">
          <span class="price">￥550.00</span>
          <span class="del-price">￥917.00</span>
          <a href="<?=$this->createUrl("package/view",array("pname"=>"shunxinka"))?>" class="purchase blue">马上抢</a>
        </div>
        <div class="white-tri blue"></div>
      </div>
    </li>
    <li>
      <img src="<?=Yii::app()->baseUrl?>/images/front/package_03.jpg" alt="舒心卡"/>
      <div class="price-wrapper">
        <h1>舒心卡</h1>
        <p>适合身体无疾病史的用户，检查身体的基本状况</p>
        <div class="yellow">
          <span class="price">￥360.00</span>
          <span class="del-price">￥562.00</span>
          <a href="<?=$this->createUrl("package/view",array("pname"=>"shuxinka"))?>" class="purchase yellow">马上抢</a>
        </div>
        <div class="white-tri yellow"></div>
      </div>
    </li>
  </ul>
</div>