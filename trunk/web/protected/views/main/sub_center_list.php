<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/center2city.css" media="screen" />
<script type="text/javascript">
  $(document).ready(function() {
    $(".item_name").click(function() {
      $this.addClass('item_name_visited');
    });
  });
</script>
<div id="top" class="clearfix">
  <div class="item_title">请选择地址</div>
  <div id="item_name_main">
    <a class='item_name' href='index.php?r=main/centertocity'>全部</a>
    <?php
    foreach ($cities as $key => $value) {
      echo "<a class='item_name' href='index.php?r=main/centertocity&province=" . $key . "'>" . $key."</a>";
    }
    ?>
  </div>
</div>
<div id="main">
  <?php
  foreach ($items["items"] as $v) {
    echo "<ul class='item_ul'><li  class='liTH'>" . $v["name"] . "</li><li  class='liTD'><strong>机构地址：</strong>" . $v["address"] . "</li><li  class='liTD'><strong>联系电话：</strong>" .$v["telephone"] . "</li><p class='item_details'>";
  ?>
  <a href='index.php?r=main/centerdetails/cid/<?=$v["id"]?>'>查看详情</a></p></ul>
  <?php }?>
</div>
<div id="footer">
  <?php
  $this->widget('CLinkPager', array(
    'header' => '',
    'firstPageLabel' => '首页',
    'lastPageLabel' => '末页',
    'prevPageLabel' => '上一页',
    'nextPageLabel' => '下一页',
    'pages' => $items["page"],
    'maxButtonCount' => 10
     )
  );
  ?>

</div>

