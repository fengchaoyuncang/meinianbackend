
<style>
    .h{display:none}
</style>
<?php // var_dump($titles);eixt();?>
<div id='big_pic'>
	<div id="app_download">
		<a href="index.php?r=main/mobiledownload">下载手机版</a>
	</div>
</div>
	
<div class="ind_roll" id="roll">
    <div class="bx_container">
        <ul class="ind_roll_interior" id="roll_interior">
            <li style="width: 1663px; height: 389px;"><a href="#" class="ind_roll_1"></a></li>
            <li style="width: 1663px; height: 389px;"><a href="#" class="ind_roll_2"></a></li>
            <li style="width: 1663px; height: 389px;"><a href="#" class="ind_roll_3"></a></li>
            <li style="width: 1663px; height: 389px;"><a href="#" class="ind_roll_4"></a></li>
        </ul>
        <div class="ind-mobile"><a href="index.php?r=main/mobiledownload"></a>
            <ul class="ind_roll_figure" id="roll_figure">
<!--                <li style="color: rgb(255, 255, 255);">1</li>-->
<!--                <li style="color: rgb(255, 255, 255);">2</li>-->
<!--                <li style="color: rgb(255, 255, 255);">3</li>-->
<!--                <li style="color: red;">4</li>-->
            </ul>
        </div>
    </div>
</div>
<div class="ind-content">
    <div class="ind-flow-external">
        <h2 class="title"><a href="#">预约体检流程</a></h2>
        <div class="decorate">
            <div class="onset"></div>
            <div class="pregnancy"></div>
        </div>
        <ul class="ind-flow clear">
            <li>
                <dl>
                    <dt><a href="#" class="ind-flow-img_1"> </a></dt>
                    <dd><a href="#">No.1</a></dd>
                    <dd><a href="#">注册账户</a></dd>
                </dl>
            </li>
            <li class="ind-flow-direction"></li>
            <li>
                <dl>
                    <dt><a href="#" class="ind-flow-img_2"> </a></dt>
                    <dd><a href="#">No.2</a></dd>
                    <dd><a href="#">选择体检机构</a></dd>
                </dl>
            </li>
            <li class="ind-flow-direction"></li>
            <li>
                <dl>
                    <dt><a href="#" class="ind-flow-img_3"> </a></dt>
                    <dd><a href="#">No.3</a></dd>
                    <dd><a href="#">填写列表提交订单</a></dd>
                </dl>
            </li>
            <li class="ind-flow-direction"></li>
            <li>
                <dl>
                    <dt><a href="#" class="ind-flow-img_4"> </a></dt>
                    <dd><a href="#">No.4</a></dd>
                    <dd><a href="#">持有效证件体检</a></dd>
                </dl>
            </li>
            <li class="ind-flow-direction"></li>
            <li>
                <dl>
                    <dt><a href="#" class="ind-flow-img_5"> </a></dt>
                    <dd><a href="#">No.5</a></dd>
                    <dd><a href="#">接受体检报告</a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <ul class="ind-content-line clear">
        <li class="ind-rapid left">
            <h2 class="title"><a href="#">快速预约通道</a></h2>
            <div class="decorate">
                <div class="onset"></div>
                <div class="pregnancy"></div>
            </div>
            <?php if (Yii::app()->user->getIsGuest() === true) { ?>
                <form id="loginform" >
                    <div id="loginformdiv" class="ind-submit">

                        <input type="text" id="username" name="user[username]" value="手机号" class="input-text input-text-1" onfocus="cls(id)" onblur="res(id)" />
                        <input id="password" type="password" name="user[password]"  value="" class="input-text input-text-1 h" onfocus="cls(id)" onblur="change(id)"  />		
                        <input id="info" type="text" value="密码" class="input-text input-text-1"  onfocus="change(id)" onblur="res(id)" />
                        <p class="ind-submit-radio content-top clear">
                            <span class="ind-submit-radio-user left">
                                <input type="radio" name="user[usertype]" value="0" checked="checked" />
                                <span>企业用户</span>
                            </span>
                            <span class="ind-submit-radio-manage left">
                                <input type="radio" name="user[usertype]" value="1" />
                                <span>企业管理员</span>
                            </span>
                        </p>
                        <a href="javascript:mdencryption();" class="input-button-1">登录</a>
                        <p class="ind-submit-text">
                            <span>没有账号？</span>
                            <a href="index.php?r=login/pageenroll">立即注册</a>
                        </p>
                    </div>
                </form>
            <?php } else {
                ?>


                <div id="loginformdiv" class="ind-submit loginedform">
                    <div class="login_item">
                        <span class="login_logo login_user"></span>
                        <span class="login_content"><?= $userinfo['name'] ?></span>
                    </div>	
<!--                    <div class="login_item"><span class="login_logo login_email"></span><span class="login_content"><?=$userinfo['email']?></span></div>                    		             -->
                    <div class="login_button"><a href="index.php?r=main/cancellation" class="input-button-1">退出</a></div>
                </div>


                <!--		<div id="loginformdiv">
                                        <p id="welcomep">welcome,<?= $userinfo['name'] ?></p>
                                        <p id="logouta"><a href="index.php?r=package/yuyue">预约</a></p>
                                </div>-->
            <?php } ?>
        </li>
        <li class="ind-selfdom left">
            <h2 class="title"><a href="#">个性体检套餐</a> <a href="index.php?r=main/packagelist" class="title-more">更多&gt;&gt;</a>
            </h2>
            <div class="decorate">
                <div class="onset"></div>
                <div class="pregnancy"></div>
            </div>
            <div class="ind-selfdom-meal content-top clear">
                <dl class="left">
                    <dt>A套餐</dt>
                    <dd>
                        <ul>
                            <li>美年大健康-80族套餐</li>
                            <li>优惠价：￥300</li>
                            <li>门市价：￥700</li>
                        </ul>
                    </dd>
                    <dd class="ind-selfdom-meal-mask"><a href="index.php?r=main/packagelist"></a></dd>
                </dl>
                <dl class="right">
                    <dt>B套餐</dt>
                    <dd>
                        <ul>
                            <li>慈铭-至尊望族（男）</li>
                            <li>优惠价：￥500</li>
                            <li>门市价：￥800</li>
                        </ul>
                    </dd>
                    <dd class="ind-selfdom-meal-mask"><a href="index.php?r=main/packagelist"></a></dd>
                </dl>
            </div>
        </li>
        <li class="ind-else left">
            <h2 class="title"><a href="#">其他体检健康管理</a></h2>
            <div class="decorate">
                <div class="onset"></div>
                <div class="pregnancy"></div>
            </div>
            <ul class="ind-manage content-top clear">
                <li>
                    <dl>
                        <dt><a href="index.php?r=main/wait" title="风险评估" class="ind-manage-1"></a></dt>
                        <dd><a href="index.php?r=main/wait">风险评估</a></dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><a href="index.php?r=main/wait" title="运动方案" class="ind-manage-2"></a></dt>
                        <dd><a href="index.php?r=main/wait">运动方案</a></dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><a href="index.php?r=main/wait" title="营养方案" class="ind-manage-3"></a></dt>
                        <dd><a href="index.php?r=main/wait">营养方案</a></dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt><a href="index.php?r=main/wait" title="心理方案" class="ind-manage-4"></a></dt>
                        <dd><a href="index.php?r=main/wait">心理方案</a></dd>
                    </dl>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="ind-content-line clear">
        <li class="ind-rapid left">
            <h2 class="title"><a href="#">健康资讯</a> <a href="index.php?r=infoPapers/getContent/paperid/11" class="title-more">更多&gt;&gt;</a>
            </h2>
            <div class="decorate">
                <div class="onset"></div>
                <div class="pregnancy"></div>
            </div>
            <div class="ind-text_roll content-top">
                <ul class="ind-article">
                    <?php foreach ($titles as $title) { ?>
                        <li><a href="<?= Yii::app()->baseUrl ?>/index.php?r=infoPapers/getContent/paperid/<?= $title->id ?>"><?= $title->title ?></a></li>
                    <?php } ?>
                    <!--		<li><a href="#">冬天怎样防止感冒？</a></li>-->
                    <!--		<li><a href="#">怎样养成好的饮食习惯?</a></li>-->
                    <!--		<li><a href="#">每天只喝饮料好吗？</a></li>-->
                    <!--		<li><a href="#">长时间早上睡觉有什么不好？</a></li>-->
                    <!--		<li><a href="#">早餐的搭配</a></li>-->
                    <!--		<li><a href="#">怎样锻炼肌肉？</a></li>-->
                    <!--		<li><a href="#">秋冬应该注意什么？</a></li>-->
                    <!--		<li><a href="#">怎样保持肤色年轻？</a></li>-->
                </ul>
            </div>
        </li>
        <li class="ind-selfdom left">
            <h2 class="title"><a href="#">合作企业联盟</a> 
                <!--	<a href="#" class="title-more">更多&gt;&gt;</a>-->
            </h2>
            <div class="decorate">
                <div class="onset"></div>
                <div class="pregnancy"></div>
            </div>
            <div class="ind-roll_img">
                <div class="ind-roll_img-left" id="left"><a href="javascript:;"> <img
                            src="<?= Yii::app()->baseUrl ?>/images/front/left.png"> </a></div>
                <div class="ind-roll_img-right" id="right"><a href="javascript:;"> <img
                            src="<?= Yii::app()->baseUrl ?>/images/front/right.png"> </a></div>

                <div id="outer" class="ind-roll_img-carry">
                    <ul id="inner" class="ind-roll_img-carry-inside">
                        <li><img
                                src="<?= Yii::app()->baseUrl ?>/images/front/guohang.jpg"
                                width="100" height="40" alt=" "></li>
                        <li><img
                                src="<?= Yii::app()->baseUrl ?>/images/front/huawei.jpg"
                                width="100" height="40" alt=" "></li>
                        <li><img
                                src="<?= Yii::app()->baseUrl ?>/images/front/baidu.jpg"
                                width="100" height="40" alt=" "></li>
                       
                    </ul>
                </div>
            </div>
        </li>
        <li class="ind-else left">
            <h2 class="title"><a href="#">覆盖城市</a> <a href="index.php?r=main/centerlist" class="title-more">更多&gt;&gt;</a>
            </h2>
            <div class="decorate">
                <div class="onset"></div>
                <div class="pregnancy"></div>
            </div>
            <ul class="ind-city-name content-top">
                <li><a href="index.php?r=main/centerlist">北京</a></li>
                <li><a href="index.php?r=main/centerlist" class="ind-city-name-border">河南</a></li>
                <li><a href="index.php?r=main/centerlist">上海</a></li>
                <li><a href="index.php?r=main/centerlist">广东</a></li>
                <li><a href="index.php?r=main/centerlist" class="ind-city-name-border">内蒙古</a></li>
                <li><a href="index.php?r=main/centerlist">西藏</a></li>
                <li><a href="index.php?r=main/centerlist">四川</a></li>
                <li><a href="index.php?r=main/centerlist" class="ind-city-name-border">哈尔滨</a></li>
                <li><a href="index.php?r=main/centerlist">新疆</a></li>
                <li><a href="index.php?r=main/centerlist">山西</a></li>
                <li><a href="index.php?r=main/centerlist" class="ind-city-name-border">吉林</a></li>
                <li><a href="index.php?r=main/centerlist">河北</a></li>
                <li><a href="index.php?r=main/centerlist">湖南</a></li>
                <li><a href="index.php?r=main/centerlist" class="ind-city-name-border">辽宁</a></li>
                <li><a href="index.php?r=main/centerlist">江苏</a></li>
            </ul>
        </li>
    </ul>
</div>
<script type="text/javascript">
function mdencryption() {
    $pass = document.getElementById('password');
    $hash = hex_md5($pass.value);
    $pass.value = $hash;

    qiucklogin();
}
                        function qiucklogin() {
                            var logform = $("#loginform");
                            $.post("<?= $this->createUrl('login/verify') ?>", $("#loginform").serialize(), function(data) {
                                if (data["status"] == 0) {
                                    if (data["data"]["usertype"] == 0) {//普通用户
                                        location.href = "<?= $this->createUrl('package/yuyue') ?>";
                                    } else {
                                        location.href = "<?= $this->createUrl('company/manager/index') ?>";
                                    }
                                } else {
                                    alert("密码错误");
                                }
                            }, "json");
                        }
                        
//                        function mdencryption() {
//                            $pass = document.getElementById('password');
//                            $result = pawregex($pass.value);
//                            if ($result){
//	                            $hash = hex_md5($pass.value);
//	                            $pass.value = $hash;
//
//                           		qiucklogin();
//                            }else{
//                                alert("输入密码格式不对");
//                            }
//                        }

                        function pawregex(str){
                            var pattern = "^[a-zA-Z][0-9a-zA-Z]{5,7}$";

                            var regex = new RegExp(pattern);
                            return regex.test(str);
                        }
                        
                        $("#roll_interior").bxCarousel({
                            display_num: 4,
                            move: 1,
                            auto: true,
                            controls: false,
                            margin: 10
                        });

                        function change(id) {
                            if (id == "info") {
                                var obj = document.getElementById(id);
                                obj.className = 'input-text input-text-1 h';
                                var password = document.getElementById('password');
                                password.className = 'input-text input-text-1';
                                password.focus();
                            }
                            if (id == "password" && document.getElementById('password').value == '') {
                                var obj = document.getElementById(id);
                                obj.className = 'input-text input-text-1 h';
                                var info = document.getElementById('info');
                                info.className = 'input-text input-text-1';
                            }
                        }
</script>