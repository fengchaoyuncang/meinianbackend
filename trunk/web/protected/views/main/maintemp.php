		<div class="hea-content">
			<div class="hea-conten-top">
				<div class="hea-conten-top-left">
					<div class="hea-conten-top-left-submit background-color">
						<h2 class="hea-conten-top-left-submit-head">
							快速预约通道
						</h2>
						<?php if($login_status === false){?>
						<form id="loginform" method="POST" action="index.php?r=login/qiuckLogin">
							<div id="loginformdiv" class="hea-conten-top-left-submit-content">
								<div class="input-cellphone" id="number">
									<input type="text" id="username" name="user[username]" value="手机号" onmouseout="yz(this.value)" onkeyup="value=value.replace(/[^\d]/g,'')" />
									<a href="#">
										<img src="<?=Yii::app()->baseUrl?>/images/front/input-zh.png" />
									</a>
								</div>
								<div class="input-password">
									<input id="password" type="password" name="user[password]" value="密码" />
									<a href="#">
										<img src="<?=Yii::app()->baseUrl?>/images/front/input-mi.png" />
									</a>
								</div>
								<input type="submit" value=" 预约" class="input-submit" onclick="mdencryption();"/>
							</div>
						</form>
						<?php }else{?>
						<div id="welcomediv" class="hea-conten-top-left-submit-content" >
						<p id="welcomep">welcome,<?="XX"?></p>
						<a href="index.php?r=main/cancellation">退出登录</a>
					</div>
					<?php }?>
					</div>
					<div class="hea-conten-top-left-roll background-color">
						<div id="slider" class="case_box">
							<ul>
								<li class="case_1">	
								</li>
								<li class="case_2">
								</li>
								<li class="case_3">
								</li>
								<li class="case_4">
								</li>
								<li class="case_5">
								</li>
							</ul>
						</div>
					</div>
					<ul class="hea-conten-top-left-schedule">
						<li class="background-color">
							<a href="#">
								<h3>1</h3>
								<dl>
									<dt>
										<img src="<?=Yii::app()->baseUrl?>/images/front/zhuan.png" alt="注册账户">
									</dt>
									<dd>
										注册账户
									</dd>
								</dl>
							</a>
						</li>
						<li class="background-color">
							<a href="#">
								<h3>2</h3>
								<dl>
									<dt>
										<img src="<?=Yii::app()->baseUrl?>/images/front/zhuan.png" alt="选择机构">
									</dt>
									<dd>
										选择机构
									</dd>
								</dl>
							</a>
						</li>
						<li class="background-color">
							<a href="#">
								<h3>3</h3>
								<dl>
									<dt>
										<img src="<?=Yii::app()->baseUrl?>/images/front/zhuan.png" alt="填写列表">
									</dt>
									<dd>
										填写列表
									</dd>
								</dl>
							</a>
						</li>
						<li class="background-color">
							<a href="#">
								<h3>4</h3>
								<dl>
									<dt>
										<img src="<?=Yii::app()->baseUrl?>/images/front/zhuan.png" alt="提交订单">
									</dt>
									<dd>
										提交订单
									</dd>
								</dl>
							</a>
						</li>
						<li class="hea-conten-top-left-schedule-last background-color">
							<a href="#">
								<h3>5</h3>
								<dl>
									<dt>
										<img src="<?=Yii::app()->baseUrl?>/images/front/zhuan.png" alt="持有效证件体检">
									</dt>
									<dd>
										持有效证件体检
									</dd>
								</dl>
							</a>
						</li>
					</ul>
				</div>
				<div class="hea-conten-top-right background-color">
					<h2>
						手机
						<span>
							APP
						</span>
					</h2>
					<h2>二维码扫描</h2>
					<a href="#" class="hea-conten-top-right-two-dimension">
						<img src="<?=Yii::app()->baseUrl?>/images/front/22.jpg">
					</a>
					<dl>
						<dt>APP下载</dt>
						<dd>
							<a href="#">
<!--								<img src="img/anniu.gif">-->
							</a>
						</dd>
					</dl>
				</div>
			</div>
			<div class="hea-conten-bottom">
				<div class="hea-conten-bottom-top">
					<div class="hea-conten-bottom-top-article">
						<div class="headline-border">
							<h2 class="headline">资讯类文章</h2>
						</div>
						<div class="head-img">
							<div class="head-img-left"></div>
							<div class="head-img-right"></div>
						</div>
						<div class="text_roll">
							<ul id="text" class="article-text">
								<li>
									<a href="#">j1111</a>
								</li>
								<li>
									<a href="#">222222</a>
								</li>
								<li>
									<a href="#">33333333333</a>
								</li>
								<li>
									<a href="#">444444444444</a> 
								</li>
								<li>
									<a href="#">5555555</a> 
								</li>
								<li>
									<a href="#">66666666</a> 
								</li>
								<li>
									<a href="#">7777777777</a> 
								</li>
								<li>
									<a href="#">8888888</a> 
								</li>
							</ul>
						</div>
					</div>
					<div class="hea-conten-bottom-top-meal">
						<div class="headline-border">
							<h2 class="headline">个性化体检套餐</h2>
							<a href="#">更多&gt;&gt;</a>
						</div>
						<div class="head-img">
							<div class="head-img-left"></div>
							<div class="head-img-right"></div>
						</div>
						<div class="meal-type">
							<a href="#" class="meal-type-left-border">
								<dl class="meal-type-left">
									<dt>A套餐</dt>
									<dd>实际圣诞节+计算机</dd>
									<dd>市场价388元</dd>
									<dd>郁云价288元</dd>
								</dl>
							</a>
							<a href="#"  class="meal-type-right-border">
								<dl class="meal-type-right">
									<dt>B套餐</dt>
									<dd>实际圣诞节+计算机</dd>
									<dd>市场价388元</dd>
									<dd>郁云价288元</dd>
								</dl>
							</a>
						</div>
						<div class="click-order-button">
							<input type="button" value="马上预约">
						</div>
					</div>
					<div class="hea-conten-bottom-top-scheme">
						<dl>
							<dt>
								<a href="#" class="img-a">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img-1.jpg" alt="风险评估">
								</a>
							</dt>
							<dd>
								<a href="#" class="text-a">
									风险评估
								</a>
							</dd>
						</dl>
						<dl>
							<dt>
								<a href="#" class="img-a">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img-2.jpg" alt="运动方案">
								</a>
							</dt>
							<dd>
								<a href="#" class="text-a">
									运动方案
								</a>
							</dd>
						</dl>
						<dl>
							<dt>
								<a href="#" class="img-a">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img-3.jpg" alt="营养方案">
								</a>
							</dt>
							<dd>
								<a href="#" class="text-a">
									营养方案
								</a>
							</dd>
						</dl>
						<dl>
							<dt>
								<a href="#" class="img-a">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img-4.jpg" alt="心理方案">
								</a>
							</dt>
							<dd>
								<a href="#" class="text-a">
									心理方案
								</a>
							</dd>
						</dl>
					</div>
				</div>
				<div class="hea-conten-bottom-bottom">
					<div class="hea-conten-bottom-bottom-city">
						<div class="headline-border">
							<h2 class="headline">覆盖城市</h2>
							<a href="#">更多&gt;&gt;</a>
						</div>
						<div class="head-img">
							<div class="head-img-left"></div>
							<div class="head-img-right"></div>
						</div>
						<ul class="city-name">
							<li>
								<a href="#">北京</a>
							</li>
							<li>
								<a href="#" class="city-name-border">河南</a>
							</li>
							<li>
								<a href="#">上海</a>
							</li>
							<li>
								<a href="#">广东</a>
							</li>
							<li>
								<a href="#" class="city-name-border">内蒙古</a>
							</li>
							<li>
								<a href="#">西藏</a>
							</li>
							<li>
								<a href="#">四川</a>
							</li>
							<li>
								<a href="#" class="city-name-border">哈尔滨</a>
							</li>
							<li>
								<a href="#">新疆</a>
							</li>
						</ul>
					</div>
					<div class="hea-conten-bottom-bottom-enterprise-alliance">
						<div class="headline-border">
							<h2 class="headline">合作企业联盟</h2>
						</div>
						<div class="head-img">
							<div class="head-img-left"></div>
							<div class="head-img-right"></div>
						</div>
						<div class="enterprise-alliance-roll">
							<div class="enterprise-alliance-roll-left" id="left">
								<a href="javascript:;">
									<img src="<?=Yii::app()->baseUrl?>/images/front/left-button.png">
								</a>
							</div>
							<div class="enterprise-alliance-roll-right" id="right">
								<a href="javascript:;">
									<img src="<?=Yii::app()->baseUrl?>/images/front/right-button.png">
								</a>
							</div>
							<div id="outer" class="enterprise-alliance-roll-carry">
								<ul id="inner">
									<li>
										<a href="#">
											<img src="http://imgstatic.baidu.com/img/image/shouye/e824b899a9014c08ba1d546d0b7b02087af4f4d0.jpg" alt=" ">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="http://imgstatic.baidu.com/img/image/shouye/cc11728b4710b912eb9467b4c2fdfc0392452226.jpg" alt=" ">
										</a>
									</li>
									<li>
										<a href="#">
<!--											<img src="http://g.hiphotos.baidu.com/image/q%3D85%3Ba0%3D+%2C1%2C1/sign=ecef0ce2a9d3fd1f3009af3f0b2f1522/267f9e2f07082838715443d8b999a9014d08f1a5.jpg" alt=" ">-->
										</a>
									</li>
									<li>
										<a href="#">
											<img src="http://imgstatic.baidu.com/img/image/shouye/30adcbef76094b3611d5992ba2cc7cd98c109db5.jpg" alt=" ">
										</a>
									</li>
									<li>
										<a href="#">
<!--											<img src="http://e.hiphotos.baidu.com/image/q%3D85%3Ba0%3D+%2C1%2C1/sign=8485d11055e736d15e13810da0317fff/7a899e510fb30f247e2c73cfc995d143ac4b0343.jpg"  alt=" ">-->
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="hea-conten-bottom-bottom-test-alliance">
						<div class="headline-border">
							<h2 class="headline headline-zui">合作体检联盟</h2>
							<a href="#">更多&gt;&gt;</a>
						</div>
						<div class="head-img head-img-zui">
							<div class="head-img-left"></div>
							<div class="head-img-right"></div>
						</div>
						<ul class="test-alliance-img">
							<li>
								<a href="#">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img.jpg" alt="">
								</a>
							</li>
							<li>
								<a href="#">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img.jpg" alt="">
								</a>
							</li>
							<li>
								<a href="#">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img.jpg" alt="">
								</a>
							</li>
							<li>
								<a href="#">
									<img src="<?=Yii::app()->baseUrl?>/images/front/img.jpg" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
