<!--
<script type="text/javascript" src="http://api.map.baidu.com/api?key=F318ae5063f8a7ed6dc51495b4cc1a11&v=1.0&services=true" ></script>
-->
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=F318ae5063f8a7ed6dc51495b4cc1a11"></script>
<style>
  #center_main{
    margin-top: 15px;
  }
  #center_right{
    height: 300px;
    width: 550px;
  }
  #center_left{
    float: left;
    width: 340px;
    margin: 60px 20px;
  }
  #center_left h2 {
    font-size: 1.2em;
    font-weight: bold;
    margin-bottom: 6px;
  }
</style>
<div id="center_main" class="clearfix">
  <div id="center_left">
    <h2 id="center_title"><a href=""><?=$subcenterinfo->name?></a></h2>
    <div class="item_main">
      <ul>
        <li>电话：<?=$subcenterinfo->telephone?></li>
        <li>地址：<?=$subcenterinfo->address?></li>
      </ul>           
    </div>
  </div>
  <div id="center_right">
    <div id="container"></div>
  </div>
</div>
<script type="text/javascript">
  // 百度地图API功能
  var map = new BMap.Map("center_right");           
  var point = new BMap.Point(<?=$subcenterinfo->gps_lon?>, <?=$subcenterinfo->gps_lat?>);
  map.centerAndZoom(point,15);
  var marker = new BMap.Marker(point);
  map.addOverlay(marker);
  marker.setAnimation(BMAP_ANIMATION_BOUNCE);
  var hotSpot = new BMap.Hotspot(point, {text: "<?=$subcenterinfo->name?>"});
  map.addHotspot(hotSpot);
  map.enableScrollWheelZoom();
  window.onload = function () {
    $('.anchorBL').remove();
  }
</script>