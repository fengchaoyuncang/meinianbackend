<script src='<?= Yii::app()->baseUrl ?>/js/jquery/cookie/jquery.cookie.js'></script>
<script src='<?= Yii::app()->baseUrl ?>/js/jquery/datapicker/glDatePicker.min.js'></script>
<link rel="stylesheet" href="<?= Yii::app()->baseUrl ?>/css/datepicker/glDatePicker.default.css" type="text/css" media="screen">
<script>
    function yuyuetuan() {
        $.post("<?= $this->createUrl('company/manager/tuandan') ?>", $("#yuyue").serialize(), function(data) {
            if (data["status"] == 0) {
                alert("预约成功");
                window.location.href="<?=$this->createUrl("tuandanlist")?>";
            }else{
                alert("预约失败，错误原因："+data["data"]);
            }
    },"json");
}
function cancel(id){
    $.post("<?= $this->createUrl('company/manager/cancelbyid') ?>",{"id":id},function(data){
        if(data["status"] == 0){
            
        }else{
            alert("取消失败，请刷新后重试");
        }
    },"json");
}


    $(function() {
        //日期选择控件
        $("input[name='yuyue[time]']").glDatePicker(
        {
            onClick: function(target, cell, date, data) {
                target.val(date.getFullYear() + '-' +
                            (date.getMonth()+1) + '-' +
                            date.getDate());

                if(data != null) {
                    alert(data.message + '\n' + date);
                }
            }
        });
    });
</script>
<table class="table table-striped">
    <tr>
        <th>套餐类型</th>
        <th>预约体检中心</th>
        <th>人员名单</th>
    </tr>
    <form id='yuyue' method="post" action="<?= $this->createUrl('company/manager/tuandan') ?>">
        <?php
        foreach ($items as $key => $item) {
            ?>
            <tr>
                <td><?= $item["package"]->name ?></td>
                <td>
                    <select name="yuyue[centers][<?= $key ?>]">
                        <?php
                        foreach ($item["centers"] as $c) {
                        ?>
                            <option value="<?= $c->center->id ?>"><?= $c->center->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <?php
                    foreach ($item["people"] as $p) {
                    ?>    
                        <?=$p->realname?>,
                    <?php
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>

            <td colspan='3'>
                预约时间：<input type='text' name='yuyue[time]' > </input>
                <input type='button' onclick="yuyuetuan();" value='预约' class='btn' />
                <a href="<?=$this->createUrl("cleantuan")?>"/>全部取消</a>
            </td>

        </tr>
    </form>
</table>