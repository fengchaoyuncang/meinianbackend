<script src='<?= Yii::app()->baseUrl ?>/js/jquery/datapicker/glDatePicker.min.js'></script>
<link rel="stylesheet" href="<?= Yii::app()->baseUrl ?>/css/datepicker/glDatePicker.default.css" type="text/css" media="screen">
<script>
function tuandancancel(id){
    $.post("<?= $this->createUrl('company/manager/tuandancancel') ?>", 
        {"id":id}, 
        function(data) {
            if (data["status"] == 0) {
                alert("取消成功");
                window.location.reload() 
            }else{
                alert("预约失败，错误信息为："+data["data"]);
            }
    },"json");
}
function reyuyue(id){
    $.post("<?= $this->createUrl('company/manager/tuandanreyuyue') ?>", 
        {"id":id,"time":$("input[name='time']").val()},
        function(data) {
                if (data["status"] == 0) {
                    window.location.reload() 
                }else{
                    alert("预约失败，错误信息为："+data["data"]);
                }
        },"json");
}
$(function(){
    $("input[name='time']").glDatePicker(
        {
            onClick: function(target, cell, date, data) {
                target.val(date.getFullYear() + '-' +
                            (date.getMonth()+1) + '-' +
                            date.getDate());

                if(data != null) {
                    alert(data.message + '\n' + date);
                }
            }
        });
});
</script>
<table class="table table-bordered">
    <tr>
        <td>预约日期</td>
        <td>
            <?php
            if($item->statuscode==3){
                echo "<input type=text name='time' value=$item->order_time />";
            }else{
                echo $item->order_time;                
            }
            ?></td>
    </tr>
    <tr>
        <td>预约体检中心</td>
        <td><?=$item->sub_center_name?></td>
    </tr>
    <tr>
        <td>预约人员清单</td>
        <td>
            <?php
            foreach ($people as $p) {
                echo $p->customer_name.",";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>预约人数</td>
        <td><?=$item->order_count?></td>
    </tr>
    <tr>
        <td>实际体检人数</td>
        <td><?=$item->order_realcount?></td>
    </tr>
    <tr>
        <td>状态</td>
        <td>
        <?php
            if($item->statuscode == 1||$item->statuscode==4){
                echo "预约待确认";
            }elseif($item->statuscode == 2){
                echo "预约成功";
            }elseif($item->statuscode==3){
                echo "预约失败，体检中心给您的反馈为：".$item->etc;
            }else{
                echo "预约取消";
            }
        ?></td>
    </tr>
    <tr>
        <td>操作</td>
        <td>
            <?php
            if($item->statuscode == 1||$item->statuscode==4){
                echo "<a href='javascript:tuandancancel(".$item->id.")'>取消预约</a>";
            }elseif($item->statuscode==3){
                echo "<a href='javascript:reyuyue(".$item->id.");'>再次预约</a> <a href='javascript:tuandancancel(".$item->id.")'>取消预约</a>";
            }else{
                echo "无";
            }
            ?>
        </td>
    </tr>
</table>