<?php
function getStatusDesc($statuscode){
    switch ($statuscode) {
        case 1:
            $value = "已预约，待确认";
            break;
        case 2:
            $value = "已确认";
            break;
        case 3:
            $value = "预约失败";
            break;
        case 4:
            $value = "已预约，待确认";
            break;
        case 5:
            $value = "已取消";
            break;
        default:
            $value="异常";
            break;
    }
    return $value; 
}
    ?>
<table class="table table-striped">
  <tr>
    <th>预约日期</th>
    <th>预约人数</th>
    <th>预约体检中心</th>
    <th>预约状态</th>
    <th>操作</th>
    <th>操作日期</th>
  </tr>
      <?php
      foreach($items as $item)
      {
      ?>
  <tr>
    <td><?=$item->order_time?></td>
    <td><?=$item->order_count?></td>
    <td><?=$item->sub_center_name?></td>
    <td><?=getStatusDesc($item->statuscode)?>
    </td>
    <td><a href="<?=$this->createUrl("tuandanview",array("tuanid"=>$item->id))?>">查看详情</a></td>
    <td><?=$item->order_time?></td>
  </tr>
  <?php
      }
  ?>
</table>