<script src='<?=Yii::app()->baseUrl?>/js/jquery/cookie/jquery.cookie.js'></script>
<script>
function insertTo(){
    var items = $("#content input:checked[name='seleitem']");
    var it = "";
    for(var i=0;i<items.length;i++){
        it=it+"."+items.eq(i).val();
    }
    $.post("<?= $this->createUrl('company/manager/yuyuetuan') ?>",{"item":it},function(data){
        if(data["status"] == 0){
            alert("预约成功");
        }else{
            alert("预约失败，错误信息："+data["data"]);
        }
    },"json");
//    $("#content input:checked[name='seleitem']").each(function(){
//        var users = $.cookie('users');
//        users=users+'.'+$(this).val();
//        //此处有问题标记，如果超过了cookie的最大长度...
//        $.cookie('users',users);
//    });
}
function selectall(t){
    if($(t).attr("checked")=="checked"){
        $("#content input[name='seleitem']").each(function(){$(this).attr("checked","checked");});
    }else{
        $("#content input[name='seleitem']").each(function(){$(this).attr("checked",false);});
    }
}
$(function(){
    $("select[name=statuscode]").val(<?=isset($_GET["statuscode"])?$_GET["statuscode"]:4?>);
});
</script>
<h1>企业员工管理</h1>
<form method="GET">
    <select name="statuscode">
        <option value="4">全部用户</option>
        <option value="0">可预约用户</option>
        <option value="1">已预约用户</option>
        <option value="2">已体检用户</option>
        <option value="3">未体检用户</option>
    </select>
    <input type="hidden" name="r" value='company/manager/index'/>
    <input type="submit" value="查询"/>
</form>
<table class="table table-striped" id='content'>
    <tr>
        <th><input onchange='selectall(this);' type="checkbox"></th>
        <th>姓名</th>
        <th>身份证号</th>
        <th>状态</th>
    </tr>
    <?php
    foreach($items->getData() as $item){
    ?>
    <tr>
        <td><input  value='<?=$item->id?>' name="seleitem" type="checkbox"></td>
        <td><?=$item->realname?></td>
        <td><?=$item->customer_no?></td>
        <td><?php
            switch ($item->statuscode){
                case 0:
                    $value="可预约";
                    break;
                case 1:
                    $value="个人预约，待确认";
                    break;
                case 2:
                    $value="个人已预约";
                    break;
                case 3:
                    $value="已体检";
                    breaek;
                case 4:
                    $value="已团约，待确认";
                    break;
                case 5:
                    $value="团约已成功";
                    break;
                default :
                    $value="异常";
                    break;
                        
            }
            echo $value;
        ?></td>
    </tr>
    <?php
    }
    ?>
    <tr>
        <td colspan="4">
        <button onclick='insertTo();' class="btn"  style='text-align:left'>加入预约名单</button>
        <span style='float: right'>
        <?php
        $this->widget('CLinkPager', array(
            'header' => '',
            'firstPageLabel' => '首页',
            'lastPageLabel' => '末页',
            'prevPageLabel' => '上一页',
            'nextPageLabel' => '下一页',
            'pages' => $items->getPagination(),
            'maxButtonCount' => 13
                )
        );
        ?>
            </span>
        </td>
    </tr>
</table>