<table>
    <form action='index.php?r=center/order/checkReported' method='post'>
       <tr><th class="headerTH">属性</th><th class="headerTH">内容</th></tr>
        <tr><td class="contentTD">ID</td><td class="contentTD"><?= $rets->id ?></td></tr>
        <tr><td class="contentTD">体检中心名称</td><td class="contentTD"><?= $rets->sub_center_name ?></td></tr>
        <tr><td class="contentTD">用户名称</td><td class="contentTD"><?= $rets->customer_name ?></td></tr>
        <tr><td class="contentTD">用户性别</td><td class="contentTD">
                <?php
                if ($rets->customer_gender == 1) {
                    echo "男";
                } elseif ($rets->customer_gender == 2) {
                    echo "女";
                } else {
                    echo "未知";
                }
                ?></td></tr>
        <tr><td class="contentTD">预约时间</td><td class="contentTD"><?= $rets->order_time ?></td></tr>
        <tr><td class="contentTD">证件号码</td><td class="contentTD"><?= $rets->customer_no ?></td></tr>
        <tr><td class="contentTD">用户电话</td><td class="contentTD"><?= $rets->customer_telephone ?></td></tr>
        <tr><td class="contentTD">用户邮箱</td><td class="contentTD"><?= $rets->email ?></td></tr>
        <tr><td class="contentTD">用户公司</td><td class="contentTD"><?= $rets->customer_company ?></td></tr>
        <tr>
            <td class="contentTD">当前状态</td>
            <td class="contentTD">
                <?php
                //获取当前订单的状态
                if ($rets->status == 1) {
                    echo "新预约订单";
                } elseif ($rets->status == 2) {
                    echo "预约成功订单";
                } elseif ($rets->status == 3) {
                    echo "已经体检";
                } elseif ($rets->status == 4) {
                    echo "取消体检";
                } elseif ($rets->status == 5) {
                    echo "取得体检报告";
                } else {
                    echo "取消预约";
                }
                ?>
            </td>
        </tr>
<!--        <tr>
            <td class="contentTD">  
                目标状态
            </td>
            <td class="contentTD">  
                <select name="status">
                    <option value="5">取得体检报告</option>               
                </select>

            </td>
        </tr>-->
        <tr><input type='hidden' name="id" value='<?= $rets->id ?>'>
        </tr>
        <tr>
            <td class="contentTD"></td>
            <td class="contentTD"><input type="button" value="返回" onclick="window.history.back()"></td>
        </tr>
</table>