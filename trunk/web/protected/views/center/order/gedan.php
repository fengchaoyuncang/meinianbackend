<form>
    <input type="hidden" value="1" name="order[type]">
    <input type="hidden" value="<?=$item->id?>" name="order[id]">
<table class="table table-bordered">
  <tr>
      <td>预约日期</td>
      <td><?=$item->order_time?></td>
  </tr>
  <tr>
      <td>公司</td>
      <td><?=$item->customer_company?></td>
  </tr>
  <tr>
      <td>姓名</td>
      <td>
          <?=$item->customer_name?>
      </td>
  </tr>
  <tr>
      <td>身份证号</td>
      <td>
          <?=$item->customer_no?>
      </td>
  </tr>
  <tr>
      <td>联系方式</td>
      <td>
          <?=$item->customer_mobile==null?$item->customer_telephone:$item->customer_mobile?>
      </td>
  </tr>
  <tr>
      <td>状态</td>
      <td>
          <?php
      switch ($item->status) {
          case 1:
          case 8:
              $value = "新预约";
              break;
          default :
              $value = "订单已处理，请关闭后刷新页面";
              break;
      }
          ?>
          <?=$value?>
      </td>
  </tr>
  <tr>
      <td>操作</td>
      <td>
          <select name="order[op]" onchange="shownote(this);" style="width:80px;">
              <option value=0>通过</option>
              <option value=1>取消</option>
          <!--    <option value=2>完全关闭</option> -->
          </select>
          <span id="note" style="display: none;">请输入可预约的日期：<input style="width:80px;" type="text" name="order[time]"></span>
      </td>
  </tr>
</table>
</form>