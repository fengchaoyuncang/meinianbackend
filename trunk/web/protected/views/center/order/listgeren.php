<script>
    function upstatus(id,status){
        if(!confirm("确认操作？")){
            return;
        }
    
        $.post("<?= $this->createUrl('upstatus') ?>",{"order[type]":1,"order[id]":id,"order[status]":status}, function(data) {
            if (data["status"] == 0) {
                window.location.reload();
            }else{
                alert("数据获取失败，错误原因："+data["data"]);
            }
        },"json");
    }
    $(function(){
        <?php
        if(isset($_GET["order"])){
            foreach($_GET["order"] as $key=>$v){
        ?>
                $("*[name='order[<?=$key?>]']").val("<?=$v?>");
        <?php        
            }
        }
        ?>
    });
</script>
<table class="table table-bordered">
    <form method="GET">
        <input type='hidden' name='r' value='<?=isset($_GET["r"])?$_GET["r"]:""?>'>
        <tr>
        <select name="type" style="width:80px;" onchange="window.location.href='<?=$this->createUrl("")?>/type/'+$(this).val();">
            <option value="1">个单</option>
            <option value="2">团单</option>
        </select>
        <select name="order[status]">
            <option value=''>显示全部</option>
            <option value="<?=B2bOrder::GEREN_SUCCESS?>">已完成预约</option>
            <option value="<?=B2bOrder::TJ_SUCCESS?>">已体检</option>
            <option value="<?=B2bOrder::TJ_CANCEL?>">取消体检</option>
        </select>
        姓名：<input type='text' value='' name='order[customer_name]' />
        证件号：<input type='text' value='' name='order[customer_no]' />
        <input type='submit' value='查询' />
        </tr>
    </form>
    <tr>
        <th><input type='checkbox' /></th>
        <th>预约日期</th>
        <th>姓名</th>
        <th>身份证号</th>
        <th>联系方式</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    <?php
    foreach($data->getData() as $item){
    ?>
    <tr>
        <td><input type='checkbox' value='<?=$item->id?>' /></td>
        <td><?=$item->order_time?></td>
        <td><?=$item->customer_name?></td>
        <td><?=$item->customer_no?></td>
        <td><?=$item->customer_telephone?></td>
        <td>
            <?php
            switch ($item->status) {
                case B2bOrder::GEREN_SUCCESS:
                    $s = "预约完成";
                    break;
                case B2bOrder::TJ_CANCEL:
                    $s = "取消体检";
                    break;
                case B2bOrder::TJ_SUCCESS:
                    $s = "完成体检但未上传体检报告";
                    break;
                case B2bOrder::TJ_REPORT:
                    $s = "完成体检并上传体检报告";
                    break;
                default:
                    $s = "异常状态";
                    break;
            }
            ?>
            <?=$s?>
        </td>
        <td>
            <?php
            if($item->status == B2bOrder::GEREN_SUCCESS){
                echo "<a href='javascript:upstatus($item->id,".B2bOrder::GEREN_CANCEL.")'>取消预约</a> ";
                echo "<a href='javascript:upstatus($item->id,".B2bOrder::TJ_SUCCESS.")'>完成体检</a> ";
                echo "<a href='javascript:upstatus($item->id,".B2bOrder::TJ_CANCEL.")'>未体检</a> ";
            }
            ?>
            
        </td>
    </tr>
    <?php
    }
    ?>
</table>
