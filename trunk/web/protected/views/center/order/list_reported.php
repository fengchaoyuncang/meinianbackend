<div id="main">
    <table id='table_list' class="aa">
        <tr><th  class='headerTH'>序号</th><th  class='headerTH'>子体检中心名称</th><th  class='headerTH'>客户名称</th><th  class='headerTH'>客户性别</th><th  class='headerTH'>预约时间</th><th  class='headerTH'>身份证号</th><th  class='headerTH'>客户单位</th><th  class='headerTH'>订单状态</th><th  class='headerTH'>操作</th></tr>
        <?php
        foreach ($rets->getData() as $v) {
            ?>
            <tr>
                <td class='contentTD'><?= $v->id ?></td>
                <td class='contentTD'><?= $v->sub_center_name ?></td>
                <td class='contentTD'><?= $v->customer_name ?></td>
                <td class='contentTD'><?php
                    if ($v->customer_gender == 1) {
                        echo "男";
                    } elseif ($v->customer_gender == 2) {
                        echo "女";
                    } else {
                        echo "未知";
                    }
                    ?> </td>
                <td class='contentTD'><?= $v->order_time ?></td> 
                <td class='contentTD'><?= $v->customer_no ?> </td>
                <td class='contentTD'><?= $v->customer_company ?></td>
                <td class='contentTD'>
                    <?php
                    if ($v->status == 1) {
                        echo "新预约";
                    } elseif ($v->status == 2) {
                        echo "预约成功";
                    } elseif ($v->status == 3) {
                        echo "已体检";
                    } elseif ($v->status == 4) {
                        echo "取消体检";
                    } elseif ($v->status == 5) {
                        echo '已获得体检报告';
                    } else {
                        echo "已取消预约";
                    }
                    ?>
                </td>
                <td class='contentTD'><a href='index.php?r=center/order/detailsreported&id=<?= $v->id ?> '>处理</a></td>
            </tr>

            <?php
        }
        ?>
    </table>
</div>

<div id="footer">

    <!--显示分页-->
    <?php
    $this->widget('CLinkPager', array(
        'header' => '',
        'firstPageLabel' => '首页',
        'lastPageLabel' => '末页',
        'prevPageLabel' => '上一页',
        'nextPageLabel' => '下一页',
        'pages' => $rets->getPagination(),
        'maxButtonCount' => 10
    ))
    ?>
</div>