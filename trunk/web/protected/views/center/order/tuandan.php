<form>
    <input type="hidden" value="2" name="order[type]">
    <input type="hidden" value="<?=$item->id?>" name="order[id]">
<table class="table table-bordered">
  <tr>
      <td>预约日期</td>
      <td><?=$item->order_time?></td>
  </tr>
  <tr>
      <td>公司</td>
      <td><?=$item->customer_company?></td>
  </tr>
  <tr>
      <td>人数</td>
      <td><?=$item->order_count?></td>
  </tr>
  <tr>
      <td>人员列表</td>
      <td>
      <?php
        foreach($people as $p){
            echo $p->customer_name.";";
        }
      ?></td>
  </tr>
  <tr>
      <td>状态</td>
      <td>
          <?php
      switch ($item->statuscode) {
          case 1:
          case 4:
              $value = "新预约";
              break;
          default :
              $value = "异常状态";
              break;
      }
          ?>
          <?=$value?>
      </td>
  </tr>
  <tr>
      <td>操作</td>
      <td>
          <select name="order[op]" onchange="shownote(this);" style="width:80px;">
              <option value=0>通过</option>
              <option value=1>取消</option>
              <option value=2>完全关闭</option>
          </select>
          <span id="note" style="display: none;">请输入可预约的日期：<input style="width:80px;" type="text" name="order[time]"></span>
      </td>
  </tr>
</table>
</form>