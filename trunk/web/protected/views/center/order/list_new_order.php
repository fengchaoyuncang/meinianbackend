<script type="text/javascript">

    var maxID = <?= $maxid ?>;
    /*
     * 获取新添加的订单，即id最大的新预约订单
     * @returns {undefined}     */
    function getNewOrder() {
        $.ajax({
            type: 'POST',
            url: "<?= $this->createUrl("center/order/getneworder") ?>",
            data: {maxID: maxID, sub_center_id: 3},
            success: function(data) {
                var options = data["data"];

                if (options.length >= 1) {
                    notify();//弹出chrome桌面提醒
                }

                for (i = 0; i < options.length; i++) {
                    //转换性别
                    if (options[i].customer_gender == 1) {
                        options[i].customer_gender = "男";
                    } else if (options[i].customer_gender == 2) {
                        options[i].customer_gender = "女";
                    } else {
                        options[i].customer_gender = "未知";
                    }
                    
                    //处理公司名称
                    if(options[i].customer_company == null){
                        options[i].customer_company="";
                    }
                    
                    $('#table_list').append("<tr>  <td class='contentTD'> <input name='subBox' type='checkbox' value="+ options[i].id + "/></td><td class='contentTD'>" + options[i].id + "</td><td class='contentTD'>" + options[i].sub_center_name + "</td><td class='contentTD'>" + options[i].customer_name + "</td><td class='contentTD'>" + options[i].customer_gender + "</td><td class='contentTD'>" + options[i].order_time + "</td><td class='contentTD'>" + options[i].customer_no + "</td><td class='contentTD'>" + options[i].customer_company + "</td><td class='contentTD'>新预约</td><td class='contentTD'><a href='index.php?r=center/order/detailssucceed&id=" + options[i].id + "'>处理</a></td></tr>");
                    maxID = options[i].id;
                }
            }
        });
    }

    /*
     *触发器，每隔一段时间获取新预约的订单 
     */
    window.setInterval("getNewOrder()", 4000);

    /*
     * chrome桌面提醒功能，只能在chrome浏览器下面实现
     */
    function notify() {
        if (window.webkitNotifications) {                                        //先判断当前的浏览器是否支持开启桌面通知 window.webkitNotifications
            var havePermission = window.webkitNotifications.checkPermission();
            if (havePermission == 0) {
                // 0 is PERMISSION_ALLOWED
                var notification = window.webkitNotifications.createNotification(
                        'http://i.stack.imgur.com/dmHl0.png',
                        '有新预约订单!',
                        ''
                        );

                notification.onclick = function() {
                    window.open("#");
                    notification.close();
                }
                notification.show();
            } else {
                window.webkitNotifications.requestPermission();
            }
        }

    }

    /*
     * 批量处理订单
     */
    $(document).ready(function() {
        //全选订单
        checkboxSelect();

        //“新预约订单”更改成“预约成功”状态
        $("#order_succeed").click(function() {
            url = "<?= $this->createUrl("center/order/handleOrders")?>";
            status = 2;
            handleOrder(url,status);
        });
        
        //“新预约订单”更改为“取消预约”状态
         $("#order_cancele").click(function() {
            url = "<?= $this->createUrl("center/order/handleOrders")?>";
            status = 6;
            handleOrder(url,status);
        });
    });

</script>
<div id="main">
    <table id='table_list' class="aa">
        <tr><th class='headerTH'><input id="checkAll" type="checkbox" />全选</th><th class='headerTH'>序号</th><th class='headerTH'>子体检中心名称</th><th class='headerTH'>客户名称</th><th class='headerTH'>客户性别</th><th class='headerTH'>预约时间</th><th class='headerTH'>身份证号</th><th class='headerTH'>客户单位</th><th class='headerTH'>订单状态</th><th class='headerTH'>操作</th></tr>
        <?php
        foreach ($rets->getData() as $v) {
            ?>
            <tr>
                <td class='contentTD'> <input name="subBox" type="checkbox" value="<?= $v->id; ?>"/></td>
                <td class='contentTD'><?= $v->id; ?></td>               
                <td class='contentTD'><?= $v->sub_center_name ?></td>
                <td class='contentTD'><?= $v->customer_name ?></td>
                <td class='contentTD'><?php
                    if ($v->customer_gender == 1) {
                        echo "男";
                    } elseif ($v->customer_gender == 2) {
                        echo "女";
                    } else {
                        echo "未知";
                    }
                    ?> </td>
                <td class='contentTD'><?= $v->order_time ?></td> 
                <td class='contentTD'><?= $v->customer_no ?> </td>
                <td class='contentTD'><?= $v->customer_company ?></td>
                <td class='contentTD'>
                    <?php
                    if ($v->status == 1) {
                        echo "新预约";
                    } elseif ($v->status == 2) {
                        echo "预约成功";
                    } elseif ($v->status == 3) {
                        echo "已体检";
                    } elseif ($v->status == 4) {
                        echo "取消体检";
                    } elseif ($v->status == 5) {
                        echo '已获取体检报告';
                    } else {
                        echo "已取消预约";
                    }
                    ?>
                </td>
                <td class='contentTD'><a href='index.php?r=center/order/detailsneworder&id=<?= $v->id ?> '>处理</a></td>
            </tr>
            <?php
        }
        ?>

    </table>
    <div id="manage_order">
        <input type="button" id="order_succeed" value="预约成功">
        <input type="button" id="order_cancele" value="取消预约">
    </div>

</div>
<div id="footer">
    <?php
    $this->widget('CLinkPager', array(
        'header' => '',
        'firstPageLabel' => '首页',
        'lastPageLabel' => '末页',
        'prevPageLabel' => '上一页',
        'nextPageLabel' => '下一页',
        'pages' => $rets->getPagination(),
        'maxButtonCount' => 10
            )
    );
    ?>
</div>
