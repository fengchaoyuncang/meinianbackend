<script src='<?= Yii::app()->baseUrl ?>/js/jquery/datapicker/glDatePicker.min.js'></script>
<link rel="stylesheet" href="<?= Yii::app()->baseUrl ?>/css/datepicker/glDatePicker.default.css" type="text/css" media="screen">
<script type="text/javascript">
    var curtr = null;
    function getOrderDetail(t)
    {
        var url = $(t).attr("url");
        $.post(url,"",function(data){
            if(data["status"] == 0){
                $("div.modal-body").html(data["data"]);
                $('#modal').modal();
                curtr = $(t).parent().parent();
            }else{
                alert("获取失败，错误信息："+data["data"]);
            }
        },"json");
    }
    function shownote(t){
        var value = $(t).val();
        if(value == 1){
            $(t).parent().find("span").show();
        }else{
            $(t).parent().find("span").hide();
        }
    }
    function order_proc(){
        $.post("<?=$this->createUrl("proc")?>",$("#order_detail").find("form").serialize(),function(data){
            if(data["status"] == 0){
                $('#modal').modal('toggle');
                if(curtr!=null){
                    curtr.remove();
                }
            }else{
                alert("获取失败，错误信息："+data["data"]);
            }
        },"json");
    }
    var maxgedanid = <?=$max["gedan"]?>;
    var maxtuandanid = <?=$max["tuan"]?>;
    function getNewOrder()
    {
        $.post("<?=$this->createUrl("getneworder")?>",{"max[gedan]":maxgedanid,"max[tuan]":maxtuandanid},function(data){
            if(data["status"] == 0){
                $("#yuyuelist tr:first").after(data["data"]["con"]);
                maxgedanid = data["data"]["max"]["gedan"];
                maxtuandanid = data["data"]["max"]["tuan"];
                notify();
            }else{
//                alert("获取失败，错误信息："+data["data"]);
            }
        },"json");
        setTimeout(getNewOrder,3000);
    }
    
    function notify() {
        if (window.webkitNotifications) {                                        //先判断当前的浏览器是否支持开启桌面通知 window.webkitNotifications
            var havePermission = window.webkitNotifications.checkPermission();
            if (havePermission == 0) {
                // 0 is PERMISSION_ALLOWED
                var notification = window.webkitNotifications.createNotification(
                        '',
                        '有新预约订单!',
                        ''
                        );

                notification.onclick = function() {
                    //window.open("#");
                    notification.close();
                }
                notification.show();
            } else {
                window.webkitNotifications.requestPermission(notify);
            }
        }

    }
    $(function(){
        setTimeout(getNewOrder,3000);
    });

</script>
<div class="form">
    <h2>订单列表</h2>

    <table class="table table-bordered" id="yuyuelist">
        <tr>
            <th><input type="checkbox"></input></th>
            <th>预约日期</th>
            <th>预约体检中心</th>
            <th>公司名称</th>
            <th>预约类型</th>
            <th>预约人数</th>
            <th>操作</th>
        </tr>
        <?php
        foreach ($items as $item) {
        ?>
        <tr value="<?=$item["id"]?>">
            <td><input type="checkbox"></input></td>
            <td><?=$item["order_time"]?></td>
            <td><?=$item["sub_center_name"]?></td>
            <td><?=$item["customer_company"]?></td>
            <td><?=$item["order_type"]===1?"个单":"团单"?></td>
            <td><?=$item["people_count"]?></td>
            <td>
                <input type="button" onclick="getOrderDetail(this);" url="<?=$item["order_type"]===1?$this->createUrl("gedan",array("id"=>$item["id"])):$this->createUrl("tuandan",array("id"=>$item["id"]))?>"
                   value="详情" name="test" />
            </td>
        </tr>
        <?php
        }
        ?>
    </table>   
</div>
<div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-footer">
       <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  </div>
  <div class="modal-body" id="order_detail">
      
  </div>
    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    <button class="btn btn-primary" onclick="order_proc();">确认</button>
  </div>
</div>




