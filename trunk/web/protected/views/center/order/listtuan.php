<script>
    function upstatus(id,status){
        if(!confirm("确认操作？")){
            return;
        }
    
        $.post("<?= $this->createUrl('upstatus') ?>",{"order[type]":2,"order[id]":id,"order[status]":status}, function(data) {
            if (data["status"] == 0) {
                window.location.reload();
            }else{
                alert("数据获取失败，错误原因："+data["data"]);
            }
        },"json");
    }
    $(function(){
        <?php
        if(isset($_GET["order"])){
            foreach($_GET["order"] as $key=>$v){
        ?>
                $("*[name='order[<?=$key?>]']").val("<?=$v?>");
        <?php        
            }
        }
        ?>
    });
</script>
<table class="table table-bordered">
    <form method="GET">
        <input type='hidden' name='r' value='<?=isset($_GET["r"])?$_GET["r"]:""?>'>
        <tr>
        <select name="type" style="width:80px;" onchange="window.location.href='<?=$this->createUrl("")?>/type/'+$(this).val();">
            <option value="1">个单</option>
            <option value="2" selected>团单</option>
        </select>
        <select name="order[statuscode]">
            <option value=''>显示全部</option>
            <option value="<?=B2bOrderTuan::TUAN_SUCCESS?>">已完成预约</option>
            <option value="<?=B2bOrderTuan::TJ_REPORT?>">已体检</option>
        </select>
        公司名：<input type='text' value='' name='order[customer_company]' />
        <input type='submit' value='查询' />
        </tr>
    </form>
    <tr>
        <th><input type='checkbox' /></th>
        <th>预约日期</th>
        <th>公司名</th>
        <th>预约人数</th>
        <th>到检人数</th>
<!--        <th>联系方式</th> -->
        <th>状态</th>
        <th>操作</th>
    </tr>
    <?php
    foreach($data->getData() as $item){
    ?>
    <tr>
        <td><input type='checkbox' value='<?=$item->id?>' /></td>
        <td><?=$item->order_time?></td>
        <td><?=$item->customer_company?></td>
        <td><?=$item->order_count?></td>
        <td><?=$item->order_realcount?></td>
<!--        <td></td>-->
        <td>
            <?php
            switch ($item->statuscode) {
                case B2bOrderTuan::TUAN_SUCCESS:
                    $s = "预约完成";
                    break;
                case B2bOrderTuan::TJ_REPORT:
                    $s = "体检完成";
                    break;
                default:
                    $s = "异常状态";
                    break;
            }
            ?>
            <?=$s?>
        </td>
        <td>
            <?php
            if($item->statuscode == B2bOrderTuan::TUAN_SUCCESS){
                echo "<a href='javascript:upstatus($item->id,".B2bOrderTuan::TUAN_CANCEL.")'>取消预约</a> ";
            }
            ?>
            
        </td>
    </tr>
    <?php
    }
    ?>
</table>
