<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>体检中心列表</title>
<style>
body{margin:0px; font-size:14px; background:#f5f5f5; color:#666;}
.cont{ max-width:960px; margin:0px auto;}
.topimg{margin:20px auto;width:50%;display:block;}
.top{margin:20px;line-height:25px;font-weight:bold;}
a{background:#FFF; display:block; padding:10px;border-radius:5px;box-shadow:0px 3px 0px #e3e4e8; line-height:25px; margin:15px 10px; color:#333; text-decoration:none;}
a strong{display:block; color:#e50065; font-size:20px; line-height:30px; text-align:center;}
a p{ margin:0px;}
a b{ color:#003586;}
a img{ display:block; margin:5px auto;}
.daohang{float:right;margin-top:-25px;background:#03998d;
	padding:2px 3px 2px 6px; width:90px; height:auto;color:#fff;
    -moz-border-radius: 5px;      /* Gecko browsers */
    -webkit-border-radius: 5px;   /* Webkit browsers */
    border-radius:5px;            /* W3C syntax */
}

</style>
</head>
<body>
	<div class="cont">
		<?php foreach($centers as $k=>$v):?>
			<a href="<?=$v['baidu_map_url']?>">
		    	<strong><?=$v['name']?></strong>
		        <!-- <img src="images/yy01.jpg" width="100%"> -->
		<p><b>地址：</b><?=$v['address']?></p>
		        <p><b>预约及咨询电话:</b><?=$v['telephone']?></p>
		        <p><b>营业时间：</b>8:00-17:30</p>
		        <p class='daohang'>点击导航 &gt;</p>
		    </a>
		<?php endforeach;?>
	</div>
</body>
</html>
