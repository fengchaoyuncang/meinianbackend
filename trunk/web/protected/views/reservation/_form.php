<?php
/* @var $this ReservationController */
/* @var $model Reservation */
/* @var $form CActiveForm */
?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css'); ?>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php Yii::app()->clientScript->registerScript('datepicker', '
(function($) {
$(function() {
$(".date").datepicker({
    "dateFormat" : "yy-mm-dd"
});
});
})(jQuery);
'); ?>
<?php Yii::app()->clientScript->registerCss('hide', '
.hide {
    display : none;
}
'); ?>
<style >
.row {
	margin-top: 10px;
}
</style>
<div class="">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reservation-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有<span class="required">*</span>号的域是必填项。</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row" >
		<?php /*echo $form->labelEx($model,'city'); ?>
        <?php
              $data = array(
			  '北京'=>
			  	array('北京美年大健康大望路分院'=>'北京美年大健康大望路分院',
					  '北京美年大健康宣武门分院'=>'北京美年大健康宣武门分院',
					  '北京美年大健康东四分院'=>'北京美年大健康东四分院',
					  '北京美年大健康绿生源分院'=>'北京美年大健康绿生源分院',
					  '北京美年大健康佳境分院'=>'北京美年大健康佳境分院'), 
			  '上海'=>
			  	array('上海小木桥总院'=>'上海小木桥总院',
					  '上海浦东长航分院'=>'上海浦东长航分院',
					  '上海长宁天山分院'=>'上海长宁天山分院',
					  '上海宜山分院'=>'上海宜山分院',
					  '上海海员分院'=>'上海海员分院',
					  '上海齐鲁分院'=>'上海齐鲁分院'
					  ), 
			  '广州'=>
			  	array('广州珠江新城分院'=>'广州珠江新城分院'), 
			  '天津'=>
			  	array('天津八里台分院'=>'天津八里台分院',
					  '天津大光明桥分院'=>'天津大光明桥分院'), 
			  '成都'=>
			  	array('成都武侯分院'=>'成都武侯分院'), 
			  '南昌'=>
			  	array('南昌长庚体检中心'=>'南昌长庚体检中心'),
			  '苏州'=>
			  	array('苏州工业园区分院'=>'苏州工业园区分院'),
			  '杭州'=>
			  	array('杭州美年大健康'=>'杭州美年大健康'),
			  '无锡'=>
			  	array('无锡美年疗养院'));
              echo $form->dropDownList($model, 'city', $data, array('prompt' => '北京'));?>
		<?php echo $form->error($model,'city'); */?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
        <?php // echo $form->textField($model,'location',array('size'=>60,'maxlength'=>64));
              $data = array(
			  '北京'=>
			  	array('北京美年大健康大望路分院'=>'北京美年大健康大望路分院',
					  '北京美年大健康宣武门分院'=>'北京美年大健康宣武门分院',
					  '北京美年大健康东四分院'=>'北京美年大健康东四分院',
					  '北京美年大健康绿生源分院'=>'北京美年大健康绿生源分院',
					  '北京美年大健康佳境分院'=>'北京美年大健康佳境分院'), 
			  '上海'=>
			  	array('上海小木桥总院'=>'上海小木桥总院',
					  '上海浦东长航分院'=>'上海浦东长航分院',
					  '上海长宁天山分院'=>'上海长宁天山分院',
					  '上海宜山分院'=>'上海宜山分院',
					  '上海海员分院'=>'上海海员分院',
					  '上海齐鲁分院'=>'上海齐鲁分院'
					  ), 
			  '广州'=>
			  	array('广州珠江新城分院'=>'广州珠江新城分院'), 
			  '天津'=>
			  	array('天津八里台分院'=>'天津八里台分院',
					  '天津大光明桥分院'=>'天津大光明桥分院'), 
			  '成都'=>
			  	array('成都武侯分院'=>'成都武侯分院'), 
			  '南昌'=>
			  	array('南昌长庚体检中心'=>'南昌长庚体检中心'),
			  '苏州'=>
			  	array('苏州工业园区分院'=>'苏州工业园区分院'),
			  '杭州'=>
			  	array('杭州美年大健康'=>'杭州美年大健康'),
			  '无锡'=>
			  	array('无锡美年疗养院'));
              echo $form->dropDownList($model, 'location', $data);?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sex'); ?>
		<?php echo $form->radioButtonList($model,'sex',array('男'=>'男', '女'=>'女'), array('separator'=>'&nbsp;', 'labelOptions'=>array('style'=>'display:inline;width:150px;'), 'template'=>"{input} {label}")); ?>
		<?php echo $form->error($model,'sex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date', array( 'class' => 'date')); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idno'); ?>
		<?php echo $form->textField($model,'idno',array('size'=>20,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'idno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'series'); ?>
        <?php // echo $form->textField($model,'series',array('size'=>60,'maxlength'=>64));
              $data = array(
'美年大健康199体检套餐     原价：男  472    女   542    '=>'美年大健康199体检套餐     原价：男  472    女   542    ',
'美年大健康299体检套餐A    原价：男  682    女   787    '=>'美年大健康299体检套餐A    原价：男  682    女   787    ',
'美年大健康299体检套餐B     原价：男  672    女   777   '=>'美年大健康299体检套餐B     原价：男  672    女   777   ',
'美年大健康399体检套餐A     原价：男 942    女 1197     '=>'美年大健康399体检套餐A     原价：男 942    女 1197     ',
'美年大健康399体检套餐B     原价：男 962    女   1147   '=>'美年大健康399体检套餐B     原价：男 962    女   1147   ',
'美年大健康499体检套餐A     原价：男 1282    女   1652  '=>'美年大健康499体检套餐A     原价：男 1282    女   1652  ',
'美年大健康499体检套餐B     原价：男 1352    女   1722  '=>'美年大健康499体检套餐B     原价：男 1352    女   1722  ',
'美年大健康555体检套餐     原价：男  1832    女   2202  '=>'美年大健康555体检套餐     原价：男  1832    女   2202  ',
'美年大健康688体检套餐     原价：男  2160    女   2532  '=>'美年大健康688体检套餐     原价：男  2160    女   2532  ',
'美年大健康888体检套餐     原价：男  3012    女   3202  '=>'美年大健康888体检套餐     原价：男  3012    女   3202  ',
'美年大健康999体检套餐    原价：男  3622    女   3812   '=>'美年大健康999体检套餐    原价：男  3622    女   3812   ',
'美年大健康1999元体检套餐    原价：男  4938    女   5128'=>'美年大健康1999元体检套餐    原价：男  4938    女   5128',
);
              echo $form->dropDownList($model, 'series', $data, array('onchange'=>'updatecnt(this.value);'));?>
		<?php echo $form->error($model,'series'); ?>
        <div id="series_cnt">
            一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、血常规18项、尿常规、肝功二项、血脂二项、肾功二项、空腹血糖<br>女性加项：妇科检查、白带常规、宫颈刮片
        </div>
	</div>

    <?php if(CCaptcha::checkRequirements() && $model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'验证码'); ?>
            <div>
                <?php $this->widget('CCaptcha',array(
							'buttonLabel'=>'重新获取验证码',
							'clickableImage'=>true,
							'imageOptions'=>array('title'=>'重新获取验证码'))); ?>
                <?php echo $form->textField($model,'verifyCode'); ?>
            </div>
            <div class="hint">请输入上面图片中的字母.
                <br/>不区分大小写.</div>
            <?php echo $form->error($model,'verifyCode'); ?>
        </div>
    <?php endif; ?>

<script type="text/javascript">
var cnt = {
'美年大健康199体检套餐     原价：男  472    女   542    ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、血常规18项、尿常规、肝功二项、血脂二项、肾功二项、空腹血糖<br>女性加项：妇科检查、白带常规、宫颈刮片',
'美年大健康299体检套餐A    原价：男  682    女   787    ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、血常规18项、尿常规、肝功二项、血脂二项、肾功三项、空腹血糖、肺功能检查、盆腔彩超               <br>女性加项：妇科检查、白带常规、宫颈刮片、红外线乳透',
'美年大健康299体检套餐B     原价：男  672    女   777   ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、血常规18项、尿常规、肝功四项、血脂四项、肾功三项、空腹血糖、中医体质辨识、盆腔彩超              <br>女性加项：妇科检查、白带常规、宫颈刮片、红外线乳透',
'美年大健康399体检套餐A     原价：男 942    女 1197     ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、血常规18项、尿常规、肝功四项、血脂四项、肾功三项、空腹血糖、骨密度检测、动脉硬化检测、盆腔彩超              <br>女性加项：妇科检查、白带常规、TCT、红外线乳透',
'美年大健康399体检套餐B     原价：男 962    女   1147   ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、甲状腺彩超、血常规18项、尿常规、肝功四项、血脂四项、肾功三项、空腹血糖、中医体质辨识、人体成分分析、盆腔彩超<br>男性专项：颈椎正位片<br>女性加项：妇科检查、白带常规、TCT、红外线乳透',
'美年大健康499体检套餐A     原价：男 1282    女   1652  ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、颈椎侧位片、腹部彩超、甲状腺彩超、血常规18项、尿常规、肝功四项、血脂四项、肾功三项、空腹血糖、甲胎蛋白测定、癌胚抗原测定、骨密度检测、盆腔彩超、中医体质辨识、经络检测<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超',
'美年大健康499体检套餐B     原价：男 1352    女   1722  ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、腹部彩超、经颅多谱勒、甲状腺彩超、血常规18项、尿常规、肝功四项、血脂四项、肾功三项、甲胎蛋白测定、癌胚抗原测定、空腹血糖、中医体质辨识、经络检测、盆腔彩超<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超',
'美年大健康555体检套餐     原价：男  1832    女   2202  ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、颈椎正侧位片、腹部彩超、经颅多谱勒、血常规18项、血流变、尿常规、肝功七项、血脂四项、肾功三项、甲胎蛋白测定、癌胚抗原测定、空腹血糖、动脉硬化检测、骨密度检测、经络检测、中医体质辨识、盆腔彩超<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超',
'美年大健康688体检套餐     原价：男  2160    女   2532  ':'一般检查、内科、外科、眼科、眼压、耳鼻喉科、口腔科、心电图、胸部正侧位片、颈椎正侧位片、腹部彩超、甲状腺彩超、血常规18项、血流变、便常规、便隐血、尿常规、肝功七项、血脂四项、肾功三项、甲胎蛋白测定、癌胚抗原测定、空腹血糖、幽门螺旋杆菌抗体测定、骨密度检测、动脉硬化检测、中医体质辨识、经络检测、中医健康管理卡六选一、盆腔彩超 、人体成分分析<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超',
'美年大健康888体检套餐     原价：男  3012    女   3202  ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、颈椎正侧位片、腹部彩超、甲状腺彩超、颈动脉彩超、血常规18项、血流变、尿常规、肝功全项、血脂四项、肾功三项、肿瘤筛查三项、空腹血糖、幽门螺旋杆菌抗体测定、骨密度检测、动脉硬化检测、中医体质辨识、经络检测、中医健康管理卡六选一、盆腔彩超、中医个性化养生方案、四诊合参<br>男性加项：尿液TCT<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超',
'美年大健康999体检套餐    原价：男  3622    女   3812   ':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、颈椎正侧位片、腹部彩超、心脏彩超、经颅多谱勒、甲状腺彩超、血常规18项、血流变、尿常规、肝功全项、血脂四项、肾功三项、肿瘤筛查三项、空腹血糖、人体成分分析、肺功能检查、面部TV区分析、骨密度检测、动脉硬化检测、经络检测、中医健康管理卡六选一、盆腔彩超、中医个性化养生方案、四诊合参<br>男性加项：尿液TCT<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超',
'美年大健康1999元体检套餐    原价：男  4938    女   5128':'一般检查、内科、外科、眼科、耳鼻喉科、口腔科、心电图、胸部正位片、颈椎正侧位片、腹部彩超、心脏彩超、颈动脉彩超、经颅多谱勒、甲状腺彩超、血常规18项、血流变、尿常规、肝功全项、血脂四项、微量元素六项、肾功三项、肿瘤筛查六项、空腹血糖、人体成分分析、幽门螺旋杆菌抗体测定、骨密度检测、动脉硬化检测、面部TV区分析、中医体质辨识、经络检测、中医个性化养生方案、中医健康管理卡六选二、四诊合参、盆腔彩超、中医调理四选二、四季养生茶一组<br>男性加项：尿液TCT<br>女性加项：妇科检查、白带常规、TCT、乳腺彩超'};
function updatecnt(key) {
	document.getElementById('series_cnt').innerHTML=cnt[key];
}
</script>

    <div class="row <?php echo $model->isNewRecord ? 'hide' : '' ?>">
        <?php echo $form->labelEx($model,'created_time'); ?>
        <?php echo $form->textField($model,'created_time'); ?>
        <?php echo $form->error($model,'created_time'); ?>
    </div>

    <div class="row <?php echo $model->isNewRecord ? 'hide' : '' ?>">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropdownList($model,'status',
            array('处理中' => '处理中',
                '成功' => '成功',
                '失败' => '失败',)); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '预定' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->