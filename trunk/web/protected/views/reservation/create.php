<?php
/* @var $this ReservationController */
/* @var $model Reservation */

$this->breadcrumbs=array(
	'Reservations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Reservation', 'url'=>array('index')),
	array('label'=>'Manage Reservation', 'url'=>array('admin')),
);
?>
<link rel="stylesheet" type="text/css" href="/css/form.css" />
<img src="/images/header.jpg" width="100%" />
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>