<?php
/* @var $this YingyangController */
/* @var $model EvaluateNutritionSuggestion */

$this->breadcrumbs=array(
	'Evaluate Nutrition Suggestions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EvaluateNutritionSuggestion', 'url'=>array('index')),
	array('label'=>'Create EvaluateNutritionSuggestion', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#evaluate-nutrition-suggestion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Evaluate Nutrition Suggestions</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'evaluate-nutrition-suggestion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'disid',
		'summary',
		'reason',
		'bring',
		'symptom',
		/*
		'nutrition_element',
		'nutrition_target',
		'nutrition_reason',
		'nutrition_prescription',
		'pro_news',
		'diet',
		'other',
		'create_at',
		'update_at',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
