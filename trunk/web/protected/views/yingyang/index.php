<?php
/* @var $this YingyangController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Evaluate Nutrition Suggestions',
);

$this->menu=array(
	array('label'=>'Create EvaluateNutritionSuggestion', 'url'=>array('create')),
	array('label'=>'Manage EvaluateNutritionSuggestion', 'url'=>array('admin')),
);
?>

<h1>Evaluate Nutrition Suggestions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
