<?php
/* @var $this YingyangController */
/* @var $model EvaluateNutritionSuggestion */

$this->breadcrumbs=array(
	'Evaluate Nutrition Suggestions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EvaluateNutritionSuggestion', 'url'=>array('index')),
	array('label'=>'Manage EvaluateNutritionSuggestion', 'url'=>array('admin')),
);
?>

<h1>Create EvaluateNutritionSuggestion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>