<?php
/* @var $this YingyangController */
/* @var $model EvaluateNutritionSuggestion */

$this->breadcrumbs=array(
	'Evaluate Nutrition Suggestions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EvaluateNutritionSuggestion', 'url'=>array('index')),
	array('label'=>'Create EvaluateNutritionSuggestion', 'url'=>array('create')),
	array('label'=>'Update EvaluateNutritionSuggestion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EvaluateNutritionSuggestion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EvaluateNutritionSuggestion', 'url'=>array('admin')),
);
?>

<h1>View EvaluateNutritionSuggestion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'disid',
		'summary',
		'reason',
		'bring',
		'symptom',
		'nutrition_element',
		'nutrition_target',
		'nutrition_reason',
		'nutrition_prescription',
		'pro_news',
		'diet',
		'other',
		'create_at',
		'update_at',
	),
)); ?>
