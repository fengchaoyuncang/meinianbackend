<?php
/* @var $this YingyangController */
/* @var $model EvaluateNutritionSuggestion */

$this->breadcrumbs=array(
	'Evaluate Nutrition Suggestions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EvaluateNutritionSuggestion', 'url'=>array('index')),
	array('label'=>'Create EvaluateNutritionSuggestion', 'url'=>array('create')),
	array('label'=>'View EvaluateNutritionSuggestion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EvaluateNutritionSuggestion', 'url'=>array('admin')),
);
?>

<h1>Update EvaluateNutritionSuggestion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>