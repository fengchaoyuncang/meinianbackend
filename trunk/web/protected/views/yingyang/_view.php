<?php
/* @var $this YingyangController */
/* @var $data EvaluateNutritionSuggestion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disid')); ?>:</b>
	<?php echo CHtml::encode($data->disid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('summary')); ?>:</b>
	<?php echo CHtml::encode($data->summary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
	<?php echo CHtml::encode($data->reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bring')); ?>:</b>
	<?php echo CHtml::encode($data->bring); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('symptom')); ?>:</b>
	<?php echo CHtml::encode($data->symptom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nutrition_element')); ?>:</b>
	<?php echo CHtml::encode($data->nutrition_element); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nutrition_target')); ?>:</b>
	<?php echo CHtml::encode($data->nutrition_target); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nutrition_reason')); ?>:</b>
	<?php echo CHtml::encode($data->nutrition_reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nutrition_prescription')); ?>:</b>
	<?php echo CHtml::encode($data->nutrition_prescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pro_news')); ?>:</b>
	<?php echo CHtml::encode($data->pro_news); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diet')); ?>:</b>
	<?php echo CHtml::encode($data->diet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other')); ?>:</b>
	<?php echo CHtml::encode($data->other); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_at')); ?>:</b>
	<?php echo CHtml::encode($data->update_at); ?>
	<br />

	*/ ?>

</div>