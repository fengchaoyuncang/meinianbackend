<?php
/* @var $this YingyangController */
/* @var $model EvaluateNutritionSuggestion */
/* @var $form CActiveForm */

// editors
?>
<!-- jquery -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<!-- jquery te -->
<?php Yii::app()->clientScript->registerScriptFile('/js/jquery/te/jquery-te-1.3.3.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile('/js/jquery/te/jquery-te-1.3.3.css'); ?>
<?php Yii::app()->clientScript->registerScript('jquery-te-for-editor', "
    (function($) {
		$(function() {
			// enable jquery texteditor
			$('textarea.editor').jqte();
		});
	})(jQuery);
", CClientScript::POS_HEAD); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'evaluate-nutrition-suggestion-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'disid'); ?>
		<?php echo $form->textField($model,'disid'); ?>
		<?php echo $form->error($model,'disid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'summary'); ?>
		<?php echo $form->textArea($model,'summary',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'summary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textArea($model,'reason',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bring'); ?>
		<?php echo $form->textArea($model,'bring',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'bring'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'symptom'); ?>
		<?php echo $form->textArea($model,'symptom',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'symptom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nutrition_element'); ?>
		<?php echo $form->textArea($model,'nutrition_element',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'nutrition_element'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nutrition_target'); ?>
		<?php echo $form->textArea($model,'nutrition_target',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'nutrition_target'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nutrition_reason'); ?>
		<?php echo $form->textArea($model,'nutrition_reason',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'nutrition_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nutrition_prescription'); ?>
		<?php echo $form->textArea($model,'nutrition_prescription',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'nutrition_prescription'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pro_news'); ?>
		<?php echo $form->textArea($model,'pro_news',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'pro_news'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'diet'); ?>
		<?php echo $form->textArea($model,'diet',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'diet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other'); ?>
		<?php echo $form->textArea($model,'other',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'other'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_at'); ?>
		<?php echo $form->textField($model,'create_at'); ?>
		<?php echo $form->error($model,'create_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_at'); ?>
		<?php echo $form->textField($model,'update_at'); ?>
		<?php echo $form->error($model,'update_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->