<style>
    .h{display:none}
    .breadcrums{float: left;}
</style>
<div class="odd">

    <form id="loginform" name="loginform">
        <dl class="odd-interior">
            <dt>
            Hello,欢迎回到郁金香云健康
            </dt>
            <dd>
                <input id="username" type="text" name="user[username]" class="input-text" value="手机号码" onfocus="cls(id)" onblur="res(id)" />
                <p>用于每次登录时的用户名</p>
            </dd>
            <dd>
                <input id="password" type="password" name="user[password]"  value="" class="input-text h" onfocus="cls(id)" onblur="change(id)"  />	
                <input id="info" type="text" class="input-text" value="密码" onfocus="change(id)" onblur="res(id)" />
                <p>
                    <a href="index.php?r=login/pageRetrieve">找回密码</a>
                </p>
            </dd>
            <dd>
                <span class="ind-submit-radio-user left">
                    <input type="radio" name="user[usertype]" value="0" checked="checked" />
                    <span>企业用户</span>
                </span>
                <span class="ind-submit-radio-manage left">
                    <input type="radio" name="user[usertype]" value="1" />
                    <span>企业管理员</span>
                </span>
            </dd>
            <dd>
                <a href="javascript:mdencryption();" class="input-submit">登		录</a>
                <p>
                    <span>没有账号？</span>
                    <a href="index.php?r=login/pageenroll">立即注册</a>
                </p>
            </dd>
        </dl>
    </form>
</div>

<script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/md5.js"></script>
<script type="text/javascript">
                    function mdencryption() {
                        $pass = document.getElementById('password');
                        $hash = hex_md5($pass.value);
                        $pass.value = $hash;

                        login();
                    }
                    
                    function login() {
                        var logform = $("#loginform");
                        $.post("<?= $this->createUrl('login/verify') ?>",
                                $("#loginform").serialize(),
                                function(data) {
                                    if (data["status"] == 0) {
                                        if (data["data"]["usertype"] == 0) {//普通用户
                                            location.href = "<?= $this->createUrl('main/index') ?>";
                                        } else {
                                            location.href = "<?= $this->createUrl('company/manager/index') ?>";
                                        }
                                    } else {
                                        alert("密码错误");
                                    }
                                }, "json");
                    }

                    function pawregex(str){
                        var pattern = "^[a-zA-Z][a-zA-Z0-9]{5,7}$";

                        var regex = new RegExp(pattern);
                        return regex.test(str);
                    }
                    
                    function change(id) {
                        if (id == "info") {
                            var obj = document.getElementById(id);
                            obj.className = 'input-text h';
                            var password = document.getElementById('password');
                            password.className = 'input-text';
                            password.focus();
                        }
                        if (id == "password" && document.getElementById('password').value == '') {
                            var obj = document.getElementById(id);
                            obj.className = 'input-text h';
                            var info = document.getElementById('info');
                            info.className = 'input-text';
                        }
                    }
</script>