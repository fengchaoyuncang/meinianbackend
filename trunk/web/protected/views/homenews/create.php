<?php
/* @var $this HomenewsController */
/* @var $model InfoHomeNews */

$this->breadcrumbs=array(
	'Info Home News'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InfoHomeNews', 'url'=>array('index')),
	array('label'=>'Manage InfoHomeNews', 'url'=>array('admin')),
);
?>

<h1>Create InfoHomeNews</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>