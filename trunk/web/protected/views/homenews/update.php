<?php
/* @var $this HomenewsController */
/* @var $model InfoHomeNews */

$this->breadcrumbs=array(
	'Info Home News'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InfoHomeNews', 'url'=>array('index')),
	array('label'=>'Create InfoHomeNews', 'url'=>array('create')),
	array('label'=>'View InfoHomeNews', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage InfoHomeNews', 'url'=>array('admin')),
);
?>

<h1>Update InfoHomeNews <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>