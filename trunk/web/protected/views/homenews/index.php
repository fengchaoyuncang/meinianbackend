<?php
/* @var $this HomenewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Info Home News',
);

$this->menu=array(
	array('label'=>'Create InfoHomeNews', 'url'=>array('create')),
	array('label'=>'Manage InfoHomeNews', 'url'=>array('admin')),
);
?>

<h1>Info Home News</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
