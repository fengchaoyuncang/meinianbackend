<?php
/* @var $this HomenewsController */
/* @var $model InfoHomeNews */

$this->breadcrumbs=array(
	'Info Home News'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List InfoHomeNews', 'url'=>array('index')),
	array('label'=>'Create InfoHomeNews', 'url'=>array('create')),
	array('label'=>'Update InfoHomeNews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InfoHomeNews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InfoHomeNews', 'url'=>array('admin')),
);
?>

<h1>View InfoHomeNews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'image',
		'uid',
		'content',
		'center_id',
		'create_at',
		'update_at',
	),
)); ?>
