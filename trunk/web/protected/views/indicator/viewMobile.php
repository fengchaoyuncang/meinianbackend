<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/viewMobile.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/viewMobile.css'); ?>
<ul class="man" id="man">
    <li class="man-line">
        <h2 class="man-l-top" name="hea">
            <a href="#"><?php echo $model->name; ?></a>
        </h2>
        <div class="man-l-bd" name="mans">
            <?php echo $model->over_view; ?>
        </div>
        <div class="man-l-bottom">
            <div><?php echo $model->value_ranges; ?></div>
        </div>
    </li>
    <?php if (!empty($model->value_ranges)) { ?>
        <li class="man-line">
            <h2 class="man-l-top" name="hea">
                <a href="#">过高的原因</a>
            </h2>
            <div class="man-l-bd" name="mans">
                <?php echo $model->over_reason; ?>
            </div>
        </li>

        <li class="man-line">
            <h2 class="man-l-top" name="hea">
               <a href="#">过低的原因</a> 
            </h2>
            <div class="man-l-bd" name="mans">
                <?php echo $model->under_reason; ?>
            </div>
        </li>
    <?php }?>
    <li class="man-line">
         <h2 class="man-l-top" name="hea">
            <a href="#">专家建议</a>
        </h2>
        <div class="man-l-bd" name="mans">
            <?php echo $model->expert_advice; ?>
        </div>
    </li>
    <li class="man-line">
         <h2 class="man-l-top" name="hea">
            <a href="#"><?php echo $model->item->item_name ?>说明</a>
        </h2>
        <div class="man-l-bd" name="mans">
            <?php echo $model->item->description; ?>
        </div>
    </li>
</ul>
<div class="man-bottom">
    <a href="wtai://wp/mc;4000988855">
        专家电话资询: 400-098-8855
    </a>
</div>
<script type="text/javascript">
    getview();
</script>