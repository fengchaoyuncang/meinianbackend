<?php
/* @var $this IndicatorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'体检指标',
);

$this->menu=array(
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>体检指标</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
