<?php
/* @var $this IndicatorController */
/* @var $model Indicator */
/* @var $form CActiveForm */
?>

<!-- jquery -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<!-- jquery te -->
<?php Yii::app()->clientScript->registerScriptFile('/js/jquery/te/jquery-te-1.3.3.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile('/js/jquery/te/jquery-te-1.3.3.css'); ?>
<?php Yii::app()->clientScript->registerScript('jquery-te-for-editor', "
    (function($) {
		$(function() {
			// enable jquery texteditor
			$('textarea.editor').jqte();
		});
	})(jQuery);
", CClientScript::POS_HEAD); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'indicator-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有<span class="required">*</span>号的域是必填项。</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'体检指标名称'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'所属体检项'); ?>
		<?php // echo $form->textField($model,'item_id');
              $data = array('' => 'Select Exam Item');
              foreach(ExamItem::model()->findAll() as $item) {
                  $data[$item->id] = $item->item_name;
              }
              echo $form->dropDownList($model, 'item_id', $data);?>
		<?php echo $form->error($model,'item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'概述'); ?>
		<?php echo $form->textArea($model,'over_view',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'over_view'); ?>
	</div>

    <div class="row template-container">
        <?php echo $form->labelEx($model,'取值范围'); ?>
        <?php echo $form->textArea($model,'value_ranges',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'value_ranges'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'偏高原因'); ?>
		<?php echo $form->textArea($model,'over_reason',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'over_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'偏低原因'); ?>
		<?php echo $form->textArea($model,'under_reason',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'under_reason'); ?>
	</div>

	<div class="row hide">
		<?php echo $form->labelEx($model,'治疗保健(废弃)'); ?>
		<?php echo $form->textArea($model,'cure_care',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'cure_care'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'专家建议'); ?>
		<?php echo $form->textArea($model,'expert_advice',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'expert_advice'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->