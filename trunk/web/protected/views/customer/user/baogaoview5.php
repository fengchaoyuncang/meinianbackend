<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->
<div id='exercise'>
	<div id="exercise_top" class="view_top">
	<div id="exercise_top_left" class="view_top_left">
		<div id="exercise_top_num" class="view_top_num">5</div>
		<div id="exercise_top_title" class="view_top_title">运动方案</div>
		<div id="exercise_top_subtitle" class="view_top_subtitle">Excise Programs</div>
	</div>
	<div id="nutrition_top_right" class="view_top_right"><span>姓名：马胜盼</span>
	<span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
	</div>
	<div id='excercise_middle'>
		<div id='sports_content_top' class='view_content_top'>
			<div id='sports_title' class='view_content_title'>运动指导</div>
			<div id='sports_subtitle'>Sports Guide</div>
		</div>
		<table class='sports_table' border='1' cellspacing='0'>
		<?php 
		foreach ($sports['benefit']['safe_sug_arr'] as $sport){
			echo "<tr class='sports_table_title'>
				<td>运动类别</td>
				<td>运动项目</td>
				<td>运动方式</td>
				<td>训练部位</td>
				<td>运动速度</td>
				<td>运动时间</td>
				<td>运动频率</td>
			</tr>
			<tr class='sports_table_tr'>";
				echo "<td>$sport->exercisetype</td>";
				echo "<td>$sport->item</td>";
				echo "<td>$sport->motion</td>";
				echo "<td>$sport->trainingsite</td>";
				echo "<td>$sport->speed</td>";
				echo "<td>$sport->times</td>";
				echo "<td class='sports_table_td_last'>&nbsp</td></tr></table>";
				/*echo "<tr class='sports_table_bottom'>
				<td colspan='7'>$sport->trainingstandard</td>
			</tr>";*/
				echo "<table class='sports_table' border='1' cellspacing='0'>
				<div class='sports_run' id='sports_weight'>
			<div class='sports_run_left' id='weight_lift_img'><img src='".Yii::app()->baseUrl.$sport->picturesource."'></div>
			<div class='sports_run_right'>
				<div class='sports_run_title'>
					运动要领
				</div>
				<div class='sports_run_content'>
					".$sport->trainingstandard."
				</div>
				<div class='sports_run_content'>
					".$sport->subjectivefeel."
				</div>
			</div>
		</div>";

			}?>
		</table>
		
			<div class='sports_run_bottom'>
					你还可以进行的运动有：<?=$sports['benefit']['safe_sug_other_str']?></div>
		
		<div id='sports_content_top' class='view_content_top'>
			<div id='sports_title' class='view_content_title'>专家建议</div>
			<div id='sports_subtitle'>Expert Advice</div>
		</div>
		<div id='sports_advice'>
			<div id='sports_advice_left'>
				<img src='<?=Yii::app()->baseUrl?>/images/front/fat.jpg'>
			</div>
			<div id='sports_advice_middle'>
				<img src='<?=Yii::app()->baseUrl?>/images/front/line.jpg'>
			</div>
			<div id='sports_advice_right'>
				<div class='sports_advice_item_top'>
					<span class='sprots_advice_right_title'>运动量：</span>
					<span class='sprots_advice_right_content'><?=$advice?></span>
				</div>
				<div class='sports_advice_item'>
					<span class='sprots_advice_right_title'>运动类型：</span>
					<span class='sprots_advice_right_content'>有氧运动</span>
				</div>
				<div class='sports_advice_item'>
					<span class='sprots_advice_right_title'>运动强度：</span>
					<span class='sprots_advice_right_content'>中等强度</span>
				</div>
				<div class='sports_advice_item'>
					<span class='sprots_advice_right_title sports_item_last'>心率范围：</span>
						<div id='sports_advice_item_bottom'>
							<span class='sprots_advice_right_content content_green'>心率达到150-年龄(次/分)</span>
							<span class='sprots_advice_right_content content_red'>不宜达到170-年龄(次/分)</span>
						</div>					
				</div>
			</div>
			<div id='sports_advice_right_green'>
				<img src='<?=Yii::app()->baseUrl?>/images/front/fat_green.jpg'>
			</div>
		</div>
		<div id='sports_kuohao'>
			<img src='<?=Yii::app()->baseUrl?>/images/front/kuohao.png'>
		</div>
		<div id="sports_advice_cotent_border" class="advice">
			<div id='advice_content_border'class="advice_content">
				培养良好的饮食行为和运动习惯是控制体重或减肥的必需措施。因此，建议<font font-weight="bold";color="#ff4c4c">超重或肥胖</font>的人每天应累积达到
		<font color='#007efd'>8000到10000步</font>的活动量。<br/><br/>
		
		       &nbsp&nbsp&nbsp&nbsp<font size='5em';weight='bold'>运动中需要氧参与能量供给才能完成的运动，称为<font color='#007efd'>有氧运动</font></font>。如步行、慢跑、骑自行车等。每个
		人体质不同，所能承受的运动负荷也不同，找到适合自己的活动强度和活动量，锻炼才会更加安全有效。更有效的
		促进健康需要进行中等强度的活动，并且每次活动应在1000步活动量或10分钟以上。<br/><br/>
		
		       &nbsp&nbsp&nbsp&nbsp根据自己的感觉也可以判断运动的强度，达到<font color='#ff9f3f'>中等强度</font>时，你会感到心跳和呼吸加快（脉搏数在100~120次/
		分），额头微微出汗。以走路为例，40分走3公里的速度亦可视为有氧运动。<br/><br/>
		
		       &nbsp&nbsp&nbsp&nbsp一般健康人还可以根据运动时的心率判断运动的强度，适宜的运动强度应使心率达到150-年龄（次/分）。除
		了体质较好者，运动心率<font color='#ff4c4c'>不宜超过170-年龄（次/分）</font>。对于老年人，这样的心率计算不一定适用，主要应根据自
		己的体质和运动中的感觉来确定强度。在运动前不仅要知道该如何动，还要了解什么样的运动对健康不利。一定要
		坚持适量、循序渐进的原则，过量的运动对身体也是有害的。</div>
			
			<div id='advice_bottom_left_border' class="advice_bottom_left"></div>
			<div id='advice_bottom_right_border' class="advice_bottom_right"></div>
		</div>
		<div style='clear:both;'></div>
		<div id="sports_back_yellow" class="advice">
			
			<div class="advice_content">
				<div id='advice_content_title'>运动禁忌：</div>
					<span>1、“高血压”—在血压控制在正常范围内，适合低中强度运动；</span>
     			 	<span>2、“冠心病”—适合低强度运动；</span>
      			 	<span>3、“颈腰椎病不适”—严禁颈部、腰部旋转运动。</span>
			</div>
			<div class="advice_bottom_left" id='advice_bottom_left70'></div>
			<div class="advice_bottom_right"></div>
		</div>
		<div style='clear:both'></div>
		<div id='sports_precautions_top' class='view_content_top'>
			<div id='sports_title' class='view_content_title'>运动注意事项</div>
			<div id='sports_subtitle'>Sports Precaution</div>
		</div>
		<table id='sports_precaution_content'>
			<tr class='sports_precaution_content_blue'>
				<td>  1、运动时需要选择适合的服装，鞋袜及相关保护装备（护膝、护腰、护腕、护肘、手套等）；</td>
			</tr>
			<tr>
				<td>
					2、运动前的热身，运动后的放松；
				</td>
			</tr>
			
			<tr class='sports_precaution_content_blue'>
				<td>
					 3、运动前后切忌饮酒、马上洗浴、暴饮暴食；
				</td>
			</tr>
			
			<tr>
				<td>
					     4、运动最佳时间在下午的5~7点；
				</td>
			</tr>
			
			<tr class='sports_precaution_content_blue'>
				<td>
					5、每天运动时间最好保持在30分钟以上；
				</td>
			</tr>
			
			<tr>
				<td>
					   6、每周保持5次的运动频率；
				</td>
			</tr>
			
			<tr class='sports_precaution_content_blue'>
				<td>
					  7、有氧和无氧相结合以低强度、中强度、多样化的运动相结合的方式为宜；
				</td>
			</tr>
			
			<tr>
				<td>
					   8、肥胖或体重过大的人群需要采用对关节压力较小的运动（自行车、游泳、椭圆机等），避免好冲击的跑跳、冲刺、急停等方式。
				</td>
			</tr>
			
		</table>
		<div id='sports_precautions_top' class='view_content_top'>
			<div id='sports_title' class='view_content_title'>运动耗能表</div>
			<div id='sports_subtitle'>Sports Energy</div>
		</div>
		<div class='sports_precautions_content'>
			<span class='sports_precautions_content_blue'>60分钟各项运动热量消耗（单位：大卡）</span>
			<table class='sports_precautions_table' id='sports_precautions_table_green' cellspacing="0" border="1">
				<tr class='sports_precautions_table_title_green'>
					<td>逛街</td>
					<td>洗碗</td>
					<td>普拉提</td>
					<td>泡澡</td>
					<td>骑单车</td>
					<td>高尔夫</td>
					<td>郊游</td>
					<td>综合有氧运动</td>
					<td>慢走</td>
					<td>起码</td>
					<td>跳舞</td>
					<td>健美操</td>
					<td>打桌球</td>
					<td>体能训练</td>
				</tr>
				<tr class='sports_precautions_table_green_tr'>
					<td>110</td>
					<td>136</td>
					<td>144</td>
					<td>168</td>
					<td>184</td>
					<td>186</td>
					<td>240</td>
					<td>252</td>
					<td>255</td>
					<td>276</td>
					<td>300</td>
					<td>300</td>
					<td>300</td>
					<td>300</td>
				</tr>
			</table>
			<div class='sports_energy_line'>
				<div class='sports_energy_left'></div>
				<div class='sports_energy_middle'>
					<div class='sports_energy_middle_top'></div>
					<div class='sports_energy_middle_middle'>300大卡</div>
					<div class='sports_energy_middle_top'></div>
				</div>
				<div class='sports_energy_left'></div>
			</div>
			<table class='sports_precautions_table' id='sports_precautions_table_orange' cellspacing="0" border="1">
				<tr class='sports_precautions_table_title_orange'>
					<td>打网球</td>
					<td>滑雪</td>
					<td>仰卧起坐</td>
					<td>跳绳</td>
					<td>打拳</td>
					<td>爬楼梯</td>
					<td>快走</td>
					<td>爬山</td>
					<td>慢跑</td>
					<td>快跑</td>
					<td>练武术</td>
					<td>游泳</td>
				</tr>
				<tr class='sports_precautions_table_orange_tr'>
					<td>352</td>
					<td>354</td>
					<td>432</td>
					<td>448</td>
					<td>450</td>
					<td>480</td>
					<td>555</td>
					<td>600</td>
					<td>655</td>
					<td>700</td>
					<td>790</td>
					<td>1036</td>
				</tr>
			</table>
			<div id='sports_energy_bottom'>
				根据自己每天的食物摄入量以及自身身体情况选择合适耗能的运动方式吧！
			</div>
		</div>
		
	</div>
	<div style='clear:both'></div>
	<div class="view_bottom">
		<div class="view_bottom_right"><img src="/marvel_b2b/sources/trunk/web/images/front/heath_report/chuanqi_logo.png">
		</div>
	</div>
</div>





