<?php
$basic = array();
foreach ($item["basic"] as $value) {
//    var_dump($value);
    $basic[$value->name] = $value->value;
}
$items = array();
foreach ($item["info"] as $it){
	if($it["parent_name"] == "basic")
		continue;
	if(!isset($items[$it["parent_name"]])){
		$items[$it["parent_name"]]["items"] = array();
		$items[$it["parent_name"]]["count"] = 0;
	}
	array_push($items[$it["parent_name"]]["items"], $it);
	$items[$it["parent_name"]]["count"]++;
}
//var_dump($items[$it["parent_name"]]["items"]);
?>
<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->
<!-- 自然医学调理方案_start -->
<div id='siysfa'>
	<div class="view_top" id="zryx_top">
	<div class="view_top_left" id="zryx_left">
		<div class="view_top_num" id="zryx_top_num">8</div>
		<div class="view_top_title" id="zryx_top_title">自然医学调理方案</div>
		<div class="view_top_subtitle" id="zryx_top_subtitle">Natural Medicine conditioning program</div>
	</div>
	<div class="view_top_right" id="nutrition_top_right"><span>姓名：马胜盼</span>
		<span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
	</div>
	<div class="view_content_top zryx_top">
			<div class="view_content_title zryx_tile">疗法及疗效</div>
			<div class="zryx_subtitle">Therapy and efficacy</div>
	</div>
	<div class='lf_item yinliao'>
		<div class='lf_item_content'>
		       将音乐中16~150Hz的低频信号，经增强放大和物理换能后，通过听觉、尤其触振动觉的传导方式，
			使音乐的“外源性振动”与人体的“内源性振动”产生同频共振，从而对人产生快速深度的放松和理疗
			作用。可提升人体免疫力；缓解头痛、偏头痛；改善失眠；化解忧郁；调节荷尔蒙等。
		</div>
		
	</div>
	<div class='lf_item xiangliao'>
		<div class='lf_item_content'>
		        采用芳香植物、药效植物所提取出来的纯净、清澈、芳香的精油，它具强渗透、高流动性和高挥发
新的特点，通过按摩肌肤或嗅觉被人体吸收，对我们的情绪和身体产生作用，达到调理亚健康，治疗身
体疾病，抚慰精神，愉悦心境的目的。
		</div>
		
	</div>
	<div class='lf_item changliao'>
		<div class='lf_item_content'>
		         肠道水疗是当今国际流行的一种纠正腹泻、治疗便秘、肠炎、调节肠道菌群失调、预防肠癌、改善
肠道内环境，促进新陈代谢，达到排毒养颜、护肤、美容、减肥、抗衰老，提高机体免疫功能等功效的
新兴保健方法，被誉为“21世纪最热门的物理性内调保健自然疗法”。
		</div>
		
	</div>
	<div class="view_content_top zryx_top">
			<div class="view_content_title zryx_tile">疗法配合方案</div>
			<div class="zryx_subtitle">Therapy coordinate</div>
	</div>
	<table id="lfphfa_table" >
                <tbody>
                	<tr class="lfphfa_table_title">
                    	<th>项目</th>
                    	<th>音疗</th>
                   		<th>香疗</th>
                    	<th>肠疗</th>
                </tr>
                <tr>
                	<td class='lfphfa_table_td_first'>促进新陈代谢</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td>&nbsp</td>                	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>消耗过多脂肪</td>
                	<td>&nbsp</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>                	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>帮助睡眠</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td>&nbsp</td>                	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>缓解压力</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td>&nbsp</td>                	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>恢复精力</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>              	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>美容养颜</td>
                	<td>&nbsp</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>               	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>调理亚健康</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chahao_lf.jpg"></td>              	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>心理调整</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td>&nbsp</td>                	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>癌症预防</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>              	
                </tr>
                  <tr>
                	<td class='lfphfa_table_td_first'>慢性病调理</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chahao_lf.jpg"></td>               	
                </tr>
                 <tr>
                	<td class='lfphfa_table_td_first'>两性保健</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td>&nbsp</td>                	
                </tr>
                 <tr>
                	<td class='lfphfa_table_td_first'>促进血液循环</td>
                	<td>&nbsp</td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>
                	<td><img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/duihao_lf.jpg"></td>             	
                </tr>
                </tbody></table>
                <div class="view_bottom">
            <div class='view_bottom_right'><img
                    src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chuanqi_logo.png">
            </div>
        </div>
</div>


