<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/arrow27.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/Chart.js"></script>
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/excanvas.js"></script>-->
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/html5-canvas-bar-graph.js"></script>-->
    <?php //Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/jquery/jquery-1.10.2.min.js"></script>
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->

       <div id='risk_assement'>
	<div id="risk_assement_top" class="view_top">
	<div id="risk_assement_top_left" class="view_top_left">
		<div id="risk_assement_top_num" class="view_top_num">2</div>
		<div id="risk_assemente_top_title" class="view_top_title">风险评估</div>
		<div id="risk_assement_top_subtitle" class="view_top_subtitle">Risk Assessment</div>
	</div>
	<div id="nutrition_top_right" class="view_top_right"><span>姓名：马胜盼</span>
	<span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
	</div>
	<div style='clear:both'></div>
	<div class='risk_middle'>
		<div class='risk_middle_top'>
			<span>正常</span>
			<span>低危</span>
			<span>中危</span>
			<span>高危</span>			
		</div>
		
	</div>
	<script>
		 $(document).ready(function() {
			 //高血压
			function drawRec1(count,level){
				   var canvas = $("#myCanvas1");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(40, j, 90, 15);
					   } 

			}
			//画第二种长方形
			function drawRec2(count,level){
				   var canvas = $("#myCanvas1");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(140, j, 90, 15);
					   } 

			}
			//画第三种长方形
			function drawRec3(count,level){
				   var canvas = $("#myCanvas2");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(40, j, 90, 15);
					   } 

			}
			function drawRec4(count,level){
				   var canvas = $("#myCanvas2");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(140, j, 90, 15);
					   } 

			}
			function drawRec5(count,level){
				   var canvas = $("#myCanvas3");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(40, j, 90, 15);
					   } 

			}
			function drawRec6(count,level){
				   var canvas = $("#myCanvas3");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(140, j, 90, 15);
					   } 


			}
			function drawRec7(count,level){
				   var canvas = $("#myCanvas4");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(40, j, 90, 15);
					   } 
			}
		
			function drawRec8(count,level){
				   var canvas = $("#myCanvas4");
				   var context = canvas.get(0).getContext("2d");
				   //根据疾病危险水平，变换方块的颜色
				   if(level=='dw'){
					   context.fillStyle = "#1da9d8";
					}else if(level=='zw'){
						context.fillStyle = "#ffbf00";
					}else if(level=='gw'){
						context.fillStyle = "#ff5959";
					}else{
						context.fillStyle = "#74b742";
					}
				   var i;
				   for(i=0;i<count;i++){
					   j=130-17*i;
					   context.fillRect(140, j, 90, 15);
					   } 
			}


			
			count=2;
			level='zw';
			<?php 
			echo "drawRec1(".$dis_properties['now_info']["高血压"]['step'].",'".$dis_properties['now_info']["高血压"]['now_level']."');";
			echo "drawRec2(".$dis_properties['future_info']["高血压"]['step'].",'".$dis_properties['future_info']["高血压"]['futrue_level']."');";
			echo "drawRec3(".$dis_properties['now_info']["冠心病"]['step'].",'".$dis_properties['now_info']["冠心病"]['now_level']."');";
			echo "drawRec4(".$dis_properties['future_info']["冠心病"]['step'].",'".$dis_properties['future_info']["冠心病"]['futrue_level']."');";
			echo "drawRec5(".$dis_properties['now_info']["脑卒中"]['step'].",'".$dis_properties['now_info']["脑卒中"]['now_level']."');";
			echo "drawRec6(".$dis_properties['future_info']["脑卒中"]['step'].",'".$dis_properties['future_info']["脑卒中"]['futrue_level']."');";
			echo "drawRec7(".$dis_properties['now_info']["糖尿病"]['step'].",'".$dis_properties['now_info']["糖尿病"]['now_level']."');";
			echo "drawRec8(".$dis_properties['future_info']["糖尿病"]['step'].",'".$dis_properties['future_info']["糖尿病"]['futrue_level']."');";
//			level='gw';
//			drawRec2(5,'gw');
//			drawRec3(count);
//			drawRec4(count);
//			drawRec5(count);
//			drawRec6(count);
//			drawRec7(4);
//			drawRec8(count);
			?>
		});
	</script>
	<div style='clear:both'></div>
	<div class='zuobian_multi'>
		<div class='zuobiao_item'>
			<div class='fxz'>风险值</div>
			<div class='gxy'>高血压</div>
			<div class='gw'>高危</div>
			<div class='zw'>中危</div>
			<div class='dw'>低危</div>
			<div class='zc'>正常</div>
			<canvas id='myCanvas1' class='risk_zuobiao'>
			
			</canvas>
			<div class='mq'>目前</div>
			<div class='wlsn'>未来10年</div>
		</div>
		<div class='zuobiao_item'>
			<div class='fxz'>风险值</div>
			<div class='gxy'>冠心病</div>
			<div class='gw'>高危</div>
			<div class='zw'>中危</div>
			<div class='dw'>低危</div>
			<div class='zc'>正常</div>
			<canvas id='myCanvas2' class='risk_zuobiao'>
			
			</canvas>
			<div class='mq'>目前</div>
			<div class='wlsn'>未来10年</div>
		</div>
		<div class='zuobiao_item'>
			<div class='fxz'>风险值</div>
			<div class='gxy'>脑卒中</div>
			<div class='gw'>高危</div>
			<div class='zw'>中危</div>
			<div class='dw'>低危</div>
			<div class='zc'>正常</div>
			<canvas id='myCanvas3' class='risk_zuobiao'>
			
			</canvas>
			<div class='mq'>目前</div>
			<div class='wlsn'>未来10年</div>
		</div>
		<div class='zuobiao_item'>
			<div class='fxz'>风险值</div>
			<div class='gxy'>糖尿病</div>
			<div class='gw'>高危</div>
			<div class='zw'>中危</div>
			<div class='dw'>低危</div>
			<div class='zc'>正常</div>
			<canvas id='myCanvas4' class='risk_zuobiao'>
			
			</canvas>
			<div class='mq'>目前</div>
			<div class='wlsn'>未来10年</div>
		</div>
	</div>
	<div style='clear:both'></div>
	<div class="view_bottom">
		<div class="view_bottom_right">
			<img src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chuanqi_logo.png">
		</div>
	</div>
</div>
    

   



