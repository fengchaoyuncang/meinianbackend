<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/arrow27.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/Chart.js"></script>
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/excanvas.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/html5-canvas-bar-graph.js"></script>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->



    <!--风险分析 start-->
    <div id='risk_analysis'>
        <div class='view_top' id="risk_analysis_top">
            <div class='view_top_left' id="risk_analysis_top_left">
                <div class='view_top_num' id="risk_analysis_top_num">3</div>
                <div class='view_top_title' id="risk_analysis_top_title">风险分析</div>
                <div class='view_top_subtitle' id='risk_analysis_top_subtitle'>Risk Analysis</div>
            </div>
            <div class='view_top_right' id='risk_analysis_top_right'><span>姓名：马胜盼</span>
                <span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
        </div>
        <div id='risk_analysis_middle'>

            <div class='view_content_top'>
                <div id='risk_analysis_title' class='view_content_title'>风险因素</div>
                <div id='risk_analysis_subtitle'>Risk Factors</div>
            </div>
            <table id='risk_factors'>
                <tr class='risk_factors_title'>
                    <th>项目</th>
                    <th>相关内容</th>
                    <th>诊断结果</th>


                <tr>


				<?php 
						if (!empty($summary["summary_ret"])){
							foreach ($summary['summary_ret'] as $key => $value){
							echo "<tr class='risk_factor_content_tr'>";
								echo "<td class='risk_factor_content_td_first'>";
				                if (!empty($key)){ 
									echo $key;
								}
								echo "</td>";
								echo "<td class='risk_factor_content_td_second'>";
							if (!empty($value['content'])){ 
									echo $value['content'];
							}
								echo "</td>";
//							if (!empty($summary['disname'])){ 
//								echo "<td class='risk_factor_content_td_second'>无</td>";
//								echo "<td>".$summary['disname']."</td>	";
//							}
							if ($value['result'] === 0){
								echo 	"<td class='risk_factor_content_td_second'>
										<img src='".Yii::app()->baseUrl."/images/front/duihao.png' />
										</td>";
							}else{
								echo "<td class='risk_factor_content_td_second'>&nbsp
										</td>";
							}
								?>
               </tr>
                	<?php 		}
					}
					?>
            </table>

            <div class='view_content_top'>
                <div id='risk_analysis_title' class='view_content_title'>疾病解读</div>
                <div id='risk_analysis_subtitle'>Report Evidence</div>
            </div>
            <div class='evidence'>	
            <?php 
            if (isset($summary["dis_summary"])){
            	foreach ($summary['dis_summary'] as $summary_ins){?>
                <div id='evicence_title'>您的
                <?php 
                	if(!empty($summary_ins['disname']))
                	{
                		echo $summary_ins['disname'];
                	}?>
                	患病为
                	<?php 
                	if(!empty($summary_ins['dislevel']))
                	{
                		echo $summary_ins['dislevel'];
                	}
                	?>危</div>
                	<div id='evidence_content'>
            		<?php if(!empty($summary_ins['resolve']))
                    {
                    	echo $summary_ins['resolve'];
//                    	foreach ($summary_ins['resolve'] as $it){
//                    		echo $it['resolve'];
//                    	}
                    }
                    ?>
					</div>
                <?php }
            }?>
            </div>
            </div>

            <div class='view_content_top'>
                <div id='risk_analysis_title' class='view_content_title'>专家意见</div>
                <div id='risk_analysis_subtitle'>Export Advice</div>
            </div>
            <div class='advice' id='medical_advice'>
                <div class='tjbz_zjyi'>
                    核心提示：专家表示，“EB病毒阳性不一定是鼻咽癌，但EB病毒阴性也不一定不会得鼻咽癌。”慢性咽炎的EB病毒也是呈阳性，中科院流行病学曾经做过的一个调查，在3000例鼻咽癌患者中，其中只有80人EB病毒检测呈阳性，仅占了26.6%，其他都是阴性，所以EB病毒阳性并不是鼻咽癌一个必然因素。
                </div>

            </div>
         <div class="view_bottom">
            <div class='view_bottom_right'><img
                    src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chuanqi_logo.png">
            </div>
        </div>
        </div>
       



