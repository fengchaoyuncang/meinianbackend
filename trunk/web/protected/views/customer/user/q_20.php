<!--<input name="Q[<?=$que->id?>][ans][id]" type="hidden" value="<?=$que->id?>">-->
<p>（1）您手脚发凉吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（2）您胃脘部、背部或腰膝部怕冷吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（3）您感到怕冷、衣服比别人穿得多吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（4）您比一般人不了寒冷（冬天的寒冷，夏天的冷空调、电扇等。</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（5）您比别人容易患感冒吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（6）您吃（喝）凉的东西会感到不舒服或者怕吃（喝）凉东西吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（7）你受凉或吃（喝）凉的东西后，容易腹泻（拉肚子）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yangxu][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>






<p>（8）您感到手脚心发热吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（9）您感觉身体、脸上发热吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（10）您皮肤或口唇干吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（11）您口唇的颜色比一般人红吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（12）您容易便秘或大便干燥吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（13）您面部两潮红或偏红吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（14）您感到眼睛干涩吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（15）您活动量稍大就容易出虚汗吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][8]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][8]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][8]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][8]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][yinxu][inturn][8]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>






<p>（16）你容易疲乏吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（17）您容易气短（呼吸短促，接不上气吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（18）您容易心慌吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（19）您容易头晕或站起时晕眩吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（20）您比别人容易患感冒吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（21）您喜欢安静、懒得说话吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（22）您说话声音无力吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（23）您活动量就容易出虚汗吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][8]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][8]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][8]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][8]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qixu][inturn][8]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>




<p>（24）您感到胸闷或腹部胀满吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（25）您感到身体学生不轻松或不爽快吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（26）您腹部肥满松软吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（27）您有额部油脂分泌多的现象吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（28）您上眼睑比别人肿（仍轻微隆起的现象）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（29）您嘴里有黏黏的感觉吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（30）您平时痰多，特别是咽喉部总感到有痰堵着吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（31）您舌苔厚腻或有舌苔厚厚的感觉吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][8]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][8]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][8]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][8]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tanshi][inturn][8]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>



<p>（32）您面部或鼻部有油腻感或者油亮发光吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（33）你容易生痤疮或疮疖吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（34）您感到口苦或嘴里有异味吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（35）您大使黏滞不爽、有解不尽的感觉吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（36）您小便时尿道有发热感、尿色浓（深）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（37）您带下色黄（白带颜色发黄）吗？（限女性回答）</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（38）您的阴囊部位潮湿吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][shire][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>



<p>（39）您的皮肤在不知不觉中会出现青紫瘀斑（皮下出血）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（40）您两颧部有细微红丝吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（41）您身体上有哪里疼痛吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（42）您面色晦黯或容易出现褐斑吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（43）您容易有黑眼圈吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（44）您容易忘事（健忘）吗</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（45）您口唇颜色偏黯吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][xueyu][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>




<p>（46）您没有感冒时也会打喷嚏吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（47）您没有感冒时也会鼻塞、流鼻涕吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（48）您有因季节变化、温度变化或异味等原因而咳喘的现象吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（49）您容易过敏（对药物、食物、气味、花粉或在季节交替、气候变化时）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（50）您的皮肤容易起荨麻疹（风团、风疹块、风疙瘩）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（51）您的因过敏出现过紫癜（紫红色瘀点、瘀斑）吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（52）您的皮肤一抓就红，并出现抓痕吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][tebing][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>




<p>（53）您感到闷闷不乐吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（54）您容易精神紧张、焦虑不安吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（55）您多愁善感、感情脆弱吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（56）您容易感到害怕或受到惊吓吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（57）您胁肋部或乳房腹痛吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（58）您无缘无故叹气吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（59）您咽喉部有异物感，且吐之不出、咽之不下吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][qiyu][inturn][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>




<p>（60）您精力充沛吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][1]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][1]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][1]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][1]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][1]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（61）您容易疲乏吗？*</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][2]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][2]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][2]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][2]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][2]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（62）您说话声音无力吗？*</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][3]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][3]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][3]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][3]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][3]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（63）您感到闷闷不乐吗？*</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][4]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][4]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][4]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][4]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][4]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（64）您比一般 人耐受不了寒冷（冬天的寒冷，夏天的冷空调、电扇）吗？*</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][5]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][5]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][5]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][5]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][5]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（65）您吃（喝）凉的东西会感到不舒服或者怕吃（喝）凉东西吗？</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][6]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][6]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][6]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][6]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][inturn][6]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（66）您容易失眠吗？*</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][7]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][7]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][7]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][7]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][7]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>
<p>（67）您容易忘事（健忘）吗？*</p>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][8]" type="radio" value="1"> 没有（根本不）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][8]" type="radio" value="2"> 很少（有一点）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][8]" type="radio" value="3"> 有时（有些）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][8]" type="radio" value="4"> 经常（相当）
    </label>
<label class="checkbox">
      <input name="Q[20][ans][pinghe][reverse][8]" type="radio" value="5"> 总是（非常）
    </label>
<br></br>