<?php
$basic = array();
foreach ($item["basic"] as $value) {
//    var_dump($value);
    $basic[$value->name] = $value->value;
}
$items = array();
foreach ($item["info"] as $it){
	if($it["parent_name"] == "basic")
		continue;
	if(!isset($items[$it["parent_name"]])){
		$items[$it["parent_name"]]["items"] = array();
		$items[$it["parent_name"]]["count"] = 0;
	}
	array_push($items[$it["parent_name"]]["items"], $it);
	$items[$it["parent_name"]]["count"]++;
}
//var_dump($items[$it["parent_name"]]["items"]);
?>
<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/front/health_report.css">
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->
    



    

   
    

<!-- 不良生活习惯改善_start -->
<div id='bad_habits'>
	<div class="view_top" id="exercise_top">
	<div class="view_top_left" id="bad_habits_top_left">
		<div class="view_top_num" id="bad_habits_top_num">6</div>
		<div class="view_top_title" id="bad_habits_top_title">不良生活习惯改善</div>
		<div class="view_top_subtitle" id="bad_habits_top_subtitle">Improvement of bad habits</div>
	</div>
	<div class="view_top_right" id="nutrition_top_right"><span>姓名：马胜盼</span>
	<span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
	</div>
	<!--戒烟_start-->
	<div class="view_content_top bad_habits_view_content_top">
			<div class="view_content_title bad_habits_tile">戒烟</div>
			<div class="bad_habits_subtitle">Give up smoking</div>
	</div>
	<div class='bad_habits_content'>
		<div class='bad_habits_content_left'>
			       吸烟是严重为威胁人类生命的20世纪瘟疫。
				已证明吸烟是心脑血管病、肺癌、呼吸道疾病的重要危险因素。
				被动吸烟同样对身体有重要损害。<br/>
				      提倡不要吸烟并避免被动吸烟！
		</div>
		<div class='bad_habits_content_right'>
			<img src="<?=Yii::app()->baseUrl?>/images/front/heath_report/smoking.jpg">
		</div>
	</div>
	<!--戒烟_end-->
	<div style='clear:both'></div>
	<!-- 限酒_start -->
	<div class="view_content_top bad_habits_view_content_top">
			<div class="view_content_title bad_habits_tile">限酒</div>
			<div class="bad_habits_subtitle">Limit alcohol</div>
	</div>
	<div class='bad_habits_content'>
		<div class='bad_habits_content_left'>
			           每天饮用适量饮酒对人体健康有益，但嗜酒成瘾万万不可！<br/>
				 推荐适量饮酒的参考值为：<br/>
				酒精量12~24g/每天，即：每天饮两小杯白酒或2罐啤酒或2杯干红葡萄酒<br/>
				（100~150毫升，约2~3两）。<br/>
				       建议您适每天饮用100ml干红！
		</div>
		<div class='bad_habits_content_right'>
			<img src="<?=Yii::app()->baseUrl?>/images/front/heath_report/red_wine.jpg">
		</div>
	</div>
	<!-- 限酒_end -->
	<div style='clear:both;'></div>
	
	<!-- 睡眠建议_start -->
		<div class="view_content_top bad_habits_view_content_top">
			<div class="view_content_title bad_habits_tile">睡眠建议</div>
			<div class="bad_habits_subtitle">sleep tips</div>
	</div>
	<div class='bad_habits_content'>
		<div class='bad_habits_content_left'>
			        <span>1、我们在睡觉前的两小时之内不要进食，这样只会让我们体内的食物堆积，这些
					都形成了脂肪，很容易发胖。我们在睡眠的时候，我们的肠胃已经进入了休眠状态，
					而那些在临睡之前进入的食物只会在肠胃你堆积，这些都是引发肠胃疾病的罪魁祸首。</span>
					<span>2、我们也切忌在入睡前跟别人聊天，因为聊天能够带动我们积极的情绪，思维
					也跟着活跃，在闭着眼睛等待睡眠的时间里面，我们就出于一种兴奋的状态。从而我
					们大家就很难入眠。</span>
					<span>3、在入睡之前，我们建议大家不要过度用脑，在临睡之前要做好各种事情。也不
					要在睡眠的时间之内想起比较复杂很费脑筋的事情。</span>
					<span>4、切忌大喜大悲的情绪，应该以一种轻松的心态入眠。</span>
					<span>5、在临睡之前，我们不要饮浓茶、喝咖啡，此类饮料都属于刺激性饮品，饮用过
					多很容易造成入眠困难。</span>
		</div>
		<div class='bad_habits_content_right'>
			<img src='<?=Yii::app()->baseUrl?>/images/front/heath_report/sleep.jpg'>
		</div>
	</div>
	<!-- 睡眠建议_end -->
	<div class="view_bottom">
		<div class="view_bottom_right"><img src="<?=Yii::app()->baseUrl?>/images/front/heath_report/chuanqi_logo.png">
		</div>
	</div>
</div>
<!-- 不良生活习惯改善_end -->
