<style type="text/css">
.sel_wrap{
    display: inline-block;
}
.sel_wrap{
    position:relative;
    width:30%;
    height:1.5em;
    background:#fff url(images/png.png) no-repeat right center;
    border:1px solid #a0a0a0;
    border-radius:2px;
    -webkit-border-radius:2px;
    -moz-border-radius:2px;
    cursor:pointer;
    _filter:alpha(opacity=0);
}
.sel_wrap span{
    display: inline-block;
    font-size: 0.7em;
    padding:0.4em 0.4em;
    /*line-height: 2em;*/
}
.sel_wrap .select{
    width:100%;
    height:1.5em;
    z-index:4;
    font-size: 1em;
    position:absolute;
    top:0;
    left:0;
    margin:0;
    padding:0;
    opacity:0;
    cursor:pointer;
}
</style>
<li class="que-line" id="<?=$que->id?>">
    <!--<span class="que-line-head" id="weight_height">
        <span class="que-line-head-title">Qusetion&nbsp;<?=$pagecount+1?></span>
       <?=$que->title?>
    </span>
    -->
 	<input name="Q[<?=$que->id?>][ans]" type="hidden">
    <div class="que-line-content" id="select_content">
        身高:<div class="sel_wrap" id="sel_wrap4">
            <span>120</span>
            <select name="Q[16][ans][height]" value="120" class="select" id="c_select4">
            </select>
        </div>
        <span>cm</span>
        体重:<div class="sel_wrap" id="sel_wrap5">
            <span>35</span>
            <select name="Q[16][ans][weight]" value="35" class="select" id="c_select5">
            </select>
        </div>
        <span>kg</span>
    </div>
</li>
<script type="text/javascript">
        function carry_m(max1,min1,max2,min2){
            var c_select4 = document.getElementById('c_select4');
            var c_select5 = document.getElementById('c_select5');
            var temp1 = "";
            for(var i = min1; i <= max1; i++){
              temp1 += "<option value=" + i + ">" + i + "</option>";
            }
            c_select4.innerHTML = temp1;
            var temp2 = "";
            for(var i = min2; i <= max2; i++){
              temp2 += "<option value=" + i + ">" + i + "</option>";
            }
            c_select5.innerHTML = temp2;
        }
        carry_m(220,120,100,35);
        function $(x){
            return document.getElementById(x);
          }
          var sel4=$("c_select4");
          var sel5=$("c_select5");
          var wrap4=$("sel_wrap4");
          var wrap5=$("sel_wrap5");
          var result4 =wrap4.getElementsByTagName("span");
          var result5 =wrap5.getElementsByTagName("span");
          wrap4.onmouseover=function(){
            this.style.backgroundColor="#fff";
          }
          wrap5.onmouseover=function(){
            this.style.backgroundColor="#fff";
          }
          wrap4.onmouseout=function(){
            this.style.backgroundColor="#fafafa";
          }
            wrap5.onmouseout=function(){
            this.style.backgroundColor="#fafafa";
          }
          sel4.onchange=function(){
            var opt=this.getElementsByTagName("option");
            var len=opt.length;
            for(i=0;i<len;i++){
              if(opt[i].selected==true){
                x=opt[i].innerHTML;
              }
            }
            result4[0].innerHTML=x;
          }
          sel5.onchange=function(){
            var opt=this.getElementsByTagName("option");
            var len=opt.length;
            for(i=0;i<len;i++){
              if(opt[i].selected==true){
                x=opt[i].innerHTML;
              }
            }
            result5[0].innerHTML=x;
          }
</script>