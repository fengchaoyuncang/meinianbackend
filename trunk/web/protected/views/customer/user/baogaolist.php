
<div id="main">
    <div id="reserved_main">
        <?php if (count($list) > 0) { ?>
            <?php foreach ($list as $it) { ?>
                <div class="reserved_list">
                    <table>
                        <tr>
                            <td><span class="reserved_title">姓名：</span></td><td><span class="reserved_content"><?= $it['name'] ?></span></td>    
                        </tr>
                        <tr>
                            <td><span class="reserved_title">证件号：</span></td><td><span class="reserved_content"> <?= $it['idcard'] ?></span></td>
                        </tr>
                        <tr>
                            <td><span class="reserved_title">体检套餐：</span></td><td><span class="reserved_content"> <?= $it['package_name'] ?></span></td>
                        </tr>
                        <tr>
                            <td> <span class="reserved_title">体检日期：</span></td>
                            <td>
                                <span class="reserved_content">
                                    <?=$it['order_time']?>
                                </span>
                            </td>

                        </tr>
                        <tr>
                            <td><span class="reserved_title">体检中心：</span></td>
                            <td>
                                <span class="reserved_content">
                                    <?=$it['center_name']?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="reserved_title">公司名称：</span></td>
                            <td>
                                <span class="reserved_content">
                                    <?=$it['customer_company']?>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id='reserve_button'><a href='<?=$this->createUrl("baogaoview",array("orderid"=>$it["id"]))?>' target='_blank'>查看详情</a></div>
            <?php }?>
        <?php }?>
    </div>  
</div>
