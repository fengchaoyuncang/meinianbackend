<?php
$basic = array();
foreach ($item["basic"] as $value) {
//    var_dump($value);
    $basic[$value->name] = $value->value;
}
$items = array();
foreach ($item["info"] as $it){
	if($it["parent_name"] == "basic")
		continue;
	if(!isset($items[$it["parent_name"]])){
		$items[$it["parent_name"]]["items"] = array();
		$items[$it["parent_name"]]["count"] = 0;
	}
	array_push($items[$it["parent_name"]]["items"], $it);
	$items[$it["parent_name"]]["count"]++;
}
//var_dump($items[$it["parent_name"]]["items"]);
?>
<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/arrow27.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/Chart.js"></script>
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/excanvas.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/html5-canvas-bar-graph.js"></script>
  
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->
    <div id="health_report">
        <div class='view_top' id="heath_report_top">
            <div class='view_top_left' id="heath_report_top_left">
                <div class='view_top_num' id="heath_report_top_num">1</div>
                <div class='view_top_title' id="heath_report_top_title">体检报告</div>
                <div class='view_top_subtitle' id='heath_report_top_subtitle'>Medical
                    Report</div>
            </div>
            <div class='view_top_right' id='heath_report_top_right'>
                <?php
                foreach ($item["basic"] as $value) {
                    echo '<span>'.$value->name.':'.$value->value.'</span>';
                }
                ?></div>
        </div>
        <div id="heath_report_main">
            <table id="heath_report_table">
                <tr class="heath_report_main_title">
                    <th style="width:150px;">类别</th>
                    <th style="width:150px;">项目</th>
                    <th>检查结果</th>                    
                    <th>正常范围</th>                    
                </tr>
                
                <?php
                $bg = 0;
                foreach ($items as $key=>$it){
                    
                ?>
                <tr class="health_report_main_content <?=$bg?"health_report_center_blue":""?>">
                    <td style='color:#7d4783;font-size:20px;'rowspan='<?=$it["count"]?>'><?=$key?></td>
                    <?php 
                    	$tr = 0;
                    	foreach($it["items"] as $i){
                    		if($tr != 0){
                    			?>
                    			<tr class="health_report_main_content <?=$bg?"health_report_center_blue":""?>">
                    			<?php 
                    		}
                    		?>
                    <td class='health_report_center'><?=!empty($i->name)?$i->name:"&nbsp"?><?=!empty($i->unit)?"":$i->unit?></td>
                    <td class='health_report_center'><?=!empty($i->value)?$i->value:"&nbsp"?></td>
                    <td class='health_report_center'><?=!empty($i->normal)?$i->normal:"&nbsp"?></td>
                    		<?php 
                    		if($tr == 0){
                    			$tr = 1;
                    		}
                    		echo "</tr>";
                    	}
                    ?>
                    
                
                </tr>        
                <?php
                $bg?$bg=0:$bg=1;
                }
                ?>
                
            </table>
        </div>
        <div class="view_bottom">
            <div class='view_bottom_right'><img
                    src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chuanqi_logo.png">
            </div>
        </div>
    </div>



  



