<?php
$basic = array();
foreach ($item["basic"] as $value) {
//    var_dump($value);
    $basic[$value->name] = $value->value;
}
$items = array();
foreach ($item["info"] as $it){
	if($it["parent_name"] == "basic")
		continue;
	if(!isset($items[$it["parent_name"]])){
		$items[$it["parent_name"]]["items"] = array();
		$items[$it["parent_name"]]["count"] = 0;
	}
	array_push($items[$it["parent_name"]]["items"], $it);
	$items[$it["parent_name"]]["count"]++;
}
//var_dump($items[$it["parent_name"]]["items"]);
?>
<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->
<!-- 中医四季养生方案_start -->
<div id='four_season'>
	<div class="view_top" id="four_season_top">
	<div class="view_top_left" id="four_season_left">
		<div class="view_top_num" id="four_season_top_num">7</div>
		<div class="view_top_title" id="four_season_top_title">中医四季养生方案</div>
		<div class="view_top_subtitle" id="four_season_top_subtitle">TCM Health At the Four Seasons</div>
	</div>
	<div class="view_top_right" id="nutrition_top_right"><span>姓名：马胜盼</span>
		<span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
	</div>
	<div id='cxqd'>
		<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/cxqd.jpg">
	</div>
	<div style='clear:both'></div>
	<!--春季养生_start  -->
	<div class='yangsheng'>
		<div class='yangsheng_title'>
			春季养生
		</div>
		<div class='yangsheng_image'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/shumiao.png">
		</div>
		<div class='yangsheng_content'>
			<span>1、精神养生：心胸开阔，乐观愉快，保持心境恬愉的好心态。</span>
			<span>2、起居调养：要求夜卧（晚睡）早起，免冠披发，松缓衣带，舒展身体，在庭院或场地信步慢性，
克服情志上倦懒思眠的状态，以助生阳之气升发。</span>
			<span>3、饮食调养：春季阳气初生，宜食辛甘发散之品，而不宜食酸收之味。</span>
			<span> 4、运动调养：多做有益于阳气升发的运动（打球、散步、跑步、做操、打拳等）。</span>	
		</div>
	</div>
	<div class='yangsheng_bottom'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/ys_bottom.jpg">
	</div>
	<!--春季养生_end  -->
	
	<!-- 夏季养生_start -->
	<div class='yangsheng' id='xjys'>
		<div class='yangsheng_title'>
			夏季养生
		</div>
		<div class='yangsheng_image'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/taiyang.png">
		</div>
		<div class='yangsheng_content'>
			<span>1、精神养生：神清气和，快乐欢畅，胸怀宽阔，精神饱满，对外界事物要有浓厚兴趣，
培养乐观外向的性格</span>
			<span>      2、起居调养：顺应自然界阳盛阴衰的变化，宜晚睡早起。</span>
			<span>      3、饮食调养：宜多食酸味以固表，多食咸味以补心。</span>
			<span>       4、运动调养：最好在清晨或傍晚较凉爽时进行轻中度运动，不宜做过分剧烈运动。</span>	
		</div>
	</div>
	<div class='yangsheng_bottom'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/xj_bottom.jpg">
	</div>
	<!-- 夏季养生_end -->
	
	<!-- 秋季养生_start -->
	<div class='yangsheng' id='qjys'>
		<div class='yangsheng_title'>
			秋季养生
		</div>
		<div class='yangsheng_image'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/shuye.png">
		</div>
		<div class='yangsheng_content'>
			<span>1、精神养生：防止忧愁。</span>
			<span>2、起居调养：早卧早起，早卧以顺应阳气之收；早起，使肺气得以舒展，且防收之太过。</span>
			<span>3、饮食调养：多吃些滋阴润燥、益气健脾、补肝清肺的饮食，以防秋燥伤阴；忌食寒凉、辛辣油炸的食物。</span>
			<span>4、运动调养：适当运动，多做一些舒畅情志的运动如爬山、骑马等。</span>	
		</div>
	</div>
	<div class='yangsheng_bottom'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/qj_bottom.jpg">
	</div>
	<!--秋季养生_end  -->
	<!-- 冬季养生_start -->
	<div class='yangsheng' id='djys'>
		<div class='yangsheng_title'>
			冬季养生
		</div>
		<div class='yangsheng_image'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/xuehua.png">
		</div>
		<div class='yangsheng_content'>
			<span>1、精神养生：防止忧愁。</span>
			<span>2、起居调养：早卧早起，早卧以顺应阳气之收；早起，使肺气得以舒展，且防收之太过。</span>
			<span>3、饮食调养：多吃些滋阴润燥、益气健脾、补肝清肺的饮食，以防秋燥伤阴；忌食寒凉、辛辣油炸的食物。</span>
			<span>4、运动调养：适当运动，多做一些舒畅情志的运动如爬山、骑马等。</span>	
		</div>
	</div>
	<div class='yangsheng_bottom'>
			<img  src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/dj_bottom.jpg">
	</div>
		<!-- 冬季养生_end -->
		 <div class="view_bottom">
            <div class='view_bottom_right'><img
                    src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chuanqi_logo.png">
            </div>
        </div>
<!--中医四季养生方案_end -->

</div>


