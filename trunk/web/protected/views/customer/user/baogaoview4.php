<head>
    <meta charset="utf-8">
    <title>郁金香云健康</title>
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/front/health_report.css">
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/arrow27.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/Chart.js"></script>
<!--    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/excanvas.js"></script>-->
    <script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/html5-canvas-bar-graph.js"></script>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
</head>
<pre>

</pre>
<div id="main_view"><!-- 体检报告-->
   
      
    

    <!--  4 营养方案-->
    <div id='nutrition'>
        <div class='view_top' id="risk_analysis_top">
            <div class='view_top_left' id="nutrition_top_left">
                <div class='view_top_num' id="nutrition_top_num">4</div>
                <div class='view_top_title' id="nutrition_top_title">营养方案</div>
                <div class='view_top_subtitle' id='nutrition_top_subtitle'>Nutrition
                    Programs</div>
            </div>
            <div class='view_top_right' id='nutrition_top_right'><span>姓名：马胜盼</span>
                <span>性别：男 年龄：18</span> <span>评估分析日期：2013-11-4</span></div>
        </div>
        <div id='nutrition_middle'>
            <div id='nutrition_content_top' class='view_content_top'>
                <div id='nutrition_title' class='view_content_title'>食谱参考</div>
                <div id='nutrition_subtitle'>Recipes Refrence</div>
            </div>
            <table id='recipes'>
                <tr>
                    <th class='recipes_th' id='canci'>餐次</th>
                    <th class='recipes_th' id='swmc'>食物名称</th>
                    <th class='recipes_th' id='liang'>量</th>
                    <th class='recipes_th' id='nengliang'>能量</th>
                </tr>
	            <?php
				//	var_dump($belongto_detail);exit();
				foreach ($meal['meal_arr'] as $mealtype => $meal_info){
					echo "<tr class='recipes_tr'>";
					echo "<td class='recipes_td_first' rowspan=".(count($meal_info)+1).">";
					if (!empty($mealtype)){ 
						echo $mealtype;
					}
					echo "</td>";
					foreach ($meal_info as $name => $value){
						echo "<tr>";
						echo "<td class='recipes_td_second'>";
						if (!empty($name)){ 
							echo $name;
						}
						echo "</td>";
						echo "<td class='recipes_td_third'>";
						if (!empty($value['mass'])){ 
							echo $value['mass'];
						}
						echo "</td>";
						echo "<td class='recipes_td_fourth'>";
						if (!empty($value['energy'])){ 
							echo $value['energy'];
						}
						echo "</td>";
						echo "</tr>";
					}
					echo "</tr>";
				}
				?>
            </table>
			<div id="nutrition_attention" class="advice">
                <div class="advice_content">
                	<span class='notes_title'>注意事项：</span>
                     <span>1、每天除日常工作的能量消耗外，在身体条件允许的条件下，还应累计60分钟中等强度以上的运动。比如快走
、游泳、打乒乓球等相应的健身运动。</span>
       <span>2、如果当日没有完成以上运动消耗，那么总能量摄入要注意降低一个级别；如果当日运动的时间和消耗加倍了，
那您在总能量摄入的选择上应该增加一个级别。</span>
       <span>3、基础代谢随年龄增长而降低，因此，中年人尤其50岁以上者，要注意减少每日种能量摄入的百分比。</span>
                </div>
                <div class="advice_bottom_left"></div>
                <div class="advice_bottom_right"></div>
            </div>
			<!--蔬菜图片和运动图片_start-->
			<div id='vegetable_sprots'>
				<div id='vegetable_sprots_left'>
				</div>
				<div id='vegetable_sprots_middle'>
					<span class='jinshi'>进<br/>食</span>
					<span class='pingheng'>平衡</span>
					<span class='yundong'>运<br/>动</span>
				</div>
				<div id='vegetable_sprots_right'>
				</div>
			</div>
			<!--蔬菜图片和运动图片_end  -->
			
			<!--营养方案专家建议_start  -->
            <div id='nutrition_content_top' class='view_content_top'>
                <div id='nutrition_title' class='view_content_title'>专家建议</div>
                <div id='nutrition_subtitle'>Expert Advice</div>
            </div>
            <!-- 
            <div id='nutrition_evidence_content'>
                对于“肉吃多少克，蔬菜吃多少克”这样的概念，中国健康促进联盟主席王陇德日前建议，人一天的食物量就相当于“10个网球”。
                十个网球的原则：不超过一个网球大小的肉；相当于两个网球大小的主食；保证三个网球大小的水果；不少于四个网球大小的蔬菜。<br />
                另外，专家建议市民在日常生活中做到“4个1”：每天吃1个鸡蛋，1斤牛奶，1小把坚果，1块扑克牌大小的豆腐，这样就做到了健康营养的摄入。</div>
                -->
               
            <div id='nutrition_evidence'>
               <div id='nutrition_evidence_left'>
               <div class='food_everyday'>
            	<span class='food_everyday_title'>
            		  每天食用八大类食物的量化建议：
            	</span>
            	<ul class='food_everyday_ul'>
            	   <li>   食物多样、谷类为主</li>
				     <li>    多吃蔬菜、水果和薯类</li>
				     <li>    常吃奶类、豆类或其制品</li>
				     <li>    经常吃适量鱼、禽、蛋、瘦肉，少吃肥肉和荤油</li>
				     <li>    食量与体力活动要平衡，保持适宜体重</li>
				  <li>       吃清淡少盐的膳食</li>
				  <li>       饮酒应限量</li>
				    <li>     吃清洁卫生、不变质的食物</li>
            	</ul>
            </div> 
                <!--  
               	<canvas id="canvas" height="450"
                    width="450"></canvas> <script>
						<?php $color = array("#F7464A","#46BFBD","#FDB45C","#949FB1","#4D5360","#F7464A","#DD5360","#8D5F60")?>
                        var doughnutData = [
                            <?php 
                            $key = 0;
                            foreach ($meal['energy_arr'] as $typename => $energy){?>
                            {
                                value: <?php echo $energy;?>,
                                color: <?php echo '"'.$color[$key++].'"';?>
                            },
                            <?php }?>

                        ];

                        var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);

                    </script>
                    -->
                    </div>
                    
                <div id='nutrition_evidence_right'>
                    <table id='nutrition_advice_table'>
                        <tr class='nutrition_advice_table_title'>
<!--                            <td class='nutrition_advice_td_first'></td>-->
                            <td class='nutrition_advice_table_td_mid'>食物类别</td>
                            <td>食用量</td>
                        </tr>

						<?php foreach ($meal['energy_arr'] as $typename => $energy){
                            if (!empty($typename) && $typename != null && isset($energy)){
                            	echo "<tr class='nutrition_advice_table_tr'>";
                            echo "<td class='nutrition_advice_table_td_mid'>";
                            	echo $typename;
                            echo "</td>";
                            echo "<td>";
                            	echo $energy;
                            echo "大卡</td>";
                        	echo "</tr>";
                            }
						}
                        ?>

                    </table>
                </div>
            </div>
            <!--营养方案专家建议_end  -->
            <div style='clear:both;'></div>
            <!--金字塔_start  -->
            
            <div class='paramid'>
            	<div style="left:25px;top:120px;position:relative">奶类及豆类<br/>奶制品每天<?=$meal['mass_arr']['奶类']?>毫升<br/>豆制品每天<?=$meal['mass_arr']['豆类']?>克</div>
            	<div style="left:25px;top:160px;position:relative">蔬菜类<br/>蔬菜类每天<?=$meal['mass_arr']['蔬菜类']?>克<br/> <br/>   </div>
            	<div style="left:25px;top:203px;position:relative">五谷类<br/>每天约<?=$meal['mass_arr']['谷物类']?>克</div>
            	<div style="left:534px;top:-144px;position:relative">油脂类<br/>每天不超过<?=$meal['mass_arr']['食用油类']?>克</div>
            	<div style="left:534px;top:-99px;position:relative">油脂类<br/>每天不超过<?=$meal['mass_arr']['肉食类']?>克</div>
            	<div style="left:534px;top:-30px;position:relative">水果类<br/>每天约<?=$meal['mass_arr']['水果类']?>克</div>
            </div>
            <!--金字塔_end  -->
            <!--
            <div class="advice" id='nutrition_attention'>
                <div class="advice_content">
                    核心提示：专家表示，“EB病毒阳性不一定是鼻咽癌，但EB病毒阴性也不一定不会得鼻咽癌。”慢性咽炎的EB病毒也是呈阳性，中科院流行病学曾经做过的一个调查，在3000例鼻咽癌患者中，其中只有80人EB病毒检测呈阳性，仅占了26.6%，其他都是阴性，所以EB病毒阳性并不是鼻咽癌一个必然因素。
                </div>
                <div class="advice_bottom_left"></div>
                <div class="advice_bottom_right"></div>
            </div>
            -->
        </div>
        <div class="view_bottom">
            <div class='view_bottom_right'><img
                    src="<?= Yii::app()->baseUrl ?>/images/front/heath_report/chuanqi_logo.png">
            </div>
        </div>

    </div>
    




