<?php
/* @var $this WebRegisterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Web Registers',
);

$this->menu=array(
	array('label'=>'Create WebRegister', 'url'=>array('create')),
	array('label'=>'Manage WebRegister', 'url'=>array('admin')),
);
?>

<h1>Web Registers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
