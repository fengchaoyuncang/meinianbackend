<?php
/* @var $this WebRegisterController */
/* @var $model WebRegister */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'web-register-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">请正确填写下面信息，我们将为您生成账号密码。带有 <span class="required">*</span>号的是必填项</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'用户名'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'性别'); ?>	
		<?php echo $form->radioButtonList($model,'gender',array('1'=>'男', '2'=>'女'), array('separator'=>'&nbsp;', 'labelOptions'=>array('style'=>'display:inline;width:150px;'), 'template'=>"{input} {label}")); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'邮箱'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'年龄'); ?>
		<?php echo $form->textField($model,'age'); ?>
		<?php echo $form->error($model,'age'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '注册' : 'Save', array( "disabled"=>"disabled")); ?>
	</div>

	<p class="note">公开注册暂时关闭，请联系美年大健康客服：4000988855</p>	
	
<?php $this->endWidget(); ?>

</div><!-- form -->