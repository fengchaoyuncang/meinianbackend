<?php
/* @var $this WebRegisterController */
/* @var $model WebRegister */

$this->breadcrumbs=array(
	'Web Registers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List WebRegister', 'url'=>array('index')),
	array('label'=>'Create WebRegister', 'url'=>array('create')),
	array('label'=>'Update WebRegister', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete WebRegister', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WebRegister', 'url'=>array('admin')),
);
?>

<h1>View WebRegister #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'gender',
		'email',
		'age',
	),
)); ?>
