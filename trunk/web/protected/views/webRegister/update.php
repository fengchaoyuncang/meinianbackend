<?php
/* @var $this WebRegisterController */
/* @var $model WebRegister */

$this->breadcrumbs=array(
	'Web Registers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List WebRegister', 'url'=>array('index')),
	array('label'=>'Create WebRegister', 'url'=>array('create')),
	array('label'=>'View WebRegister', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage WebRegister', 'url'=>array('admin')),
);
?>

<h1>Update WebRegister <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>