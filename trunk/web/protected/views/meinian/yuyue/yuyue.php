<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
<link rel="stylesheet" type="text/css" href="http://42.96.177.143:8080/assets/cfb259c2/jui/css/base/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/front/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/button.css" />
<style type="text/css">
/*<![CDATA[*/

.hide {
	display : none;
}

/*]]>*/
.control-label {
	float: left;
	width: 110px;
	padding-top: 5px;
	text-align: right;
}
.buttons{margin-left:300px;}
.note{margin-left:30px;margin-top:20px;}
.required{font-weight:bold;}
select{width:197px;}
input, textarea, .uneditable-input {
width: 180px;
}
.top{
	margin:15px 10px 0;text-align:center;
	
  }
.top a{
	font-size:20px;
	padding:3px 15px;
	background: -webkit-gradient(linear, 0 0, 0 100%, from(red), to(blue));
  	background: #e50065; /* for non-css3 browsers */


	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e50065', endColorstr='#003686'); /* for IE */
	background: -webkit-gradient(linear, left top, right bottom, from(#e50065), to(#003686)); /* for webkit browsers */
	background: -moz-linear-gradient(left, #e50065, #003686); /* for firefox 3.6+ */
	}
</style>
<script type="text/javascript" src="http://42.96.177.143:8080/assets/cfb259c2/jquery.min.js"></script>
<script type="text/javascript" src="http://42.96.177.143:8080/assets/cfb259c2/jui/js/jquery-ui.min.js"></script>
<title>在线预约</title>
</head>

<body>
<div data-role="page">
  <link rel="stylesheet" type="text/css" href="http://42.96.177.143:8080/css/form.css" />
  <img src="http://42.96.177.143:8080/images/header.jpg" width="100%" />
  <style >
.row {
    margin-top: 10px;
}
</style>

  <div class="">
    <div class='top'>
    <a href="http://www.youketu.com/admin3/mobile.php?act=module&id=10&name=research&do=research&weid=83" class="button button-pill button-highlight">
    	平安保险快速预约通道 >>>
    </a>
      
    </div>
      <div style="color:red"><?=$error?></div>
    <form id="reservation-form" method="post">
      <p class="note">带有<span class="required">*</span>号的域是必填项。</p>
      <div class="row" > </div>
      <div class="row">
        <label class="control-label" for="ReservationForm_location">体检中心*</label>
        <select name="User[examcenter]" id="User[examcenter]">
			<option value="太阳宫分院">太阳宫分院</option>
			<option value="东四分院（美年中医医院）">东四分院（美年中医医院）</option>
			<option value="宣武区分院">宣武区分院</option>
			<option value="佳境分院">佳境分院</option>
			<option value="大望路分院">大望路分院</option>
			<option value="绿生源分院">绿生源分院</option>
        </select>

      </div>
      <div class="row">
        <label class="control-label"  for="ReservationForm_location">体检套餐*</label>
        <input type="text" name="User[tjpackage]"/ class="required">
        <!-- 
           <select name="User[tjpackage]" class="bs-docs-example" id="User[tjpackage]">
				<option value="基础型体检套餐">基础型体检套餐</option>
				<option value="标准型体检套餐">标准型体检套餐</option>
				<option value="经典型体检套餐">经典型体检套餐</option>
				<option value="温情体检套餐">温情体检套餐</option>
				<option value="亲情体检套餐">亲情体检套餐</option>
				<option value="豪华体检套餐">豪华体检套餐</option>
				<option value="尊享体检套餐">尊享体检套餐</option>
        	</select>
 -->
      </div>

      <div class="row">
        <label class="control-label"  for="ReservationForm_name" class="required">姓名 <span class="required">*</span></label>
        <input class="input" type='text' name='User[realname]'>
      </div>
      <div class="row">
        <label class="control-label"  for="ReservationForm_date">体检时间</label>
        <input class="date input" type='text' name='User[booktime]'>
      </div>
      <div class="row">
        <label class="control-label"  for="ReservationForm_idno">身份证号*</label>
        <input class="input" type='text' name='User[identity_no]'>
      </div>
      <div class="row">
        <label class="control-label"  for="ReservationForm_phone" class="required">手机号<span class="required">*</span></label>
        <input class="input" type='text' name='User[phone]'>
      </div>
      
      <div class="row">
        <label class="control-label"  for="ReservationForm_card">体检卡号</label>
        <input class="input" type='text' name='User[examno]'>
      </div>
      <div class="row hide">
        <label class="control-label"  for="ReservationForm_created_time">申请时间</label>
        <input class="input" name="ReservationForm[created_time]" id="ReservationForm_created_time" type="text" />
      </div>
      <div class="row hide">
        <label class="control-label" for="ReservationForm_status">状态</label>
        <select name="ReservationForm[status]" id="ReservationForm_status">
          <option value="处理中">处理中</option>
          <option value="成功">成功</option>
          <option value="失败">失败</option>
        </select>
      </div>
      <div class="row">
        <input class="btn btn-large btn-block btn-primary" type="submit" name="yt0" value="预定" />
      </div>
    
    </form>
  </div>
  <!-- form --> 
  
</div>
<!-- page -->
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
	//日历插件时间格式
	(function($) {
	$(function() {
	$(".date").datepicker({
	    "dateFormat" : "yy-mm-dd"
	});
	});
	})(jQuery);

	
	//表单验证
//	$("#reservation-form").submit(function(){
//		alert("hello");
//		if($(".input").val()==""){
//			alert("你好");
//				$("input[name='User[realname]']").css("border":"solid 1px #ff0000");
//				return false;
//			}
//	});

	$("form").submit(function(){
		//验证体检套餐非空
		if($("input[name='User[tjpackage]']").val()==""){
			$("input[name='User[tjpackage]']").css("border","solid 1px #ff0000");
			return false;
			}
		
		//验证姓名非空
		if($("input[name='User[realname]']").val()==""){
			$("input[name='User[realname]']").css("border","solid 1px #ff0000");
			return false;
		}
		
		
		//验证身份证号非空
		if($("input[name='User[examno]']").val()==""){
			$("input[name='User[examno]']").css("border","solid 1px #ff0000");
			return false;
			}
		
		//验证手机号非空
		if($("input[name='User[phone]']").val()==""){
			$("input[name='User[phone]']").css("border","solid 1px #ff0000");
			return false;
			}
	});


});
/*]]>*/
</script>
</body>
</html>
