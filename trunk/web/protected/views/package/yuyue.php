<script src='<?= Yii::app()->baseUrl ?>/js/jquery/cookie/jquery.cookie.js'></script>
<script src='<?= Yii::app()->baseUrl ?>/js/jquery/datapicker/glDatePicker.min.js'></script>
<link rel="stylesheet" href="<?= Yii::app()->baseUrl ?>/css/datepicker/glDatePicker.default.css" type="text/css" media="screen">
<script>
    function tianchong(data) {
        if (data["status"] != 0) {
            alert("数据错误");
            return;
        }
        var value = data["data"];
        $("#bookexam span[name='gongsiname'").val(value["company_name"]);
        $("#bookexam span[name='realname'").val(value["company_name"]);
        $("#bookexam span[name='gongsiname'").val(value["company_name"]);
        $("#bookexam span[name='gongsiname'").val(value["company_name"]);
        $("#bookexam span[name='gongsiname'").val(value["company_name"]);
        $("#bookexam span[name='gongsiname'").val(value["company_name"]);
    }
    function submityuyue() {
        $.post("<?= $this->createUrl('package/yuyue') ?>", $("#bookexam").serialize(), function(data) {
            if (data["status"] == 0) {
                location.href = "index.php?r=package/reservedlist";
            } else {
                alert("预约失败" + data["data"]);
            }
        }, "json");
    }
    $(function() {
        //日期选择控件
        $("input[name='appoint[date]']").glDatePicker(
                {
                    onClick: function(target, cell, date, data) {
                        target.val(date.getFullYear() + '-' +
                                (date.getMonth() + 1) + '-' +
                                date.getDate());

                        if (data != null) {
                            alert(data.message + '\n' + date);
                        }
                    }
                });
       //控制文本框文字的显示和消失
       $(":text").focus(function(){
           this.value="";
       });
    });
    function appear(){
        if (document.getElementById('date').value == ""){
        	document.getElementById('date').value = "选择时间";
        }
    }
    function reservelist(){
        location.href="<?=Yii::app()->createUrl("package/reservedlist")?>";
    }
</script>

<div id="package_main">
    <form id="bookexam"  method="post">
    <div id="package_main_top">
        <input type="hidden" name="uid" value="<?= $userinfo["userid"] ?>">
        <ul>
            <li>姓名：<?= $userinfo["name"] ?></li>
            <li>公司名称：<?= $userpackage["company_name"] ?></li>
            <li>体检套餐：<?= $userpackage["packages"][0]["name"] ?></li>
            <li>预约类型：<?= $userinfo["idflag"] == 1 ? "企业用户" : "普通用户" ?></li>
        </ul>            
    </div>
    <div id="package_main_bottom">
        <dl>
            <dt>
            <select class='input_select' name='appoint[subCenterID]'>
                <option  value="">预约地点</option>
                <?php
                if (isset($userpackage["packages"][0]["centers"])) {
                    foreach ($userpackage["packages"][0]["centers"] as $center) {                        
                        echo "<option  class='input_select_option' value=".$center["centerID"].">".$center["name"]."&nbsp".$center["address"]."</option>";                        
                    }
                }
                ?>
            </select>  
            </dt>
            <dt>
            <input id="date" class='input-text' name="appoint[date]" value="选择时间" type="text" onblur="appear();"></input>
            </dt>
            <dt>
            <input class='yuyue_input-submit' type="button" value="提交预约" onclick="submityuyue();" <?= $userpackage["packages"] ? "" : "disabled" ?>>
            <input class='yuyue_input-submit' type="button" value="预约记录" onclick="reservelist();" <?= $userpackage["packages"] ? "" : "disabled" ?>>
            </dt>
        </dl>
    </div>
        </form>
</div>   
