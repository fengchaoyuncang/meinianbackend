<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
        <meta content="no" name="apple-mobile-web-app-capable" />
        <meta content="black" name="apple-mobile-web-app-status-bar-style" />
        <meta content="telephone=no" name="format-detection" />
        <title>出错啦</title>
        <style>
            .error{width:320px; height:350px; line-height:30px; text-align:center;position:absolute;left:50%;top:50%; margin:-156px  auto auto -160px;}
            .error img{width: 85px;height: 85px;}
        </style>
    </head>
    <body>
        <div class="error">
            <div class="img"><img src="images/app/error.png"></div>
            <div>您的网络环境运行不正常</div>
            <div>请稍后再试!</div>
        </div>
    </body>
</html>