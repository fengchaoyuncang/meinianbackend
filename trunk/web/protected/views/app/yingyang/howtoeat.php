<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>怎么吃</title>
<style type="text/css">
body{margin:0px;background:#eee;font-size:14px; color:#3a3a3a;}
a,a:link,a:active,a:visited,a:hover{-webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;}
.tu{ width:100%;}
.tit{ height:35px; line-height:35px; text-align:center; font-size:16px;}
.ul{ padding:5px 0px 0px 0px; background:#FFF;width:100%; overflow:auto;}
.ul p{padding:0px; line-height:30px; text-align:center;float:left;width:33%;}
.ul p img{ display:block; margin:0px auto; width:90%;}
.cont{ line-height:25px; padding:10px 15px;background:#FFF;}
.cont1{padding:10px 0px;background:#FFF;}
.cont2{padding:10px 15px;background:#FFF; line-height:25px;}
.cont2 p{ padding:5px 0px; text-indent:20px; background:url(images/app/tj_08.png) no-repeat 0px 9px;background-size:14px;}
.cont3{padding:10px 15px;background:#FFF; line-height:25px;}
.cont3 p{ padding:5px 0px; text-indent:20px; background:url(images/app/tj_09.png) no-repeat 0px 9px;background-size:14px;}
.tit{ height:35px; line-height:35px; text-align:center; font-size:16px;}
.img_div{position: relative;font-size: 12px; background:url(images/app/ystlyz_img.jpg) no-repeat;background-size:315px; width:310px; height:200px; margin:0px auto; overflow:hidden;}
.img_div div {position: absolute; line-height:14px; width:190px;}
.center{ text-align:center; line-height:30px;background:#FFF;}
.tid{color:#00aeff;height: 30px;background-color: #FFF;text-align: center;}
.tit1{background:url(images/app/tj_26.png) no-repeat 10px 7px #FFF;background-size:18px;height:35px; line-height:35px;padding-left:30px; color:#000;}
.tit2{background:url(images/app/tj_26.png) no-repeat 10px -25px #FFF;background-size:18px;height:35px; line-height:35px;padding-left:30px; color:#000;}
.tit3{background:url(images/app/tj_26.png) no-repeat 10px -56px #FFF;background-size:18px;height:35px; line-height:35px;padding-left:30px; color:#000;}
.tit4{background:url(images/app/tj_26.png) no-repeat 10px -88px #FFF;background-size:18px;height:35px; line-height:35px;padding-left:30px; color:#000;}
.table,.table td{border-collapse:collapse;border:solid 1px #e3e4e8;font-size:14px;line-height:20px;margin:0px auto;}
.table{width:100%;}
.table td{ padding:4px; font-size:12px;}
.table img{ display:block; margin:8px auto; width:80%;}
.p{margin:0px; background:#FFF; padding:0px 10px; line-height:32px;}
.p span{width:8px; height:8px; display:inline-block; background:#ff7070;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px; overflow:hidden; margin:0px 5px;}
.p b{width:8px; height:8px; display:inline-block; background:#ccebfb;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px; overflow:hidden; margin:0px 5px;}
.cont0{padding:0px 15px;background:#FFF;overflow:hidden;position:relative;border-bottom:solid 1px #e3e4e8; height:0px;-moz-transition: height 0.6s ease-in;
	-webkit-transition: height 0.6s ease-in;
	-o-transition: height 0.6s ease-in;
	transition: height 0.6s ease-in;
}

</style>

</head>

<body>
<div class="tit">防病饮茶</div>
<div class="ul">
    <p><a href="<?=$this->createUrl("app/yingyang/tea",array("name"=>"wulong"))?>"><img src="images/app/tu01.jpg">乌龙茶</a></p>
    <p><a href="<?=$this->createUrl("app/yingyang/tea",array("name"=>"lvca"))?>"><img src="images/app/tu02.jpg">绿茶</a></p>
    <p><a href="<?=$this->createUrl("app/yingyang/tea",array("name"=>"hongca"))?>"><img src="images/app/tu03.jpg">红茶</a></p> 
</div>
<?php
if($result === false)
{
    echo "<div class=tid>此为样例方案，登陆后可以查看更详细方案</div>";
}
?>
<div class="tit">饮食建议</div>
<div class="cont">我们在您的健康信息和健康体检数据的基础上，充分考虑了到您的性别、年龄、生活习惯和疾病史状况，给出的综合建议：</div>

<div class="tit1">饮食调理原则</div>
<div class="cont1">
    <div class="img_div">
        <div style="left:5px;top:75px">奶制品100克<br/>豆制品50克</div>
        <div style="left:5px;top:125px">蔬菜<br/>400-500克</div>
        <div style="left:5px;top:168px">五谷<br/>300-500克</div>
        <div style="left:225px;top:20px">油脂<br/>不超过25克</div>
        <div style="left:225px;top:65px">鱼禽肉蛋<br/>125-200克</div>
        <div style="left:225px;top:110px">水果<br/>100-200克</div>
    </div>
</div>

<div class="tit2">根据您的体重指数体质偏胖的膳食参考</div>
<?php 
$min = floor($meal["total"]/100)*100;
?>
<div class="center"><?=$min?>-<?=$min+100?>千卡食谱</div>
    <div class="cont">
    <table class="table">
      <tr>
        <td height="20" align="center">餐次</td>
        <td width="25%" align="center">食物名称</td>
        <td width="25%" align="center">食物重量</td>
        <td width="25%" align="center">食物能量<br>单位:卡</td>
      </tr>
      <?php foreach($meal["meal"] as $key=>$me):
          $food = array();
          $nl = array();
          $zl = array();
            foreach ($me as $k=>$it) {
                array_push($food,$k);
                array_push($zl,$it["mass"]);
                array_push($nl,floor($it["energy"]));
            }
          ?>
       <tr>
        <td height="20" align="center"><?= $key?></td>
        <td width="25%" align="center"><?=  implode("<br>", $food)?></td>
        <td width="25%" align="center"><?=  implode("<br>", $zl)?></td>
        <td width="25%" align="center"><?=  implode("<br>", $nl)?></td>
      </tr>
      <?php endforeach;?>
      
      
    </table>
    </div>
<?php 
if(is_array($result)):
    
foreach($result as $key=>$items):?>
<div name="food" class="tit3" onclick="ann(this)">根据您<?=$key?>风险较高，您宜食食物是<span></span></div>
	<div class="cont0">
    <table class="table">
        <?php  
            $count = floor(count($items)/3);
            for($i = 0;$i<$count;$i++):
            ?>
               <tr>
        <td width="33%" align="center"><img src="<?=$items[$i*3]["image_url"]?>"><?=$items[$i*3]["name"]?></td>
        <td width="33%" align="center"><img src="<?=$items[$i*3+1]["image_url"]?>"><?=$items[$i*3+1]["name"]?></td>
        <td width="33%" align="center"><img src="<?=$items[$i*3+2]["image_url"]?>"><?=$items[$i*3+2]["name"]?></td>
      </tr> 
                
       <?php endfor;?>
       
    </table>
    </div>
<?php endforeach;?>
<?php endif;?>
<div class="tit4">饮食注意</div>
    <?php
    if(isset($forbid["unfood"])&&isset($forbid["forbidfood"])&&is_array($forbid["unfood"])&&is_array($forbid["forbidfood"])):
                foreach ($forbid["unfood"] as $it):?>
            <p class="p"><span></span><?=$it?></p>
        <?php        endforeach;?>
    <?php
                foreach ($forbid["forbidfood"] as $it):?>
            <p class="p"><b></b><?=$it?></p>
        <?php        endforeach;?>
        <?php        endif;?>
</body>
<script language="javascript">
function getNextElement(node){    
    if(node.nextSibling.nodeType == 1){ 
        return node.nextSibling;    
    }    
    if(node.nextSibling.nodeType == 3){ 
        return getNextElement(node.nextSibling);    
    }    
    return null;
}
function ann(an){
	if(an.className=="tit3"){
		var h = getNextElement(an).getElementsByTagName("table")[0].offsetHeight;
		an.getElementsByTagName("span")[0].className="span";
		an.className="tit3 tit30";
		getNextElement(an).style.height=h+20+"px";
	}else{
		an.getElementsByTagName("span")[0].className="";
		an.className="tit3";
		getNextElement(an).style.height="0px";
	}
}
window.onload=function(){
		var aa = window.document.getElementsByName("food")[0];
		var h = getNextElement(aa).getElementsByTagName("table")[0].offsetHeight;
                      
		getNextElement(aa).style.height=h+20+"px";
		aa.className="tit3 tit30";
};

</script>
</html>
