<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>红茶</title>
<style type="text/css">
body{margin:0px;background:#eee;font-size:14px; color:#3a3a3a;}  
.tu{ width:100%;}
.tit{ height:30px; line-height:30px; margin:5px 20px; padding:0px 23px;}
.tit1{ color:#ff3e3e; background:url(images/app/tj_07.png) no-repeat left 3px;background-size: 20px;}
.tit2{ color:#ffc600; background:url(images/app/tj_07.png) no-repeat left -25px;background-size: 20px;}
.tit3{ color:#00e19f; background:url(images/app/tj_07.png) no-repeat left -56px;background-size: 20px;}
.tit4{ color:#009cff; background:url(images/app/tj_07.png) no-repeat left -85px;background-size: 20px;}
.tit5{ color:#7d36ff; background:url(images/app/tj_07.png) no-repeat left -114px;background-size: 20px;}
.tit6{ color:#ff5abf; background:url(images/app/tj_07.png) no-repeat left -144px;background-size: 20px;}
.cont{ line-height:25px;margin:0px 20px;}
</style>

</head>

<body>
<img src="images/app/hongca.jpg" class="tu">
<div class="tit tit1">红茶的别名</div>
<div class="cont">祁红、滇红</div>
<div class="tit tit2">注解</div>
<div class="cont">红茶属全发酵茶，是以适宜的茶树新牙叶为原料，经萎凋、揉捻（切）、发酵、干燥等一系列工艺过程精制而成的茶。萎凋是红茶初制的重要工艺，红茶在初制时称为“乌茶”。红茶因其干茶冲泡后的茶汤和叶底色呈红色而得名。中国红茶品种主要有：祁红、霍红、滇红、越红、苏红、川红、吴红等，尤以祁门红茶最为著名。</div>
<div class="tit tit3">营养成分</div>
<div class="cont">红茶富含胡萝卜素、维生素A、钙、磷、镁、钾、咖啡碱、异亮氨酸、亮氨酸、赖氨酸、谷氨酸、丙氨酸、天门冬氨酸等多种营养元素。红茶在发酵过程中多酚类物质的化学反应使鲜叶中的化学成分变化较大，会产生茶黄素、茶红素等成分，其香气比鲜叶明显增加，形成红茶特有的色、香、味。</div>
<div class="tit tit4">药性功效</div>
<div class="cont">红茶具有暖胃养生、提神益思、消除疲劳、消除水肿、止泻、抗菌、增强免疫等功效。
    红茶有助于胃肠消化，能促进食欲，可有效防治心肌梗死、强壮心肌的功能、降低血糖值与高血压、预防蛀牙与食物中毒等。
</div>
<div class="tit tit5">宜食</div>
<div class="cont">红茶偏温，老少皆宜，尤适合胃寒之人饮用。冬季宜多饮红茶。</div>
<div class="tit tit6">忌食</div>
<div class="cont">夏季应少饮。</div>
</body>
</html>
