<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>绿茶</title>
<style type="text/css">
body{margin:0px;background:#eee;font-size:14px; color:#3a3a3a;}
.tu{ width:100%;}
.tit{ height:30px; line-height:30px; margin:5px 20px; padding:0px 23px;}
.tit1{ color:#ff3e3e; background:url(images/app/tj_07.png) no-repeat left 3px;background-size: 20px;}
.tit2{ color:#ffc600; background:url(images/app/tj_07.png) no-repeat left -25px;background-size: 20px;}
.tit3{ color:#00e19f; background:url(images/app/tj_07.png) no-repeat left -56px;background-size: 20px;}
.tit4{ color:#009cff; background:url(images/app/tj_07.png) no-repeat left -85px;background-size: 20px;}
.tit5{ color:#7d36ff; background:url(images/app/tj_07.png) no-repeat left -114px;background-size: 20px;}
.tit6{ color:#ff5abf; background:url(images/app/tj_07.png) no-repeat left -144px;background-size: 20px;}
.cont{ line-height:25px;margin:0px 20px;}
</style>

</head>

<body>
<img src="images/app/lvca.jpg" class="tu">
<div class="tit tit1">绿茶的别名</div>
<div class="cont">苦茗</div>
<div class="tit tit2">注解</div>
<div class="cont">绿茶属不发酵茶，以适宜茶树新梢为原料，经杀青、揉捻、干燥等一系列工艺制作而成。绿茶因其干茶呈绿色、冲泡后的茶汤呈碧绿、叶底呈翠绿色而得名。绿茶一般分为炒青、烘青、晒青和蒸青4种，以形美、色翠、清香、味和四绝著称。绿茶以将鲜叶内的天然物质较多的保留为其特性，其茶叶中的茶多酚、咖啡碱就保留了鲜叶的85%以上，叶绿素保留了50%左右，维生素损失也较其它茶要少。有名的绿茶品种有西湖龙井、黄山毛峰、洞庭碧螺春等。</div>
<div class="tit tit3">营养成分</div>
<div class="cont">绿茶中富含叶绿素、儿茶素、茶氨酸、咖啡碱、茶多酚、维生素A、维生素Ｃ、胡萝卜素、钙、磷、钾、镁、氟等成分，儿茶素有较强的抗自由基作用，可防治癌症。
</div>
<div class="tit tit4">药性功效</div>
<div class="cont">绿茶性凉、味甘，常饮绿茶可消脂去腻、清热解毒、利尿排毒、坚固牙齿、提神醒脑、强心抗癌、减肥健美，可增强肾脏和肝脏的功能、防止恶性贫血和胆固醇增高，对肝炎、肾炎、白血病等具有辅助功效。此外，常饮绿茶还能增强辩色力，防治夜盲症。
</div>
<div class="tit tit5">宜食</div>
<div class="cont">高脂血症、糖尿病、高血压、白血病、贫血、冠心病、肝炎、肾炎、肠炎、腹泻、夜盲症、嗜睡症、动脉粥样硬化、心动过缓、肥胖症及人体各部位的癌症等症患者宜食；长期吸烟饮酒过多，发热口渴、头痛目昏、小便不利及进油腻饮食或奶类食品过多者也宜食。饮茶应以饮清淡温热为宜，热茶进入胃中可促进胃液分泌，有助于对食物的消化。</div>
<div class="tit tit6">忌食</div>
<div class="cont">失眠、胃寒、孕妇及产妇在哺乳期者忌饮绿茶；临睡前忌饮浓茶；忌用茶水送服土茯苓、威灵仙、人参、西洋参、安眠药和含铁质补血的药。此外，饮茶应忌多、忌浓、忌冷、忌隔夜。</div>
</body>
</html>
