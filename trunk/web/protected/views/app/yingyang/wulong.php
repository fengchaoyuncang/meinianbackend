<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>乌龙茶</title>
<style type="text/css">
body{margin:0px;background:#eee;font-size:14px; color:#3a3a3a;}
.tu{ width:100%;}
.tit{ height:30px; line-height:30px; margin:5px 20px; padding:0px 23px;}
.tit1{ color:#ff3e3e; background:url(images/app/tj_07.png) no-repeat left 3px;background-size: 20px;}
.tit2{ color:#ffc600; background:url(images/app/tj_07.png) no-repeat left -25px;background-size: 20px;}
.tit3{ color:#00e19f; background:url(images/app/tj_07.png) no-repeat left -56px;background-size: 20px;}
.tit4{ color:#009cff; background:url(images/app/tj_07.png) no-repeat left -85px;background-size: 20px;}
.tit5{ color:#7d36ff; background:url(images/app/tj_07.png) no-repeat left -114px;background-size: 20px;}
.tit6{ color:#ff5abf; background:url(images/app/tj_07.png) no-repeat left -144px;background-size: 20px;}
.cont{ line-height:25px;margin:0px 20px;}
</style>

</head>

<body>
<img src="images/app/wulong.jpg" class="tu">
<div class="tit tit1">乌龙茶的别名</div>
<div class="cont">青茶、半发酵茶</div>
<div class="tit tit2">注解</div>
<div class="cont">乌龙茶属于半发酵茶，制茶时，经轻度萎调和局部发酵，然后采用绿茶的制作方法，进行高温杀青，使茶叶形成“七分绿，三分红”，有“绿叶红镶边”的独特之处，其色、香、味兼有红绿花的特点。</div>
<div class="tit tit3">营养成分</div>
<div class="cont">茶叶是富含维生素K的饮品，而且还含维生素C等成分，具有抗血小板凝集、促进膳食纤维溶解、降血压、降血脂的作用，对防治心血管疾病十分有利。</div>
<div class="tit tit4">药性功效</div>
<div class="cont">茶中含有氟、茶多酚等成分，饮茶能防龋固齿。
 
茶中维生素A、维生素E含量丰富，并含有多种抗癌防衰的微量元素。它是天然的健美饮料，有助于保持皮肤光洁白嫩，减少皱纹，还能抗氧化、防辐射、提高免疫力、预防肿瘤。
 
茶叶还具有提神醒脑、振奋精神、增强免疫、消除疲劳等作用。乌龙茶祛脂减力强。</div>
<div class="tit tit5">宜食</div>
<div class="cont">适宜高血压、高血脂、冠心病、动脉硬化、糖尿病、油腻食品食用过多、醉酒者。</div>
<div class="tit tit6">忌食</div>
<div class="cont">不适宜发热、肾功能不良、心血管疾病、习惯性便秘、消化道溃疡、神经衰弱、失眠、孕妇、哺乳期妇女、儿童</div>
</body>
</html>
