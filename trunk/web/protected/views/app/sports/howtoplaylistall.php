<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>推荐运动</title>
<style type="text/css">
body{margin:0px;background:#fff;font-size:14px; color:#3a3a3a;}
.tit{height:35px; line-height:35px; text-align:center; font-size:16px;min-width:315px;}
.cont{padding:10px 10px 10px 10px; background:#FFF; min-width:295px; line-height:20px;}
.tt01 .l{ float:left; padding-left:10px;}
.tt01 .r{ float:right; color:#01aeff; text-decoration:none; padding-right:10px;}
.tt01{ border:1px #eee solid; background:#f6f8fa; height:35px; line-height:35px;}
.tt02{border:1px #eee solid; border-top:0px; height:140px;}
.tt02 .l .b1{color:#6be190;}
.tt02 .l .b2{color:#00cdec;}
.tt02 .l .b3{color:#ffc0c8;}
.tt02 .r b{font-weight: normal;display: block;padding: 0px 0px 5px 0px;}
.tt02 .l{float:left; width:150px; padding:5px 0px 0px 10px; line-height:25px;}
.tt02 .r{float:left; width:110px; padding:20px 0px 0px 0px;line-height:18px;}
.tt02 .r span{ display:inline-block;width:11px; height:11px; background:url(images/app/tj_15.png) no-repeat;background-size:11px; margin:0px auto auto 2px; overflow:hidden;}
a{-webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;}

</style>

</head>

<body>
<div class="cont">
    <div class="tt01"><span class="l">推荐运动</span></div>
<?php
    foreach($items as $key=>$it)
    {
    ?>
    <a href="<?=$this->createUrl("play",array("sportid"=>$it["id"]))?>">
    <div class="tt02">
   	  	<div class="l">	
                    <b class="b<?=$it["type"]?>"><?=$it['typename']?></b> <br><?=$it['title']?><br>
       		<img src="<?=$it["image_url"]?>" width="105">
        </div>
        <div class="r">
            <b>难度指数</b>
            反应<?php
            for($i=0;$i<$it["fanying"];$i++){
                echo "<span></span>";
            }
            ?><br>
            力量<?php
            for($i=0;$i<$it["liliang"];$i++){
                echo "<span></span>";
            }
            ?><br>
            耐力<?php
            for($i=0;$i<$it["naili"];$i++){
                echo "<span></span>";
            }
            ?><br>
            柔韧<?php
            for($i=0;$i<$it["rouren"];$i++){
                echo "<span></span>";
            }
            ?><br>
            平衡<?php
            for($i=0;$i<$it["pingheng"];$i++){
                echo "<span></span>";
            }
            ?><br>
        </div>
    </div>
</a>
    <?php
    }
    ?>
    
</div>

</body>
</html>
