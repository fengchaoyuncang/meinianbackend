<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title><?=$item["title"]?></title>
<style type="text/css">
body{margin:0px;background:#fff;font-size:14px; color:#3a3a3a;}
.tit{height:35px; line-height:35px; text-align:center; font-size:16px;min-width:315px;}
.cont{padding:0px 10px 10px 10px;min-width:295px; line-height:20px;}

.tt02 .l .b1{color:#6be190;}
.tt02 .l .b2{color:#00cdec;}
.tt02 .l .b3{color:#ffc0c8;}
.tt02{height:150px;}
.tt02 .l{float:left; width:150px; padding:15px 0px 0px 10px; line-height:25px;font-size:16px;}
.tt02 .r{float:left; width:100px; padding:25px 0px 0px 0px;line-height:18px;}
.tt02 .r span{  display:inline-block;width:11px; height:11px; background:url(images/app/tj_15.png) no-repeat;background-size:11px; margin:0px auto auto 2px; overflow:hidden;}
.tt02 .r b{display:block;font-weight:normal; display:block;padding:0px 0px 5px 0px;}
.cont strong{ display:block; margin:10px;}
.ifr{display: block; margin:6px auto 0; width: 100%; height:240px;}
#video-wrapper {
  display: block; 
  width: 100%;
  height:240px;
}
.v{
  padding:15px 0 0 0;
}
a{-webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;}
body {
  margin: 0;
  padding: 0;
}
#video-title {
  font-size: 1.2em;
  padding-left: 16px;
}
#video-switch {
  margin: 10px;
  padding-bottom: 20px;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;
}
.choose-video {
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 22px;
  text-align: center;
  margin-right: 10px;
  margin-bottom: 10px;
  display: block;
  float: left;
  background: #e2e2e2;
  cursor: pointer;
}
.chosen {
  background: #3498db;
  color: #f6f6f6;
}
</style>
<script src="<?=Yii::app()->baseUrl?>/js/jquery/jqueryCut.js"></script>
</head>

<body>
<div class="cont">
  
  <div class="tt02">
  <div class="l"> 
      <b class="b<?=$item["type"]?>"><?=$item['typename']?></b> <br><?=$item['title']?><br>
      <img src="<?=$item["image_url"]?>" width="105">
    </div>
    <div class="r">
      <b>难度指数</b>
      反应<?php
      for($i=0;$i<$item["fanying"];$i++){
        echo "<span></span>";
      }
      ?><br>
      力量<?php
      for($i=0;$i<$item["liliang"];$i++){
        echo "<span></span>";
      }
      ?><br>
      耐力<?php
      for($i=0;$i<$item["naili"];$i++){
        echo "<span></span>";
      }
      ?><br>
      柔韧<?php
      for($i=0;$i<$item["rouren"];$i++){
        echo "<span></span>";
      }
      ?><br>
      平衡<?php
      for($i=0;$i<$item["pingheng"];$i++){
        echo "<span></span>";
      }
      ?><br>
    </div>
  </div>
  
  <div>
    <?=$item['desc']?>
  </div>
  
</div>
<div class="v">
  <strong id="video-title"></strong>

  <div id="video-content" class="ifr"></div>
  <script type="text/javascript" src="http://player.youku.com/jsapi"></script>

</div>
<div id="video-switch">
  <?php 
  foreach ($item["video"] as $key=>$it)
  {
  ?>
  <div class="choose-video" data-video-title="<?=$it["title"]?>" data-video-add="<?=$it["url"]?>"><?=$key + 1?></div>
  <?php
  }?>
  <div style="clear: both"></div>
</div>
<script>
  var oVideoSwitch = document.getElementById('video-switch');
  var oVideoTitle = document.getElementById('video-title');
  var oVideoContent = document.getElementById('video-content');
  oVideoSwitch.addEventListener('click', function (event) {
    if(event.target.nodeName == 'DIV') { 
      oVideoTitle.innerHTML = event.target.getAttribute('data-video-title');
    }
  }, false)
  window.onload = function () {
    if(1 === oVideoSwitch.firstChild.nodeType) {
      oVideoTitle.innerHTML = oVideoSwitch.firstChild.getAttribute('data-video-title');
    } else {
      oVideoTitle.innerHTML = oVideoSwitch.firstChild.nextSibling.getAttribute('data-video-title');
    }
  }
  $(function () {
    var str = $('#video-switch > div:first').attr('data-video-add');
    player = new YKU.Player('video-content',{
      client_id: '3da261c686d6bd32',
      vid: str.substr(30)
    });
    $('#video-wrapper').html(str);
    $('#video-switch > div:first').addClass('chosen');
    $('#video-switch').click(function (event) {
      if(event.target.nodeName == 'DIV') {
        $('#video-switch > div').removeClass('chosen');
        $(event.target).addClass('chosen');
        str = $(event.target).attr('data-video-add');
        player = new YKU.Player('video-content',{
          client_id: '3da261c686d6bd32',
          vid: str.substr(30)
        });
      }
    });
  });
</script>
</body>
</html>
