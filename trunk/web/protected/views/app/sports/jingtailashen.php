<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title><?=$item["title"]?></title>
<style type="text/css">
body{margin:0px;background:#fff;font-size:14px; color:#3a3a3a;}
.tit{height:35px; line-height:35px; text-align:center; font-size:16px;min-width:315px;}
.cont{padding:0px 10px 10px 10px;min-width:295px; line-height:20px;}

.tt02 .l .b1{color:#6be190;}
.tt02 .l .b2{color:#00cdec;}
.tt02 .l .b3{color:#ffc0c8;}
.tt02{height:150px;}
.tt02 .l{float:left; width:150px; padding:15px 0px 0px 10px; line-height:25px;font-size:16px;}
.tt02 .r{float:left; width:100px; padding:25px 0px 0px 0px;line-height:18px;}
.tt02 .r span{  display:inline-block;width:11px; height:11px; background:url(images/app/tj_15.png) no-repeat;background-size:11px; margin:0px auto auto 2px; overflow:hidden;}
.tt02 .r b{display:block;font-weight:normal; display:block;padding:0px 0px 5px 0px;}
.cont strong{ display:block; margin:10px;}
.ifr{display:block; margin:5px auto; height:240px;padding:5px 0px 5px 0px;}
.title{font-weight: bolder;margin:12px 0 12px 0}
.wzti{padding:0 0 0 10px;background: url(images/app/jt/tubiao.png) no-repeat left 8px;background-size: 6px;}
.imgwz img{width:100%;}
.img{width: 160px;margin: 10px auto;}
</style>


</head>

<body>
<div class="cont">
    
    <div class="tt02">
   	<div class="l">	
        	<b class="b<?=$item["type"]?>"><?=$item['typename']?></b> <br><?=$item['title']?><br>
       		<img src="<?=$item["image_url"]?>" width="105">
        </div>
        <div class="r">
            <b>难度指数</b>
            反应<?php
            for($i=0;$i<$item["fanying"];$i++){
                echo "<span></span>";
            }
            ?><br>
            力量<?php
            for($i=0;$i<$item["liliang"];$i++){
                echo "<span></span>";
            }
            ?><br>
            耐力<?php
            for($i=0;$i<$item["naili"];$i++){
                echo "<span></span>";
            }
            ?><br>
            柔韧<?php
            for($i=0;$i<$item["rouren"];$i++){
                echo "<span></span>";
            }
            ?><br>
            平衡<?php
            for($i=0;$i<$item["pingheng"];$i++){
                echo "<span></span>";
            }
            ?><br>
        </div>
    </div>
    <div class="title">
        OL提神+瘦身 6分钟释放能量操
    </div>
    <div class="imgwz">
        
            <div class='wzti'>全身舒展运动：</div>
                    <div class ="wz">
身体直立，双腿分开比肩稍宽，两手手指交叉。先掌心向下，伸直双臂，将掌心朝下压，维持20秒；然后将双臂举至头顶，变掌心向上，朝上伸展双臂，维持20秒。
        </div>
            <div class="img">
        <img src="images/app/jt/tu_jt_01.jpg"></div>    
    </div>
    <div class="imgwz">
        
            <div class='wzti'>上臂拉伸运动：</div>
                    <div class ="wz">
双臂上举，手心相对，然后以肘部为中心弯曲。首先右手手掌扶住左手肘部，往右方拉伸，维持20秒；然后换左手手掌扶住右手肘部，往左方拉伸，同样维持20秒。</div>
            <div class="img">
        <img src="images/app/jt/tu_jt_02.jpg"></div>    
    </div>
    <div class="imgwz">
        
            <div class='wzti'>体侧伸展运动：</div>
                    <div class ="wz">
右手叉腰，左手向上伸，然后身体向右侧弯曲，维持20秒后换边。</div>
            <div class="img">
        <img src="images/app/jt/tu_jt_03.jpg"></div>    
    </div>
    <div class="imgwz">
        
            <div class='wzti'>胸肩扩展运动：</div>
                    <div class ="wz">
双腿分开，上身往下倾至水平，双臂往后抬举至背部上方，两手手指交叉，掌心向下，用力往后伸展，维持30秒。</div>
            <div class="img">
        <img src="images/app/jt/tu_jt_04.jpg"></div>    
    </div>
    <div class="imgwz">
        
            <div class='wzti'>腿部拉伸运动：</div>
                    <div class ="wz">
双腿分开，上身下倾至水平，左手从背后绕过扶住右侧腰部，右手顺着左腿往下压，维持20秒，然后换边。</div>
            <div class="img">
        <img src="images/app/jt/tu_jt_05.jpg"></div>    
    </div>
    <div class="imgwz">
        
            <div class='wzti'>腹部收紧运动：</div>
                    <div class ="wz">
首先坐在椅子前端，双手向后抓住椅子两侧(一定要抓紧并且保持平衡)，然后双腿朝前方尽量伸直，最后撑起双臂，以脚跟为支撑点挺起胸腹部，尽量保持身体是一条直线，维持30秒。</div>
            <div class="img">
        <img src="images/app/jt/tu_jt_06.jpg"></div>    
    </div>
</div>

  
</body>
</html>
