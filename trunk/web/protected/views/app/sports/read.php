<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title><?=$item["title"]?></title>
<style type="text/css">
body{margin:0px;background:#fff;font-size:14px; color:#3a3a3a;}
.tit{height:35px; line-height:35px; text-align:center; font-size:16px;min-width:315px;}
.cont{padding:0px 10px 10px 10px;min-width:295px; line-height:20px;}

.tt02 .l .b1{color:#6be190;}
.tt02 .l .b2{color:#00cdec;}
.tt02 .l .b3{color:#ffc0c8;}
.tt02{height:150px;}
.tt02 .l{float:left; width:150px; padding:15px 0px 0px 10px; line-height:25px;font-size:16px;}
.tt02 .r{float:left; width:100px; padding:25px 0px 0px 0px;line-height:18px;}
.tt02 .r span{  display:inline-block;width:11px; height:11px; background:url(images/app/tj_15.png) no-repeat;background-size:11px; margin:0px auto auto 2px; overflow:hidden;}
.tt02 .r b{display:block;font-weight:normal; display:block;padding:0px 0px 5px 0px;}
.cont strong{ display:block; margin:10px;}
.ifr{display:block; margin:5px auto; height:240px;padding:5px 0px 5px 0px;}
.v{padding:15px 0px 0px 10px;}
.title01{ height: 40px;line-height: 40px;padding: 10px 0px 0px 19px;background: url(images/app/tj_27.png) no-repeat left 18px;background-size: 18px;}
.title02{ height: 40px;line-height: 40px;padding: 10px 0px 0px 19px;background: url(images/app/tj_27.png) no-repeat left -28px;background-size: 18px;}
.title03{ height: 40px;line-height: 40px;padding: 10px 0px 0px 19px;background: url(images/app/tj_27.png) no-repeat left -76px;background-size: 18px;}
</style>


</head>

<body>
<div class="cont">
    
    <div class="tt02">
   	<div class="l">	
        	<b class="b<?=$item["type"]?>"><?=$item['typename']?></b> <br><?=$item['title']?><br>
       		<img src="<?=$item["image_url"]?>" width="105">
        </div>
        <div class="r">
            <b>难度指数</b>
            反应<?php
            for($i=0;$i<$item["fanying"];$i++){
                echo "<span></span>";
            }
            ?><br>
            力量<?php
            for($i=0;$i<$item["liliang"];$i++){
                echo "<span></span>";
            }
            ?><br>
            耐力<?php
            for($i=0;$i<$item["naili"];$i++){
                echo "<span></span>";
            }
            ?><br>
            柔韧<?php
            for($i=0;$i<$item["rouren"];$i++){
                echo "<span></span>";
            }
            ?><br>
            平衡<?php
            for($i=0;$i<$item["pingheng"];$i++){
                echo "<span></span>";
            }
            ?><br>
        </div>
    </div>
    <div class="title01">内容简介</div>
    <div>
        <?=$item['content']?>
    </div>
    <div class="title02">作者简介</div>
    <div>
        <?=$item['author']?>
    </div>
    <div class="title03">目录</div>
    <div>
        <?=$item['tablesofcontents']?>
    </div>
</div>

    <div id="youkuplayer" style="width:100%;height: 200px"></div>
    
</body>
</html>
