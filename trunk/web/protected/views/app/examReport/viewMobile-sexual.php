<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery/jqueryCut.js'); ?>
<?php
$this->pageTitle = $model->name;
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/viewMobile.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/viewMobile.css'); ?>
<ul class="man" id="man">
  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###"><?php echo $model->name; ?></a></h2>
    <div class="man-l-bd" name="mans">
      <?php echo $model->over_view; ?>
      <div class="man-bottom man-bottom-ling">
        <?php echo CHtml::link('百度百科一下' . $model->name,
      'http://wapbaike.baidu.com/search?word=' . $model->name,
      array('data-role' => 'button')); ?>
      </div>
    </div> 
    <div class="man-l-bottom">
      <div class="range-wrapper">
        <?php
        $COEFFICIENT = 0.3;
        $MIN_POINTS = 10;
        preg_match('/^(?P<f_min>\d+(\.\d*)?),(?P<f_max>\d+(\.\d*)?),(?P<m_min>\d+(\.\d*)?),(?P<m_max>\d+(\.\d*)?)(\s*\n\s*(?P<unit>\S*(.*\S)?)\s*)?$/',
          $model->value_ranges, $matches);

        $f_min = $matches['f_min'];
        $f_max = $matches['f_max'];
        $m_min = $matches['m_min'];
        $m_max = $matches['m_max'];
        $unit = array_key_exists('unit', $matches) ?  $matches['unit'] : '';
        $range_f_step = pow(10, -
        max(
          strlen(preg_replace('/^\d*\./', '', (string) $f_min )),
          strlen(preg_replace('/^\d*\./', '', (string) $f_max ))
        ));
        $range_m_step = pow(10, -
        max(
          strlen(preg_replace('/^\d*\./', '', (string) $m_min )),
          strlen(preg_replace('/^\d*\./', '', (string) $m_max ))
        ));
        $range_f_step = ($f_max - $f_min) / $range_f_step < $MIN_POINTS ? $range_f_step / 10 : $range_f_step;
        $range_m_step = ($m_max - $m_min) / $range_m_step < $MIN_POINTS ? $range_m_step / 10 : $range_m_step;
        $f_interval = ($f_max - $f_min) * $COEFFICIENT / $range_f_step;
        $f_interval = (int)$f_interval * $range_f_step;
        $m_interval = ($m_max - $m_min) * $COEFFICIENT / $range_m_step;
        $m_interval = (int)$m_interval * $range_m_step;
        $range_f_min = $f_min - $f_interval;
        $range_f_max = $f_max + $f_interval;
        $range_m_min = $m_min - $m_interval;
        $range_m_max = $m_max + $m_interval;

        $curr_f_value = ($f_min + $f_max) / 2;
        $curr_m_value = ($m_min + $m_max) / 2;
        ?>

        <label for="sex-selector">请选择:</label>

        <div id="gender" name="sex-selector" data-role="slider" data-mini="true" >
          <div id="zman" class="zman" value="female">
            男
          </div>
          <div class="among" id="among">
          </div>
          <div id="woman" class="woman" value="female">
            女
          </div>
        </div>
        <?php echo CHtml::hiddenField('f_min', $f_min); ?>
        <?php echo CHtml::hiddenField('f_max', $f_max); ?>
        <?php echo CHtml::hiddenField('m_min', $m_min); ?>
        <?php echo CHtml::hiddenField('m_max', $m_max); ?>
        <div class="plan">
          <input type="text" class="plan-text" value="" id="text_value_two">
          <input type="range" class='outer1' 
            name="curr_f_value"
            id="text_range_f"
            value="<?php echo $curr_f_value; ?>"
            min="<?php echo $range_f_min; ?>"
            max="<?php echo $range_f_max; ?>"
             step="<?php echo $range_f_step; ?>"/>
          <input type="range" class='outer1' 
            name="curr_m_value"
            id="text_range_m"
            value="<?php echo $curr_m_value; ?>"
            min="<?php echo $range_m_min; ?>"
            max="<?php echo $range_m_max; ?>"
            step="<?php echo $range_m_step; ?>"/>
        </div>
        <div>
          <h2 class="man-l-bottom-h">正常值范围</h2>
          <div id="text_text_f">
            <?php echo "$f_min - $f_max $unit"; ?>
          </div>
          <div id="text_text_m">
            <?php echo "$m_min - $m_max $unit"; ?>
          </div>
        </div>
      </div>
    </div>   
  </li>

  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###">过高的原因</a></h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->over_reason; ?></div></div>
  </li>

  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###">过低的原因</a></h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->under_reason; ?></div></div>
  </li>

  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###">专家建议</a></h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->expert_advice; ?></div></div>
  </li>

  <li class="man-line">
    <h2 class="man-l-top" name="hea">
      <a href="###"><?php echo $model->item->item_name ?>说明</a>
    </h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->item->description; ?></div></div>
  </li>
</ul>
<div class="man-bottom">
  <a href="wtai://wp/mc;4000119091">专家电话资询: 400-011-9091</a>
</div>
