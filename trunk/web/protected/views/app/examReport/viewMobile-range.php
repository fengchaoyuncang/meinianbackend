<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery/jqueryCut.js'); ?>
<?php
$this->pageTitle = $model->name;
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/viewMobile.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/viewMobile.css'); ?>
<ul class="man" id="man">
  <li class="man-line">
    <h2 class="man-l-top" name="hea">
      <a href="###"><?php echo $model->name; ?></a>
    </h2>
    <div class="man-l-bd" name="mans">
      <?php echo $model->over_view; ?>
      <div class="man-bottom man-bottom-ling">
        <?php echo CHtml::link('百度百科一下' . $model->name,
        'http://wapbaike.baidu.com/search?word=' . $model->name,
        array('data-role' => 'button')); ?> 
      </div>
    </div> 
    <div class="man-l-bottom">
      <div class="man-l-bottom-inner">
        <?php
        $COEFFICIENT = 0.3;
        $MIN_POINTS = 10;

        preg_match('/^(?P<min>\d+(\.\d*)?),(?P<max>\d+(\.\d*)?)(\s*\n\s*(?P<unit>\S*(.*\S)?)\s*)?$/',
          $model->value_ranges, $matches);
        $min = $matches['min'];
        $max = $matches['max'];

        $unit = array_key_exists('unit', $matches) ?  $matches['unit'] : '';

        $range_step = pow(10, -
        max(
          strlen(preg_replace('/^\d*\./', '', (string) $min )),
          strlen(preg_replace('/^\d*\./', '', (string) $max ))
        ));
        $range_step = ($max - $min ) / $range_step < $MIN_POINTS ? $range_step / 10 : $range_step;
        $interval = ($max - $min) * $COEFFICIENT / $range_step;
        $interval = (int)$interval * $range_step;
        $range_min = $min - $interval;
        $range_max = $max + $interval;
        $curr_value = ($min + $max) / 2;
        ?>
        <div class="plan">
          <input type="text" class="plan-text" value="" id="text_value">
          <input type="range" class='outer1' 
            id="text_range"
            name="curr_value"
            value="<?php echo $curr_value; ?>"
            min="<?php echo $range_min; ?>"
            max="<?php echo $range_max; ?>"
            step="<?php echo $range_step; ?>"/>
        </div>
        
        <h2 class="man-l-bottom-h"><a href="###">正常值范围</a></h2>
        <div>
          <?php echo CHtml::hiddenField('min', $min); ?>
          <?php echo CHtml::hiddenField('max', $max); ?>
          <?php echo "$min - $max $unit"; ?>
        </div>
      </div>
    </div>
  </li>
  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###">过高的原因</a></h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->over_reason; ?></div>
    </div>
  </li>
  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###">过低的原因</a></h2>
    <div class="man-l-bd" name="mans"><div><?php echo $model->under_reason; ?></div></div>
  </li>

  <li class="man-line">
    <h2 class="man-l-top" name="hea"><a href="###">专家建议</a></h2>
    <div class="man-l-bd" name="mans"><div><?php echo $model->expert_advice; ?></div></div>
  </li>

  <li class="man-line">
    <h2 class="man-l-top" name="hea">
      <a href="###"><?php echo $model->item->item_name ?>说明</a>
    </h2>
    <div class="man-l-bd" name="mans"><div><?php echo $model->item->description; ?></div></div>
  </li>
</ul>
<div class="man-bottom">
  <a href="wtai://wp/mc;4000119091">专家电话资询: 400-011-9091</a>
</div>
