<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery/jqueryCut.js'); ?>
<?php
$this->pageTitle = $model->name;
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/viewMobile.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/viewMobile.css'); ?>
<ul class="man" id="man">
  <li class="man-line">
    <h2 class="man-l-top" name="hea">
      <a href="#"><?php echo $model->name; ?></a>
    </h2>
    <div class="man-l-bd" name="mans">
      <?php echo $model->over_view; ?>
    </div>
    <div class="man-l-bottom">
      <div><?php echo $model->value_ranges; ?></div>
    </div>
  </li>
  <?php if (!empty($model->value_ranges)) { ?>
    <li class="man-line">
      <h2 class="man-l-top" name="hea">
        <a href="#">过高的原因</a>
      </h2>
      <div class="man-l-bd" name="mans">
        <div><?php echo $model->over_reason; ?></div>
      </div>
    </li>

    <li class="man-line">
      <h2 class="man-l-top" name="hea">
         <a href="#">过低的原因</a> 
      </h2>
      <div class="man-l-bd" name="mans">
        <div><?php echo $model->under_reason; ?></div>
      </div>
    </li>
  <?php }?>
  <li class="man-line">
     <h2 class="man-l-top" name="hea">
      <a href="#">专家建议</a>
    </h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->expert_advice; ?></div>
    </div>
  </li>
  <li class="man-line">
     <h2 class="man-l-top" name="hea">
      <a href="#"><?php echo $model->item->item_name ?>说明</a>
    </h2>
    <div class="man-l-bd" name="mans">
      <div><?php echo $model->item->description; ?></div>
    </div>
  </li>
</ul>
<div class="man-bottom">
  <a href="wtai://wp/mc;4000119091">
    专家电话资询: 400-011-9091
  </a>
</div>
