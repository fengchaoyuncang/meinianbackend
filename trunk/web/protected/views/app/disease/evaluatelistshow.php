<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>慢性病风险评估结果</title>
<script>
            var getElementsByClass = function(searchClass, node, tag) {
                var classElements = new Array();
                if (node == null)
                    node = document;
                if (tag == null)
                    tag = '*';
                var els = node.getElementsByTagName(tag);
                var elsLen = els.length;
                var pattern = new RegExp("(^|\\s)" + searchClass + "(\\s|$)");
                for (i = 0, j = 0; i < elsLen; i++) {
                    if (pattern.test(els[i].className)) {
                        classElements[j] = els[i];
                        j++;
                    }
                }
                return classElements;
            }

        
            window.onload = function(){
                var width = getElementsByClass("fx_1", null, "div");
                width = width[0].clientWidth;
                var bar = getElementsByClass("fx_3", null, "div");
                for(var i=0;i<bar.length;i++){
                    bar[i].style.width = width+"px";
                }
            };
        </script>
<style type="text/css">
body{margin:0px;background:#eee;font-size:14px; color:#3a3a3a;}
a{text-decoration:none;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);}
.fx{padding:30px 10px 25px 85px; position:relative; line-height:16px; margin:20px 10px; background:#FFF; color:#666;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px; -moz-box-shadow:0px 3px 0px #e3e4e8;-webkit-box-shadow:0px 3px 0px #e3e4e8;box-shadow:0px 3px 0px #e3e4e8;}
.fx .zi{ position:absolute;left:20px;top:30px;}
.fx .fx_1{height:16px;background: -webkit-gradient(linear, left top, right bottom, from(#c4f6de), to(rgb(255, 202, 202))); position:relative;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;width:100%;}
.fx .fx_1 .t1{ width:4px; height:4px; background:#FFF;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px; display:block; position:absolute;left:33%;top:6px;}
.fx .fx_1 .t2{ width:4px; height:4px; background:#FFF;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px; display:block; position:absolute;left:66%;top:6px;}
.fx .fx_2{overflow:hidden;height:16px;-moz-border-radius:9px 0px 0px 9px;-webkit-border-radius:9px 0px 0px 9px;border-radius:9px 0px 0px 9px; position:relative; float:left;}
.fx .fx_3{height:16px;background: -webkit-gradient(linear, left top, right bottom, from(#39e08f), color-stop(0.5, orange), to(rgb(255, 0, 0))); width:100%;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;}
.fx .ts1,.fx .ts2,.fx .ts3{float:left; width:0px; height:16px; overflow:visible;}
.fx .ts1 span{position:relative;top:-18px;left:-17px; display:block; width:32px; height:18px; font-size:12px; text-align:center; line-height:16px; background:url(images/app/tj_11.png) 0px -43px;background-size:32px; color:#FFF;}
.fx .ts2 span{ position:relative;top:-18px;left:-17px;display:block; width:32px; height:18px; font-size:12px; text-align:center; line-height:16px; background:url(images/app/tj_11.png) 0px -21px;background-size:32px; color:#FFF;}
.fx .ts3 span{ position:relative;top:-18px;left:-17px;display:block; width:32px; height:18px; font-size:12px; text-align:center; line-height:16px; background:url(images/app/tj_11.png);background-size:32px; color:#FFF;}
</style>

</head>

<body>
    <?php
    foreach($result["items"] as $it){
        $value = $it["value"];
        if($value["percent"]>=$value["high"]){
            $tag = "高危";
            $tagclass = "ts3";
        }elseif($value["percent"]>$value["low"]){
            $tag = "中危";
            $tagclass = "ts2";
        }else{
            $tag = "正常";
            $tagclass = "ts1";
        }
    ?>
    <!--进度条-->
    <a href="<?=$this->createUrl("app/disease/evaluatebyid",array("uid"=>$_GET["uid"],"disid"=>$it["id"]))?>">
    <div class="fx">
    	<div class="zi"><?=$it["name"]?></div>
    	<div class="fx_1">
            <div class="fx_2" style="width:<?=$value["percent"]?>%;">
            	<div class="fx_3"></div>
            </div>
            <span class="<?=$tagclass?>"><span><?=$tag?></span></span>
            <span class="t1"></span>
            <span class="t2"></span>
        </div>
    </div>
    </a>
    <?php
    }
    ?>
    

<a href="<?=$this->createUrl("app/yingyang/howtoeat",array("uid"=>$_GET["uid"]))?>"><img src="images/app/tj_13.png" width="95%" style="margin-top:20px;"></a>

</body>
</html>
