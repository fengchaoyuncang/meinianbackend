<?php
//此处用于数据的转换
//计算风险度
//var_dump($result);
//$result['value'] =1.7;
//$evavalue = $result["value"];
//if($evavalue['percent'] > $evavalue["high"]){
//    $fengxianmiaosu = "高危";
//}elseif($evavalue['percent'] > $evavalue["low"]){
//    $fengxianmiaosu = "中危";
//}else{
//    $fengxianmiaosu = "低危";
//}

$value = $result["value"];
if ($value["percent"] >= $value["high"]) {
    $tag = "高危";
    $tagclass = "ts3";
} elseif ($value["percent"] > $value["low"]) {
    $tag = "中危";
    $tagclass = "ts2";
} else {
    $tag = "正常";
    $tagclass = "ts1";
}
?>

<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
        <meta content="no" name="apple-mobile-web-app-capable" />
        <meta content="black" name="apple-mobile-web-app-status-bar-style" />
        <meta content="telephone=no" name="format-detection" />
        <title><?= $result['name'] ?>风险评估结果</title>
        <script>
            var getElementsByClass = function(searchClass, node, tag) {
                var classElements = new Array();
                if (node == null)
                    node = document;
                if (tag == null)
                    tag = '*';
                var els = node.getElementsByTagName(tag);
                var elsLen = els.length;
                var pattern = new RegExp("(^|\\s)" + searchClass + "(\\s|$)");
                for (i = 0, j = 0; i < elsLen; i++) {
                    if (pattern.test(els[i].className)) {
                        classElements[j] = els[i];
                        j++;
                    }
                }
                return classElements;
            }


            window.onload = function() {
                var width = getElementsByClass("fx_1", null, "div");
                width = width[0].clientWidth;
                var bar = getElementsByClass("fx_3", null, "div");
                for (var i = 0; i < bar.length; i++) {
                    bar[i].style.width = width + "px";
                }
            };
        </script>
        <style type="text/css">
            body{margin:0px;background:#eee;font-size:14px; color:#3a3a3a;}
            .tit{height:35px; line-height:35px;font-size:16px;min-width:295px; padding:0px 10px; font-weight:bold;}
            .span1{ color:#ff4040;}
            .span2{ color:#48cfad;}
            .cont{padding:10px; line-height:25px; background:#FFF; min-width:295px;}
            .cont b{ color:#ff5640;}
            .cont strong{ display:block; padding:5px 0px;}
            .title01{height:40px; line-height:40px; padding:0px 0px 0px 20px; background:url(images/app/tj_10.png) no-repeat 5px 15px;background-size:8px;}
            .table,.table td{border-collapse:collapse;border:solid 1px #e3e4e8;font-size:14px;line-height:20px;margin:0px auto;}
            .table td{ padding:4px;}
            .table td p {padding:0px;width:30px; margin:0px; height:30px; display:inline-block; background:url(images/app/tj_06.png) no-repeat 0px -50px;}
            .table{width:100%;}
            .table tr:nth-child(2n-1){ background:#f6f8fa;}
            .fxx{margin:10px 0 0 0;}
            .fx{padding:20px 0px 5px 65px; position:relative; line-height:16px;}
            .fx .zi{ position:absolute;left:0px;top:20px;}
            .fx .fx_1{height:16px;background: -webkit-gradient(linear, left top, right bottom, from(#c4f6de), to(rgb(255, 202, 202))); position:relative;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;width:100%;}
            .fx .fx_1 .t1{ width:4px; height:4px; background:#FFF;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px; display:block; position:absolute;left:33%;top:6px;}
            .fx .fx_1 .t2{ width:4px; height:4px; background:#FFF;-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px; display:block; position:absolute;left:66%;top:6px;}
            .fx .fx_2{overflow:hidden;height:16px;-moz-border-radius:9px 0px 0px 9px;-webkit-border-radius:9px 0px 0px 9px;border-radius:9px 0px 0px 9px; position:relative; float:left;}
            .fx .fx_3{height:16px;background: -webkit-gradient(linear, left top, right bottom, from(#39e08f), color-stop(0.5, orange), to(rgb(255, 0, 0))); width:100%;-moz-border-radius:9px;-webkit-border-radius:9px;border-radius:9px;}
            .fx .ts1,.fx .ts2,.fx .ts3{float:left; width:0px; height:16px; overflow:visible;}
            .fx .ts1 span{position:relative;top:-18px;left:-17px; display:block; width:32px; height:18px; font-size:12px; text-align:center; line-height:16px; background:url(images/app/tj_11.png) 0px -43px;background-size:32px; color:#FFF;}
            .fx .ts2 span{ position:relative;top:-18px;left:-17px;display:block; width:32px; height:18px; font-size:12px; text-align:center; line-height:16px; background:url(images/app/tj_11.png) 0px -21px;background-size:32px; color:#FFF;}
            .fx .ts3 span{ position:relative;top:-18px;left:-17px;display:block; width:32px; height:18px; font-size:12px; text-align:center; line-height:16px; background:url(images/app/tj_11.png);background-size:32px; color:#FFF;}
        </style>

    </head>

    <body>
        <div class="tit"><?= $result['name'] ?>风险评估结果</div>
        <div class="cont">
            您未来10年<?= $result['name'] ?>的发病风险等级：<b><?= $tag ?></b>
            
                <!--进度条-->
                <div class="fx">

                    <div class="zi">当前风险</div> 
                    <div class="fx_1">
                        <div class="fx_2" style="width:<?= $value["percent"] ?>%;">
                            <div class="fx_3"></div>
                        </div>
                        <span class="<?= $tagclass ?>"><span><?= $tag ?></span></span>
                        <span class="t1"></span>
                        <span class="t2"></span>
                    </div>
                </div>
                <!--进度条-->
                <div class="fx">
                    <div class="zi">理想风险</div>
                    <div class="fx_1">
                        <div class="fx_2" style="width:10%;">
                            <div class="fx_3"></div>
                        </div>
                        <span class="ts1"><span>正常</span></span>
                        <span class="t1"></span>
                        <span class="t2"></span>
                    </div>
                </div>
            
            <div class="fxx">
            <!--进度条-->
            <strong>当前风险</strong>
            根据您的生活习惯以及体检报告，未来10年内，您的高血压发病风险值
            <strong>理想风险</strong>
            您理想的风险值
            </div>
        </div>
        <div class="tit">可能诱发高血压的危险因素</div>
        <div class="cont">
            <table class="table"> 
                <tr>
                    <td height="32" align="center"><strong>危险因素</strong></td>
                    <td align="center"><strong>本次</strong></td>
                    <td align="center"><strong>评估参考值</strong></td>
                </tr>

                <?php
                foreach ($result["yinsu"] as $it) {
                    if ($it["abnormal"] !== 1 || $it["jiedu"] == null)
                        continue;
                    ?>        
                    <tr height="32" align="center">
                        <td height="32"  align="center"><?= $it["name"] ?></td>
                        <td align="center">
                            <?= $it["abnormal"] == 1 ? '<span class="span1">' : "" ?>
                            <?= $it["value"] ?>
                            <?= $it["abnormal"] == 1 ? '</span>' : "" ?>
                        </td>
                        <td  align="center"><?= $it["des"] ?></td>
                    </tr>
                    <?php
                }
                ?>

            </table>
        </div>
        <div class="tit">危险因素解读</div>
        <?php
        foreach ($result["yinsu"] as $it) {
            if ($it["abnormal"] !== 1 || $it["jiedu"] == null)
                continue;
            ?>
            <div class="cont"><strong><?= $it["name"] ?></strong>
                    <?= $it["jiedu"] ?>
            </div>
            <?php
        }
        ?>

    </body>
</html>
