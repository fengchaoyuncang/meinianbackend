<?php
//此处用于数据的转换
//计算风险度
$shinianfengxian = (($result['value']*1000)/($result['ceil']*1000))*80;
$lixiangfengxian = (($result['int_med_val_low']*0.3*1000)/($result['ceil']*1000))*80;
if($result['value'] > $result['int_med_val_high']){
    $fengxianmiaosu = "高风险";
}elseif($result['value']<=$result['int_med_val_high']&&$result['value']>$result['int_med_val_low']){
    $fengxianmiaosu = "中等风险";
}else{
    $fengxianmiaosu = "低风险";
}
//风险因素描述

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$disdetail->name?>风险评估结果</title>
<style>
* {
	margin: 0;
	padding: 0;
}

body {
	font-family: 华文细黑;
}

p {
	font-size: 14px;
	margin: 5px 15px;
	line-height: 20px;
}

#headLine {
	background: #005ec6;
	line-height: 50px;
	color: white;
	position: relative;
}

#backBtn {
	height: 50px;
	width: 50px;
	position: absolute;
	left: 0;
	top: 0;
        background: url(<?=Yii::app()->baseUrl?>/images/pinggu/back_button.png) no-repeat center;
	background-size: 50px;
}

#settingBtn {
	height: 50px;
	width: 50px;
	position: absolute;
	right: 0;
	top: 0;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/setting_button.png) no-repeat center;
	background-size: 50px;
}

#itemTitle {
	font-size: 22px;
	text-align: center;
}

.tabs {
	line-height: 30px;
	font-size: 16px;
	width: 106px;
	text-align: center;
	border-right: white 1px solid;
	display: inline-block;
}

.tabs:last-child {
	border-right: none;
}

.tabs span {
	display: block;
	border-bottom: 3px transparent solid;
}

.tabs[select="true"] span {
	color: #005ec6;
	border-bottom: 3px #005ec6 solid;
}

#tabs {
	font-size: 0em;
	border-top: 1px #ccc solid;
	border-bottom: 1px #ccc solid;
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(245,245,245,1) 10%, rgba(245,245,245,1) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(10%,rgba(245,245,245,1)), color-stop(100%,rgba(245,245,245,1)));
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(245,245,245,1) 10%,rgba(245,245,245,1) 100%);
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(245,245,245,1) 10%,rgba(245,245,245,1) 100%);
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(245,245,245,1) 10%,rgba(245,245,245,1) 100%);
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(245,245,245,1) 10%,rgba(245,245,245,1) 100%);
}

.title_lvl_1 {
	padding: 15px 0;
}

.title_lvl_1 span {
	display: inline-block;
	background: #005ec6;
	-webkit-border-top-right-radius: 3px;
	-webkit-border-bottom-right-radius: 3px;
	-moz-border-radius-topright: 3px;
	-moz-border-radius-bottomright: 3px;
	border-top-right-radius: 3px;
	border-bottom-right-radius: 3px;
	padding: 0 20px 0 15px;
	line-height: 40px;
	font-size: 16px;
	color: white;
}

.title_lvl_2 {
	padding: 15px 0;
}

.title_lvl_2 span {
	line-height: 33px;
	font-size: 16px;
	padding-left: 60px;
}

.title_gxyfxpgjg {
	color: #2897d2;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/gxyfxpgjg.png) no-repeat 15px;
	background-size: 33px;
}

.high_risk {
	color: #ff4242;
}

.list_gxyfxpgjg {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_gxyfxpgjg.png) no-repeat left 3px;
	background-size: 14px;
}

.title_knyfgxydwxys {
	color: #ec8b0e;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/knyfgxydwxys.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

table {
	border: 1px #979797 solid;
	text-align: center;
	width: 290px;
	margin: 15px;
	margin-right: 0;
	white-space: nowrap;
	font-size: 12px;
	border-collapse: collapse;

}

thead {
	background: #f4f4f4;
	font-weight: bold;
}

tbody tr {
	border-top: #979797 1px solid;
}

tbody tr:nth-child(2n){
	background: #f4f4f4;
}

th, td {
	border-right: #979797 1px solid;
	line-height: 36px;
}

th:last-child, td:last-child {
	border: none;
}

.title_wxysjd {
	color: #ea4942;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/wxysjd.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

.list_wxysjd {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_wxysjd.png) no-repeat left 3px;
	background-size: 14px;
}

.title_dnshxgdjy {
	color: #de8cc6;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/dnshxgdjy.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

.list_dnshxgdjy {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_dnshxgdjy.png) no-repeat left 3px;
	background-size: 14px;
}

.hr_1 {
	height: 45px;
}

.hr_2 {
	height: 30px;
}

.hr_3 {
	height: 15px;
}

.title_ystlyz {
	color: #efc000;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/ystlyz.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

.list_ystlyz {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_ystlyz.png) no-repeat left 3px;
	background-size: 14px;
}

img {
	max-width: 320px;
	padding: 15px 0;
}

.img_div {
	position: relative;
	font-size: 12px;
}

.img_div div {
	position: absolute;
}

.title_yljy {
	color: #2897d2;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/yljy.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

.list_yljy {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_yljy.png) no-repeat left 3px;
	background-size: 14px;
}

.title_ydfa {
	color: #86ad21;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/ydfa.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

.list_ydfa {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_ydfa.png) no-repeat left 3px;
	background-size: 14px;
}

.title_ydzysx {
	color: #ea880c;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/ydzysx.png) no-repeat 15px;
	background-size: 33px;
	margin-top: 45px;
}

.list_ydzysx {
	text-indent: 1.2em;
	background: url(<?=Yii::app()->baseUrl?>/images/pinggu/list_ydzysx.png) no-repeat left 3px;
	background-size: 14px;
}

.chart {
	border: #ccc 1px solid;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	padding: 20px;
	height: 100px;
	margin: 15px;
	position: relative;
}

.chart_bg {
	background: #f4f4f4;
	border: #ccc 1px solid;
	border-left: black 1px solid;
	border-bottom: black 1px solid;
	height: 80px;
	width: 200px;
	position: absolute;
	left: 70px;
	top: 20px;

}

.recent_risk, .ideal_risk {
	width: 25px;
	position: absolute;
	bottom: 0;
}

.recent_risk {
	background: #ea4a40;
	left: 38px;
}

.ideal_risk {
	background: #89ad22;
	left: 138px;
}

.chart_grid {
	border-bottom: #ccc 1px solid;
	height: 13px;
	margin-bottom: 14px;
	position: relative;
}

.chart_grid_2 {
	position: absolute;
	height: 27px;
	border-top: black 1px solid;
	border-bottom: black 1px solid;
	width: 5px;
	left: -5px;
	top: -15px;
}

.chart_grid_3 {
	position: absolute;
	height: 9px;
	border-top: black 1px solid;
	border-bottom: black 1px solid;
	width: 3px;
	right: 0;
	top: 8px;
}

.char_y_high, .char_y_mid, .char_y_low {
	position: absolute;
	left: 10px;
	font-size: 12px;
}

.char_y_high {
	top: 24px;
}

.char_y_mid {
	top: 52px;
}

.char_y_low {
	top: 80px;
}

.char_x_recent, .char_x_ideal {
	position: absolute;
	top: 106px;
	font-size: 12px;
}

.char_x_recent {
	left: 96px;
}

.char_x_ideal {
	left: 196px;
}

.suggest_ydfa {
	position: relative;
	margin-bottom: 20px;
}

.suggest_ydfa img {
	margin-left: 15px;
	padding: 0;
	height: 45px;
}

.ydmc, .ydms {
	line-height: 45px;
	color: #6a6a6a;
	font-size: 14px;
	position: absolute;
	top: 0;
}

.ydmc {
	left: 70px;
}

.ydms {
	left: 160px;
}
.res-button {
    float: right;
    margin: 20px 5% 10px 0;
    color: #fff;
    font-size: 1em;
    padding: 0.2em 1em;
    background: #77c7dd;
    border: 0;
    border-radius: 5px;
    text-decoration: none;
}
</style>
</head>
<body>

<div class="title_lvl_1"><span><?=$disdetail->name?>风险评估</span></div>
<div class="title_lvl_2 title_gxyfxpgjg"><span><?=$disdetail->name?>风险评估结果</span></div>
<p>您未来10年<?=$disdetail->name?>的发病风险等级:<span class="high_risk"><?=$fengxianmiaosu?></span></p>
<div class="chart">
<div class="char_y_high">高风险</div>
<div class="char_y_mid">中等风险</div>
<div class="char_y_low">低风险</div>
<div class="char_x_recent">当前风险</div>
<div class="char_x_ideal">理想风险</div>
<div class="chart_bg">
<div class="chart_grid"></div>
<div class="chart_grid">
<div class="chart_grid_2">
<div class="chart_grid_3"></div>
</div>
</div>
<div class="chart_grid">
<div class="chart_grid_2">
<div class="chart_grid_3"></div>
</div>
</div>
<div class="recent_risk" style="height: <?=$shinianfengxian?>px"></div>
<div class="ideal_risk" style="height: <?=$lixiangfengxian?>px"></div>
</div>
</div>
<p class="list_gxyfxpgjg"><strong>当前风险：</strong>根据您的生活习惯以及体检报告，未来10年内，您的<?=$disdetail->name?>发病风险值</p>
<p class="list_gxyfxpgjg"><strong>理想风险：</strong>您理想的风险值</p>
<div class="title_lvl_2 title_knyfgxydwxys"><span>可能诱发<?=$disdetail->name?>的危险因素</span></div>
<table>
<thead>
<tr><th>危险因素</th><th>本次</th><th>评估参考值</th></tr>
</thead>
<tbody>
<?php
    foreach($yinsu as $it){
?>        
        <tr>
            <td><?=$it["name"]?></td>
            <td>
                <?=$it["abnormal"]==1?'<span class="high_risk">':""?>
                <?=$it["value"]?>
                <?=$it["abnormal"]==1?'</span>':""?>
            </td>
            <td><?=$it["des"]?></td>
        </tr>
<?php
    }
?>

</tbody>
</table>
<div class="title_lvl_2 title_wxysjd"><span>危险因素解读</span></div>
<?php
foreach ($yinsu as $it){
    if($it["abnormal"]!==1||$it["jiedu"] == null)
        continue;
?>
<p class="list_wxysjd"><strong><?=$it["name"]?></strong></p>
<p><?=$it["jiedu"]?></p>
<div class="hr_3"></div>
<?php
}
?>

<div class="title_lvl_2 title_dnshxgdjy"><span>对您生活习惯的建议</span></div>
<?php
foreach ($yinsu as $it){
    if($it["abnormal"]!==1||empty($it["jianyi"]))
        continue;
?>
<p class="list_dnshxgdjy"><strong><?=$it["jianyi"]["title"]?></strong></p>
<p><?=$it["jianyi"]["content"]?></p>
<div class="hr_3"></div>
<?php
}
?>
<div class="hr_1"></div>
<div class="title_lvl_1"><span>饮食建议</span></div>
<p>我们在您健康信息和健康体检数据的基础上，充分考虑了到您的性别、年龄、职业、生活习惯和疾病史状况。作为一名中年女性，您的职业特点是，可能会接触某些工业物质，在生活习惯上有吸烟/饮酒的嗜好。结合您的饮食、运动情况，专家为您设计了健康指导方案。</p>
<div class="title_lvl_2 title_ystlyz"><span>饮食调理原则是：</span></div>
<div class="img_div">
<img src="<?=Yii::app()->baseUrl?>/images/pinggu/ystlyz_img.png" />
<div style="left:5px;top:75px">奶制品100克<br/>豆制品50克</div>
<div style="left:5px;top:125px">蔬菜<br/>400-500克</div>
<div style="left:5px;top:175px">五谷<br/>400-500克</div>
<div style="left:232px;top:30px">油脂<br/>不超过100克</div>
<div style="left:232px;top:75px">鱼、禽、肉、蛋<br/>125-200克</div>
<div style="left:232px;top:125px">水果<br/>400-500克</div>
</div>
<p class="list_ystlyz">顺应自然的变化规律，运用相应的养生手段，进行合理的膳食搭配。</p>
<p class="list_ystlyz">您要做到饮食多样化，进食的食物要营养丰富、偏于清补，要适当多补充钙质。</p>
<p class="list_ystlyz">您患有高血压，所以的饮食应注意：定时定量，少食多餐；要适当摄入脂肪含量低、蛋白品质高的食物，如无脂奶粉、鱼类、豆制品等；要少用或禁用高钠、高脂肪、高胆固醇食物，如动物内脏、肥肉、鸡蛋黄、咸菜、腌制食品等。</p>
<p class="list_ystlyz">食盐摄入：因为您患有高血压，所以您的食盐量应严格控制在每日2-3克。</p>
<div class="title_lvl_2 title_yljy"><span>饮水、茶、其他饮料等建议</span></div>
<p class="list_yljy">成人每摄取1千卡能量约需水1毫升，这意味着成人每日需水量约2500毫升，此需水量包括茶、咖啡及其他饮料。</p>
<p class="list_yljy">茶叶内含茶多酚，它可防止维生素C氧化，有助于维生素C在体内的利用，并可排除有害的铬离子。此外茶叶还含有钾、钙、镁、锌、氟等微量元素。因此每天用4-6克茶叶（相当于2-3杯袋泡茶）冲泡，长期服用，对人体有益。</p>
<p class="list_yljy">因为您为高血压患者，所以不宜喝咖啡
女性月经、怀孕、哺乳、更年期不宜饮茶；饮茶注意事项：一、不要喝浓茶二、不要喝得太烫三、不要喝隔夜茶四、饥饿时不要喝茶五、睡前不要喝茶六、不要与药物一起服用。</p>
<div class="hr_1"></div>
<div class="title_lvl_1"><span>运动建议</span></div>
<p>当今社会，生活节奏紧张，竞争激烈，我们整天忙碌于工作、学习之中。忽略了运动，由于缺少运动所导致的非健康因素、亚健康状态、各种疾病日益显现出来。运动对健康是非常必要的。它不仅可使我们生活得健康、美丽、幸福、长寿，而且可使疾病的发生率下降。</p>
<div class="title_lvl_2 title_ydfa"><span>运动方案</span></div>
<p class="list_ydfa"><strong>运动时间</strong></p>
<p>每周至少3次，经常运动者可以坚持每周锻炼5～6次，每次运动持续45～60分钟</p>
<div class="hr_3"></div>
<p class="list_ydfa"><strong>运动节奏</strong></p>
<p>前10～15分钟热身活动如：伸展运动、关节活动等，后5～10分钟整理活动，逐渐回到日常平静水平</p>
<div class="hr_3"></div>
<p class="list_ydfa"><strong>有氧运动种类推荐</strong></p>
<div class="hr_3"></div>
<div class="suggest_ydfa"><img src="<?=Yii::app()->baseUrl?>/images/pinggu/yd_yy.png" /><div class="ydmc">游泳</div><div class="ydms">30分钟≈198千卡能量</div></div>
<div class="suggest_ydfa"><img src="<?=Yii::app()->baseUrl?>/images/pinggu/yd_mp.png" /><div class="ydmc">慢跑</div><div class="ydms">30分钟≈198千卡能量</div></div>
<div class="suggest_ydfa"><img src="<?=Yii::app()->baseUrl?>/images/pinggu/yd_jytc.png" /><div class="ydmc">减压体操</div><div class="ydms">30分钟≈198千卡能量</div></div>
<div class="suggest_ydfa"><img src="<?=Yii::app()->baseUrl?>/images/pinggu/yd_ymq.png" /><div class="ydmc">羽毛球</div><div class="ydms">30分钟≈198千卡能量</div></div>
<div class="suggest_ydfa"><img src="<?=Yii::app()->baseUrl?>/images/pinggu/yd_jz.png" /><div class="ydmc">举重</div><div class="ydms">30分钟≈198千卡能量</div></div>
<div class="title_lvl_2 title_ydzysx"><span>运动注意事项：</span></div>
<p class="list_ydzysx">运动训练不要做过分低头弯腰的动作</p>
<p class="list_ydzysx">不要做大幅度的快速动作</p>
<p class="list_ydzysx">运动后不宜有疲劳感，否则提示运动强度过大。在运动中若出现任何不适，均应终止运动，以免发生或加重不良反应，且每次运动中要避免作“全力以赴”的运动，应适可而止</p>
<p class="list_ydzysx">冬季锻炼应注意御寒保暖，增加室内预备活动时间。</p>

<div>
    <a href="index.php?r=app/disease/analysis/uid/<?=$uid?>/disid/<?=$disid?>/token/<?=$token?>/quesagain/queagain" class="res-button">重新测试</a>
</div>
</body>
</html>
<?php
/*
<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset='utf-8' />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/css/start.css"> -->
<link rel="stylesheet" type="text/css"
	href="<?=Yii::app()->baseUrl?>/css/result.css">
<script type="text/javascript"
	src="<?=Yii::app()->baseUrl?>/js/result.js"></script>
</head>
<body>
<!-- <div class="res">
		<div class="res-top">
			<p class="res-t-top">
				<?=$disdetail->name?>
			</p>
			<div class="res-t-strip">
				<div class="res-t-s-top" id="color">
					<span class="res-t-s-t-l"></span>
					<span class="res-t-s-t-z"></span>
					<span class="res-t-s-t-r"></span>
				</div>
				<div class="res-t-s-bottom" id="gap">
					<span class="res-t-s-b-l">0</span>
					<span><?=$result['int_med_val_low']?></span>
					<span><?=$result['int_med_val_high']?></span>
					<span><?=$result['ceil']?></span>
				</div>
				<div class="res-t-s-tpo">
					<div class="res-t-s-t-po" id="pulley"></div>
				</div>
			</div>
		</div>
		<ol class="res-text">
		<?php foreach ($resolve as $value){?>
		<li>
		<p>
		   <?=$value['resolve']?>
		</p>
		</li>
		<?php }?>
		</ol>
		<div class="res-text-h-right">
       	 	<a href="index.php?r=disease/listOut">其它疾病风险</a>
    	</div>
	</div> -->
		<div class="res">
			<div class="res-plan">
				<h2>评估结果</h2>
				<div class="res-plan-show">
					<div class="res-plan-show-border" id="color">
						<span class="res-plan-left"></span>
						<span class="res-plan-centre"></span> 
						<span class="res-plan-right"></span>
					</div>
					<div class="_pulley" id="_pulley">
					<div class="res-plan-show-position" id="pulley">
					</div>
				</div>
			</div>
			<div class="res-plan-text">
				<span class="res-plan-text-left">正常</span> 
				<span class="res-plan-text-centre">低危</span> 
				<span class="res-plan-text-right">高危</span>
			</div>
			<div class="res-plan-p">
				<p class="p-text"><?=$disdetail['outline']?></p>
			</div>
			<div class="res-plan-p">
				<?php foreach ($resolve as $value){?>
				<p class="p-text">
					<?=$value['resolve']?>
					<?php }?>
				</p>
			</div>
		</div>
		<ul class="res-text">
			<li class="res-text-line">
				<h3>营养建议</h3>
				<?php if ($nutritionsuggestion != null){?>
				   <p class="p-text"><?=$nutritionsuggestion['target']?></p>
				   <p class="p-text"><?=$nutritionsuggestion['mechanism']?></p>
				<?php }?>
				<h3>营养方案</h3>
				<h4>有益</h4>
				<?php if (isset($nutrition_sug_arr) && ($nutrition_sug_arr != null)){?>
				<?php foreach ($nutrition_sug_arr["benefit"] as $item){?>
				  <p class="p-text"><?=$item?></p>
				<?php }?>
				<h4>不建议</h4>
				<?php foreach ($nutrition_sug_arr["harmful"] as $item){?>
				  <p class="p-text"><?=$item?></p>
				<?php }?>
				<?php 
					}?>
			</li>
			<li class="res-text-line">
				<h3>运动方案</h3>
				<h4>有益</h4>
				<?php if (isset($sports_sug_arr) && ($sports_sug_arr != null)){?>
				<?php foreach ($sports_sug_arr["benefit"]["safe_sug_arr"] as $item){?>
				  <p class="p-text"><?php echo "<tr><td>$item->item</td><td>$item->motion</td><td>$item->trainingsite</td></tr>"?></p>
				<?php }
				}
				?>
				<h4>同时还可做的运动</h4>
				你还可以进行的运动有：<?php 
				echo $sports_sug_arr['benefit']['safe_sug_other_str'];
			?>
			</li>
		</ul>
<!--		<a href="javascript:void(0);" class="res-button" onclick="wtai://to/other">其他疾病风险</a>-->
			<a href="index.php?r=disease/analysis/uid/<?=$uid?>/disid/<?=$disid?>/token/<?=$token?>/quesagain/queagain" class="res-button">重新测试</a>
	</div>
	<script type="text/javascript" >
		carry(<?=$result['value']?>, <?=$result['int_med_val_low']?>, <?=$result['int_med_val_high']?>, <?=$result['ceil']?>);
	</script>
</body>
</html>
 
 */
 ?>
