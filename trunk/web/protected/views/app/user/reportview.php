<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta content="no" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<title>体检报告详情</title>
<style type="text/css">
body{margin:0px;background:#fff;font-size:14px; color:#3a3a3a;}
.tit{height:45px; line-height:45px;font-size:15px;min-width:290px; padding:0px 15px 0px 10px; border-bottom:1px solid #e3e4e8; overflow:hidden;}
.tit .l{ float:left;font-weight:bold;width: 50%;}
.tit .r{ float:right;font-size:12px;}
.tit .l span{ float:left;width:6px;height:6px;background:#60afff; margin:19px 8px 0px 8px;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;}
.tit .r span{ float:left;width:16px;height:16px;background:#ff8383; margin:14px 2px 10px 8px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px; line-height:16px; text-align:center; color:#FFF;}
.cont{padding:0px 10px 10px 10px; background:#FFF; min-width:295px; line-height:20px;}
a{ -webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none; display: block; width: 100%; height: 100%;}
.div{ height:auto; overflow:hidden; outline: 0; display: none;
  /*-moz-transition: height 0.4s ease-in;
  -webkit-transition: height 0.4s ease-in;
  -o-transition: height 0.4s ease-in;
  transition: height 0.4s ease-in;*/
  border-bottom:1px solid #e3e4e8;
  -webkit-user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;
 }
.div1{height:auto;overflow:hidden; outline: 0;
  /*-moz-transition: height 0.4s ease-in;
  -webkit-transition: height 0.4s ease-in;
  -o-transition: height 0.4s ease-in;
  transition: height 0.4s ease-in;*/
  border-bottom:1px solid #e3e4e8; text-decoration:none; -webkit-user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;}
.ul{padding:0px; margin:0px;list-style:none;  overflow:auto;}
.ul li{line-height:45px;list-style:none;margin:0px;padding:0px; overflow:auto; clear:both;}
.ul li a{display:block;height:45px;color:#3a3a3a;text-decoration: none;}
.ul .a{display:block;color:#3a3a3a;text-decoration: none;}
.ul li .span1{float:left;width:25%; padding:13px 0px 12px 32px; line-height:20px;}
.ul li .span2{float:left;width:50%;padding:13px 0px 12px 0px; line-height:20px;}
.ul .li01{background-color: #ffdcdc;background-size:15px;}
.ul .li01 .span2{padding-left:16px; background:url(images/app/tj_005.png) no-repeat 0px 17px;}
.ul .li02{background-color:  #bcf6e7;background-size:15px;}
.ul .li02 .span2{padding-left:16px; background:url(images/app/tj_005.png) no-repeat 0px -52px;}
.ul .li03{background-color:  #fff;background-size:15px;}
.ul .li03 .span2{padding-left:16px; background:url(images/app/tj_005.png) no-repeat 0px -118px;}
.ul .lino{background-image:url(images/app/tj_28.png);background-repeat:  no-repeat;background-position:  right 17px;background-size:15px;}
.ul .a{line-height:22px; padding:15px 10px 15px 20px; clear:both;background:url(images/app/tj_28.png) no-repeat right 50%;background-size:15px; overflow:auto;}
.ul .a .span1{float:left; width:30%; padding-right:15px;}
.ul .a .span2{float:left; width:60%;}
</style>
<script src="<?=Yii::app()->baseUrl?>/js/jquery/jqueryCut.js"></script>
<!--<script src="<?=Yii::app()->baseUrl?>/js/jquery/mobile/jquery.mobile-1.3.0.min.js"></script><script src="<?=Yii::app()->baseUrl?>/js/jquery/jqueryCut.js"></script>-->
</head>
<?php
?>
<body>
<div class="tit">
  <a href="###"><div class="l"><span></span>基本信息</div></a>
</div>
<div class="div">
  <ul class="ul">
    <?php
    foreach ($result["basic"]["items"] as $it):
    ?>
      <li><span class="span1"><?=$it->name?></span><span class="span2"><?=$it->value?></span></li>
    <?php
    endforeach;
    ?>
  </ul>
</div>
  <?php foreach ($result as $key=>$it): 
      if($key == "basic")
        continue;
    ?>
<div class="tit">
  <a href="###"><div class="l"><span></span><?=$key?></div>
  <div class="r">
      <?php if($it["issue"]!=0):?>
          <span><?=$it["issue"]?></span>异常值          
      <?php endif?>
  </div>
  </a>
</div>
<div class="div">
  <ul class="ul">
       <?php
    foreach ($it["items"] as $row):
      
    ?>
      <li class="<?=$row->indicator!==null?"lino":""?> <?=$row->issue=='↑'?"li01":($row->issue=='↓'?"li02":"li03")?>">
        <?php
        if($row->indicator!==null){
          echo '<a href="'.$this->createUrl("app/examReport/viewMobile/",array("id"=>$row->indicator->id)).'">';
        }
        ?>
          <span class="span1"><?=$row->name?></span>
          <span class="span2"><?=$row->value?><?=$row->unit?><?=$row->normal!=""?"[ ".$row->normal." ]":""?></span>
        <?php
        if($row->indicator!==null){
          echo '</a>';
        }
        ?>
      </li>
    <?php
    endforeach;
    ?>
  <ul>
</div>        
  <?php  endforeach;?>

</body>
</html>
<script language="javascript">
/*function getNextElement(node){    
  if(node.nextSibling.nodeType == 1){ 
    return node.nextSibling;    
  }    
  if(node.nextSibling.nodeType == 3){ 
    return getNextElement(node.nextSibling);    
  }    
  return null;
}
function ann(an){
  if(getNextElement(an).className=="div"){
    var h = getNextElement(an).getElementsByTagName("ul")[0].offsetHeight;
    getNextElement(an).style.height=h+2+"px";
    getNextElement(an).className="div1";
  }else{
    getNextElement(an).style.height="0px";
    getNextElement(an).className="div";
  }
}*/
$(function () {
  $('.tit').click(function (event) {
    $(this).next('div').slideToggle('normal');
  })
})
/*window.onload=function(){
    var aa = window.document.getElementsByTagName("div")[0];
    var h = getNextElement(aa).getElementsByTagName("ul")[0].offsetHeight;
    getNextElement(aa).style.height=h+2+"px";
    getNextElement(aa).className="div1";
};*/
</script>