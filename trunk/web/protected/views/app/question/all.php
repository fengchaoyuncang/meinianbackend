<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?= Yii::app()->baseUrl ?>/css/question.css">
        <title>风险评估调查问卷</title>
    </head>
</html>
<style type="text/css">
    body{margin:0px;padding-top:15px; background:#eee;-moz-box-shadow:0 4px 4px #ccc inset;-webkit-box-shadow:0 4px 4px #ccc inset;box-shadow:0 4px 4px #ccc inset;}
    a{-webkit-tap-highlight-color: rgba(0, 0, 0, 0);color: #000; text-decoration:none;}
    .sort{height:44px; border-bottom:1px solid #eee; background:#FFF; font-size:14px;display:-moz-box;display:-webkit-box;display:box;-moz-box-orient:horizontal;-webkit-box-orient:horizontal;box-orient:horizontal;}
    .sort01{ background:url(images/app/tj_06.png) no-repeat right top #FFFFFF;}
    .sort02{ background:url(images/app/tj_06.png) no-repeat right bottom #FFFFFF;}
    .sort span{ line-height:44px; display:block;}
    .sort span:active{background:#f7f7f7;}
    .sort .span1{width:70px;color:#3a3a3a; padding-left:20px;}
    .sort .span5{padding-left:20px;color:#3a3a3a;}
    .sort .span2{width:46px;color:#ccc; border:1px solid #f5f5f5; background:none;display:block;outline: none;height:22px;font-size:14px; margin-top:10px;}
    .sort .span4{width:120px;color:#ccc; border:1px solid #f5f5f5; background:none;display:block;outline: none;height:22px;font-size:14px; margin-top:6px;}
    .sort .span3{width:20px; text-align:center; color:#3a3a3a;margin-top: 2px;}
    .tit{padding-left:20px;height:44px; line-height:44px; background:#eee; font-size:14px;}
    .submit{display:block; width:300px;margin:20px auto;height:45px; font-size:16px; color:#FFF; background:#00aeff;border:0px;-moz-border-radius:10px;-webkit-border-radius:10px;border-radius:10px;}
    .submit:active{background:#5ebce8;}
</style>
<script language="javascript">
    function do1(sth) {
        if (sth.className == "sort sort01") {
            sth.className = "sort sort02";
            sth.getElementsByTagName("input")[0].value = "1";
        } else {
            sth.className = "sort sort01";
            sth.getElementsByTagName("input")[0].value = "0";
        }
    }
</script>

<form action="" method="post"><!--给name赋值，可以直接提交后台处理。-->
    <div class="tit">
        个人基本情况
    </div>
    <div class="sort">
        <span class="span1">出生日期</span>
        <select class="span2" name="Q[1][ans][year]" style="width: 65px">
            <?php
            for ($i=1930;$i<2014;$i++) {
            ?>
            <option value="<?=$i?>" <?=$i==1970?"selected":""?>>
                <?=$i?>
            </option>
            <?php
            }
            ?>
            
        </select>
        <input class="span2" name="Q[1][ans][month]" type="hidden" value="1">
        <input class="span2" name="Q[1][ans][day]" type="hidden" value="1">


    </div>
    <div class="sort">
        <span class="span1">身&nbsp;&nbsp;&nbsp;&nbsp;高</span>
        <select class="span2" name="Q[16][ans][height]" style="width: 60px">
            <?php
            for ($i=130;$i<241;$i++) {
            ?>
            <option value="<?=$i?>" <?=$i==170?"selected":""?>>
                <?=$i?>
            </option>
            <?php
            }
            ?>
            
        </select><span class="span3">cm</span>

    </div>
    <div class="sort">
        <span class="span1">体&nbsp;&nbsp;&nbsp;&nbsp;重</span>
        <select class="span2" name="Q[16][ans][weight]" style="width: 60px">
            <?php
            for ($i=30;$i<131;$i++) {
            ?>
            <option value="<?=$i?>" <?=$i==65?"selected":""?>>
                <?=$i?>
            </option>
            <?php
            }
            ?>
            
        </select>

        <span class="span3">kg</span>
    </div>
    <div class="sort">
        <span class="span1">性&nbsp;&nbsp;&nbsp;&nbsp;别</span>
        <select class="span2" name="Q[2][ans][gender]" type="select"  style="width: 50px">
        <option value="1">男</option>
        <option value="2">女</option>
        </select>
        
    </div>
    <div class="sort">
        <span class="span1">腰&nbsp;&nbsp;&nbsp;&nbsp;围</span>
        <select class="span2" name="Q[5][ans][wc]" style="width: 60px">
            <?php
            for ($i=25;$i<151;$i++) {
            ?>
            <option value="<?=$i?>" <?=$i==60?"selected":""?>>
                <?=$i?>
            </option>
            <?php
            }
            ?>
            
        </select>
        <span class="span3">cm</span>
    </div>
    <div class="tit">
        个人疾病（史）
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">高&nbsp;血&nbsp;压</span>
        <input name="Q[4][ans][hypertension_history]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">冠&nbsp;心&nbsp;病</span>
        <input name="Q[4][ans][coronary_history]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">糖&nbsp;尿&nbsp;病</span>
        <input name="Q[4][ans][diabetes_history]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">高&nbsp;血&nbsp;脂</span>
        <input name="Q[4][ans][hyperlipemia_history]" type="hidden" value="0">
    </div>
    <div class="tit">
        家族疾病史
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">高&nbsp;血&nbsp;压</span>
        <input name="Q[3][ans][hypertension_family_history]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">冠&nbsp;心&nbsp;病</span>
        <input name="Q[3][ans][coronary_family_history]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">糖&nbsp;尿&nbsp;病</span>
        <input name="Q[3][ans][diabetes_family_history]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">脑&nbsp;卒&nbsp;中</span>
        <input name="Q[3][ans][stroke_family_history]" type="hidden" value="0">
    </div>
    <div class="tit">
        生活习惯
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">吸烟</span>
        <input name="Q[6][ans][smoking]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">存在被动吸烟的情况</span>
        <input name="Q[6][ans][smoked]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">服用过避孕药</span>
        <input name="Q[8][ans][contraceptive]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">打鼾</span>
        <input name="Q[9][ans][hulu]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">每周运动未达3次且每次30分钟以上</span>
        <input name="Q[10][ans][sports]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">每日静坐时间超过8小时</span>
        <input name="Q[11][ans][stillsit]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">平均每日饮酒量超过60克</span>
        <input name="Q[12][ans][drink]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">平均每日摄入肉类超过4两</span>
        <input name="Q[14][ans][meat]" type="hidden" value="0">
    </div>

    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">平均每日摄入蔬菜水果不足8两</span>
        <input name="Q[13][ans][fruit]" type="hidden" value="0">
    </div>
    <div class="sort sort01" onclick="do1(this)">
        <span class="span5">口味偏咸且常吃腌制食品</span>
        <input name="Q[15][ans][salt_food]" type="hidden" value="0">
    </div>
    <input type="submit" class="submit"/>
</form>