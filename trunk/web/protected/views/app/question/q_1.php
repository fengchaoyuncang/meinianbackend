
<style type="text/css">
.sel_wrap1{
    display: inline-block;
    position:relative;
    width:22%;
    height:1.5em;
    background:#fff url(images/png.png) no-repeat right center;
    border:1px solid #a0a0a0;
    border-radius:2px;
    -webkit-border-radius:2px;
    -moz-border-radius:2px;
    cursor:pointer;
    _filter:alpha(opacity=0);
}
.sel_wrap1 span{
    display: inline-block;
    font-size: 0.8em;
    /*line-height: 2em;*/
    padding:0.4em 0.4em;
}
.sel_wrap1 .select{
    width:100%;
    height:1.5em;
    z-index:4;
    font-size: 1em;
    position:absolute;
    top:0;
    left:0;
    margin:0;
    padding:0;
    opacity:0;
    cursor:pointer;
}

</style>
<li class="que-line-select" id="<?=$que->id?>"><!--
   <span class="que-line-head" id="birth_top">
        <span class="que-line-head-title">Qusetion&nbsp;<?=$pagecount+1?></span>
        <?=$que->title?>
      
    </span>
     
  --><input name="Q[<?=$que->id?>][id]" type="hidden" value="<?=$que->id?>" />

    <div class="que-line-content" id="select_content">
        <div class="sel_wrap1" id="sel_wrap1">
            <span>1980</span>
            <select name="Q[1][ans][year]" id="selYear" class="select">
            </select>
        </div>
        <span>年</span>
        <div class="sel_wrap1" id="sel_wrap2">
            <span>1</span>
            <select name="Q[1][ans][month]" id="selMonth" class="select">
            </select>
        </div>
        <span>月</span>
        <div class="sel_wrap1" id="sel_wrap3">
            <span>1</span>
            <select name="Q[1][ans][day]" id="selDay" class="select">
            </select>
        </div>
        <span>日</span>
    </div>
</li>
<script type="text/javascript">
function DateSelector(selYear, selMonth, selDay) { 
this.selYear = selYear; 
this.selMonth = selMonth; 
this.selDay = selDay; 
this.selYear.Group = this; 
this.selMonth.Group = this; 
// 给年份、月份下拉菜单添加处理onchange事件的函数 
if (window.document.all != null) // IE 
{ 
this.selYear.attachEvent("onchange", DateSelector.Onchange); 
this.selMonth.attachEvent("onchange", DateSelector.Onchange); 
} 
else // Firefox 
{ 
this.selYear.addEventListener("change", DateSelector.Onchange, false); 
this.selMonth.addEventListener("change", DateSelector.Onchange, false); 
} 
if (arguments.length == 4) // 如果传入参数个数为4，最后一个参数必须为Date对象 
this.InitSelector(arguments[3].getFullYear(), arguments[3].getMonth() + 1, arguments[3].getDate()); 
else if (arguments.length == 6) // 如果传入参数个数为6，最后三个参数必须为初始的年月日数值 
this.InitSelector(arguments[3], arguments[4], arguments[5]); 
else // 默认使用当前日期 
{ 
var dt = new Date(); 
this.InitSelector(dt.getFullYear(), dt.getMonth() + 1, dt.getDate()); 
} 
} 
// 增加一个最大年份的属性 
DateSelector.prototype.MinYear = 1933; 
// 增加一个最大年份的属性 
DateSelector.prototype.MaxYear = (new Date()).getFullYear(); 
// 初始化年份 
DateSelector.prototype.InitYearSelect = function () { 
// 循环添加OPION元素到年份select对象中 
for (var i = this.MaxYear; i >= this.MinYear; i--) { 
// 新建一个OPTION对象 
var op = window.document.createElement("OPTION"); 
// 设置OPTION对象的值 
op.value = i; 
// 设置OPTION对象的内容 
op.innerHTML = i; 
// 添加到年份select对象 
this.selYear.appendChild(op); 
} 
} 
// 初始化月份 
DateSelector.prototype.InitMonthSelect = function () { 
// 循环添加OPION元素到月份select对象中 
for (var i = 1; i < 13; i++) { 
// 新建一个OPTION对象 
var op = window.document.createElement("OPTION"); 
// 设置OPTION对象的值 
op.value = i; 
// 设置OPTION对象的内容 
op.innerHTML = i; 
// 添加到月份select对象 
this.selMonth.appendChild(op); 
} 
} 
// 根据年份与月份获取当月的天数 
DateSelector.DaysInMonth = function (year, month) { 
var date = new Date(year, month, 0); 
return date.getDate(); 
} 
// 初始化天数 
DateSelector.prototype.InitDaySelect = function () { 
// 使用parseInt函数获取当前的年份和月份 
var year = parseInt(this.selYear.value); 
var month = parseInt(this.selMonth.value); 
// 获取当月的天数 
var daysInMonth = DateSelector.DaysInMonth(year, month); 
// 清空原有的选项 
this.selDay.options.length = 0; 
// 循环添加OPION元素到天数select对象中 
for (var i = 1; i <= daysInMonth; i++) { 
// 新建一个OPTION对象 
var op = window.document.createElement("OPTION"); 
// 设置OPTION对象的值 
op.value = i; 
// 设置OPTION对象的内容 
op.innerHTML = i; 
// 添加到天数select对象 
this.selDay.appendChild(op); 
} 
} 
// 处理年份和月份onchange事件的方法，它获取事件来源对象（即selYear或selMonth） 
// 并调用它的Group对象（即DateSelector实例，请见构造函数）提供的InitDaySelect方法重新初始化天数 
// 参数e为event对象 
DateSelector.Onchange = function (e) { 
var selector = window.document.all != null ? e.srcElement : e.target; 
selector.Group.InitDaySelect(); 
} 
// 根据参数初始化下拉菜单选项 
DateSelector.prototype.InitSelector = function (year, month, day) { 
// 由于外部是可以调用这个方法，因此我们在这里也要将selYear和selMonth的选项清空掉 
// 另外因为InitDaySelect方法已经有清空天数下拉菜单，因此这里就不用重复工作了 
this.selYear.options.length = 0; 
this.selMonth.options.length = 0; 
// 初始化年、月 
this.InitYearSelect(); 
this.InitMonthSelect(); 
// 设置年、月初始值 
this.selYear.selectedIndex = this.MaxYear - year; 
this.selMonth.selectedIndex = month - 1; 
// 初始化天数 
this.InitDaySelect(); 
// 设置天数初始值 
this.selDay.selectedIndex = day - 1; 
}  
var selYear = window.document.getElementById("selYear"); 
var selMonth = window.document.getElementById("selMonth"); 
var selDay = window.document.getElementById("selDay"); 
// 新建一个DateSelector类的实例，将三个select对象传进去 
new DateSelector(selYear, selMonth, selDay, 1980, 1, 1); 
// 也可以试试下边的代码 
// var dt = new Date(2004, 1, 29); 
// new DateSelector(selYear, selMonth ,selDay, dt); 
function $(x){
return document.getElementById(x);
}
var sel1=$("selYear");
var sel2=$("selMonth");
var sel3=$("selDay");
var wrap1=$("sel_wrap1");
var wrap2=$("sel_wrap2");
var wrap3=$("sel_wrap3");
var result1 =wrap1.getElementsByTagName("span");
var result2 =wrap2.getElementsByTagName("span");
var result3 =wrap3.getElementsByTagName("span");
wrap1.onmouseover=function(){
this.style.backgroundColor="#fff";
}
wrap2.onmouseover=function(){
this.style.backgroundColor="#fff";
}
wrap3.onmouseover=function(){
this.style.backgroundColor="#fff";
}
wrap1.onmouseout=function(){
this.style.backgroundColor="#fafafa";
}
wrap2.onmouseout=function(){
this.style.backgroundColor="#fafafa";
}
wrap3.onmouseout=function(){
this.style.backgroundColor="#fafafa";
}
sel1.onchange=function(){
var opt=this.getElementsByTagName("option");
var len=opt.length;
for(i=0;i<len;i++){
  if(opt[i].selected==true){
    x=opt[i].innerHTML;
  }
}
result1[0].innerHTML=x;
}
sel2.onchange=function(){
var opt=this.getElementsByTagName("option");
var len=opt.length;
for(i=0;i<len;i++){
  if(opt[i].selected==true){
    x=opt[i].innerHTML;
  }
}
result2[0].innerHTML=x;
}
sel3.onchange=function(){
var opt=this.getElementsByTagName("option");
var len=opt.length;
for(i=0;i<len;i++){
  if(opt[i].selected==true){
    x=opt[i].innerHTML;
  }
}
result3[0].innerHTML=x;
}
</script>
