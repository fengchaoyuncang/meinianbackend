<style type="text/css">
.sel_wrap22{
    display: inline-block;
    position:relative;
    width:22%;
    height:1.5em;
    background:#fff url(images/png.png) no-repeat right center;
    border:1px solid #a0a0a0;
    border-radius:2px;
    -webkit-border-radius:2px;
    -moz-border-radius:2px;
    cursor:pointer;
    _filter:alpha(opacity=0);
}
.sel_wrap22 span{
    display: inline-block;
    font-size: 0.8em;
    /*line-height: 2em;*/
    padding:0.4em 0.4em;
}
.sel_wrap22 .select{
    width:100%;
    height:1.5em;
    z-index:4;
    font-size: 1em;
    position:absolute;
    top:0;
    left:0;
    margin:0;
    padding:0;
    opacity:0;
    cursor:pointer;
}
</style>
<li class="que-line" id="<?=$que->id?>">
    <span class="que-line-head">
<!--        <span class="que-line-head-title">Qusetion&nbsp;<?=$pagecount+1?></span>-->
    <?=$que->title?>
    </span>
    <input name="Q[<?=$que->id?>][id]" type="hidden" value="<?=$que->id?>" />
    <ul class="que-line-content">
        <li class="sel_wrap22" id="sel_wrap22">
            <span>男</span>
            <select name="Q[2][ans][sex]" class="select" id="c_select22">
                <option value="男">男</option>
                <option value="女">女</option>
            </select>
        </li>
    </ul>
</li>
<script type="text/javascript">
function $(x){
    return document.getElementById(x);
  }
  var sel22=$("c_select22");
  var wrap22=$("sel_wrap22");
  var result22 =wrap22.getElementsByTagName("span");
  wrap22.onmouseover=function(){
    this.style.backgroundColor="#fff";
  }
  wrap22.onmouseout=function(){
    this.style.backgroundColor="#fafafa";
  }
  sel22.onchange=function(){
    var opt=this.getElementsByTagName("option");
    var len=opt.length;
    for(i=0;i<len;i++){
      if(opt[i].selected==true){
        x=opt[i].innerHTML;
      }
    }
    result22[0].innerHTML=x;
  }
</script>     