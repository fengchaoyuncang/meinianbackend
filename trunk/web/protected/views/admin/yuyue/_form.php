<?php
/* @var $this YuyueController */
/* @var $model WeixinYuyue */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'weixin-yuyue-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'realname'); ?>
		<?php echo $form->textField($model,'realname',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'realname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'examno'); ?>
		<?php echo $form->textField($model,'examno',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'examno'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'identity_no'); ?>
		<?php echo $form->textField($model,'identity_no',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'identity_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'examcenter'); ?>
		<?php echo $form->textField($model,'examcenter',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'examcenter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'booktime'); ?>
		<?php echo $form->textField($model,'booktime'); ?>
		<?php echo $form->error($model,'booktime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tjpackage'); ?>
		<?php echo $form->textField($model,'tjpackage',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'tjpackage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->