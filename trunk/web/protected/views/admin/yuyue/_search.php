<?php
/* @var $this YuyueController */
/* @var $model WeixinYuyue */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realname'); ?>
		<?php echo $form->textField($model,'realname',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'examno'); ?>
		<?php echo $form->textField($model,'examno',array('size'=>15,'maxlength'=>15)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'identity_no'); ?>
		<?php echo $form->textField($model,'identity_no',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'examcenter'); ?>
		<?php echo $form->textField($model,'examcenter',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'booktime'); ?>
		<?php echo $form->textField($model,'booktime'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tjpackage'); ?>
		<?php echo $form->textField($model,'tjpackage',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->