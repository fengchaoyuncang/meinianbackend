<?php
/* @var $this YuyueController */
/* @var $model WeixinYuyue */

$this->breadcrumbs=array(
	'威信预约'=>array('admin'),
	'创建',
);

$this->menu=array(
//	array('label'=>'List WeixinYuyue', 'url'=>array('index')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>创建</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>