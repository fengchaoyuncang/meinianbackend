<?php
/* @var $this YuyueController */
/* @var $model WeixinYuyue */

$this->breadcrumbs=array(
	'Weixin Yuyues'=>array('index'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List WeixinYuyue', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>查看微信预约 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'header'=>'id',
			'name'=>'id',
			'htmlOptions'=>array('width'=>'20px')
		),
		'phone',
		'realname',
		'examno',
		'identity_no',
		'examcenter',
		'booktime',
		'tjpackage',
		'status',
	),
)); ?>
