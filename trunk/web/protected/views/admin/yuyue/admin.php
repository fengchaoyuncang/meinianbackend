<?php
/* @var $this YuyueController */
/* @var $model WeixinYuyue */

$this->breadcrumbs=array(
	'微信预约'=>array('admin'),
	'管理',
);

$this->menu=array(
//	array('label'=>'List WeixinYuyue', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#weixin-yuyue-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>微信预约管理</h1>
<p>
你可以用(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>)进行高级搜索
</p>

<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'weixin-yuyue-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'header'=>'id',
			'name'=>'id',
			'htmlOptions'=>array(
				'width'=>'30px'
			),
		),
		array(
			'header'=>'电话',
			'name'=>'phone',
			'htmlOptions'=>array(
				'width'=>'150px'
			),
		),
		array(
			'header'=>'姓名',
			'name'=>'realname',
			'htmlOptions'=>array(
				'width'=>'100px'
			),
		),
		'examno',
		'identity_no',
		'examcenter',
		'booktime',
		/*
		'tjpackage',
		'status',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{view}{update}{delete}',
			'htmlOptions' => array('width' => '30px'),
		),
	),
)); ?>
