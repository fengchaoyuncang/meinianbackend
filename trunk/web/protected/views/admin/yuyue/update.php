<?php
/* @var $this YuyueController */
/* @var $model WeixinYuyue */

$this->breadcrumbs=array(
	'微信预约'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'更新',
);

$this->menu=array(
//	array('label'=>'List WeixinYuyue', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>更新微信预约 <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>