<?php
/* @var $this YuyueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Weixin Yuyues',
);

$this->menu=array(
	array('label'=>'Create WeixinYuyue', 'url'=>array('create')),
	array('label'=>'Manage WeixinYuyue', 'url'=>array('admin')),
);
?>

<h1>Weixin Yuyues</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
