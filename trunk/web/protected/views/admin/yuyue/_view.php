<?php
/* @var $this YuyueController */
/* @var $data WeixinYuyue */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realname')); ?>:</b>
	<?php echo CHtml::encode($data->realname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('examno')); ?>:</b>
	<?php echo CHtml::encode($data->examno); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('identity_no')); ?>:</b>
	<?php echo CHtml::encode($data->identity_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('examcenter')); ?>:</b>
	<?php echo CHtml::encode($data->examcenter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('booktime')); ?>:</b>
	<?php echo CHtml::encode($data->booktime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tjpackage')); ?>:</b>
	<?php echo CHtml::encode($data->tjpackage); ?>
	<br />

	<?php 
        
        /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />
         * 

	*/ ?>

</div>