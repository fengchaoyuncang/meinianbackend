<?php
/* @var $this SubcenterController */
/* @var $model B2bSubCenter */

$this->breadcrumbs=array(
	'体检中心'=>array('admin'),
	'体检中心',
	'创建',
);

$this->menu=array(
	//array('label'=>'体检中心列表', 'url'=>array('index')),
	array('label'=>'管理体检中心', 'url'=>array('admin')),
);
?>

<h1>创建体检中心</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>