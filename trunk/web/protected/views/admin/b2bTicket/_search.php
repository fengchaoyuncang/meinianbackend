<?php
/* @var $this B2bTicketController */
/* @var $model B2bTicket */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activecode'); ?>
		<?php echo $form->textField($model,'activecode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	

	<div class="row">
		<?php echo $form->label($model,'companyName'); ?>
		<?php echo $form->textField($model,'companyName'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'groupid'); ?>
		<?php echo $form->textField($model,'groupid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'identity_no'); ?>
		<?php echo $form->textField($model,'identity_no',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realname'); ?>
		<?php echo $form->textField($model,'realname',array('size'=>60,'maxlength'=>200)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'companyName'); ?>
		<?php echo $form->textField($model,'companyName',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tstatus'); ?>
	
		<?php //echo $form->dropDownList($model,'tstatus',array(''=>'','1'=>'未激活','2'=>'预约处理中，可取消','3'=>'预约确认，电话取消','4'=>'体检完成','5'=>'取消预约')); ?>
		<?php 
			echo $form->dropDownList($model,'tstatus',
				array(
					''=>'全部',
					B2bTicket::UNACTIVATED=>'未激活',
					B2bTicket::YUYUE_PROCESSING=>'预约处理中，可取消',
					B2bTicket::YUYUE_CONFIRM=>'预约确认，电话取消',
					B2bTicket::FINISHED=>'体检完成',
					B2bTicket::YUYUE_CANCEL=>'取消预约'
					)	
			); 
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packageName'); ?>
		<?php echo $form->textField($model,'packageName'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'centerName'); ?>
		<?php echo $form->textField($model,'centerName'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'examdate'); ?>
		<?php echo $form->textField($model,'examdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startdate'); ?>
		<?php echo $form->textField($model,'startdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enddate'); ?>
		<?php echo $form->textField($model,'enddate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('搜索'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->