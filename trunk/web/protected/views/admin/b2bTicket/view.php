<?php
/* @var $this B2bTicketController */
/* @var $model B2bTicket */

$this->breadcrumbs = array(
    '预约管理' => array('admin'),
    $model->id,
);

$this->menu = array(
    //array('label'=>'创建订单', 'url'=>array('create')),
    array('label' => '修改订单', 'url' => array('update', 'id' => $model->id)),
    //array('label'=>'删除订单', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label' => '管理订单', 'url' => array('admin')),
);
?>

<h1>查看预约</h1>
<?php if (isset($_GET['note'])) {
    echo $_GET['note'];
}
?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
//        'id',
        'codetype',
        'activecode',
//        'uid',
//        'companyid',
//        'groupid',
        'identity_no',
        'realname',
        'telephone',
        'tstatus',
        'package.name',
        'center.name',
        'examdate',
        'startdate',
        'enddate',
//		'create_at',
//		'update_at',
    ),
)); ?>
