<?php
/* @var $this B2bTicketController */
/* @var $model B2bTicket */

$this->breadcrumbs = array(
//	'订单管理'=>array('admin'),
    '预约管理'
);

$this->menu = array(
//    array('label' => '创建订单', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-ticket-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>预约管理</h1>

<p>
    你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) 来指明搜搜条件。
</p>

<?php //echo CHtml::link('高级搜索', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php
//var_dump($model->codetype);exit;
$this->widget('bootstrap.widgets.TbGridView', array(

    'id' => 'b2b-ticket-grid',
    'dataProvider' => $searchmethod,
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('width' => '50px'),
        ),
        array(
            'name' => 'codetype',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->codetype===null?"":B2bTicket::getCodetypeDesc($data->codetype);',
            'filter' => CHtml::dropDownList('B2bTicket[codetype]', $model->codetype,
                    array(
                        '' => '全部',
                        B2bTicket::SHOPINGCODERESERVATION => '商城购买预约',
//                        B2bTicket::FCODERESERVATION => 'F码预约',
                        B2bTicket::NOCODERESERVATION => '直接预约',
                    )
                ),
        ),
        array(
            'name' => 'packageName',
            'value' => '$data->package===null?"":$data->package->name;',
            'htmlOptions' => array('width' => '160px'),
        ),
        'realname',
        array(
            'name' => 'companyName',
            'value' => '$data->company===null?"":$data->company->name;',
            'htmlOptions' => array('width' => '100px'),
        ),
        /*'groupid',*/
        array(
            'name' => 'activecode',
            'value' => '$data->activecode===null?"":B2bTicket::getActivecodeDesc($data->activecode);',
            'htmlOptions' => array('width' => '100px'),
        ),
//        'activecode',
        /*
        'realname',
        'telephone',*/
        array(
            'name' => 'tstatus',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->tstatus===null?"":B2bTicket::getStatusDesc($data->tstatus)',
            'filter' => CHtml::dropDownList('B2bTicket[tstatus]', $model->tstatus,
                    array(
                        '' => '全部',
                        B2bTicket::UNACTIVATED => '未激活',
                        B2bTicket::YUYUE_PROCESSING => '预约处理中，可取消',
                        B2bTicket::YUYUE_CONFIRM => '预约确认，电话取消',
                        B2bTicket::FINISHED => '体检完成',
                        B2bTicket::YUYUE_CANCEL => '取消预约',
                    )
                ),
        ),
        /*
		'centerid',
		'examdate',
		'startdate',
		'enddate',
		'create_at',
		'update_at',
		*/
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}{delete}',
            'htmlOptions' => array('width' => '30px'),
        ),
    ),
)); ?>
