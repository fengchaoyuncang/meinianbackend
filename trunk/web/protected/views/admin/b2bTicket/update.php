<?php
/* @var $this B2bTicketController */
/* @var $model B2bTicket */

$this->breadcrumbs=array(
	$model->id=>array('view','id'=>$model->id),
	'修改订单',
);

$this->menu=array(
	//array('label'=>'订单列表', 'url'=>array('index')),
	//array('label'=>'创建订单', 'url'=>array('create')),
	array('label'=>'查看订单', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理订单', 'url'=>array('admin')),
);
?>

<h1>修改预约</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>