<?php
/* @var $this B2bTicketController */
/* @var $model B2bTicket */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'b2b-ticket-form',
    'enableAjaxValidation' => false,
)); ?>

<p class="note">带有 <span class="required">*</span>为必填项</p>

<?php echo $form->errorSummary($model); ?>

<div class="row">
<!--    --><?php //echo $form->labelEx($model, 'codetype'); ?>
<!--    --><?php //switch ($model->codetype) {
//        case B2bTicket::SHOPINGCODERESERVATION:
//            echo $form->textField($model, 'codetype', array('class' => 'input input_r input_pryk', 'value' => '商城购买预约'));
//            break;
//        case B2bTicket::FCODERESERVATION:
//            echo $form->textField($model, 'codetype', array('class' => 'input input_r input_pryk', 'value' => 'F码团购预约'));
//            break;
//
//        case B2bTicket::NOCODERESERVATION:
//            echo $form->textField($model, 'codetype', array('class' => 'input input_r input_pryk', 'value' => '直接预约'));
//            break;
//    }
//    ?>

    <?php echo $form->error($model, 'codetype'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model, 'activecode'); ?>
    <?php echo $form->textField($model, 'activecode', array('size' => 20, 'maxlength' => 20)); ?>
    <?php echo $form->error($model, 'activecode'); ?>
</div>

<!--<div class="row">-->
<!--    --><?php //echo $form->labelEx($model, 'uid'); ?>
<!--    --><?php //echo $form->textField($model, 'uid'); ?>
<!--    --><?php //echo $form->error($model, 'uid'); ?>
<!--</div>-->

<!--<div class="row">-->
<!--    --><?php //echo $form->labelEx($model, 'companyid'); ?>
<!--    --><?php //echo $form->textField($model, 'companyid'); ?>
<!--    --><?php //echo $form->error($model, 'companyid'); ?>
<!--</div>-->

<!--<div class="row">-->
<!--    --><?php //echo $form->labelEx($model, 'groupid'); ?>
<!--    --><?php //echo $form->textField($model, 'groupid'); ?>
<!--    --><?php //echo $form->error($model, 'groupid'); ?>
<!--</div>-->

<div class="row">
    <?php echo $form->labelEx($model, 'identity_no'); ?>
    <?php echo $form->textField($model, 'identity_no', array('size' => 50, 'maxlength' => 50)); ?>
    <?php echo $form->error($model, 'identity_no'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'realname'); ?>
    <?php echo $form->textField($model, 'realname', array('size' => 60, 'maxlength' => 200)); ?>
    <?php echo $form->error($model, 'realname'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'telephone'); ?>
    <?php echo $form->textField($model, 'telephone', array('size' => 20, 'maxlength' => 20)); ?>
    <?php echo $form->error($model, 'telephone'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'tstatus'); ?>
    <?php
    echo $form->dropDownList($model, 'tstatus',
        array(
            B2bTicket::UNACTIVATED => '未激活',
            B2bTicket::YUYUE_PROCESSING => '预约处理中，可取消',
            B2bTicket::YUYUE_CONFIRM => '预约确认，电话取消',
            B2bTicket::FINISHED => '体检完成',
            B2bTicket::YUYUE_CANCEL => '取消预约'
        )
    );
    ?>
    <?php echo $form->error($model, 'tstatus'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'packageid'); ?>
    <!--    --><?php //echo $form->textField($model, 'packageid'); ?>
    <?php
    if (!empty($model->packageid)) {
        $pack_obj = B2bPackage::model()->findByAttributes(array("id" => $model->packageid));
        if($pack_obj != null){
            $packagename = $pack_obj->name;
        }else{
            $packagename = "无";
        }
        echo $form->textField($model, '', array('class' => 'input input_r input_pryk', 'value' => "{$packagename}"));

    }
    ?>
    <?php echo $form->error($model, 'packageid'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'centerid'); ?>
<!--    --><?php //echo $form->textField($model, 'centerid'); ?>
    <?php
    if (!empty($model->centerid)) {
        $subcenter_obj = B2bSubCenter::model()->findByAttributes(array("id" => $model->centerid));
        if($subcenter_obj != null){
            $subcenter_name = $subcenter_obj->name;
        }else{
            $subcenter_name = "无";
        }
        echo $form->textField($model, '', array('class' => 'input input_r input_pryk', 'value' => "{$subcenter_name}"));

    }
    ?>
    <?php echo $form->error($model, 'centerid'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'examdate'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'B2bTicket[examdate]',
        'attribute' => 'B2bTicket[examdate]',
        'value' => $model->isNewRecord ? date("Y-m-d") : $model->examdate,
        // additional javascript options for the date picker plugin
        'options' => array(
            'altField' => 'examdate',
            'dateFormat' => 'yy-mm-dd',
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            'autocomplete' => 'true'
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;background-color:white;color:#000;',
        ),
    ));
    ?>

    <?php //echo $form->textField($model,'examdate'); ?>
    <?php echo $form->error($model, 'examdate'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'startdate'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'B2bTicket[startdate]',
        'value' => $model->isNewRecord ? date("Y-m-d") : $model->startdate,
        // additional javascript options for the date picker plugin
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;background-color:white;color:#000;',
        ),
    ));
    ?>
    <?php //echo $form->textField($model,'startdate'); ?>
    <?php echo $form->error($model, 'startdate'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'enddate'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'B2bTicket[enddate]',
        'value' => $model->isNewRecord ? date("Y-m-d") : $model->enddate,
        // additional javascript options for the date picker plugin
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;background-color:white;color:#000;',
        ),
    ));
    ?>
    <?php //echo $form->textField($model,'enddate'); ?>
    <?php echo $form->error($model, 'enddate'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'create_at'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'B2bTicket[create_at]',
        'value' => $model->isNewRecord ? date("Y-m-d") : $model->create_at,
        // additional javascript options for the date picker plugin
        'options' => array(
            'language' => '',
            'dateFormat' => 'yy-mm-dd',
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;background-color:white;color:#000;',
        ),
    ));
    ?>
    <?php //echo $form->textField($model,'create_at'); ?>
    <?php echo $form->error($model, 'create_at'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'update_at'); ?>
    <?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'B2bTicket[update_at]',
        'value' => $model->isNewRecord ? date("Y-m-d") : $model->update_at,
        // additional javascript options for the date picker plugin
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;background-color:white;color:#000;',
        ),
    ));
    ?>
    <?php //echo $form->textField($model,'update_at'); ?>
    <?php echo $form->error($model, 'update_at'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->