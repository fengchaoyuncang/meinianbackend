<?php
/* @var $this ExtuserController */
/* @var $model B2bExternalUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-external-user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有<span class="required">*</span> 项为必填项.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'realname'); ?>
		<?php echo $form->textField($model,'realname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'realname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_certificate'); ?>
		<?php echo $form->textField($model,'customer_certificate'); ?>
		<?php echo $form->error($model,'customer_certificate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_no'); ?>
		<?php echo $form->textField($model,'customer_no',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'customer_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_id'); ?>
		<?php echo $form->textField($model,'company_id'); ?>
		<?php echo $form->error($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'company_name'); ?>
		<?php echo $form->textField($model,'company_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'company_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'package_id'); ?>
		<?php echo $form->textField($model,'package_id'); ?>
		<?php echo $form->error($model,'package_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'level'); ?>
		<?php echo $form->textField($model,'level',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'level'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
		<?php echo $form->dropDownList($model,'gender',array(B2bExternalUser::GENDER_UNKNOW=>'未知',B2bExternalUser::GENDER_BOY=>'男',B2bExternalUser::GENDER_GIRL=>'女')
						
			); ?>
		<?php //echo $form->textField($model,'gender'); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'married'); ?>
		<?php echo $form->dropDownList($model,'married',array(B2bExternalUser::MARRIED_NO=>'未婚',B2bExternalUser::MARRIED_YES=>'已婚',B2bExternalUser::MARRIED_UNKNOW=>'未知')
						
			); ?>
		<?php //echo $form->textField($model,'married'); ?>
		<?php echo $form->error($model,'married'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'statuscode'); ?>
		<?php echo $form->dropDownList($model,'statuscode',
			array(B2bExternalUser::YUYUE_ENABLE=>'正常，可预约',
				B2bExternalUser::GEREN_YUYUEING=>'已预约，待确认',
				B2bExternalUser::GEREN_YUYUED=>'已预约确认',
				B2bExternalUser::TJ_FINISH=>'已体检',
				B2bExternalUser::TUAN_YUYUEING=>'团预约,待确认',
				B2bExternalUser::TUAN_YUYUED=>'团约，已确认',
				)
						
			); ?>
		<?php //echo $form->textField($model,'statuscode'); ?>
		<?php echo $form->error($model,'statuscode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_at'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			    'name'=>'B2bExternalUser[create_at]',
				'attribute' => 'B2bExternalUser[create_at]',
				'value' => $model->isNewRecord?date("Y-m-d"):$model->create_at, 
			    // additional javascript options for the date picker plugin
			    'options'=>array(
					'altField' => 'create_at',
				'dateFormat' => 'yy-mm-dd',
			        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
					'autocomplete' => 'true' 
				),
			    'htmlOptions'=>array(
			        'style'=>'height:20px;background-color:white;color:#000;',
			    ),
			));
		?>
		<?php // echo $form->textField($model,'create_at'); ?>
		<?php echo $form->error($model,'create_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_at'); ?>
		<?php //echo $form->textField($model,'update_at'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			    'name'=>'B2bExternalUser[update_at]',
				'attribute' => 'B2bExternalUser[update_at]',
				'value' => $model->isNewRecord?date("Y-m-d"):$model->create_at, 
			    // additional javascript options for the date picker plugin
			    'options'=>array(
					'altField' => 'update_at',
				'dateFormat' => 'yy-mm-dd',
			        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
					'autocomplete' => 'true' 
				),
			    'htmlOptions'=>array(
			        'style'=>'height:20px;background-color:white;color:#000;',
			    ),
			));
		?>
		<?php echo $form->error($model,'update_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->