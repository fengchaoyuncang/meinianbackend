<?php
/* @var $this ExtuserController */
/* @var $model B2bExternalUser */

$this->breadcrumbs=array(
	'企业账户管理'=>array('admin'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h3>查看用户<?php echo $model->realname; ?>详情</h3>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'realname',
		'customer_certificate',
		'customer_no',
		'company_id',
		'company_name',
		'package.name',
		'level',
		array(
			'name'=>'gender',
			'value'=>B2bExternalUser::getGenderDesc($model->gender),
		),
		'married',
		'statuscode',
		'create_at',
		'update_at',
	),
)); ?>
