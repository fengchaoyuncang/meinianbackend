<?php
/* @var $this ExtuserController */
/* @var $model B2bExternalUser */

$this->breadcrumbs=array(
	'企业用户管理'=>array('admin'),
	'管理',
);

$this->menu=array(
	//array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-external-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>企业用户管理</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	
	'id'=>'b2b-external-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'id',
			'htmlOptions'=>array('width'=>'40px'),
		),
		'realname',
//		'customer_certificate',
		'customer_no',
//		'company_id',
		'company_name',
		array(
			'name'=>'package.name',
			'value'=>'$data->package->name',
		),
		
		/*
		'level',
		'gender',
		'married',
		'statuscode',
		'create_at',
		'update_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array("style"=>"width:80px;")
		),
	),
)); ?>
