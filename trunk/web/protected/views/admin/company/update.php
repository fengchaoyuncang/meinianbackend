<?php
/* @var $this CompanyController */
/* @var $model B2bCompany */

$this->breadcrumbs=array(
	'公司管理'=>array('admin'),
	$model->name=>array('view','id'=>$model->id),
	'更新',
);

$this->menu=array(
	//array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h3>更新 <?php echo $model->name; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>