<?php
/* @var $this ExamItemController */
/* @var $model ExamItem */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID号'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'体检项目名称'); ?>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'描述'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'类型'); ?>
		<?php //echo $form->textField($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',
				array(
					ExamItem::NORMAL=>"常规",
					ExamItem::ABNORMAL=>"非常规"
				)
						
		); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('搜索'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->