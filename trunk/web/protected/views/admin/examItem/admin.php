<?php
/* @var $this ExamItemController */
/* @var $model ExamItem */

$this->breadcrumbs=array(
	'体检项目'=>array('admin'),
	'管理',
);

$this->menu=array(
	//array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exam-item-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>管理</h1>

<p>
你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) 来指明搜搜条件。
</p>

<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
	//$this->widget('zii.widgets.grid.CGridView', array(
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'exam-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'item_name',
		//'description',
		 array(   
            'header' => '描述',  
            'name' => 'description',  
            'value' => '(substr($data->description, 0, 30) . ( (strlen($data->description) > 30) ? \'...\' : \'\' ) ) '  
        ),
          array(
			'name'=>'type',
          	'value'=>'$data->type==ExamItem::NORMAL?"常规":"非常规"',
			'htmlOptions'=>array('width'=>'100px'),
			 'filter'=>CHtml::dropDownList('ExamItem[type]',$model->type,  
                array(
                	''=>'全部',
                    ExamItem::NORMAL=>'常规',
                    ExamItem::ABNORMAL=>'非常规',
                )
		),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
		
	),
)); ?>
