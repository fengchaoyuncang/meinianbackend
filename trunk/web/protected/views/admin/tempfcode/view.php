<?php
/* @var $this B2bTicketFcodeController */
/* @var $model B2bTicketFcode */

$this->breadcrumbs=array(
	'管理F码预约'=>array('admin'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List B2bTicketFcode', 'url'=>array('index')),
//	array('label'=>'Create B2bTicketFcode', 'url'=>array('create')),
	array('label'=>'更新F码预约', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Delete B2bTicketFcode', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理列表', 'url'=>array('admin')),
);
?>

<h1>查看 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'activecode',
//		'uid',
//		'companyid',
//		'groupid',
		'identity_no',
		'realname',
		'telephone',
		'tstatus',
//		'tuangouid',
        'package.name',
        'center.name',
		'examdate',
		'startdate',
		'enddate',
		'create_at',
		'update_at',
	),
)); ?>
