<?php
/* @var $this B2bUserController */
/* @var $model B2bUser */

$this->breadcrumbs=array(
	'掌上体检用户管理'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List B2bUser', 'url'=>array('index')),
//	array('label'=>'Create B2bUser', 'url'=>array('create')),
	array('label'=>'查看订单', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理订单', 'url'=>array('admin')),
);
?>

<h1>更新 用户<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>