<?php
/* @var $this B2bUserController */
/* @var $model B2bUser */

$this->breadcrumbs=array(
	'B2b Users'=>array('admin'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List B2bUser', 'url'=>array('index')),
//	array('label'=>'Create B2bUser', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>掌上体检用户管理</h1>

<!--<p>-->
<!--    你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>-->
<!--    or <b>=</b>) 来指明搜搜条件。-->
<!--</p>-->

<?php //echo CHtml::link('高级搜索','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'b2b-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions' => array('width' => '50px'),
        ),
		'username',

//		'alias',
        array(
            'name' => 'gender',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->gender===null?"":B2bUser::getGenderDesc($data->gender);',
            'filter' => CHtml::dropDownList('B2bUser[gender]', $model->gender,
                    array(
                        '' => '全部',
                        B2bUser::GENDER_UNKNOWN => '不祥',
                        B2bUser::GENDER_MAN => '男',
                        B2bUser::GENDER_WOMAN => '女',
                    )
                ),
        ),
		'identity_no',
		'realname',
		/*
		'password',
		'center_id',
		'email',
		'telephone',
		'create_at',
		'update_at',
		*/
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
            'htmlOptions' => array('width' => '30px'),
        ),
	),
)); ?>
