<?php
/* @var $this B2bUserController */
/* @var $model B2bUser */

$this->breadcrumbs=array(
	'掌上体检用户管理'=>array('admin'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List B2bUser', 'url'=>array('index')),
//	array('label'=>'Create B2bUser', 'url'=>array('create')),
	array('label'=>'修改订单', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Delete B2bUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理订单', 'url'=>array('admin')),
);
?>

<h1>掌上体检用户<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
//		'alias',
		'gender',
		'identity_no',
		'realname',
//		'password',
//		'center_id',
		'email',
		'telephone',
		'create_at',
		'update_at',
	),
)); ?>
