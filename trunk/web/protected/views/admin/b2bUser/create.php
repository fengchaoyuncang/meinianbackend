<?php
/* @var $this B2bUserController */
/* @var $model B2bUser */

$this->breadcrumbs=array(
	'掌上体检用户管理'=>array('admin'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List B2bUser', 'url'=>array('index')),
	array('label'=>'管理订单', 'url'=>array('admin')),
);
?>

<h1>Create B2bUser</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>