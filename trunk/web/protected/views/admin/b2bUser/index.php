<?php
/* @var $this B2bUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'掌上体检用户管理',
);

$this->menu=array(
//	array('label'=>'Create B2bUser', 'url'=>array('create')),
	array('label'=>'Manage B2bUser', 'url'=>array('admin')),
);
?>

<h1>B2b Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
