<?php
/* @var $this B2bUserController */
/* @var $model B2bUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'alias'); ?>
<!--		--><?php //echo $form->textField($model,'alias',array('size'=>20,'maxlength'=>20)); ?>
<!--		--><?php //echo $form->error($model,'alias'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'gender'); ?>
        <?php echo $form->dropDownList($model, 'gender',
            array(
                B2bUser::GENDER_UNKNOWN => '不祥',
                B2bUser::GENDER_MAN => '男',
                B2bUser::GENDER_WOMAN => '女'
            )
        );?>
        <?php //echo $form->textField($model,'gender'); ?>

		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'identity_no'); ?>
		<?php echo $form->textField($model,'identity_no',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'identity_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'realname'); ?>
		<?php echo $form->textField($model,'realname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'realname'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'password'); ?>
<!--		--><?php //echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
<!--		--><?php //echo $form->error($model,'password'); ?>
<!--	</div>-->

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'center_id'); ?>
<!--		--><?php //echo $form->textField($model,'center_id',array('size'=>60,'maxlength'=>100)); ?>
<!--		--><?php //echo $form->error($model,'center_id'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telephone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_at'); ?>
		<?php echo $form->textField($model,'create_at'); ?>
		<?php echo $form->error($model,'create_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_at'); ?>
		<?php echo $form->textField($model,'update_at'); ?>
		<?php echo $form->error($model,'update_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->