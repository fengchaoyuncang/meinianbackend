<?php
/* @var $this IndicatorController */
/* @var $model Indicator */

$this->breadcrumbs=array(
	'体检指标'=>array('admin'),
	'管理',
);

$this->menu=array(
//	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#indicator-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>管理体检指标</h1>

<p>
你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) 来指明搜搜条件。
</p>

<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'indicator-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'item_id',
 		array(   
            'header' => '概述',  
            'name' => 'over_view',  
            'value' => '(substr($data->over_view, 0, 30) . ( (strlen($data->over_view) > 30) ? \'...\' : \'\' ) ) '  
        ),
        
         array(   
            'header' => '偏高原因',  
            'name' => 'over_reason',  
            'value' => '(substr($data->over_reason, 0, 30) . ( (strlen($data->over_reason) > 30) ? \'...\' : \'\' ) ) '  
        ),

         array(   
            'header' => '偏低原因',  
            'name' => 'under_reason',  
            'value' => '(substr($data->under_reason, 0, 30) . ( (strlen($data->under_reason) > 30) ? \'...\' : \'\' ) ) '  
        ),

		/*
		'cure_care',
		'expert_advice',
		'value_ranges',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
