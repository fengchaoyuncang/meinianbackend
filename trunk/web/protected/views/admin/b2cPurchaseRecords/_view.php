<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $data B2cPurchaseRecords */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::encode($data->uid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unitprice')); ?>:</b>
	<?php echo CHtml::encode($data->unitprice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalprice')); ?>:</b>
	<?php echo CHtml::encode($data->totalprice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageid')); ?>:</b>
	<?php echo CHtml::encode($data->packageid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activecodes')); ?>:</b>
	<?php echo CHtml::encode($data->activecodes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num')); ?>:</b>
	<?php echo CHtml::encode($data->num); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aliid')); ?>:</b>
	<?php echo CHtml::encode($data->aliid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serialnumber')); ?>:</b>
	<?php echo CHtml::encode($data->serialnumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_at')); ?>:</b>
	<?php echo CHtml::encode($data->update_at); ?>
	<br />

	*/ ?>

</div>