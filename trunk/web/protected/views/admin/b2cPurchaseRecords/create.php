<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $model B2cPurchaseRecords */

$this->breadcrumbs=array(
	'购买管理'=>array('admin'),
	'创建',
);

$this->menu=array(
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>创建购买记录</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>