<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'购买管理'=>array('admin'),
	'购买记录',
);

$this->menu=array(
//	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>购买记录</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
