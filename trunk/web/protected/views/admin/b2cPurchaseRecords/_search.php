<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $model B2cPurchaseRecords */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realName'); ?>
		<?php echo $form->textField($model,'realName',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unitprice'); ?>
		<?php echo $form->textField($model,'unitprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'totalprice'); ?>
		<?php echo $form->textField($model,'totalprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packageName'); ?>
		<?php echo $form->textField($model,'packageName'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activecodes'); ?>
		<?php echo $form->textField($model,'activecodes',array('size'=>60,'maxlength'=>2048)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php //echo $form->textField($model,'status'); ?>
		<?php 
			echo $form->dropDownList($model,'status',
				array(
					''=>'全部',
					B2cPurchaseRecords::NO_PAID=>'未付款',
					B2cPurchaseRecords::HAVE_PAID=>'已付款',
					)	
			);
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aliid'); ?>
		<?php echo $form->textField($model,'aliid',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'serialnumber'); ?>
		<?php echo $form->textField($model,'serialnumber',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_at'); ?>
		<?php echo $form->textField($model,'create_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_at'); ?>
		<?php echo $form->textField($model,'update_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('搜索'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->