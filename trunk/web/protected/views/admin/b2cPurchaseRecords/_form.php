<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $model B2cPurchaseRecords */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2c-purchase-records-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有 <span class="required">*</span>为必选项</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'uid'); ?>
		<?php echo $form->textField($model,'uid',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unitprice'); ?>
		<?php echo $form->textField($model,'unitprice'); ?>
		<?php echo $form->error($model,'unitprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'totalprice'); ?>
		<?php echo $form->textField($model,'totalprice'); ?>
		<?php echo $form->error($model,'totalprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'packageid'); ?>
		<?php echo $form->textField($model,'packageid'); ?>
		<?php echo $form->error($model,'packageid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'activecodes'); ?>
		<?php echo $form->textField($model,'activecodes',array('size'=>60,'maxlength'=>2048)); ?>
		<?php echo $form->error($model,'activecodes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num'); ?>
		<?php echo $form->textField($model,'num'); ?>
		<?php echo $form->error($model,'num'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'status'); ?>
<!--		--><?php //echo $form->textField($model,'status'); ?>
<!--		--><?php //echo $form->error($model,'status'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'aliid'); ?>
		<?php echo $form->textField($model,'aliid',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'aliid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serialnumber'); ?>
		<?php echo $form->textField($model,'serialnumber',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'serialnumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_at'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			    'name'=>'B2cPurchaseRecords[create_at]',
				'value' => $model->isNewRecord?date("Y-m-d"):$model->create_at,
			    // additional javascript options for the date picker plugin
			    'options'=>array(
					'dateFormat' => 'yy-mm-dd',
			        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
			    ),
			    'htmlOptions'=>array(
			        'style'=>'height:20px;background-color:white;color:#000;',
			    ),
			));
		?>
		<?php //echo $form->textField($model,'create_at'); ?>
		<?php echo $form->error($model,'create_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_at'); ?>
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			    'name'=>'B2cPurchaseRecords[update_at]',
				'value' => $model->isNewRecord?date("Y-m-d"):$model->update_at,
			    // additional javascript options for the date picker plugin
			    'options'=>array(
					'dateFormat' => 'yy-mm-dd',
			        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
			    ),
			    'htmlOptions'=>array(
			        'style'=>'height:20px;background-color:white;color:#000;',
			    ),
			));
		?>
		<?php //echo $form->textField($model,'update_at'); ?>
		<?php echo $form->error($model,'update_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->