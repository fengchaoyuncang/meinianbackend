<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $model B2cPurchaseRecords */

$this->breadcrumbs=array(
	'购买管理'=>array('admin'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>查看购买记录 #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'uid',
		array(
			'name'=>'unitprice',
			'value'=>$model->unitprice/100,
		),
		array(
			'name'=>'totalprice',
			'value'=>$model->totalprice/100,
		),
		'packageid',
		array(
			'class'=>"DataColumn",
			'name'=>'activecodes',
			'htmlOptions'=>array(
				'htmlOptions'=>array('style'=>'background-color:#FFFFCC !important;'),
		)
		),
		'num',
//		'status',
		'aliid',
		'serialnumber',
		'create_at',
		'update_at',
	),
)); ?>
