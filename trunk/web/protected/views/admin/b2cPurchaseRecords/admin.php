<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $model B2cPurchaseRecords */

$this->breadcrumbs=array(
	'购买管理'=>array('admin'),
	'管理',
);

//$this->menu=array(
//	array('label'=>'创建', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2c-purchase-records-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>购买记录管理</h1>



<?php echo CHtml::link('高级搜索','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'b2c-purchase-records-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'id',
			'htmlOptions'=>array('width'=>'50px'),
		),
		array(
			'name'=>'packageName',
			'value'=>'$data->package===null?"":$data->package->name;',
			'htmlOptions'=>array('width'=>'120px'),
		),
		array(
			'name'=>'realName',
			'value'=>'$data->username===null?"":$data->username->realname;',
			'htmlOptions'=>array('width'=>'80px'),
			
		),
		'num',
		array(
			"name"=>"totalprice",
			'value'=>'$data->totalprice===null?"":$data->totalprice/100;',
			"htmlOptions"=>array('width'=>'50px')
		),
		array(
			'name'=>'unitprice',
			'value'=>'$data->unitprice===null?"":$data->unitprice/100;',
			'htmlOptions'=>array('width'=>'60px')
		),
		array(
			'name'=>'aliid',
			'value'=>'$data->aliid',
			'htmlOptions'=>array('width'=>'60px')
		),
		array(
			'name'=>'activecodes',
			'value'=>'$data->activecodes',
			'htmlOptions'=>array('width'=>'160px')
		),
		
//		array( 'name'=>'author_search', 'value'=>'$data->author->username' ),
		
		
		
		/*'activecodes',*/
		
		array(
			'name'=>'status',
			'value'=>'$data->status==B2cPurchaseRecords::NO_PAID?"未付款":"已付款"',
			'htmlOptions'=>array('width'=>'150px'),
			 'filter'=>CHtml::dropDownList('B2cPurchaseRecords[status]',$model->status,  
                array(
                	''=>'全部',
                    B2cPurchaseRecords::NO_PAID=>'未付款',
                    B2cPurchaseRecords::HAVE_PAID=>'已付款',
                )               
		),
		),
		/*
		'aliid',
		'serialnumber',
		'create_at',
		'update_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view}{update}',
		),
	),
)); ?>
