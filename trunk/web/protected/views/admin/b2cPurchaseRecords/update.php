<?php
/* @var $this B2cPurchaseRecordsController */
/* @var $model B2cPurchaseRecords */

$this->breadcrumbs=array(
	'购买管理'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'创建',
);

$this->menu=array(
//	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>更新购买记录#<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>