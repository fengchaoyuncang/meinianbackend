<?php
/* @var $this B2bTicketFcodeController */
/* @var $model B2bTicketFcode */

$this->breadcrumbs=array(
	'管理团购商'=>array('admin'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List B2bTicketFcode', 'url'=>array('index')),
	array('label'=>'管理列表', 'url'=>array('admin')),
);
?>

<h1>Create B2bTicketFcode</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>