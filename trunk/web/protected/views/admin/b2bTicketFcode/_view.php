<?php
/* @var $this B2bTicketFcodeController */
/* @var $data B2bTicketFcode */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activecode')); ?>:</b>
	<?php echo CHtml::encode($data->activecode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::encode($data->uid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('companyid')); ?>:</b>
	<?php echo CHtml::encode($data->companyid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('groupid')); ?>:</b>
	<?php echo CHtml::encode($data->groupid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('identity_no')); ?>:</b>
	<?php echo CHtml::encode($data->identity_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realname')); ?>:</b>
	<?php echo CHtml::encode($data->realname); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telephone')); ?>:</b>
	<?php echo CHtml::encode($data->telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tstatus')); ?>:</b>
	<?php echo CHtml::encode($data->tstatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tuangouid')); ?>:</b>
	<?php echo CHtml::encode($data->tuangouid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('packageid')); ?>:</b>
	<?php echo CHtml::encode($data->packageid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('centerid')); ?>:</b>
	<?php echo CHtml::encode($data->centerid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('examdate')); ?>:</b>
	<?php echo CHtml::encode($data->examdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startdate')); ?>:</b>
	<?php echo CHtml::encode($data->startdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enddate')); ?>:</b>
	<?php echo CHtml::encode($data->enddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_at')); ?>:</b>
	<?php echo CHtml::encode($data->update_at); ?>
	<br />

	*/ ?>

</div>