<?php
/* @var $this B2bTicketFcodeController */
/* @var $model B2bTicketFcode */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'activecode'); ?>
		<?php echo $form->textField($model,'activecode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uid'); ?>
		<?php echo $form->textField($model,'uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'companyid'); ?>
		<?php echo $form->textField($model,'companyid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'groupid'); ?>
		<?php echo $form->textField($model,'groupid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'identity_no'); ?>
		<?php echo $form->textField($model,'identity_no',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'realname'); ?>
		<?php echo $form->textField($model,'realname',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tstatus'); ?>
		<?php echo $form->textField($model,'tstatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tuangouid'); ?>
		<?php echo $form->textField($model,'tuangouid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'packageid'); ?>
		<?php echo $form->textField($model,'packageid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'centerid'); ?>
		<?php echo $form->textField($model,'centerid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'examdate'); ?>
		<?php echo $form->textField($model,'examdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startdate'); ?>
		<?php echo $form->textField($model,'startdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enddate'); ?>
		<?php echo $form->textField($model,'enddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_at'); ?>
		<?php echo $form->textField($model,'create_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_at'); ?>
		<?php echo $form->textField($model,'update_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->