<?php
/* @var $this B2bTicketFcodeController */
/* @var $model B2bTicketFcode */

$this->breadcrumbs=array(
	'管理F码预约'=>array('admin'),
	'管理',
);

//$this->menu=array(
//	array('label'=>'List B2bTicketFcode', 'url'=>array('index')),
//	array('label'=>'Create B2bTicketFcode', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-ticket-fcode-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>管理团购商</h1>

<p>
    你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) 来指明搜搜条件。
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'b2b-ticket-fcode-grid',
	'dataProvider'=>$model->search("specail"),
	'filter'=>$model,
	'columns'=>array(
//		'id',
//		'activecode',
//		'uid',
//		'companyid',
//		'groupid',
//		'identity_no',
        array(
            'name' => 'packageName',
            'value' => '$data->package===null?"":$data->package->name;',
            'htmlOptions' => array('width' => '160px'),
        ),
        'realname',
//        array(
//            'name' => 'companyName',
//            'value' => '$data->company===null?"":$data->company->name;',
//            'htmlOptions' => array('width' => '100px'),
//        ),
        /*'groupid',*/
        array(
            'name' => 'activecode',
            'value' => '$data->activecode===null?"":B2bTicketFcode::getActivecodeDesc($data->activecode);',
            'htmlOptions' => array('width' => '160px'),
        ),
//        'activecode',
        /*
        'realname',
        'telephone',*/
        array(
            'name' => 'tuangouid',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->tuangouid===null?"":B2bTicketFcode::getTuangouDesc($data->tuangouid)',
            'filter' => CHtml::dropDownList('B2bTicketFcode[tuangouid]', $model->tuangouid,
                    $tuangoucompany
                ),
        ),
        array(
            'name' => 'tstatus',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->tstatus===null?"":B2bTicketFcode::getStatusDesc($data->tstatus)',
            'filter' => CHtml::dropDownList('B2bTicketFcode[tstatus]', $model->tstatus,
                    array(
                        '' => '全部',
                        B2bTicketFcode::UNACTIVATED => '未激活',
                        B2bTicketFcode::YUYUE_PROCESSING => '预约处理中，可取消',
                        B2bTicketFcode::YUYUE_CONFIRM => '预约确认，电话取消',
                        B2bTicketFcode::FINISHED => '体检完成',
                        B2bTicketFcode::YUYUE_CANCEL => '取消预约',
                    )
                ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
            'htmlOptions' => array('width' => '30px'),
        ),
	),
)); ?>
