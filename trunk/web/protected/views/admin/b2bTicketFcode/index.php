<?php
/* @var $this B2bTicketFcodeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'管理团购商',
);

$this->menu=array(
//	array('label'=>'Create B2bTicketFcode', 'url'=>array('create')),
	array('label'=>'管理列表', 'url'=>array('admin')),
);
?>

<h1>管理团购商</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
