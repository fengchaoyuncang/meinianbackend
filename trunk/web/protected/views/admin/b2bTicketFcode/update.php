<?php
/* @var $this B2bTicketFcodeController */
/* @var $model B2bTicketFcode */

$this->breadcrumbs=array(
	'管理F码预约'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'更新F码预约',
);

$this->menu=array(
//	array('label'=>'List B2bTicketFcode', 'url'=>array('index')),
//	array('label'=>'Create B2bTicketFcode', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理列表', 'url'=>array('admin')),
);
?>

<h1>Update B2bTicketFcode <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>