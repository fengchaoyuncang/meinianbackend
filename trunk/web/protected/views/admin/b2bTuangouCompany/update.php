<?php
/* @var $this B2bTuangouCompanyController */
/* @var $model B2bTuangouCompany */

$this->breadcrumbs=array(
	'管理'=>array('admin'),
//	$model->name=>array('view','id'=>$model->id),
	'更新',
);

$this->menu=array(
//	array('label'=>'List B2bTuangouCompany', 'url'=>array('index')),
	array('label'=>'创建团购商', 'url'=>array('create')),
	array('label'=>'查看团购商', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理团购商', 'url'=>array('admin')),
);
?>

<h1>升级团购商</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>