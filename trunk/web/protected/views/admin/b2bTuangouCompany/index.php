<?php
/* @var $this B2bTuangouCompanyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'B2b Tuangou Companies',
);

$this->menu=array(
	array('label'=>'创建团购商', 'url'=>array('create')),
	array('label'=>'管理团购商', 'url'=>array('admin')),
);
?>

<h1>B2b Tuangou Companies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
