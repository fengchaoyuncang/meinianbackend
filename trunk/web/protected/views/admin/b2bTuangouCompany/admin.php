<?php
/* @var $this B2bTuangouCompanyController */
/* @var $model B2bTuangouCompany */

$this->breadcrumbs=array(
	'管理'=>array('admin'),
	'管理',
);

$this->menu=array(
//	array('label'=>'List B2bTuangouCompany', 'url'=>array('index')),
	array('label'=>'创建团购商', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-tuangou-company-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>管理团购商</h1>

<p>
    你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) 来指明搜搜条件。
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'b2b-tuangou-company-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
        array(
            'name' => 'type',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->type===null?"":B2bTuangouCompany::getypeDesc($data->type)',
            'filter' => CHtml::dropDownList('B2bTuangouCompany[type]', $model->type,
                    array(
                        '' => '全部',
                        B2bTuangouCompany::TUANGOU_CANYU => '参与',
                        B2bTuangouCompany::TUANGOU_WEICANYU => '未参与',
                    )
                ),
        ),
		'telephone',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
            'htmlOptions' => array('width' => '30px'),
        ),
	),

)); ?>
