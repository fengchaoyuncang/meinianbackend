<?php
/* @var $this B2bTuangouCompanyController */
/* @var $model B2bTuangouCompany */

$this->breadcrumbs=array(
	'管理'=>array('admin'),
	'创建',
);

$this->menu=array(
//	array('label'=>'List B2bTuangouCompany', 'url'=>array('index')),
	array('label'=>'管理团购商', 'url'=>array('admin')),
);
?>

<h1>创建团购商</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>