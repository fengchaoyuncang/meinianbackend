<?php
/* @var $this B2bTuangouCompanyController */
/* @var $model B2bTuangouCompany */

$this->breadcrumbs=array(
	'管理'=>array('admin'),
	$model->name,
);

$this->menu=array(
//	array('label'=>'List B2bTuangouCompany', 'url'=>array('index')),
	array('label'=>'创建团购商', 'url'=>array('create')),
	array('label'=>'升级团购商', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'删除团购商', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理团购商', 'url'=>array('admin')),
);
?>

<h1>查看团购商</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'id',
		'name',
		'type',
		'telephone',
	),
)); ?>
