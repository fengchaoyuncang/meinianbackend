<?php
/* @var $this B2bTuangouCompanyController */
/* @var $model B2bTuangouCompany */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-tuangou-company-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'type'); ?>
<!--		--><?php //echo $form->textField($model,'type'); ?>
<!--		--><?php //echo $form->error($model,'type'); ?>
<!--	</div>-->
    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php
        echo $form->dropDownList($model, 'type',
            array(
                B2bTuangouCompany::TUANGOU_CANYU => '参与',
                B2bTuangouCompany::TUANGOU_WEICANYU => '未参与',
            )
        );
        ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telephone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->