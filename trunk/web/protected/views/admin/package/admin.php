<?php
/* @var $this PackageController */
/* @var $model B2bPackage */

$this->breadcrumbs=array(
	'套餐管理'=>array('admin'),
);

$this->menu=array(
	//array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-package-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>套餐管理</h3>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'b2b-package-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'id',
			'htmlOptions'=>array('width'=>'50px'),
		),
		array(
			'name'=>'name',
			'htmlOptions'=>array('width'=>'200px'),
		),
		array(
			'name'=>'org_price',
			'htmlOptions'=>array('width'=>'80px'),
			'value'=>'$data->org_price===null?"":$data->org_price/100',
		),
		array(
			'name'=>'sale_price',
			'htmlOptions'=>array('width'=>'80px'),
			'value'=>'$data->sale_price===null?"":$data->sale_price/100',
		),
		array(
            'name'=>'suitable_gender',
			'htmlOptions'=>array('width'=>'80px'),
            'filter'=>CHtml::dropDownList('B2bPackage[suitable_gender]',$model->suitable_gender,  
                array(
                	''=>'全部',
                    B2bPackage::COMMEN_GENDER=>'通用',
                    B2bPackage::MALE=>'男性',
                    B2bPackage::FEMEAL=>'女性',
                )
            ),
        ),
		array(
            'name'=>'suitable_married',
			'htmlOptions'=>array('width'=>'80px'),
            'filter'=>CHtml::dropDownList('B2bPackage[suitable_married]',$model->suitable_married,  
                array(
                	''=>'全部',
                    B2bPackage::MARRIAGE_ALL=>'通用',
                    B2bPackage::MARRIAGE_SINGLE=>'未婚',
                    B2bPackage::MARRIAGE_DOUBLE=>'已婚'
                )
            ),
        ),
		/*
		'status',
		'description',
		'create_at',
		'update_at',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'htmlOptions'=>array('style'=>'width: 75px'),
		),
	),
)); ?>
