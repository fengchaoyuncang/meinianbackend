<?php
/* @var $this PackageController */
/* @var $model B2bPackage */
/* @var $form CActiveForm */
?>
<style>
input { display: inline; margin-right: 10px; }
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-package-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row">
	<h3><?=$model->name?></h3>
	</div>

	<input type="hidden" name="package_id" value=<?=$model->id?>>
	
	<?php foreach ($basicitemclassify as $classify){?>
	<h3><input type="checkbox" name="classify[<?=$classify->id?>][]" value="" onClick="checkall(name);"><?=$classify->item_name?></h3>
	<?php 	foreach ($basicitem[$classify->id] as $item){
				?>
	<div class="row">
		<input type="checkbox" name="classify[<?=$classify->id?>][]" value="<?=$item["ins"]->id?>"
		<?php if ($item["check"] === true){?>
		checked="true"
		<?php }?>
		><?=$item["ins"]->name?>
	</div>
	<?php 
		}
	}?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
var flage = 0;
function checkall(name){
	var box_arr = document.getElementsByName(name);
	if (flage){
		for (var i = 0; i < box_arr.length; i++){
			box_arr[i].checked = false;
		}
		flage = 0;
	}else{
		for (var i = 0; i < box_arr.length; i++){
			box_arr[i].checked = true;
		}
		flage = 1;
	}
}
</script>