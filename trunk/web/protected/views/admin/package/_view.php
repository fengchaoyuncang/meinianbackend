<?php
/* @var $this PackageController */
/* @var $data B2bPackage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->name), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_price')); ?>:</b>
	<?php echo CHtml::encode($data->org_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sale_price')); ?>:</b>
	<?php echo CHtml::encode($data->sale_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suitable_gender')); ?>:</b>
	<?php echo CHtml::encode($data->suitable_gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suitable_married')); ?>:</b>
	<?php echo CHtml::encode($data->suitable_married); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('examitems')); ?>:</b>
	<?php $this->widget('ext.wdueditor.WDueditor',array(
	        //'model' => $model,
			'attribute' => $data->examitems,
			//'name'=>$data->examitems,
	        'toolbars' =>array(
	            'FullScreen','Source','Undo', 'Redo','Bold'
	        ),
		));?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('introduce')); ?>:</b>
	<?php $this->widget('ext.wdueditor.WDueditor',array(
	        'model' => $model,
			'name'=>$data->introduce,
	        'toolbars' =>array(
	            'FullScreen','Source','Undo', 'Redo','Bold'
	        ),
		));?> 
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_at')); ?>:</b>
	<?php echo CHtml::encode($data->update_at); ?>
	<br />

	*/ ?>

</div>