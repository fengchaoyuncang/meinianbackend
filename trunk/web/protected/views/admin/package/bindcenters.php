<?php
/* @var $this PackageController */
/* @var $model B2bPackage */

$this->breadcrumbs=array(
	'套餐管理'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'管理', 'url'=>array('admin')),
//	array('label'=>'创建', 'url'=>array('create')),
//	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'管理', 'url'=>array('admin')),
//	array('label'=>'绑定体检中心', 'url'=>array('bindcenters',"id"=>$model->id)),
//	array('label'=>'绑定体检项目', 'url'=>array('bindexamitems',"id"=>$model->id)),
);
?>
<script>
    function getcity(){
        $.getJSON("<?=$this->createUrl("admin/subcenter/city")?>", {"province":$("select[name=province]").find("option:selected").text()},function(data){
                if(data["status"]==0){
                    $("select[name=city]").empty();
                    for(var i=0;i<data["data"].length;i++){
                        $("<option>"+data["data"][i]+"</option>").appendTo($("select[name=city]"));
                    }
                }
            });
    }
    function searchaddr(){
        $.getJSON("<?=$this->createUrl("admin/subcenter/getsubcenters")?>", {"city":$("select[name=city]").find("option:selected").text(),
                "name":$("input[name=brand]").val()},function(data){
                if(data["status"]==0){
                    $("#centers tr[name=center]").remove();
                    for(var i=0;i<data["data"].length;i++){
                        $("#centeritem td").eq(0).find("input").val(data["data"][i]["id"]);
                        $("#centeritem td").eq(1).text(data["data"][i]["id"]);
                        $("#centeritem td").eq(2).text(data["data"][i]["name"]);
                        $("#centeritem td").eq(3).text(data["data"][i]["address"]);
                        $("#centers").append($("#centeritem").html());
                    }
                }
            });
    }  
    function bindcenters(){
        var sel = $("#centers tr").find("td:first input:checked");
        var params =[];
        for(var i=0;i<sel.length;i++){
            params[i] = sel.eq(i).val();
        }
        $.post("<?=$this->createUrl("bindcenters")?>",{"id":<?=$model->id?>,"bingcenters":params},function(data){
            if(data["status"]==0){
                    window.location.reload();
            }else{
                alert("绑定失败，错误信息："+data["data"])
            }
        });
    }
    function selectAll(t){
        if($(t).prop("checked")){
            $("#centers tr").find("td:first input").prop("checked",true);
        }else{
            $("#centers tr").find("td:first input").prop("checked",false);
        }
    }
    function delcenter(t,id) {
        if(!confirm("确认要删除？")){
            return;
        }
        $.post("<?=$this->createUrl("delcenter")?>",{"id":id},function(data){
            if(data["status"]==0){
                 $("tr[name=center"+data["data"]+"]").remove();   
            }
        });
    }
    
    $(function(){
            $.getJSON("<?=$this->createUrl("admin/subcenter/province")?>", function(data){
                if(data["status"]==0){
                    $("select[name=province]").empty();
                    for(var i=0;i<data["data"].length;i++){
                        $("<option>"+data["data"][i]+"</option>").appendTo($("select[name=province]"));
                    }
                }
            });
    });  
</script>
<h3><?php echo $model->name; ?>可用的体检中心</h3>


<table>
    <tr>
        <th>名称</th>
        <th>地址</th>
        <th>操作</th>
    </tr>
<?php foreach ($centers as $it) { ?>
     <tr name="center<?=$it->id?>">
        <td><?=$it->center->name?></td>
        <td><?=$it->center->address?></td>
        <td><a href="javascript:delcenter(this,<?=$it->id?>)">刪除</a></td>
    </tr>       
<?php } ?>

    
</table>
<table class="table table-bordered" id="centers">
    <form id="search[addr]">
    <tr>
        <th><input type="checkbox" onclick="selectAll(this);"></th>
        <th>省份</th>
        <th>
            <select name="province" style="width:100px;" onchange="getcity();">
            </select>
        </th>
        <th>城市</th>
        <th>
            <select name="city" style="width:100px;">
            </select>
        </th>
        <th>名称</th>
        <th>
            <input name="brand" value="" type="text" />
        </th>
        <th>
            <input onclick="searchaddr();" type="button" value="搜索" />
        </th>
        <th>
            <input onclick="bindcenters();" type="button" value="绑定体检中心" />
        </th>
    </tr>
        </from>
        
</table>
<table style="display: none" id="centeritem">
    <tr name="center">
        <td><input type="checkbox" value="" /></td>
        <td></td>
        <td colspan="4"></td>
        <td colspan="3"></td>
    </tr>
</table>
