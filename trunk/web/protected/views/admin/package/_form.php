<?php
/* @var $this PackageController */
/* @var $model B2bPackage */
/* @var $form CActiveForm */
?>
<!-- jquery -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<!-- jquery te -->
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/js/jquery/te/jquery-te-1.3.3.css">
<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/jquery/te/jquery-te-1.3.3.min.js"></script>
<?php Yii::app()->clientScript->registerScript('jquery-te-for-editor', "
    (function($) {
		$(function() {
			// enable jquery texteditor
			$('textarea.editor').jqte();
		});
	})(jQuery);

", CClientScript::POS_HEAD); ?>
<style>
html, body {
		padding:0px;
		background: #E5E5E5;
		}
input { display: inline; margin-right: 10px; }
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-package-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有 <span class="required">*</span> 项为必填项</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_price'); ?>
		<?php echo $form->textField($model,'org_price'); ?>
		<?php echo $form->error($model,'org_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price'); ?>
		<?php echo $form->error($model,'sale_price'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'suitable_gender'); ?>
		<?php echo $form->radioButtonList(
				$model,
				'suitable_gender',
				array(
					B2bPackage::COMMEN_GENDER=>"通用",
					B2bPackage::FEMEAL=>'女',
					B2bPackage::MALE=>"男",
				),
                array('style'=>'display:inline;width:50px;',"separator"=>" ",
                            "labelOptions"=>array("style"=>"display:inline;"))); ?>
		<?php echo $form->error($model,'suitable_gender'); ?>
	</div>
<!--    //套餐适用的婚姻状态,0,通用，1未婚，2已婚。-->
<!--    const MARRIAGE_ALL=0;-->
<!--    const MARRIAGE_SINGLE=1;-->
<!--    const MARRIAGE_DOUBLE=2;-->
	<div class="row">
		<?php echo $form->labelEx($model,'suitable_married'); ?>
		<?php echo $form->radioButtonList(
			$model,
			'suitable_married',
			array(
				B2bPackage::MARRIAGE_ALL=>"通用",
				B2bPackage::MARRIAGE_SINGLE=>"未婚",
				B2bPackage::MARRIAGE_DOUBLE=>"已婚"
				),
               array('style'=>'display:inline;width:50px;',"separator"=>" ",
                            "labelOptions"=>array("style"=>"display:inline;"))); ?>
		<?php echo $form->error($model,'suitable_married'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'examitems'); ?>
		<?php 
//		$this->widget('ext.wdueditor.WDueditor',array(
//	        'model' => $model,
//			'name'=>'B2bPackage[examitems]',
//			'attribute' => 'examitems',
//	        'toolbars' =>array(
//	            'Undo','Redo','ForeColor','BackColor', 'Bold','Italic','Underline','JustifyLeft','JustifyCenter','JustifyRight','InsertImage','ImageNone','ImageLeft','ImageRight','ImageCenter'
//	        ),
//	         'height'=>'200'
//		));
		?>
		<?php echo $form-> textArea($model,'examitems',array('size'=>60,'maxlength'=>1000,'class'=>'editor')); ?>
		<?php echo $form->error($model,'examitems'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'introduce'); ?>
		<?php  
//		$this->widget('ext.wdueditor.WDueditor',array(
//	        'model' => $model,
//			'name'=>'B2bPackage[introduce]',
//		 	'attribute' => 'introduce',
//	        'toolbars' =>array(
//	            'Undo','Redo','ForeColor','BackColor', 'Bold','Italic','Underline','JustifyLeft','JustifyCenter','JustifyRight','InsertImage','ImageNone','ImageLeft','ImageRight','ImageCenter'
//	        ),
//	        'language' =>'zh-cn',
//	        'height'=>'300'
//		));
		?> 

		<?php echo $form-> textArea($model,'introduce',array('size'=>60,'maxlength'=>1000,'class'=>'editor')); ?>
		<?php echo $form->error($model,'introduce'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array('0'=>'正常','1'=>'下架')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type',array('0'=>'套餐','1'=>'商城','2'=>'团购')); ?>
        <?php echo $form->error($model,'type'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form-> textArea($model,'description',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '更新' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
