<?php
/* @var $this BasicexamitemclassifyController */
/* @var $model BasicExamItemClassify */
//
//$this->breadcrumbs=array(
//	'基本项'=>array('index'),
//	$model->id,
//);

$this->menu=array(
//	array('label'=>'列出基本项', 'url'=>array('index')),
//	array('label'=>'创建基本项', 'url'=>array('create')),
	array('label'=>'修改基本项', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'删除基本项', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理基本项', 'url'=>array('admin')),
);
?>

<!--<h1>View BasicExamItemClassify #<?php echo $model->id; ?></h1>-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_name',
//		'description',
//		'type',
//		'content',
//		'diagnosis',
//		'objective',
//		'analysis',
//		'suggestion',
	),
)); ?>
