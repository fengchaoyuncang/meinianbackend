<?php
/* @var $this BrandController */
/* @var $model B2bBrand */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-center-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有<span class="required">*</span> 为必填项</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'icon'); ?>
		<?php echo $form->textField($model,'icon',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'icon'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'tags'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'createtime'); ?>
		<?php //echo $form->textField($model,'createtime',array('size'=>60,'maxlength'=>128)); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			    'name'=>'B2bBrand[createtime]',
				'attribute' => 'B2bBrand[createtime]',
				'value' => $model->isNewRecord?date("Y-m-d"):$model->createtime, 
			    // additional javascript options for the date picker plugin
			    'options'=>array(
					'altField' => 'createtime',
				'dateFormat' => 'yy-mm-dd',
			        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
					'autocomplete' => 'true' 
				),
			    'htmlOptions'=>array(
			        'style'=>'height:20px;background-color:white;color:#000;',
			    ),
			));
		?>
		<?php echo $form->error($model,'createtime'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '更新'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->