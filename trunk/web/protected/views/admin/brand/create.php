<?php
/* @var $this BrandController */
/* @var $model B2bBrand */

$this->breadcrumbs=array(
	'品牌管理'=>array('admin'),
	'创建',
);

$this->menu=array(
//	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>新建体检中心品牌</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>