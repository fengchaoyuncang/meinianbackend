<?php
/* @var $this BrandController */
/* @var $model B2bBrand */

$this->breadcrumbs=array(
	'品牌管理'=>array('admin'),
	'管理',
);

$this->menu=array(
//	array('label'=>'品牌列表', 'url'=>array('index')),
	array('label'=>'新建', 'url'=>array('create')),
);

//Yii::app()->clientScript->registerScript('search', "
//$('.search-button').click(function(){
//	$('.search-form').toggle();
//	return false;
//});
//$('.search-form form').submit(function(){
//	$('#b2b-center-grid').yiiGridView('update', {
//		data: $(this).serialize()
//	});
//	return false;
//});
//");
?>

<h1>品牌管理</h1>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TBGridView', array(
	'id'=>'b2b-center-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	 
	'columns'=>array(
		array(
			'name'=>'id',
			'htmlOptions'=>array(
				'width'=>'40px',
			),
		),
		array(
			'name'=>'name',
			'htmlOptions'=>array(
				'width'=>'140px',
			),
		),
		array(
			'name'=>'description',
			'value' => '(substr($data->description, 0, 30) . ( (strlen($data->description) > 30) ? \'...\' : \'\' ) ) ' ,
			'htmlOptions'=>array(
				'width'=>'140px',
			),
		),
        'icon',
		array(
			'name'=>'create_at',
			'htmlOptions'=>array(
				'width'=>'140px',
			),
		),
		array(
			'name'=>'tags',
			'htmlOptions'=>array(
				'width'=>'100px',
			),
		),
		array(
			'name'=>'email',
			'htmlOptions'=>array(
				'width'=>'100px',
			),
		),
		array(
			'name'=>'update_at',
			'htmlOptions'=>array(
				'width'=>'140px',
			),
		),
		array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 75px'),
		),
	),
)); ?>
