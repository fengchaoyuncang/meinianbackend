<?php
/* @var $this BasicexamitemController */
/* @var $model BasicExamItem */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'basic-exam-item-form',
	'enableAjaxValidation'=>false,
)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->

<!--	<?php echo $form->errorSummary($model); ?>-->

	<div class="row">
		<?php echo $form->labelEx($model,'名称'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID'); ?>
<!--		<?php echo $form->textField($model,'item_id'); ?>-->
		<?php echo $form->dropDownList($model,'item_id',CHtml::listData($model_basic_classify, "id", "item_name"));?>
<!--//		<?php echo $form->error($model,'item_id'); ?>-->
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'over_view'); ?>
		<?php echo $form->textArea($model,'over_view',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'over_view'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'over_reason'); ?>
		<?php echo $form->textArea($model,'over_reason',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'over_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'under_reason'); ?>
		<?php echo $form->textArea($model,'under_reason',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'under_reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cure_care'); ?>
		<?php echo $form->textArea($model,'cure_care',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'cure_care'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expert_advice'); ?>
		<?php echo $form->textArea($model,'expert_advice',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'expert_advice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'value_ranges'); ?>
		<?php echo $form->textArea($model,'value_ranges',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'value_ranges'); ?>
	</div>
-->
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->