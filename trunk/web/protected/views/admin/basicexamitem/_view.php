<?php
/* @var $this BasicexamitemController */
/* @var $data BasicExamItem */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_id')); ?>:</b>
	<?php echo CHtml::encode($data->item_id); ?>
	<br />

	<!--<b><?php echo CHtml::encode($data->getAttributeLabel('over_view')); ?>:</b>
	<?php echo CHtml::encode($data->over_view); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('over_reason')); ?>:</b>
	<?php echo CHtml::encode($data->over_reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('under_reason')); ?>:</b>
	<?php echo CHtml::encode($data->under_reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cure_care')); ?>:</b>
	<?php echo CHtml::encode($data->cure_care); ?>
	<br />

	--><?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('expert_advice')); ?>:</b>
	<?php echo CHtml::encode($data->expert_advice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value_ranges')); ?>:</b>
	<?php echo CHtml::encode($data->value_ranges); ?>
	<br />

	*/ ?>

</div>