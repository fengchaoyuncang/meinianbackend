<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $model B2bCenterNotpermittedDay */

$this->breadcrumbs=array(
	'管理'=>array('admin'),
//	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List B2bCenterNotpermittedDay', 'url'=>array('index')),
//	array('label'=>'Create B2bCenterNotpermittedDay', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>更新</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>