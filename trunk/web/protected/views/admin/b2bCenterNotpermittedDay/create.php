<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $model B2bCenterNotpermittedDay */

$this->breadcrumbs=array(
	"日期管理"=>array('index'),
	'创建',
);

$this->menu=array(
//	array('label'=>'List B2bCenterNotpermittedDay', 'url'=>array('index')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>创建</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>