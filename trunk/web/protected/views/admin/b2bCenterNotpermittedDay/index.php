<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'B2b Center Notpermitted Days',
);

$this->menu=array(
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>B2b Center Notpermitted Days</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
