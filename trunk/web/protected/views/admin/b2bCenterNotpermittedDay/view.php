<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $model B2bCenterNotpermittedDay */

$this->breadcrumbs=array(
	'日期管理'=>array('index'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List B2bCenterNotpermittedDay', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
//	array('label'=>'Delete B2bCenterNotpermittedDay', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>查看<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'int',
//		'centerid',
		'centername',
		'notpermitteddate',
        'type'
	),
)); ?>
