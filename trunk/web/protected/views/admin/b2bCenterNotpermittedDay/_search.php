<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $model B2bCenterNotpermittedDay */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'id'); ?>
<!--		--><?php //echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
<!--	</div>-->

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'centerid'); ?>
<!--		--><?php //echo $form->textField($model,'centerid'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->label($model,'centername'); ?>
		<?php echo $form->textField($model,'centername',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notpermitteddate'); ?>
		<?php echo $form->textField($model,'notpermitteddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->