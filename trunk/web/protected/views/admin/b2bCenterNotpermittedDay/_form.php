<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $model B2bCenterNotpermittedDay */
/* @var $form CActiveForm */
?>
<script>
    var arrcenterid = new Array();
    var arrcentername = new Array();

    function getcity() {
        $.getJSON("<?=$this->createUrl("admin/subcenter/city")?>", {"province": $("select[name=province]").find("option:selected").text()}, function (data) {
            if (data["status"] == 0) {
                $("select[name=city]").empty();
                for (var i = 0; i < data["data"].length; i++) {
                    $("<option>" + data["data"][i] + "</option>").appendTo($("select[name=city]"));
                }
            }
        });
    }
    function searchaddr() {
        $.getJSON("<?=$this->createUrl("admin/subcenter/getsubcenters")?>",
            {"city": $("select[name=city]").find("option:selected").text(),
                "name": $("input[name=brand]").val()},
            function (data) {
                $length = data["data"].length - 1;
                if (data["status"] == 0) {
                    $("#centername").empty();
                    for (var i = 0; i < data["data"].length; i++) {
                        $("<option>" + data["data"][i]['name'] + "</option>").appendTo($("#centername"));
                        arrcenterid[i] = data["data"][i]['id'];
                        arrcentername[i] = data["data"][i]['name'];
                    }
                    $("#centerid").val(data["data"][$length]['id']);
                }
            });
    }
    $(function () {
        $.getJSON("<?=$this->createUrl("admin/subcenter/province")?>", function (data) {
            if (data["status"] == 0) {
                $("select[name=province]").empty();
                for (var i = 0; i < data["data"].length; i++) {
                    $("<option>" + data["data"][i] + "</option>").appendTo($("select[name=province]"));
                }
            }
        });
    <?php
        if(isset($model->centername)){?>
            $("<option>" + <?=isset($model->centername)?"'".$model->centername."'":""?> + "</option>").appendTo($("#centername"));
        <?php }?>
    });
    function changecenterid(sel){
        var centerid;
        for(var i = 0; i < arrcentername.length; i++){
            if(arrcentername[i] == sel.value){
                $("#centerid").val(arrcenterid[i]);
            }//;arrcenterid
        }

    }
</script>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'b2b-center-notpermitted-day-form',
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <!--	<div class="row">-->
    <!--		--><?php //echo $form->labelEx($model,'centerid'); ?>
    <!--		--><?php //echo $form->textField($model,'centerid'); ?>
    <!--		--><?php //echo $form->error($model,'centerid'); ?>
    <!--	</div>-->
    <table class="table table-bordered" id="centers">
        <form id="search[addr]">
            <tr>
<!--                <th><input type="checkbox" onclick="selectAll(this);"></th>-->
                <th>省份</th>
                <th>
                    <select name="province" style="width:100px;" onchange="getcity();">
                    </select>
                </th>
                <th>城市</th>
                <th>
                    <select name="city" style="width:100px;">
                    </select>
                </th>
                <th>
                    <input onclick="searchaddr();" type="button" value="搜索"/>
                </th>
            </tr>
            </from>

    </table>

    <div style='display:none'>
    <input id="centerid" name="B2bCenterNotpermittedDay[centerid]" type='hidden' value="<?=isset($model->centerid)?$model->centerid:""?>"></div>
    <div class="row">
        <?php echo $form->labelEx($model, 'centername'); ?>
    <tr>
        <select id="centername" name="B2bCenterNotpermittedDay[centername]" style="width:300px;" onchange="changecenterid(this);" >
        </select>
    </tr>
        <?php echo $form->error($model, 'centername'); ?>
    </div>
<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model, 'centername'); ?>
<!--        --><?php //echo $form->textField($model, 'centername', array('size' => 20, 'maxlength' => 20)); ?>
<!--        --><?php //echo $form->error($model, 'centername'); ?>
<!--    </div>-->
<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model, 'centername'); ?>
<!--        --><?php //echo $form->dropDownList($model, 'centername', array('name'=>"centername",'size' => 20, 'maxlength' => 20)); ?>
<!--        --><?php //echo $form->error($model, 'centername'); ?>
<!--    </div>-->

    <!--	<div class="row">-->
    <!--		--><?php //echo $form->labelEx($model,'notpermitteddate'); ?>
    <!--		--><?php //echo $form->textField($model,'notpermitteddate'); ?>
    <!--		--><?php //echo $form->error($model,'notpermitteddate'); ?>
    <!--	</div>-->
    <div class="row">
        <?php echo $form->labelEx($model, 'notpermitteddate'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'B2bCenterNotpermittedDay[notpermitteddate]',
            'value' => $model->isNewRecord ? date("Y-m-d") : $model->notpermitteddate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;background-color:white;color:#000;',
            ),
        ));
        ?>
        <?php echo $form->error($model, 'notpermitteddate'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php
        echo $form->dropDownList($model, 'type',
            array(
                B2bCenterNotpermittedDay::YOUXIAO => '有效',
                B2bCenterNotpermittedDay::WUXIAO => '无效',
            )
        );
        ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->