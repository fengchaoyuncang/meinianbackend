<?php
/* @var $this B2bCenterNotpermittedDayController */
/* @var $model B2bCenterNotpermittedDay */

$this->breadcrumbs=array(
	'日期管理'=>array('admin'),
	'管理',
);

$this->menu=array(
//	array('label'=>'List B2bCenterNotpermittedDay', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-center-notpermitted-day-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>管理体检日期</h1>

<p>
    你可以在每个搜索域中输入这些比较操作符的一个(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) 来指明搜搜条件。
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!--<div class="search-form" style="display:none">-->
<?php //$this->renderPartial('_search',array(
//	'model'=>$model,
//)); ?>
<!--</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'b2b-center-notpermitted-day-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'int',
//		'centerid',
		'centername',
		'notpermitteddate',
        array(
            'name' => 'type',
            'htmlOptions' => array('width' => '150px'),
            'value' => '$data->type===null?"":B2bCenterNotpermittedDay::getCodetypeDesc($data->type)',
            'filter' => CHtml::dropDownList('B2bCenterNotpermittedDay[type]', $model->type,
                    array(
                        '' => '全部',
                        B2bCenterNotpermittedDay::YOUXIAO => '有效',
                        B2bCenterNotpermittedDay::WUXIAO => '无效',
                    )
                ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}',
            'htmlOptions' => array('width' => '30px'),
        ),
	),
)); ?>
