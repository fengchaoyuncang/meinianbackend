<?php
/* @var $this ExamItemController */
/* @var $model ExamItem */
/* @var $form CActiveForm */
?>
<!-- jquery -->
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<!-- jquery te -->
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/js/jquery/te/jquery-te-1.3.3.css">
<style>
	html, body {
		padding:0px;
		background: #E5E5E5;
		}
</style>
<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/jquery/te/jquery-te-1.3.3.min.js"></script>
<?php Yii::app()->clientScript->registerScript('jquery-te-for-editor', "
    (function($) {
		$(function() {
			// enable jquery texteditor
			$('textarea.editor').jqte();
		});
	})(jQuery);

", CClientScript::POS_HEAD); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exam-item-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有<span class="required">*</span>号的域是必填项。</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'体检项目名'); ?>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'item_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'描述'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>30, 'class'=>'editor')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'类型'); ?>
		<?php echo $form->dropDownList($model,'type',
            array(
                '0'=>'常规体检项目',
                '1'=>'非常规体检项目',
            )); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'diagnosis'); ?>
        <?php echo $form->textArea($model,'diagnosis',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'diagnosis'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'objective'); ?>
        <?php echo $form->textArea($model,'objective',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'objective'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'analysis'); ?>
        <?php echo $form->textArea($model,'analysis',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'analysis'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'suggestion'); ?>
        <?php echo $form->textArea($model,'suggestion',array('rows'=>6, 'cols'=>50, 'class'=>'editor')); ?>
        <?php echo $form->error($model,'suggestion'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->