<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php Yii::app()->clientScript->registerScriptFile('/js/jquery/mobile/jquery.mobile-1.3.0.min.js'); ?>
<?php Yii::app()->clientScript->registerCssFile('/js/jquery/mobile/jquery.mobile-1.3.0.min.css'); ?>

<div data-role="content">
    <div data-role="collapsible-set" data-inset="false">
        <div data-role="collapsible" data-collapsed="false">
            <h3><?php echo $model->item_name; ?></h3>
            <div><?php echo $model->description; ?></div>
        </div>
<?php
/*
        <div data-role="collapsible">
            <h3>检查内容</h3>
            <div><?php echo $model->content; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>诊断标准</h3>
            <div><?php echo $model->diagnosis; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>临床意义</h3>
            <div><?php echo $model->objective; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>异常分析</h3>
            <div><?php echo $model->analysis; ?></div>
        </div>

        <div data-role="collapsible">
            <h3>专家建议</h3>
            <div><?php echo $model->suggestion; ?></div>
        </div>
*/
?>
    </div>
</div>