<form class="form-horizontal">
    <div class="control-group">
        <label class="control-label" for="inputEmail">用户名</label>
        <div class="controls">
            <input type="text" id="User[username]" placeholder="用户名">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword">密码</label>
        <div class="controls">
            <input type="password" id="User[password]" placeholder="密码">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Email</label>
        <div class="controls">
            <input type="text" id="User[email]" placeholder="Email">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPassword">权限</label>
        <div class="controls">
            <select id="User[role]">
<?php
foreach($roles as $it){
    echo "<option value=$it[id]>$it[name]</option>";
}
?>

            </select>
            <select id="User[centerid]">
<?php
foreach($centers as $it){
    echo "<option value=$it->id>$it->name</option>";
}
?>

            </select>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">创建</button>
        </div>
    </div>
</form>