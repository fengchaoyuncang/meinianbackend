<?php

/**
 * This is the model class for table "meinian_meinian_questionnaireuestionnaire".
 *
 * The followings are the available columns in table 'meinian_questionnaire':
 * @property integer $id
 * @property string $title
 * @property string $type
 */
class EvaluateQuestionnaire extends CActiveRecord {


    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EvaluateQuestionnaire the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'evaluate_questionnaire';
    }

    /**
     * 
     * Enter description here ...
     * @param unknown_type $uid
     * @param unknown_type $hum_tags_arr
     */
    public function saveHumanTags($uid, $hum_tags_arr) {
        $hum_ret = EvaluateHumanHealthTag::model()->findByAttributes(array("uid" => $uid));
        if (!empty($hum_ret)) {
            if (empty($hum_ret->badtags)) {
                $str = implode(",", $hum_tags_arr);
                $hum_ret->badtags = $str;
            } else {
                foreach ($hum_tags_arr as $key => $value) {
                    if (strpos($hum_ret->badtags, $value) === false) {
                        $hum_ret->badtags .= "," . $value;
                    }
                }
            }

            $hum_ret->save();
        } else {
            $hum_ins = new EvaluateHumanHealthTag();
            $hum_ins->uid = $uid;
            $str = implode(",", $hum_tags_arr);
            $hum_ins->badtags = $str;
//			var_dump($hum_ins);exit();
            $hum_ins->save();
        }
    }

    /**
     *
     * Enter description here ...
     * @param unknown_type $user the use has use
     * @param unknown_type $q array of all the result form has had q['page_id']['ans']['element']
     */
    public function saveQAnswer($uid, $q) {
//        $user = EvaluateUserDiselement::model()->findByAttributes(
//                    array("uid"=>$uid,),array("order"=>"id desc")
//                );
//        if($user === null){
//            $user = new EvaluateUserDiselement();
//            $user->uid = $uid;
//            try {
//                if(false === $user->save()){
//                    throw new Exception("create new EvaluateUserDiselement item error");
//                }
//            } catch (Exception $exc) {
//                return $exc->getMessage();
//            }
//        }
        $items = array();
        try {
            
            foreach ($q as $key => $value) {
                $fc = "saveQA_" . $key;
                $value['ans'] = isset($value['ans']) ? $value['ans'] : array();
                if(method_exists($this, $fc)){
                    $ret = $this->$fc($uid, $value["ans"]);
                    if($ret !== null){
                        $items = array_merge($items, $ret);
                    }
                }
            }
            
            if(false === B2bUserQuestionResult::model()->saveResult($uid,$items)){
                throw new Exception("update $uid EvaluateUserDiselement item error");
            }
            return true;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }

    public function saveQA_17($uid, $q) {
        $anserw_array = array();
        if ($q['teacher'] == 'yes') {
            array_push($anserw_array, '肩膀');
        }
        if ($q['driver'] == 'yes') {
            array_push($anserw_array, '腰');
        }
        if ($q['whitecollar'] == 'yes') {
            array_push($anserw_array, '颈椎');
        }
        $anserw_str = implode(",", $anserw_array);
//        $user->badtags .= $anserw_str;
    }

    private function saveQA_18($uid, $q) {
        
        $anserw_array = array();
        if (($q['watchtelephone'] == 'yes') || ($q['watchingontrain'] == 'yes') || ($q['watchinbed'] == 'yes')) {
            array_push($anserw_array, '眼睛');
        }

        $anserw_str = implode(",", $anserw_array);
//        $user->badtags .= $anserw_str;
    }

    private function saveQA_1($uid, $q) {
        
        $item = null;
        if (isset($q["year"], $q["month"], $q["day"])) {
            $item["age"] = $q["year"] . "-" . $q["month"] . "-" . $q["day"];
        }

        return $item;
    }

    private function saveQA_2($uid, $q) {
        $item = null;
        if (isset($q["gender"]) && in_array($q["gender"], array(JSONHelper::MAN, JSONHelper::WOMAN))) {
            $item["gender"] = $q["gender"];
        }
        return $item;
    }

    private function saveQA_3($uid, $q) {
        $item = null;
        if (!empty($q["hypertension_family_history"])) {
            $item['hypertension_family_history'] = 1;
        } else {
            $item['hypertension_family_history'] = 0;
        }
        if (!empty($q["coronary_family_history"])) {
            $item['coronary_family_history'] = 1;
        } else {
            $item['coronary_family_history'] = 0;
        }
        if (!empty($q["stroke_family_history"])) {
            $item['stroke_family_history'] = 1;
        } else {
            $item['stroke_family_history'] = 0;
        }
        if (!empty($q["diabetes_family_history"])) {
            $item['diabetes_family_history_single'] = 1;
            $item['diabetes_family_history_double'] = 1;
//            if ($q["diabetes_family_history"] == "diabetes_family_history_single") {
//                $user->diabetes_family_history_single = 1;
//                $user->diabetes_family_history_double = 0;
//            } elseif ($q["diabetes_family_history"] == "diabetes_family_history_double") {
//                $user->diabetes_family_history_single = 1;
//                $user->diabetes_family_history_double = 1;
//            }
        } else {
            $item['diabetes_family_history_single'] = 0;
            $item['diabetes_family_history_double'] = 0;
        }
        return $item;
    }

    private function saveQA_4($uid, $q) {
        $item = null;
        $hum_tags_arr = array();
        if (isset($q["hypertension_history"]) && $q["hypertension_history"] == 1) {
            $item['hypertension_history'] = 1;
            array_push($hum_tags_arr, "高血压");
        } else {
            $item['hypertension_history'] = 0;
        }

        if (isset($q["coronary_history"]) && $q["coronary_history"] == 1) {
            $item['coronary_history'] = 1;
            array_push($hum_tags_arr, "冠心病");
        } else {
            $item['coronary_history'] = 0;
        }


        if (isset($q["diabetes_history"]) && $q["diabetes_history"] == 1) {
            $item['diabetes_history'] = 1;
            array_push($hum_tags_arr, "糖尿病");
        } else {
            $item['diabetes_history'] = 0;
        }
        
        if (isset($q["hyperlipemia_history"]) && $q["hyperlipemia_history"] == 1) {
            $item['hyperlipemia_history'] = 1;
            array_push($hum_tags_arr, "高血脂");
        } else {
            $item['hyperlipemia_history'] = 0;
        }

//        $this->saveHumanTags($user->uid, $hum_tags_arr);
        return $item;
    }

    private function saveQA_5($uid, $q) {
        $item = null;
        if (isset($q["wc"]) && $q["wc"] != 0) {
            $item['wc'] = $q["wc"];
        }
        return $item;
    }

    //吸烟
    private function saveQA_6($uid, $q) {
        $item = null;
        if (isset($q["smoking"])) {
            if ($q["smoking"] == 1) {
                $item['smoking'] = 1;
                $item['smoked'] = 0;
            } else {
                $item['smoking'] = 0;
                if (isset($q["smoked"])) {
                    if ($q["smoked"] == 1) {
                        $item['smoked'] = 1;
                    } else {
                        $item['smoked'] = 0;
                    }
                }
            }
        }
        return $item;
    }

    private function saveQA_7($uid, $q) {
        $item = null;
        if (isset($q["smoked"])) {
            if ($q["smoked"] == 1) {
                $item['smoked'] = 1;
            } else {
                $item['smoked'] = 0;
            }
        }
        return $item;
    }
    //服用避孕药
    private function saveQA_8($uid, $q) {
        $item = null;
        if (isset($q["contraceptive"])) {
            if ($q["contraceptive"] == 1) {
                $item['contraceptive'] = 1;
            } else {
                $item['contraceptive'] = 0;
            }
        }
        return $item;
    }

    private function saveQA_9($uid, $q) {
        $item = null;
        if (isset($q["hulu"])) {
            if ($q["hulu"] == 1) {
                $item['hulu'] = 1;
            } else {
                $item['hulu'] = 0;
            }
        }
        return $item;
    }

    private function saveQA_10($uid, $q) {
        $item = null;
        if (isset($q["sports"])) {
            if ($q["sports"] == 1) {
                $item['sports'] = 1;
            } else {
                $item['sports'] = 0;
            }
        }
        return $item;
    }

    private function saveQA_11($uid, $q) {
        $item = null;
        if (isset($q["stillsit"])) {
            if ($q["stillsit"] == 1) {
                $item['stillsit'] = 1;
            } else {
                $item['stillsit'] = 0;
            }
        }
        return $item;
    }
    //饮酒
    private function saveQA_12($uid, $q) {
        $item = null;
        if (isset($q["drink"])) {
            if ($q["drink"] == 1) {
                $item['drink'] = 1;
            } else {
                $item['drink'] = 0;
            }
        }
        return $item;
    }

    private function saveQA_13($uid, $q) {
        $item = null;
        if (isset($q["fruit"])) {
            if ($q["fruit"] == 1) {
                $item['fruit'] = 1;
            } else {
                $item['fruit'] = 0;
            }
        }
        return $item;
    }

    private function saveQA_14($uid, $q) {
        $item = null;
        if (isset($q["meat"])) {
            if ($q["meat"] == 1) {
                $item['meat'] = 1;
            } else {
                $item['meat'] = 0;
            }
        }
        return $item;
    }
    private function saveQA_15($uid, $q) {
        $item = null;
        if (isset($q["salt_food"])) {
            if ($q["salt_food"] == "yes") {
                $item['salt_food'] = 1;
            } else {
                $item['salt_food'] = 0;
            }
        }
        return $item;
    }
    

    private function saveQA_16($uid, $q) {
        $item = null;
        $hum_tags_arr = array();
        if (!empty($q['height']) && !empty($q['weight'])) {
            $item['height'] = $q["height"];
            $item['weight'] = $q["weight"];
            $item['weightLevel'] = $q["weight"] / (($q["height"] / 100) * ($q["height"] / 100));
            $item['weightLevel'] = round($item['weightLevel'], 2);
            if ($item['weightLevel'] > 24) {
                array_push($hum_tags_arr, "肥胖");
//                $this->saveHumanTags($user->uid, $hum_tags_arr);
            }
        }
        return $item;
    }




    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'option' => array(self::HAS_MANY, 'MeinianQuestionOptions', array('question_id' => 'id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'type' => 'Type',
            'questype' => 'Questype',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('questype', $this->questype, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,

        ));
    }

    public function getOptions($qid) {
        $criteria = new CDbCriteria;
        //		/$criteria->select = 't.title,t.type,t.id,option.name,option.description';
        $criteria->compare('t.id', $qid);
        $criteria->with = 'option';

        $result = EvaluateQuestionnaire::model()->find($criteria);

        return $result;
    }

    public function getQuesinfo($qid_str) {
        $criteria = new CDbCriteria();
        $criteria->addInCondition("id", $qid_str);
        $criteria->order = "questype";

        return EvaluateQuestionnaire::model()->findAll($criteria);
    }

}
