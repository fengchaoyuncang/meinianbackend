<?php

/**
 * This is the model class for table "bind_user".
 *
 * The followings are the available columns in table 'bind_user':
 * @property integer $id
 * @property string $app_id
 * @property string $bind_sns_id
 * @property string $type
 */
class BindUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BindUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bind_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_id, bind_sns_id, type', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, app_id, bind_sns_id, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_id' => 'App',
			'bind_sns_id' => 'Bind Sns',
			'type' => 'Type',
		);
	}

	public function doInsert($uid, $type, $snsid)
	{
		try
		{
			$connection = Yii::app()->db;
			$sql = "insert into bind_user(app_id, sns_id, type) values(:uid, :snsid, :type) on duplicate key update update_time=CURRENT_TIMESTAMP";
			$command = $connection->createCommand($sql);
			$command->bindParam(":uid", $uid, PDO::PARAM_STR);
			$command->bindParam(":snsid", $snsid, PDO::PARAM_INT);
			$command->bindParam(":type", $type, PDO::PARAM_INT);
			$command->execute();
			return 0;
		}
		catch(exception $e)
		{
			return 1;
		}
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('bind_sns_id',$this->bind_sns_id,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}