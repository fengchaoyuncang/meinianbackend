<?php

/**
 * This is the model class for table "evaluate_disease_element".
 *
 * The followings are the available columns in table 'evaluate_disease_element':
 * @property integer $id
 * @property string $field_name
 * @property string $name
 * @property string $description
 * @property string $suggest
 */
class EvaluateDiseaseElement extends CActiveRecord
{
        const TYPEQUESTION = 1;
    const TYPEREPORT = 2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateDiseaseElement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_disease_element';
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'field_name' => 'Field Name',
			'name' => 'Name',
			'description' => 'Description',
			'suggest' => 'Suggest',
		);
	}


	
	/**
	 * 通过诱因字符串查找诱因名称
	 * Enter description here ...
	 */
	public function getNameOfElement($element_ids){
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $element_ids);
		
		$result = EvaluateDiseaseElement::model()->findAll($criteria);
		
		return $result;
	}
	
	public function getIdByNames($element_names_null){
		$criteria = new CDbCriteria();
		$criteria->select = 'id';
		$criteria->addInCondition('name', $element_names_null);
		
		$result = EvaluateDiseaseElement::model()->findAll($criteria);
		return $result;
	}
	
	
	public function getSummarySuggestion(){
		$criteria = new CDbCriteria();
		$criteria->compare("suggest", 1);
		
		return EvaluateDiseaseElement::model()->findAll($criteria);
	}
	
}