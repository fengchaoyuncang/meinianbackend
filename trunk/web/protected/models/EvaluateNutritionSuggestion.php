<?php

/**
 * This is the model class for table "evaluate_nutrition_suggestion".
 *
 * The followings are the available columns in table 'evaluate_nutrition_suggestion':
 * @property integer $id
 * @property integer $disid
 * @property string $summary
 * @property string $reason
 * @property string $bring
 * @property string $symptom
 * @property string $nutrition_element
 * @property string $nutrition_target
 * @property string $nutrition_reason
 * @property string $nutrition_prescription
 * @property string $pro_news
 * @property string $diet
 * @property string $other
 * @property string $create_at
 * @property string $update_at
 */
class EvaluateNutritionSuggestion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateNutritionSuggestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_nutrition_suggestion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('disid, bring, symptom, nutrition_element, nutrition_target, nutrition_reason, nutrition_prescription, update_at', 'required'),
			array('disid', 'numerical', 'integerOnly'=>true),
			array('summary, reason, bring, symptom, pro_news, diet, other', 'length', 'max'=>2000),
			array('create_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, disid, summary, reason, bring, symptom, nutrition_element, nutrition_target, nutrition_reason, nutrition_prescription, pro_news, diet, other, create_at, update_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'disid' => 'Disid',
			'summary' => 'Summary',
			'reason' => 'Reason',
			'bring' => 'Bring',
			'symptom' => 'Symptom',
			'nutrition_element' => 'Nutrition Element',
			'nutrition_target' => 'Nutrition Target',
			'nutrition_reason' => 'Nutrition Reason',
			'nutrition_prescription' => 'Nutrition Prescription',
			'pro_news' => 'Pro News',
			'diet' => 'Diet',
			'other' => 'Other',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('disid',$this->disid);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('bring',$this->bring,true);
		$criteria->compare('symptom',$this->symptom,true);
		$criteria->compare('nutrition_element',$this->nutrition_element,true);
		$criteria->compare('nutrition_target',$this->nutrition_target,true);
		$criteria->compare('nutrition_reason',$this->nutrition_reason,true);
		$criteria->compare('nutrition_prescription',$this->nutrition_prescription,true);
		$criteria->compare('pro_news',$this->pro_news,true);
		$criteria->compare('diet',$this->diet,true);
		$criteria->compare('other',$this->other,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}