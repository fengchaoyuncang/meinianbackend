<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dongbowu
 * Date: 5/30/13
 * Time: 10:31 PM
 * To change this template use File | Settings | File Templates.
 */

class ReservationForm extends Reservation {

    public $verifyCode;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        ));
    }
}