<?php

/**
 * This is the model class for table "meinian_examination_basic".
 *
 * The followings are the available columns in table 'meinian_examination_basic':
 * @property integer $id
 * @property string $coronary_family_history
 * @property string $stroke_family_history
 * @property integer $uid
 * @property string $name
 * @property string $sex
 * @property string $check_time
 * @property string $no
 * @property string $card_no
 * @property integer $height
 * @property double $weight
 * @property double $weightLevel
 * @property integer $systolic_blood_pressure
 * @property integer $diastolic_blood_pressure
 * @property string $hypertension_family_history
 * @property string $diabetes_family_history_single
 * @property string $diabetes_family_history_double
 * @property string $hypertension_history
 * @property string $coronary_history
 * @property string $diabetes_history
 * @property string $hypercholesterolemia_history
 * @property string $hyperlipidemia_history
 * @property string $smoking
 * @property string $smoked
 * @property string $sports
 * @property string $salt_food
 * @property string $drink
 * @property string $fruit
 * @property double $meat
 * @property string $contraceptive
 * @property string $hulu
 * @property string $stillsit
 * @property double $wc
 * @property double $blood_triglycerides
 * @property double $blood_cholesterol
 * @property string $blood_sugar
 * @property string $lack_activity
 */
class EvaluateExaminationBasic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateExaminationBasic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_examination_basic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('coronary_family_history, stroke_family_history, uid, name, sex, check_time, no, card_no, height, weight, weightLevel, systolic_blood_pressure, diastolic_blood_pressure, hypertension_family_history, diabetes_family_history_single, diabetes_family_history_double, hypertension_history, coronary_history, diabetes_history, hypercholesterolemia_history, hyperlipidemia_history, smoking, smoked, sports, salt_food, drink, fruit, meat, contraceptive, hulu, stillsit, wc, blood_triglycerides, blood_cholesterol, blood_sugar, lack_activity', 'required'),
			array('uid, height, systolic_blood_pressure, diastolic_blood_pressure', 'numerical', 'integerOnly'=>true),
			array('weight, weightLevel, meat, wc, blood_triglycerides, blood_cholesterol', 'numerical'),
			array('coronary_family_history, stroke_family_history, name, no, hypertension_family_history, diabetes_family_history_single, diabetes_family_history_double, hypertension_history, coronary_history, diabetes_history, hypercholesterolemia_history, hyperlipidemia_history, smoking, smoked, sports, salt_food, drink, fruit, contraceptive, hulu, stillsit, blood_sugar, lack_activity', 'length', 'max'=>20),
			array('sex', 'length', 'max'=>1),
			array('card_no', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, coronary_family_history, stroke_family_history, uid, name, sex, check_time, no, card_no, height, weight, weightLevel, systolic_blood_pressure, diastolic_blood_pressure, hypertension_family_history, diabetes_family_history_single, diabetes_family_history_double, hypertension_history, coronary_history, diabetes_history, hypercholesterolemia_history, hyperlipidemia_history, smoking, smoked, sports, salt_food, drink, fruit, meat, contraceptive, hulu, stillsit, wc, blood_triglycerides, blood_cholesterol, blood_sugar, lack_activity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'coronary_family_history' => 'Coronary Family History',
			'stroke_family_history' => 'Stroke Family History',
			'uid' => 'Uid',
			'name' => 'Name',
			'sex' => 'Sex',
			'check_time' => 'Check Time',
			'no' => 'No',
			'card_no' => 'Card No',
			'height' => 'Height',
			'weight' => 'Weight',
			'weightLevel' => 'Weight Level',
			'systolic_blood_pressure' => 'Systolic Blood Pressure',
			'diastolic_blood_pressure' => 'Diastolic Blood Pressure',
			'hypertension_family_history' => 'Hypertension Family History',
			'diabetes_family_history_single' => 'Diabetes Family History Single',
			'diabetes_family_history_double' => 'Diabetes Family History Double',
			'hypertension_history' => 'Hypertension History',
			'coronary_history' => 'Coronary History',
			'diabetes_history' => 'Diabetes History',
			'hypercholesterolemia_history' => 'Hypercholesterolemia History',
			'hyperlipidemia_history' => 'Hyperlipidemia History',
			'smoking' => 'Smoking',
			'smoked' => 'Smoked',
			'sports' => 'Sports',
			'salt_food' => 'Salt Food',
			'drink' => 'Drink',
			'fruit' => 'Fruit',
			'meat' => 'Meat',
			'contraceptive' => 'Contraceptive',
			'hulu' => 'Hulu',
			'stillsit' => 'Stillsit',
			'wc' => 'Wc',
			'blood_triglycerides' => 'Blood Triglycerides',
			'blood_cholesterol' => 'Blood Cholesterol',
			'blood_sugar' => 'Blood Sugar',
			'lack_activity' => 'Lack Activity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('coronary_family_history',$this->coronary_family_history,true);
		$criteria->compare('stroke_family_history',$this->stroke_family_history,true);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('check_time',$this->check_time,true);
		$criteria->compare('no',$this->no,true);
		$criteria->compare('card_no',$this->card_no,true);
		$criteria->compare('height',$this->height);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('weightLevel',$this->weightLevel);
		$criteria->compare('systolic_blood_pressure',$this->systolic_blood_pressure);
		$criteria->compare('diastolic_blood_pressure',$this->diastolic_blood_pressure);
		$criteria->compare('hypertension_family_history',$this->hypertension_family_history,true);
		$criteria->compare('diabetes_family_history_single',$this->diabetes_family_history_single,true);
		$criteria->compare('diabetes_family_history_double',$this->diabetes_family_history_double,true);
		$criteria->compare('hypertension_history',$this->hypertension_history,true);
		$criteria->compare('coronary_history',$this->coronary_history,true);
		$criteria->compare('diabetes_history',$this->diabetes_history,true);
		$criteria->compare('hypercholesterolemia_history',$this->hypercholesterolemia_history,true);
		$criteria->compare('hyperlipidemia_history',$this->hyperlipidemia_history,true);
		$criteria->compare('smoking',$this->smoking,true);
		$criteria->compare('smoked',$this->smoked,true);
		$criteria->compare('sports',$this->sports,true);
		$criteria->compare('salt_food',$this->salt_food,true);
		$criteria->compare('drink',$this->drink,true);
		$criteria->compare('fruit',$this->fruit,true);
		$criteria->compare('meat',$this->meat);
		$criteria->compare('contraceptive',$this->contraceptive,true);
		$criteria->compare('hulu',$this->hulu,true);
		$criteria->compare('stillsit',$this->stillsit,true);
		$criteria->compare('wc',$this->wc);
		$criteria->compare('blood_triglycerides',$this->blood_triglycerides);
		$criteria->compare('blood_cholesterol',$this->blood_cholesterol);
		$criteria->compare('blood_sugar',$this->blood_sugar,true);
		$criteria->compare('lack_activity',$this->lack_activity,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * 通过诱因名称串以及用户id找出需要的诱因值
	 * Enter description here ...
	 */
	public function getValuesOfElement($element_names, $examination_id){
		$criteria = new CDbCriteria();
		$criteria->select = $element_names;
		$criteria->compare('id', $examination_id);
		$result = EvaluateExaminationBasic::model()->findAll($criteria);
		
		return $result;
	}
	
	/**
	 * 
	 * Enter description here ...
	 */
	static public function getIdOfUser($user){
		// TODO !! implement me
		$exmanition = EvaluateExaminationBasic::model()->findAllByAttributes(array('uid' => $user), array('order' => 'check_time'));
		if (count($exmanition) === 0){
			$exmanition = new EvaluateExaminationBasic();
			$exmanition->uid = $user;
			if(!$exmanition->save(false))
			{
				var_dump($exmanition->getErrors());
				exit();
			}
		}else{
			$exmanition = $exmanition[0];
		}
		return $exmanition;
	}
        /**
     * set data from hash array intent to insert datas to database
     * @param type $data_arr (hash array) e.g. array('name'=>'admin','password'=>'123456')
     */
    public function setData($data_arr,$array=null){
        foreach ($data_arr as $key => $value) {
            $this->$key = $value;
        }
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->$key = $value;
			}
		}
    }
}