<?php

/**
 * This is the model class for table "evaluate_report_other".
 *
 * The followings are the available columns in table 'evaluate_report_other':
 * @property integer $id
 * @property integer $uid
 * @property string $ECG
 * @property string $liver
 * @property string $gallbladder
 * @property string $pancreas
 * @property string $spleen
 * @property string $kidneys
 * @property string $uterus
 * @property string $anteroposterior_chest
 * @property string $lateral_cervical_spine
 * @property string $preliminary_opinion_one
 * @property string $breast
 * @property string $doppler
 * @property string $preliminary_opinion_two
 */
class EvaluateReportOther extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportOther the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_other';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, ECG, liver, gallbladder, pancreas, spleen, kidneys, uterus, anteroposterior_chest, lateral_cervical_spine, preliminary_opinion_one, breast, doppler, preliminary_opinion_two', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('ECG, liver, gallbladder, pancreas, spleen, kidneys, uterus, anteroposterior_chest, lateral_cervical_spine, preliminary_opinion_one, breast, doppler, preliminary_opinion_two', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, ECG, liver, gallbladder, pancreas, spleen, kidneys, uterus, anteroposterior_chest, lateral_cervical_spine, preliminary_opinion_one, breast, doppler, preliminary_opinion_two', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'ECG' => 'Ecg',
			'liver' => 'Liver',
			'gallbladder' => 'Gallbladder',
			'pancreas' => 'Pancreas',
			'spleen' => 'Spleen',
			'kidneys' => 'Kidneys',
			'uterus' => 'Uterus',
			'anteroposterior_chest' => 'Anteroposterior Chest',
			'lateral_cervical_spine' => 'Lateral Cervical Spine',
			'preliminary_opinion_one' => 'Preliminary Opinion One',
			'breast' => 'Breast',
			'doppler' => 'Doppler',
			'preliminary_opinion_two' => 'Preliminary Opinion Two',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('ECG',$this->ECG,true);
		$criteria->compare('liver',$this->liver,true);
		$criteria->compare('gallbladder',$this->gallbladder,true);
		$criteria->compare('pancreas',$this->pancreas,true);
		$criteria->compare('spleen',$this->spleen,true);
		$criteria->compare('kidneys',$this->kidneys,true);
		$criteria->compare('uterus',$this->uterus,true);
		$criteria->compare('anteroposterior_chest',$this->anteroposterior_chest,true);
		$criteria->compare('lateral_cervical_spine',$this->lateral_cervical_spine,true);
		$criteria->compare('preliminary_opinion_one',$this->preliminary_opinion_one,true);
		$criteria->compare('breast',$this->breast,true);
		$criteria->compare('doppler',$this->doppler,true);
		$criteria->compare('preliminary_opinion_two',$this->preliminary_opinion_two,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}