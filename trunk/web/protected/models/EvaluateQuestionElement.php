<?php

/**
 * This is the model class for table "meinian_question_element".
 *
 * The followings are the available columns in table 'meinian_question_element':
 * @property integer $id
 * @property integer $question_id
 * @property integer $element_id
 * @property string $element_name
 */
class EvaluateQuestionElement extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateQuestionElement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_question_element';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('question_id, element_id, element_name', 'required'),
		array('question_id, element_id', 'numerical', 'integerOnly'=>true),
		array('element_name', 'length', 'max'=>32),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('id, question_id, element_id, element_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'elements' => array(self::BELONGS_TO, 'MeinianDiseaseElement', array('element_id'=>'id')),
			'questionnaires' =>array(self::BELONGS_TO, 'EvaluateQuestionnaire', array('question_id'=>'id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'question_id' => 'Question',
			'element_id' => 'Element',
		'element_name' => 'Element Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('question_id',$this->question_id);
		$criteria->compare('element_id',$this->element_id);
		$criteria->compare('element_name',$this->element_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function getQuestionOfElement($elements_array){
		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.element_id', $elements_array);
		$criteria->with = array("elements","questionnaires");
			
		$result = EvaluateQuestionElement::model()->findAll($criteria);
		return $result;
	}
}