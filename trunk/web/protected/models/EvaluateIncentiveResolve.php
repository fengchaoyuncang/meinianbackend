<?php

/**
 * This is the model class for table "meinian_incentive_resolve".
 *
 * The followings are the available columns in table 'meinian_incentive_resolve':
 * @property integer $diseaseid
 * @property string $element
 * @property string $resolve
 */
class EvaluateIncentiveResolve extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateIncentiveResolve the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_incentive_resolve';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('diseaseid, element', 'required'),
		array('diseaseid', 'numerical', 'integerOnly'=>true),
		array('element', 'length', 'max'=>32),
		array('resolve', 'length', 'max'=>1000),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('diseaseid, element, resolve', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'diseaseid' => 'Diseaseid',
			'element' => 'Element',
			'resolve' => 'Resolve',
		);
	}

	public function primaryKey(){
		return array('diseaseid', 'element');
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('diseaseid',$this->diseaseid);
		$criteria->compare('element',$this->element,true);
		$criteria->compare('resolve',$this->resolve,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * 1. get the disease_id and the element to unique the advice
	 * 2. return the advice
	 */
	public function getResolve($disid, $element){
		$criteria = new CDbCriteria();
		/*$criteria->compare("diseaseid", $disid);
		 $criteria->compare("element", $element);*/
		$criteria->addCondition("diseaseid=:diseaseid");
		$criteria->addCondition("element=:element");
		$criteria->params[':diseaseid'] = $disid;
		$criteria->params[':element'] = $element;
		$ret = EvaluateIncentiveResolve::model()->find($criteria);

		return $ret;
	}

	/**
	 * 1. get the disease id and get the element
	 * 2. delete the row
	 * 3. return ret?1:0
	 * Enter description here ...
	 * $disid int
	 * $element array
	 */
	public function deleteResolve($disid, $element){
		$criteria = new CDbCriteria();
		$criteria->compare("diseaseid", $disid);
		$criteria->addInCondition("element", $element);

		$ret = EvaluateIncentiveResolve::model()->deleteAll($criteria);
		return print_r($criteria);
		/*		if ($ret != 0){
			return 1;
			}else{
			return 0;
			}
			*/
	}

	public function getResolvesOfEles($disid, $elements){
		$criteria = new CDbCriteria();
		$criteria->select = "resolve";
		$criteria->addCondition("diseaseid=:diseaseid");
		$criteria->params[':diseaseid'] = $disid;
		$criteria->addInCondition("element",$elements);
		$ret = EvaluateIncentiveResolve::model()->findAll($criteria);
		
		return $ret;
	}
}