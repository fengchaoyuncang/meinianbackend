<?php

/**
 * This is the model class for table "basic_exam_item".
 *
 * The followings are the available columns in table 'basic_exam_item':
 * @property integer $id
 * @property string $name
 * @property integer $item_id
 * @property string $over_view
 * @property string $over_reason
 * @property string $under_reason
 * @property string $cure_care
 * @property string $expert_advice
 * @property string $value_ranges
 */
class BasicExamItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasicExamItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'basic_exam_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('over_view, over_reason, under_reason, cure_care, expert_advice, value_ranges', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, item_id, over_view, over_reason, under_reason, cure_care, expert_advice, value_ranges', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
					'basicexamitemclassify' => array(self::BELONGS_TO, 'BasicExamItemClassify', array('item_id' => 'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => '项目名',
			'item_id' => 'Item',
			'over_view' => 'Over View',
			'over_reason' => 'Over Reason',
			'under_reason' => 'Under Reason',
			'cure_care' => 'Cure Care',
			'expert_advice' => 'Expert Advice',
			'value_ranges' => 'Value Ranges',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('over_view',$this->over_view,true);
		$criteria->compare('over_reason',$this->over_reason,true);
		$criteria->compare('under_reason',$this->under_reason,true);
		$criteria->compare('cure_care',$this->cure_care,true);
		$criteria->compare('expert_advice',$this->expert_advice,true);
		$criteria->compare('value_ranges',$this->value_ranges,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>20,),
		));
	}
}