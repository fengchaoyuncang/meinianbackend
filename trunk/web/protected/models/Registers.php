<?php

/**
 * This is the model class for table "registers".
 *
 * The followings are the available columns in table 'registers':
 * @property integer $id
 * @property string $dist_name
 * @property string $phone
 * @property string $licence
 */
class Registers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Registers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'registers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dist_name, phone, licence', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dist_name, phone, licence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dist_name' => 'Dist Name',
			'phone' => 'Phone',
			'licence' => 'Licence',
		);
	}

	public function findInfo($dist_name, $licence)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('dist_name',$dist_name);
		$criteria->compare('licence',$licence);
		$record = $this->find($criteria);
		return $record;	
	}
	
	public function saveInfo($phone, $dist_name, $licence)
	{
		try
		{
			$connection = Yii::app()->db;
			$sql = "insert into registers(dist_name, phone, licence) values(:dist_name, :phone, :licence) on duplicate key update phone=:phone";
			$command = $connection->createCommand($sql);
			$command->bindParam(":dist_name", $dist_name, PDO::PARAM_STR);
			$command->bindParam(":phone", $phone, PDO::PARAM_STR);
			$command->bindParam(":licence", $licence, PDO::PARAM_STR);
			$command->execute();
			return 0;
		}
		catch(exception $e)
		{
			return 1;
		}	
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dist_name',$this->dist_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('licence',$this->licence,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}