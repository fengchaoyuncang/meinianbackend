<?php

/**
 * This is the model class for table "admin_user".
 *
 * The followings are the available columns in table 'admin_user':
 * @property integer $id
 * @property string $username
 * @property string $alias
 * @property string $password
 * @property integer $center_id
 * @property integer $company_id
 * @property string $email
 * @property string $telphone
 */
class AdminUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdminUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, alias, password,email, telphone', 'required'),
			array('center_id, company_id', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>30),
			array('alias, telphone', 'length', 'max'=>20),
			array('password', 'length', 'max'=>50),
			array('email', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, alias, password, center_id, company_id, email, telphone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'company' => array(self::BELONGS_TO,'B2bCompany',array('company_id'=>'id')),
                    'center' => array(self::BELONGS_TO,'B2bSubCenter',array('center_id'=>'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => '用户名',
			'alias' => '别名',
			'password' => '密码',
			'center_id' => '体检中心',
			'company_id' => '公司',
			'email' => '邮件',
			'telphone' => '联系电话',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('center_id',$this->center_id);
		$criteria->compare('company_id',$this->company_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telphone',$this->telphone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}