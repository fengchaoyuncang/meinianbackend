<?php

/**
 * This is the model class for table "evaluate_incentive_percentage".
 *
 * The followings are the available columns in table 'evaluate_incentive_percentage':
 * @property integer $disease
 * @property string $element
 * @property string $gender
 * @property string $age
 * @property string $percentage
 */
class EvaluateIncentivePercentage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateIncentivePercentage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_incentive_percentage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */

	public function primaryKey(){
		return array('disease', 'element', 'gender', 'age');
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('disease, element, gender, age, percentage', 'required'),
		array('disease, gender, age', 'numerical', 'integerOnly'=>true),
		array('element, age', 'length', 'max'=>32),
		array('gender', 'length', 'max'=>11),
		array('percentage', 'length', 'max'=>100),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('disease, element, gender, age, percentage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'disease' => 'Disease',
			'element' => 'Element',
			'gender' => 'Gender',
			'age' => 'Age',
			'percentage' => 'Percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('disease',$this->disease);
		$criteria->compare('element',$this->element,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('age',$this->age,true);
		$criteria->compare('percentage',$this->percentage,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 *
	 * 0: no gender diff 1:man 2.woman
	 * age range is fixed  so there is 0 ranger to X pieces
	 * Enter description here ...
	 */
	public function getGenderAge($disid, $element){
		$criteria = new CDbCriteria();
		$criteria->addCondition("disease=:dis");
		$criteria->addCondition("element=:element");
		$criteria->params = array(":dis" => $disid, ":element" => $element);
		$ret = EvaluateIncentivePercentage::model()->findAll($criteria);
		if ($ret === null){
			$ret = "hello";
		}
		return $ret;
	}

	/**
	 * 1. get the disease_id	element	age gender percentage
	 * 2. whether the db doesn't has the row
	 * 2.1 if not 	create a new row
	 * 2.2 if	update the row
	 * 2.2.1 if the age[gender] is 0   1,delete all the age 2,create age = 0 row
	 * 2.2.2 if the age[gender] not 0  1,delete the age = 0 row 2,create a new row
	 *
	 * Enter description here ...
	 */
	public function updatePercentage($disid, $element, $age, $gender, $percentage){
		/*//查找
		$criteria = new CDbCriteria();
		$criteria->addCondition("disease=:diseaseid");
		$criteria->addCondition("element=:element");

		$criteria->params[':diseaseid'] = $disid;
		$criteria->params[':element'] = $element;
		
		if ($age != 0 && $gender != 0){
			$criteria->addCondition("age=:age");
			$criteria->addCondition("gender=:gender");
			$criteria->params[':gender'] = $gender;
			$criteria->params[':age'] = $age;
		}
		if ($age != 0 && $gender == 0){
			$criteria->addCondition("age=:age");
			$criteria->params[':age'] = $age;
			$criteria->addNotInCondition("gender", array($gender));
		}
		if ($age == 0 && $gender != 0){
			$criteria->addCondition("gender=:gender");
			$criteria->params[':gender'] = $gender;
			$criteria->addNotInCondition("age", array($gender));
		}
		if ($age == 0&& $gender == 0){
			
		}

		$percentage_ret_arr = EvaluateIncentivePercentage::model()->findAll($criteria);
		if (isset($percentage_ret_arr)){
			foreach ($percentage_ret_arr as &$it){
				$it->delete();
			}
		}*/

		/*$criteria_find = new CDbCriteria();
		$criteria_find->addCondition("disease=:diseaseid");
		$criteria_find->addCondition("element=:element");
		$criteria_find->addCondition("age=:age");
		$criteria_find->addCondition("gender=:gender");

		$criteria_find->params[':diseaseid'] = $disid;
		$criteria_find->params[':element'] = $element;
		$criteria_find->params[':gender'] = $gender;
		$criteria_find->params[':age'] = $age;

		$percentage_ret = EvaluateIncentivePercentage::model()->find($criteria_find);

		if (isset($percentage_ret)){
			$percentage_ret->age = $age;
			$percentage_ret->gender = $gender;
			$percentage_ret->percentage = $percentage;
			$ret = $percentage_ret->update();
		}else{
		echo $disid;
		echo $element;
		exit();*/
			$inc_resolve = new EvaluateIncentivePercentage();
			$inc_resolve->disease = $disid;
			$inc_resolve->element = $element;
			$inc_resolve->age = $age;
			$inc_resolve->gender = $gender;
			$inc_resolve->percentage = $percentage;
				
			$ret = $inc_resolve->save();
			
		return $ret;
	}
	
	/**
	 * get the percentages by element
	 * Enter description here ...
	 * @param unknown_type $ele_arr
	 */
	public function getPerByEles($disid, $ele_arr, $age, $gender){
		$criteria = new CDbCriteria();
		$criteria->addCondition("disease=:disid");
		$criteria->addCondition("age=:age");
		$criteria->addCondition("gender=:gender");
		$criteria->params = array(":disid" => $disid, ":age" => $age, ":gender" => $gender);
		$criteria->addInCondition("element", $ele_arr);
		
		$ret = EvaluateIncentivePercentage::model()->findAll($criteria);
		
		return $ret;
	}
	
	
}