<?php

/**
 * This is the model class for table "licences".
 *
 * The followings are the available columns in table 'licences':
 * @property integer $id
 * @property string $dist_name
 * @property string $licence
 */
class Licences extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Licences the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licences';
	}

	public function checkExist($dist_name, $licence)
	{	
		$criteria=new CDbCriteria;
		$criteria->compare('dist_name',$dist_name);
		$criteria->compare('licence',$licence);
		$record = $this->find($criteria);
		if($record == null)
		{
			return false;
		}
		else
		{
			return true;
		}	
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dist_name, licence', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dist_name, licence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dist_name' => 'Dist Name',
			'licence' => 'Licence',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dist_name',$this->dist_name,true);
		$criteria->compare('licence',$this->licence,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}