<?php

/**
 * This is the model class for table "evaluate_multi_suggestion".
 *
 * The followings are the available columns in table 'evaluate_multi_suggestion':
 * @property integer $id
 * @property integer $diseaseid
 * @property string $name
 * @property string $precautions
 * @property string $purpose
 * @property string $item
 * @property string $strength
 * @property string $times
 * @property string $frequency
 */
class EvaluateMultiSuggestion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateMultiSuggestion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_multi_suggestion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, precautions, purpose, item, strength, times, frequency', 'required'),
			array('diseaseid', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>32),
			array('precautions, purpose, item, strength, times, frequency', 'length', 'max'=>2000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, diseaseid, name, precautions, purpose, item, strength, times, frequency', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'diseaseid' => 'Diseaseid',
			'name' => 'Name',
			'precautions' => 'Precautions',
			'purpose' => 'Purpose',
			'item' => 'Item',
			'strength' => 'Strength',
			'times' => 'Times',
			'frequency' => 'Frequency',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('diseaseid',$this->diseaseid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('precautions',$this->precautions,true);
		$criteria->compare('purpose',$this->purpose,true);
		$criteria->compare('item',$this->item,true);
		$criteria->compare('strength',$this->strength,true);
		$criteria->compare('times',$this->times,true);
		$criteria->compare('frequency',$this->frequency,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * 
	 * sql: select count(*) from tb_product
	 * Enter description here ...
	 */
	public function getTotalCount(){
		$criteria = new CDbCriteria();
		$criteria->select = "count(id) as name";
		$ret = EvaluateMultiSuggestion::model()->find($criteria);
		
		return $ret;
	}
	
}