<?php

/**
 * This is the model class for table "evaluate_hum_constitution".
 *
 * The followings are the available columns in table 'evaluate_hum_constitution':
 * @property integer $id
 * @property integer $uid
 * @property integer $pinghescore
 * @property integer $yangxuscore
 * @property integer $yinxuscore
 * @property integer $qixuscore
 * @property integer $tanshiscore
 * @property integer $shirescore
 * @property integer $xueyuscore
 * @property integer $tebingscore
 * @property integer $qiyuscore
 * @property string $belongto
 * @property string $tendency
 */
class EvaluateHumConstitution extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateHumConstitution the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_hum_constitution';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, uid, pinghescore, yangxuscore, yinxuscore, qixuscore, tanshiscore, shirescore, xueyuscore, tebingscore, qiyuscore', 'numerical', 'integerOnly'=>true),
			array('belongto, tendency', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, pinghescore, yangxuscore, yinxuscore, qixuscore, tanshiscore, shirescore, xueyuscore, tebingscore, qiyuscore, belongto, tendency', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'pinghescore' => 'Pinghescore',
			'yangxuscore' => 'Yangxuscore',
			'yinxuscore' => 'Yinxuscore',
			'qixuscore' => 'Qixuscore',
			'tanshiscore' => 'Tanshiscore',
			'shirescore' => 'Shirescore',
			'xueyuscore' => 'Xueyuscore',
			'tebingscore' => 'Tebingscore',
			'qiyuscore' => 'Qiyuscore',
			'belongto' => 'Belongto',
			'tendency' => 'Tendency',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('pinghescore',$this->pinghescore);
		$criteria->compare('yangxuscore',$this->yangxuscore);
		$criteria->compare('yinxuscore',$this->yinxuscore);
		$criteria->compare('qixuscore',$this->qixuscore);
		$criteria->compare('tanshiscore',$this->tanshiscore);
		$criteria->compare('shirescore',$this->shirescore);
		$criteria->compare('xueyuscore',$this->xueyuscore);
		$criteria->compare('tebingscore',$this->tebingscore);
		$criteria->compare('qiyuscore',$this->qiyuscore);
		$criteria->compare('belongto',$this->belongto,true);
		$criteria->compare('tendency',$this->tendency,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}