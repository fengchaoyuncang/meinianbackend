<?php

class orderServices {
    /*
     * 获取订单列表
     * @param $status 订单状态
     * @return array
     */

    public function getOrderEachPage($status) {
        $session = new CHttpSession;
        $session->open();
        $sub_center_id = $session['sub_center_id'];  // 从session中获取子体检中心id
        $sub_center_id = 3;    //测试用

        $criteria = new CDbCriteria();
        $criteria->condition = "status=:status and sub_center_id=:sub_center_id";
        $criteria->params = array(":status" => $status, ":sub_center_id" => $sub_center_id);
        
        

        return new CActiveDataProvider("B2bOrder", array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 10,
            ),
        ));
    }

    /*
     * 处理订单
     * @param $status 订单状态
     * @param $id 订单id
     * return 布尔值
     */

    public function checkOrder() {
        if (!isset($_POST['status'])) {
            return JSONHelper::ERROR_ILLEGAL_SUB_CENTER_ID;
        }

        if (!isset($_POST['id'])) {
            return JSONHelper::ERROR_ILLEGAL_ORDER_ID;
        }
        $status = $_POST['status']; //获取订单状态
        $id = $_POST['id'];  //获取订单id
        $rets = B2bOrder::model()->findByPk($id);
        if ($rets === NULL) {
            return JSONHelper::ERROR_STATUS;
        }

        $rets->status = $status;
        return $rets;
    }

    /*
     * 从预约成功的订单中筛选符合某些条件的订单
     * @param $conditon 筛选条件
     *         1 已过预约日期
     *         2 过体检日，已经完成体检
     *         3 过体检日，未完成体检
     */

    public function selectSucceedOrders($statusCode) {
        $status = 2;  //预约成功的订单
        $now = date("Y-m-d");

        $session = new CHttpSession;
        $session->open();
        $sub_center_id = $session['sub_center_id'];  // 从session中获取子体检中心id
        $sub_center_id = 3;    //测试用
        //分页
        $criteria = new CDbCriteria;
        if ($statusCode == 1) {
            $criteria->condition = "status=:status and sub_center_id=:sub_center_id and order_time<:now";
            $criteria->params = array(":status" => $status, ":sub_center_id" => $sub_center_id, ":now" => $now);
        }else{
            $criteria->condition = "status=:status and sub_center_id=:sub_center_id";
            $criteria->params = array(":status" => $status, ":sub_center_id" => $sub_center_id);
        }
        
        
        
         return new CActiveDataProvider("B2bOrder", array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 10,
            ),
        ));
    }

    /*
     * 根据订单状态，获取相应类型订单中的最大id
     * @param $sub_center_id  子体检中心id
     * @param $status  订单状态
     * return 返回相应类型订单的最大id
     */

    public function getOrderMaxId($status) {
        //从session中获取子体检中心id
        $session = new CHttpSession;
        $session->open();
        $sub_center_id = $session['sub_center_id'];
        $sub_center_id = 3;    //测试用
        
        //获取新预约订单中的最大id
        $con = new CDbCriteria;
        $con->select = "MAX(ID) as id";
        $con->condition = "status=:status and sub_center_id=:sub_center_id";
        $con->params = array(":sub_center_id" => $sub_center_id, ":status" => $status);
        $ret = B2bOrder::model()->find($con);
        return $ret->id;
    }

    /*
     * 获取体检中心新预约的订单,此函数每隔一段时间自动一次，完成新预约订单在页面的自动显示
     * @param $maxID 新预约订单中的最大id
      $status 订单状态
     * return 返回新订单的jason格式
     */

    public function getNewOrder($maxID, $status) {
        //从session中获取子体检中心id
        $session = new CHttpSession;
        $session->open();
        $sub_center_id = $session['sub_center_id'];
        $sub_center_id = 3;    //测试用

        $rets = B2bOrder::model()->findAll("`id`>:maxID and `sub_center_id` = :sub_center_id and `status` = :status", array(":status" => $status, ":sub_center_id" => $sub_center_id, ":maxID" => $maxID));
        $newOrder = array();
        foreach ($rets as $k => $v) {
            $item['id'] = $v->id;
            $item['sub_center_name'] = $v->sub_center_name;
            $item['customer_name'] = $v->customer_name;
            $item['customer_gender'] = $v->customer_gender;
            $item['order_time'] = $v->order_time;
            $item['customer_no'] = $v->customer_no;
            $item['customer_company'] = $v->customer_company;
            array_push($newOrder, $item);
        }
        return JSONHelper::echoJSON(true, $newOrder);
    }
    
    /*
     * 批量处理订单
     * @param $_POST['status'] 订单状态
     * @param $_POST['userIds'] json格式的用户id
     */
        public function handleOrders() {
        if (isset($_POST['status']) and isset($_POST['userIds'])) {
            $status = $_POST['status'];
            $userIds = $_POST['userIds'];
            $userIds = json_decode($userIds);

            $str = "";
            foreach ($userIds as $key => $value) {
                $str.=$value;
                $str.=",";
            }
            $str = substr($str, 0, -1);

            $sql = "update b2b_order set status=$status where id in ($str)";
            $result = yii::app()->db->createCommand($sql);
            $query = $result->execute();
            echo $query;
        }else{
            echo 0;
        }
    }

}

?>
 