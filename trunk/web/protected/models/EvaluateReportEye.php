<?php

/**
 * This is the model class for table "evaluate_report_eye".
 *
 * The followings are the available columns in table 'evaluate_report_eye':
 * @property integer $id
 * @property integer $uid
 * @property string $vision_uncorrection_right
 * @property string $vision_uncorrection_left
 * @property string $vision_correction_right
 * @property string $vision_correction_left
 * @property string $color_vision
 * @property string $extraocular
 * @property string $eye_other
 * @property string $ophthalmoscopy
 * @property string $preliminary_opinion
 */
class EvaluateReportEye extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportEye the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_eye';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, vision_uncorrection_right, vision_uncorrection_left, vision_correction_right, vision_correction_left, color_vision, extraocular, eye_other, ophthalmoscopy, preliminary_opinion', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('vision_uncorrection_right, vision_uncorrection_left, vision_correction_right, vision_correction_left', 'length', 'max'=>20),
			array('color_vision, extraocular, eye_other, ophthalmoscopy, preliminary_opinion', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, vision_uncorrection_right, vision_uncorrection_left, vision_correction_right, vision_correction_left, color_vision, extraocular, eye_other, ophthalmoscopy, preliminary_opinion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'vision_uncorrection_right' => 'Vision Uncorrection Right',
			'vision_uncorrection_left' => 'Vision Uncorrection Left',
			'vision_correction_right' => 'Vision Correction Right',
			'vision_correction_left' => 'Vision Correction Left',
			'color_vision' => 'Color Vision',
			'extraocular' => 'Extraocular',
			'eye_other' => 'Eye Other',
			'ophthalmoscopy' => 'Ophthalmoscopy',
			'preliminary_opinion' => 'Preliminary Opinion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('vision_uncorrection_right',$this->vision_uncorrection_right,true);
		$criteria->compare('vision_uncorrection_left',$this->vision_uncorrection_left,true);
		$criteria->compare('vision_correction_right',$this->vision_correction_right,true);
		$criteria->compare('vision_correction_left',$this->vision_correction_left,true);
		$criteria->compare('color_vision',$this->color_vision,true);
		$criteria->compare('extraocular',$this->extraocular,true);
		$criteria->compare('eye_other',$this->eye_other,true);
		$criteria->compare('ophthalmoscopy',$this->ophthalmoscopy,true);
		$criteria->compare('preliminary_opinion',$this->preliminary_opinion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}