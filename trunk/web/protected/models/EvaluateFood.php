<?php

/**
 * This is the model class for table "evaluate_food".
 *
 * The followings are the available columns in table 'evaluate_food':
 * @property integer $id
 * @property string $name
 * @property string $image_url
 * @property integer $type
 * @property string $tags
 * @property string $create_at
 * @property string $update_at
 */
class EvaluateFood extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateFood the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_food';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
        
        public function getRecFoods($disname)
        {
            $condition = new CDbCriteria();
            $condition->addSearchCondition("tags", $disname.";");
            try {
                $ret = $this->findAll($condition);
            } catch (Exception $exc) {
                return false;
            }
            return $ret;
        }
	
}