<?php

/**
 * This is the model class for table "evaluate_report_log".
 *
 * The followings are the available columns in table 'evaluate_report_log':
 * @property integer $id
 * @property integer $uid
 * @property string $no
 */
class EvaluateReportLog extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EvaluateReportLog the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'evaluate_report_log';
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            "ticket" => array(self::BELONGS_TO, 'B2bTicket', array("tid" => "id")),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'uid' => 'Uid',
            'no' => 'No',
        );
    }

}
