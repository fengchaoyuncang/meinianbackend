<?php

/**
 * This is the model class for table "b2b_user".
 *
 * The followings are the available columns in table 'b2b_user':
 * @property integer $id
 * @property string $username
 * @property string $alias
 * @property integer $gender
 * @property string $identity_no
 * @property string $realname
 * @property string $password
 * @property string $center_id
 * @property string $email
 * @property string $telephone
 */
class B2bUser extends CActiveRecord {

    const GENDER_UNKNOWN = 0;
    const GENDER_MAN = 1;
    const GENDER_WOMAN = 2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return B2bUser the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'b2b_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
		return array(
			array('username, identity_no, realname, password, email, telephone', 'required'),
			array('gender', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>30),
			array('alias, identity_no, telephone', 'length', 'max'=>20),
			array('realname, center_id, email', 'length', 'max'=>100),
			array('password', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, alias, gender, identity_no, realname, password, center_id, email, telephone', 'safe', 'on'=>'search'),
		);
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
             "appointment" => array(self::HAS_MANY,'B2bExternalUser',array('identity_no'=>'customer_no'))
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => '用户手机号',
            'alias' => 'Alias',
            'gender' => '性别',
            'identity_no' => '身份证',
            'realname' => '姓名',
            'password' => '密码',
            'center_id' => '中心',
            'email' => 'Email',
            'telephone' => '电话',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('gender', $this->gender);
        $criteria->compare('identity_no', $this->identity_no, true);
        $criteria->compare('realname', $this->realname, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('center_id', $this->center_id, true);
        $criteria->compare('email', $this->email, true);
//        $criteria->compare('telphone', $this->telphone, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function verify($username, $password) {
        $ret = $this->findByAttributes(array('username'=>$username));
        if ($ret === null) {
            return false;
        }
        if ($ret["password"] !== $password) {
            return false;
        }
        return $ret;
    }

    //检测电话号码是否重复
    public function checkPhoneRepeat($telephone) {
        $ret = $this->findByAttributes(array("username" => $telephone));
        // TODO return !($ret !== null);
        if ($ret !== null) {
            return false;
        } else {
            return true;
        }
    }

    //检测邮箱是否重复
    public function checkEmailRepeat($email) {
        $ret = $this->findByAttributes(array("email" => $email));
        if ($ret !== null) {
            return false;
        } else {
            return true;
        }
    }
    
      /*
       * 根据用户uid获取用户信息
      */
       public function getUserInfoByUid($uid){
            $criteria = new CDbCriteria;
            $criteria->condition = 'id=:uid';
            $criteria->params=array(':id'=>$uid);
            return $this->find($criteria); 
       }

    /*
     * 转换性别
     * @param $gender
     * return 状态描述
     */
    static function getGenderDesc($gender)
    {

        switch ($gender) {
            case B2bUser::GENDER_UNKNOWN:
                $gender = "不祥";
                break;
            case B2bUser::GENDER_MAN:
                $gender = "男";
                break;
            case B2bUser::GENDER_WOMAN:
                $gender = "女";
                break;
        }
        return $gender;
    }

}