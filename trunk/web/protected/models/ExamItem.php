<?php

/**
 * This is the model class for table "exam_item".
 *
 * The followings are the available columns in table 'exam_item':
 * @property integer $id
 * @property string $item_name
 * @property string $description
 * @property integer $type
 * @property string $content
 * @property string $diagnosis
 * @property string $objective
 * @property string $analysis
 * @property string $suggestion
 *
 * The followings are the available model relations:
 * @property Indicator[] $indicators
 */
class ExamItem extends CActiveRecord
{
	//体检项目类型，0常规；1 非常规
	const NORMAL=0;
	const ABNORMAL=1;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ExamItem the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'info_exam_item';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type', 'numerical', 'integerOnly'=>true),
            array('item_name', 'length', 'max'=>255),
            array('description, content, diagnosis, objective, analysis, suggestion', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, item_name, description, type, content, diagnosis, objective, analysis, suggestion', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'indicators' => array(self::HAS_MANY, 'Indicator', 'item_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'item_name' => '体检项名称',
            'description' => '描述',
            'type' => '类型',
            'content' => '检查内容',
            'diagnosis' => '诊断标准',
            'objective' => '临床意义',
            'analysis' => '异常分析',
            'suggestion' => '专家建议',
        	
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;
        $criteria->order='t.id desc';
        $criteria->compare('id',$this->id);
        $criteria->compare('item_name',$this->item_name,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('type',$this->type);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('diagnosis',$this->diagnosis,true);
        $criteria->compare('objective',$this->objective,true);
        $criteria->compare('analysis',$this->analysis);
        $criteria->compare('suggestion',$this->suggestion);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
       		'pagination'=>array(
				'pageSize'=>20,
			),
        ));
    }

	public function listAllNames()
	{
       //搜索出所有的item
        $criteria = new CDbCriteria;
        $criteria->select="id, item_name";
        $rows = ExamItem::model()->findAll($criteria);
		$ret = array();
		foreach($rows as $row)
		{
			$id = $row->id;
			$name = $row->item_name;
			$ret[$id] = $name;
		}
		return $ret;
	}
	
    /**
     * Retrieves a list of models of all the records in exam_item table.
     * @return array of models.
     */
    public function listAll($type)
    {
        //搜索出所有的item，然后在对应的搜索出所有的indicator
        $criteria = new CDbCriteria;
        $criteria->select="*";
        $criteria->order="id";
        /*$type等于空串，或者* 则返回所有值。否则返回对应type的值*/
        if(strlen($type)!=0 && strcmp($type, "*")!=0)
        {
            $criteria->compare("type", $type);
        }
        $rows = ExamItem::model()->findAll($criteria);
        return $rows;
    }

    /**
     * Conver the model to an array.
     * @return an array.
     */
    public function extent2Indicator($model)
    {
        $ret = array();
        if($model!=null)
        {
            $itemId=$model->id;
            $criteria = new CDbCriteria;
            $criteria->select="id, name";
            $criteria->compare("item_id", $itemId);
            $relatedIndicatorsRows = Indicator::model()->findAll($criteria);
            $indicators = array();
            for($i=0; $i < count($relatedIndicatorsRows); $i++)
            {
                $indicator = $relatedIndicatorsRows[$i];
                $indicatorArray = array("iid"=>$indicator->id, "iname"=>$indicator->name);
                array_push($indicators, $indicatorArray);
                //$itemsArray[$i] = $model->model2array($all_exam_items[$i]);
            }
            $ret= array('item_id'=>$model->id, 'item_name'=>$model->item_name, 'type'=>$model->type, 'indicators'=>$indicators);
        }
        return $ret;
    }
}