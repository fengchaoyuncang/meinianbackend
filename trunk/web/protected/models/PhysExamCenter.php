<?php

/**
 * This is the model class for table "phys_exam_center".
 *
 * The followings are the available columns in table 'phys_exam_center':
 * @property integer $Id
 * @property string $brand_name
 * @property string $shop_name
 * @property string $prov
 * @property string $city
 * @property string $address_in_city
 * @property string $phone
 * @property string $business_hour
 * @property double $gps_lat
 * @property double $gps_lon
 */
class PhysExamCenter extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PhysExamCenter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
//		return 'phys_exam_center';
        return 'b2b_sub_center';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('gps_lat, gps_lon', 'numerical'),
            array('brand_name, shop_name, prov, city, address_in_city, phone, business_hour', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('brand_name, shop_name, prov, city, address_in_city, phone, business_hour', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getCenterByPk($cid) {
        $ret = array();
        $row = $this->findByPk($cid);
        if (null != $row) {
            $oneRegion = $this->covertRow2Array($row);
            array_push($ret, $oneRegion);
        }
        return $ret;
    }

    public function lbsSearch($gps_lat, $gps_lon, $radius, $cn = "", $centers = array()) {
        $block = LBSHelper::getAround($gps_lat, $gps_lon, $radius);
        $ret = array();
        $center = implode(",", $centers);
        if (empty($center)) {
            echo JSONHelper::retInfo(false, JSONHelper::ERROR_ILLEGAL_UID);
            Yii::app()->end();
        }
        $criteria = new CDbCriteria;
        $criteria->select = "*";
        $criteria->distinct = true;
        if (strlen($cn) > 0) {
            $criteria->condition = "gps_lat>=:minLat and gps_lat<=:maxLat and gps_lon>=:minLon and gps_lon<=:maxLon and brand_name=:brand_name and status=:status";
            $criteria->params = array(
                ":minLat" => $block["minLat"],
                ":maxLat" => $block["maxLat"],
                ":minLon" => $block["minLon"],
                ":maxLon" => $block["maxLon"],
                ":status" => 0,
                ":brand_name" => $cn);
        } else {
            $criteria->condition = "id in ($center) and gps_lat>=:minLat and gps_lat<=:maxLat and gps_lon>=:minLon and gps_lon<=:maxLon and status=:status";
            $criteria->params = array(
                ":minLat" => $block["minLat"],
                ":maxLat" => $block["maxLat"],
                ":minLon" => $block["minLon"],
                ":status" => 0,
                ":maxLon" => $block["maxLon"]);
            $criteria->addInCondition("id", $centers);
        }
        $criteria->order = "gps_lat, gps_lon";
        $rows = $this->findAll($criteria);
        foreach ($rows as $row) {
            $oneRegion = $this->covertRow2Array($row);
            array_push($ret, $oneRegion);
        }
        return $ret;
    }

    public function getCenters($prov, $city, $cn) {
        $ret = array();
        $criteria = new CDbCriteria;
        $criteria->select = "*";
        $criteria->distinct = true;
        $criteria->condition = "(prov=:prov and	city=:city) and brand_name=:name";
        $criteria->params = array(":prov" => $prov, ":city" => $city, ":name" => $cn);
        $criteria->order = "shop_name";
        $rows = $this->findAll($criteria);
        foreach ($rows as $row) {
            $oneRegion = $this->covertRow2Array($row);
            array_push($ret, $oneRegion);
        }
        return $ret;
    }

    private function covertRow2Array($row) {
        return array(
            "id" => $row->id,
//				"brand_name"=>$row->brand_name, 
            "name" => $row->name,
            "province" => $row->province,
            "city" => $row->city,
            "address" => $row->address,
            "telephone" => $row->telephone,
//				"business_hour"=>$row->business_hour, 
            "gps_lat" => $row->gps_lat,
            "gps_lon" => $row->gps_lon,
            "baidu_map_url" => $row->baidu_map_url);
    }

    public function getAllRegions($cn) {
        $ret = array();
        $criteria = new CDbCriteria;
        $criteria->select = "brand_name, prov, city";
        $criteria->distinct = true;
        $criteria->order = "brand_name, prov, city";
        if (strlen($cn) > 0) {
            $criteria->condition = "brand_name='$cn'";
        }
        $regions = $this->findAll($criteria);
        //整理成以branda_name为key的数组，值为城市列表的数组
        foreach ($regions as $row) {
            $brand_name = $row->brand_name;
            $oneBrandRegionArray = array_key_exists($brand_name, $ret) ? $ret[$brand_name] : array();
            $oneRegion = array("prov" => $row->prov, "city" => $row->city);
            array_push($oneBrandRegionArray, $oneRegion);
            $ret[$brand_name] = $oneBrandRegionArray;
        }
        return $ret;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'brand_name' => 'Brand Name',
            'shop_name' => 'Shop Name',
            'prov' => 'Prov',
            'city' => 'City',
            'address_in_city' => 'Address In City',
            'phone' => 'Phone',
            'business_hour' => 'Business Hour',
            'gps_lat' => 'Lat',
            'gps_lon' => 'Lon',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('brand_name', $this->brand_name, true);
        $criteria->compare('shop_name', $this->shop_name, true);
        $criteria->compare('prov', $this->prov, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('address_in_city', $this->address_in_city, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('business_hour', $this->business_hour, true);
        $criteria->compare('gps_lat', $this->gps_lat);
        $criteria->compare('gps_lon', $this->gps_lon);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}