<?php
class SportsService
{
    public function getRecList($arr)
    {
//        if(!isset($arr["uid"])){
//            return JSONHelper::ERROR_ILLEGAL_REQUEST;
//        }
        $sport = (isset($arr["sport"])&&in_array($arr['sport'], array("fanying","rouren","liliang","naili","pingheng")))?$arr["sport"]:"fanying";
        
        $ret = EvaluateSports::model()->getRecSports($sport);
        $items = array();
        if(count($ret) === 0){
            return JSONHelper::ERROR_SERVICE;
        }
        foreach($ret as $it){
            $it = (object)$it;
            $item["id"] = $it->id;
            $item["title"] = $it->title;
            $item["fanying"] = $it->fanying;
            $item["liliang"] = $it->liliang;
            $item["naili"] = $it->naili;
            $item["rouren"] = $it->rouren;
            $item["pingheng"] = $it->pingheng;
            $item["image_url"] = $it->image_url;
            $item["type"] = $it->type;
            $item["typename"] = EvaluateSports::getSportTypeName($it->type);
            array_push($items, $item);
        }
        return $items;
    }
    /**
     * 
     */
    public function getList($arr)
    {
//        if(!isset($arr["uid"])){
//            return JSONHelper::ERROR_ILLEGAL_REQUEST;
//        }
        $sport = isset($arr["sport"])&&  in_array($arr['sport'], array("fanying","rouren","liliang","naili","pingheng"))?$arr["sport"]:"fanying";
        
        $ret = EvaluateSports::model()->getSportsList($sport);
        $items = array();
        if(count($ret) === 0){
            return JSONHelper::ERROR_SERVICE;
        }
        foreach($ret as $it){
            $it = (object)$it;
            $item["id"] = $it->id;
            $item["title"] = $it->title;
            $item["fanying"] = $it->fanying;
            $item["liliang"] = $it->liliang;
            $item["naili"] = $it->naili;
            $item["rouren"] = $it->rouren;
            $item["pingheng"] = $it->pingheng;
            $item["image_url"] = $it->image_url;
            $item["type"] = $it->type;
            $item["typename"] = EvaluateSports::getSportTypeName($it->type);
            array_push($items, $item);
        }
        return $items;
    }
    
    public function getDetailById($arr)
    {
        if(!isset($arr["sportid"])){
            return JSONHelper::ERROR_ILLEGAL_REQUEST;
        }
        $sportid = $arr["sportid"];
        $ret = EvaluateSports::model()->findByPk($sportid);
        if($ret === null){
            return JSONHelper::ERROR_NOEXIST_SPORT;
        }
        $item["id"] = $ret->id;
        $item["title"] = $ret->title;
        $item["fanying"] = $ret->fanying;
        $item["liliang"] = $ret->liliang;
        $item["naili"] = $ret->naili;
        $item["rouren"] = $ret->rouren;
        $item["pingheng"] = $ret->pingheng;
        $item["image_url"] = $ret->image_url;
        $item["type"] = $ret->type;
        $item["desc"] = $ret->desc;
        $item["contenttype"] = $ret->contenttype;
        $item["content"] = $ret->content;
        $item["author"] = $ret->author;
        $item["tablesofcontents"] = $ret->tablesofcontents;
        $item["typename"] = EvaluateSports::getSportTypeName($ret->type);
        $item["video"] = array();
        if(intval($ret->contenttype) == EvaluateSports::CONTENT_VIDEO)
        {
            $ret = EvaluateSportsList::model()->findAllByAttributes(array("sportid" => $sportid));
            
            foreach ($ret as $it) {
                $v["title"]=$it->title;
                $v["url"]=$it->video_url;
                array_push($item["video"], $v);
            }
        }
        return $item;
    }

}
