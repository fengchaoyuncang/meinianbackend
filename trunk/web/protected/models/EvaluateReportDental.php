<?php

/**
 * This is the model class for table "evaluate_report_dental".
 *
 * The followings are the available columns in table 'evaluate_report_dental':
 * @property integer $id
 * @property integer $uid
 * @property string $lip
 * @property string $oral_mucosa
 * @property string $tooth
 * @property string $periodontal
 * @property string $tongue
 * @property string $palate
 * @property string $parotid
 * @property string $submandibular
 * @property string $sublingual_gland
 * @property string $TMJ
 * @property string $dental_other
 * @property string $preliminary_opinion
 */
class EvaluateReportDental extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportDental the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_dental';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, lip, oral_mucosa, tooth, periodontal, tongue, palate, parotid, submandibular, sublingual_gland, TMJ, dental_other, preliminary_opinion', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('lip, oral_mucosa, tooth, periodontal, tongue, palate, parotid, submandibular, sublingual_gland, TMJ, preliminary_opinion', 'length', 'max'=>200),
			array('dental_other', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, lip, oral_mucosa, tooth, periodontal, tongue, palate, parotid, submandibular, sublingual_gland, TMJ, dental_other, preliminary_opinion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'lip' => 'Lip',
			'oral_mucosa' => 'Oral Mucosa',
			'tooth' => 'Tooth',
			'periodontal' => 'Periodontal',
			'tongue' => 'Tongue',
			'palate' => 'Palate',
			'parotid' => 'Parotid',
			'submandibular' => 'Submandibular',
			'sublingual_gland' => 'Sublingual Gland',
			'TMJ' => 'Tmj',
			'dental_other' => 'Dental Other',
			'preliminary_opinion' => 'Preliminary Opinion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('lip',$this->lip,true);
		$criteria->compare('oral_mucosa',$this->oral_mucosa,true);
		$criteria->compare('tooth',$this->tooth,true);
		$criteria->compare('periodontal',$this->periodontal,true);
		$criteria->compare('tongue',$this->tongue,true);
		$criteria->compare('palate',$this->palate,true);
		$criteria->compare('parotid',$this->parotid,true);
		$criteria->compare('submandibular',$this->submandibular,true);
		$criteria->compare('sublingual_gland',$this->sublingual_gland,true);
		$criteria->compare('TMJ',$this->TMJ,true);
		$criteria->compare('dental_other',$this->dental_other,true);
		$criteria->compare('preliminary_opinion',$this->preliminary_opinion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}