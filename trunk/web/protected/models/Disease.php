<?php

class Disease {

    const GAOXUEYA = 1;
    const GUANXINBING = 2;
    const NAOZUZHONG = 3;
    const TANGNIAOBING = 4;

    private $_id; //disease id
    private $_reportInfo = array();

    function __construct($id = null) {
        $this->_id = intval($id);
    }

    /**
     * 删除所有的basic
     * Enter description here ...
     * @param unknown_type $uid
     * @param unknown_type $disid
     */
    static public function deleteUserInfo($uid, $disid) {
        $disease_ret = EvaluateDiseaseType::model()->findByPk($disid);
        $ele_arr = array();
        $ele_name_str = $disease_ret->element_name;
        $ret = EvaluateExaminationBasic::model()->findByAttributes(array("uid" => $uid));

        $ele_arr = explode(",", $ele_name_str);

        foreach ($ele_arr as $ele) {
            $ret->$ele = NULL;
        }
        $ret->update();

        return $ret;
    }

    /**
     * 获取相关诱因
     */
    public function getElements() {
        $criteria = new CDbCriteria;
        $criteria->condition = "t.id =:id";
        $criteria->params = array(':id' => $this->_id);
        //$criteria->with=array('order',);
        $ret = EvaluateDiseaseType::model()->findByPk($this->_id);
        if ($ret === null) {
            return false;
        }

        return explode(",", $ret->element_name);
    }

    static public function getUserInfo($uid) {
        $ret = EvaluateExaminationBasic::model()->findAllByAttributes(array("uid" => $uid));
        if (0 === count($ret)) {
            $ret = new EvaluateExaminationBasic;
            $ret->uid = $uid;
            $re = $ret->save(false);
        } else {
            $ret = $ret[0];
        }
        return $ret;
    }

    public function assess($elements,$reportinfo = array(), $future = 0) {
        if(is_array($elements)){
            $elements = new CObject($elements);
        }else{
            return false;
        }
        $this->_reportInfo = $reportinfo;
        switch ($this->_id) {
            case Disease::GAOXUEYA:
                $ret = $this->Gaoxueya($elements, $this->_id, $future);
                break;
            case Disease::GUANXINBING:
                $ret = $this->Guanxinbing($elements, $this->_id, $future);
                break;
            case Disease::NAOZUZHONG:
                $ret = $this->Naozuzhong($elements, $this->_id, $future);
                break;
            case Disease::TANGNIAOBING:
                $ret = $this->Tangniaobing($elements, $this->_id, $future);
                break;
            default:
                break;
        }
       
        return $ret;
    }
    /**
     * 危险因素详解
     */
    public function yinsujiedu($userinfo, $userReportInfo,$eles) {
        if(is_array($userinfo)){
            $userinfo = new CObject($userinfo);
        }
        foreach($userReportInfo as $key=>$it){
            $userinfo->$key = $it["issue"];
        }
        
        $ret = EvaluateDiseaseElement::model()->findAllByAttributes(array(
            "field_name" => $eles
        ));
        $items = array();
        foreach ($ret as $it) {
            $col = $it->field_name;
            if ($userinfo->$col === null) {
                continue;
            }
            $item["name"] = $it->name;
            $item["des"] = $it->description;
            $item["abnormal"] = 0;
            $item["jiedu"] = null;
            $item["jianyi"] = array();



            //下面是处理错误有的特殊情况
            switch ($col) {
                case "age":
                    $item["value"] = $this->getAge($userinfo->$col);
                    break;
                case "weightLevel":
                    $item["value"] = number_format($userinfo->$col, 1, '.', '');
                    if ($userinfo->$col > 24) {
                        $item["abnormal"] = 1;
                    }
                    break;
                
                case "blood_triglycerides":
                case "blood_cholesterol":
                    if (intval($userinfo->$col) == 2) {
                        $item["abnormal"] = 1;
                        $item["value"] = "偏低";
                    } elseif (intval($userinfo->$col) == 1) {
                        $item["abnormal"] = 1;
                        $item["value"] = "偏高";
                    } else {
                        $item["value"] = "正常";
                    }
                case "blood_triglycerides":
                case "blood_cholesterol":
                case "blood_sugar":
                    if(intval($userReportInfo[$col]["issue"]) == 1){
                        $item["abnormal"] = 1;
                    }else{
                        $item["abnormal"] = 0;
                    }
                    $item["value"] = $userReportInfo[$col]["value"];
                    break;
                default:
                    if (intval($userinfo->$col) == 1) {
                        $item["abnormal"] = 1;
                        $item["value"] = "有";
                    } else {
                        $item["value"] = "无";
                    }
                    break;
            }
            if ($item["abnormal"] === 1) {
                $ret = EvaluateIncentiveResolve::model()->findByAttributes(
                        array("diseaseid" => $this->_id, "element" => $col));
//                var_dump($ret);exit();
                if (!empty($ret->resolve)) {
                    $item["jiedu"] = $ret->resolve;
                } else {
                    $item["jiedu"] = '';
                }
                if ((!empty($ret->suggest) && !empty($ret->suggesttitle)) && ($ret->suggest != null && $ret->suggesttitle != null)) {
                    $item["jianyi"]["title"] = $ret->suggesttitle;
                    $item["jianyi"]["content"] = $ret->suggest;
                }
            }
            array_push($items, $item);
        }
        return $items;
    }

    /**
     * 1diseaeid => $this->_id	element => criteria(element = 1)
     * get the user's examination ret = getuserinfo()
     *
     * 2get the element = 1 examination
     * foreach(ret as key => value)
     * if (value == 1)
     * ele_arr[] = key
     *
     * 3ele_arr ==> ele_str
     * 4get the element resolve by diseaseid and ele_str
     * 5 some special element solve
     */
    public function getAdvice($userinfo) {
        $items = $this->getElements();
        $ele_arr = array();
        foreach ($items as $key => $element_name) {
            $col = $element_name;
            //if the user's method
            if ($userinfo->$col == 1) {
                array_push($ele_arr, $col);
            }
        }

        $resolve = EvaluateIncentiveResolve::model()->getResolvesOfEles($this->_id, $ele_arr);

        $res_arr = array();
        foreach ($resolve as $key => $value) {
            $res_arr[] = $value;
        }
        return $res_arr;
    }

    public function getAge($bir, $future = 0) {

        $birthday = date_create($bir);
        $age = date_diff(date_create(), $birthday);
        return $age->y + $future;
    }

    /**
     * 1	get the user element list
     * 2	get the age range number,
     * 		get the gender number
     * 3
     * calculate the result
     * $elements	all the user elements
     * return $ret the result of the
     *
     * Enter description here ...
     */
    public function Gaoxueya($elements, $disid, $future) {
        $ret = array();
        $age = $this->getAge($elements->age, $future);
        if ($age <= 40) {
            $ret['int_med_val_low'] = 2.715834647;
            $ret['int_med_val_high'] = 5.715834647;
            $ret['ceil'] = 8.339;
        } else {
            $ret['int_med_val_low'] = 0.857;
            $ret['int_med_val_high'] = 1.581;
            $ret['ceil'] = 8.339;
        }

//        if ($elements->gender == JSONHelper::MAN) {
//            $gender = JSONHelper::MAN;
//        } else {
//            $gender = JSONHelper::WOMAN;
//        }
        $gender = $elements->gender;
        if ($elements->hypertension_history) {
            $ret['value'] = $ret['ceil'] * 0.95;
            return $ret;
        }
        //获取年龄区间
        $age_num = $this->getAgeRangeCount($age);
        //疾病相关诱因数组
        $dis_rela_ele_arr = $this->getElements();
        //年龄性别权重值
       
        $percentage_ret = EvaluateIncentivePercentage::model()->findAllByAttributes(
                array("disease" => $disid,
                    "age" => $age_num,
                    "gender" => $gender,
                    "element" => $dis_rela_ele_arr
                )
        );


        if (empty($percentage_ret)) {
            return false;
        }
        /*
         * 1. get all of the unique disease relation element's percentage
         * 2. get each of the percentage
         * 		1. if the percentage of one element as A string has not "#"
         * 			get the user A's value if 1 plus the percentage
         * 		2. else push the string having "#" into a array
         * 			get the user A's value to and the array[value] is the A's percentage
         */
        /*
         * use this code later */
        //计算权重
        $value = $this->colculatePercentage($elements, $percentage_ret);
        
        //针对体检报告的数据
        foreach($this->_reportInfo as $key => $e){
            if($e["issue"]){
                $r = EvaluateIncentivePercentage::model()->findByAttributes(
                        array("disease"=>  $this->_id,"element"=>$key,
                            "gender"=>$elements->gender,"age"=>$age_num));
                if($r !== null){
                    $value += $r->percentage;
                }
                
            }
        }
        
        $ret["value"] = $value;

        return $ret;
    }

    public function Guanxinbing($elements, $disid, $future) {
        $age = $this->getAge($elements->age, $future);

        if ($age <= 40) {
            $ret['int_med_val_low'] = 5.232;
            $ret['int_med_val_high'] = 10.232;
            $ret['ceil'] = 16.676;
        } else {
            $ret['int_med_val_low'] = 0.743;
            $ret['int_med_val_high'] = 1.797;
            $ret['ceil'] = 16.676;
        }
        $gender = $elements->gender;
        if ($elements->coronary_history) {
            $ret['value'] = $ret['ceil'] * 0.95;
            return $ret;
        }

        $age_num = $this->getAgeRangeCount($age);
        $dis_rela_ele_arr = $this->getElements();

        $special_arr = array("diabetes_family_history_single", "diabetes_family_history_double");
        $dis_rela_ele_arr = array_diff($dis_rela_ele_arr, $special_arr);

        $percentage_ret = EvaluateIncentivePercentage::model()->getPerByEles(
                $this->_id, $dis_rela_ele_arr, $age_num, $gender);

        $value = $this->colculatePercentage($elements, $percentage_ret);

        $special_percent_ret = EvaluateIncentivePercentage::model()->getPerByEles(
                $this->_id, $special_arr, $age_num, $gender);
        
        $special_percent_arr = array();
        foreach ($special_percent_ret as $it) {
            $special_percent_arr["$it->element"] = $it->percentage;
        }
        if ($elements->diabetes_family_history_double != 0 || $elements->diabetes_family_history_single != 0) {
            $value += $special_percent_arr["diabetes_family_history_single"];
        }
        //针对体检报告的数据
        foreach($this->_reportInfo as $key => $e){
            if($e["issue"]){
                $r = EvaluateIncentivePercentage::model()->findByAttributes(
                        array("disease"=>  $this->_id,"element"=>$key,
                            "gender"=>$gender,"age"=>$age_num));
                if($r !== null){
                    $value += $r->percentage;
                }
            }
        }
        $ret['value'] = $value;

        return $ret;
    }

    public function Naozuzhong($elements, $disid, $future) {
        $age = $this->getAge($elements->age, $future);
        if ($age <= 40) {
            $ret['int_med_val_low'] = 3.129;
            $ret['int_med_val_high'] = 16.953;
            $ret['ceil'] = 18.953;
        } else {
            $ret['int_med_val_low'] = 1.506;
            $ret['int_med_val_high'] = 2.310;
            $ret['ceil'] = 18.953;
        }
        $gender = $elements->gender;
        
        $age_num = $this->getAgeRangeCount($age);
        $dis_rela_ele_arr = $this->getElements();

        $percentage_ret = EvaluateIncentivePercentage::model()->getPerByEles($this->_id, $dis_rela_ele_arr, $age_num, $gender);

        $value = $this->colculatePercentage($elements, $percentage_ret);
        
        //针对体检报告的数据
        foreach($this->_reportInfo as $key => $e){
            if($e["issue"]){
                $r = EvaluateIncentivePercentage::model()->findByAttributes(
                        array("disease"=>  $this->_id,"element"=>$key,
                            "gender"=>$gender,"age"=>$age_num));
                if($r !== null){
                    $value += $r->percentage;
                }
                
            }
        }
        $ret["value"] = $value;

        return $ret;
    }

    public function Tangniaobing($elements, $disid, $future) {
        $age = $this->getAge($elements->age, $future);
        if ($age <= 40) {
            $ret['int_med_val_low'] = 1.683;
            $ret['int_med_val_high'] = 10.683;
            $ret['ceil'] = 13.133;
        } else {
            $ret['int_med_val_low'] = 0.569;
            $ret['int_med_val_high'] = 1.409;
            $ret['ceil'] = 13.133;
        }
        $gender = $elements->gender;
        if ($elements->diabetes_history) {
            $ret['value'] = $ret['ceil'] * 0.95;
            return $ret;
        }

        $age_num = $this->getAgeRangeCount($age);
        $dis_rela_ele_arr = $this->getElements();

        $special_arr = array("diabetes_family_history_single", "diabetes_family_history_double");
        $dis_rela_ele_arr = array_diff($dis_rela_ele_arr, $special_arr);
        $percentage_ret = EvaluateIncentivePercentage::model()->getPerByEles($this->_id, $dis_rela_ele_arr, $age_num, $gender);

        $value = $this->colculatePercentage($elements, $percentage_ret);

        $special_percent_ret = EvaluateIncentivePercentage::model()->getPerByEles($this->_id, $special_arr, $age_num, $gender);
        $special_percent_arr = array();
        foreach ($special_percent_ret as $it) {
            $special_percent_arr["$it->element"] = $it->percentage;
        }
        if ($elements->diabetes_family_history_double != 0) {
            $value += isset($special_percent_arr["diabetes_family_history_double"]) ?
                    $special_percent_arr["diabetes_family_history_double"] : 0;
        } else {
            if ($elements->diabetes_family_history_single != 0) {
                $value += isset($special_percent_arr["diabetes_family_history_single"]) ?
                        $special_percent_arr["diabetes_family_history_single"] : 0;
            }
        }
        
        //针对体检报告的数据
        foreach($this->_reportInfo as $key => $e){
            if($e["issue"]){
                $r = EvaluateIncentivePercentage::model()->findByAttributes(
                        array("disease"=>  $this->_id,"element"=>$key,
                            "gender"=>$elements->gender,"age"=>$age_num));
                if($r !== null){
                    $value += $r->percentage;
                }
                
            }
        }

        $ret["value"] = $value;

        return $ret;
    }

    /**
     * colculate the percentage of one user's disease,
     * Enter description here ...
     * @param string $percentage_ret
     * @param int $value
     */
    public function colculatePercentage($elements, $percentage_ret) {
        
        $value = 0;
        $i = 0;
        foreach ($percentage_ret as $it) {
             
            if (strpos($it->percentage, "|") != false) {
                
                $per_arr = explode("|", $it->percentage);
                $range_arr_temp = explode("#", $per_arr[0]);
                $per_arr_temp = explode("#", $per_arr[1]);
                $pos = 0;
                $col = $it->element;

                while ($pos < sizeof($range_arr_temp) && ($range_arr_temp[$pos] < $elements->$col)) {
                    $pos++;
                }
              
                $value += $per_arr_temp[$pos];
            } else {
                $col = $it->element;
                //年龄和性别是一定要加到用户风险权重中的，其他要根据用户具体情况，判断
                if ($elements->$col == 1 || $col == "age" || $col == "gender") {
                    $value += $it->percentage;
                }
            }
        }
        return $value;
    }

    /**
     * 获取年龄区间，用实际年龄对应evaluate_incentive_percentage表中的age字段
     */
    public function getAgeRangeCount($age) {
        if ($age < 35) {
            $age_num = 0;
        }
        if ($age < 45 && $age >= 35) {
            $age_num = 1;
        }
        if ($age >= 45 && $age < 55) {
            $age_num = 2;
        }
        if ($age >= 55 && $age < 65) {
            $age_num = 3;
        }
        if ($age >= 65 && $age < 75) {
            $age_num = 4;
        }
        if ($age >= 75) {
            $age_num = 5;
        }

        return $age_num;
    }

    public function getDisType($disid) {
        $type = "";
        switch ($disid) {
            case Disease::GAOXUEYA:
                $type = "高血压";
                break;
            case Disease::GUANXINBING:
                $type = "冠心病";
                break;
            case Disease::NAOZUZHONG:
                $type = "脑卒中";
                break;
            case Disease::TANGNIAOBING:
                $type = "糖尿病";
                break;
            default:
                $type = "未知";
                break;
        }
        return $type;
    }

    /*
      //高血压
      private function Gaoxueya($elements)
      {
      $value = 0.0;
      $birthday = date_create($elements->age);
      $int_med_val_low = 0.857;
      $int_med_val_high = 1.581;
      $ceil = 8.339;

      $age = date_diff(date_create(), $birthday);
      if ($age->y <= 40){
      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      }elseif($age->y >= 65){
      $value+=3.15396469130012;
      }elseif($age->y >= 55&&$age->y < 65){
      $value+=2.32908635074596;
      }elseif($age->y >= 45&&$age->y < 55){
      $value+=1.00931355639876;
      }else{
      $value-=0.326490933043033;
      }
      if($elements->weightLevel > 24){
      $value+=0.857875680851199;
      }
      if($elements->smoking == 1){
      $value+=0.395108508439545;
      }
      if($elements->sports == 1){
      $value+=0.261682242990654;
      }
      if($elements->salt_food == 1){
      $value+=0.227799227799228;
      }
      if($elements->drink == 1){
      $value+=0.224949777059141;
      }
      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      }

      //冠心病
      /**
     * disease, age, sex, incentives
     *
     * Enter description here ...
     * @param unknown_type $elements
     */
    /* private function Guanxinbing($elements)
      {
      $value = 0.0;
      $birthday = date_create($elements->age);
      $age = date_diff(date_create(), $birthday);

      $int_med_val_low = 0.743;
      $int_med_val_high = 1.797 ;
      $ceil = 16.676 ;


      //年龄
      if ($age->y <=40){
      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      }elseif($age->y >= 65){
      $value+=9.170278311143;
      }elseif($age->y >= 55&&$age->y < 65){
      $value+=2.9277785540186;
      }elseif($age->y >= 45&&$age->y < 55){
      $value+=0.956521739130435;
      }elseif($age->y >= 35&&$age->y < 45){
      $value-=0.393404551788203;
      }

      //BMI
      if($elements->weightLevel > 23&&$elements->weightLevel < 28){
      $value+=0.236933149808415;
      }elseif ($elements->weightLevel > 27){
      $value+=0.653143823512645;
      }

      //高血压家族史
      if($elements->hypertension_family_history == 1){
      $value+=0.431246174341987;
      }

      //高血压病史
      if($elements->hypertension_history == 1){
      $value+=0.800436666992537;
      }

      //吸烟
      if($elements->smoking == 1){
      $value+=0.672521765694211;
      }

      //血甘油三酯＞1.7mmol/L
      //if($elements->high_triglycerides == 1){
      //	$value+=1.03126075879639;
      //}

      //冠心病家族史
      if($elements->coronary_family_history == 1){
      $value+=1.79658697385489;
      }

      //糖尿病家族史
      if($elements->diabetes_family_history_single == 1&&$elements->diabetes_family_history_double == 1){
      $value+=0.470615812969252;
      }
      //糖尿病病史
      if($elements->diabetes_history == 1){
      $value+=0.470615812969252;
      }

      //高胆固醇血症病史
      if($elements->hypercholesterolemia_history == 1){
      $value+=0.786987217670667;
      }

      //脑卒中家族史
      if($elements->stroke_family_history == 1){
      $value+=0.119714472809433;
      }

      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      }

      //脑卒中
      /**
     * session['disid'] => disease_id
     * elements => Disease->getElements(disease_id)
     *
     *
     * Enter description here ...
     * @param unknown_type $elements
     */
    /* private function Naozuzhong($elements)
      {
      $value = 0.0;
      $birthday = date_create($elements->age);
      $age = date_diff(date_create(), $birthday);

      if ($age->y > 40){
      $int_med_val_low = 2.310;
      $int_med_val_high = 1.506;
      $ceil = 18.953;
      //年龄45~54
      }else{
      $int_med_val_low = 3.129;
      $int_med_val_high = 18.953;
      $ceil = 18.953;
      }
      if($age->y >= 45&&$age->y < 55){
      $value+=0.327856324035154;
      }

      //BMI≥28 BMI≥24
      if($elements->weightLevel > 23&&$elements->weightLevel < 28){
      $value+=0.774533766364215;
      }

      //高血压家族史
      if($elements->hypertension_family_history == 1){
      $value+=1.50683682771194;
      }

      //高血压史
      if($elements->hypertension_history == 1){
      $value+=2.31076556803809;
      }

      //被动吸烟
      if($elements->smoked == 1){
      $value+=0.510177281680893;
      }

      //被动吸烟
      if($elements->smoking == 1){
      $value+=0.459241865145924;
      }

      //体育运动少
      if($elements->sports == 1){
      $value+=0.893927628513744;
      }

      //高盐饮食
      if($elements->salt_food == 1){
      $value+=0.107142857142857;
      }

      //过量饮酒（＞4杯/d）
      if($elements->drink == 1){
      $value+=0.872255025114042;
      }

      //蔬菜水果少
      if($elements->fruit == 1){
      $value+=0.298119964189794;
      }

      //口服避孕药
      if($elements->contraceptive == 1){
      $value+=1.13358070500928;
      }

      //打鼾
      if($elements->hulu == 1){
      $value+=0.0;
      }

      //脑卒中家族史
      if($elements->stroke_family_history == 1){
      $value+=3.20115078160573;
      }

      //冠心病
      if($elements->coronary_history == 1){
      $value+=3.12940510942431;
      }

      //糖尿病
      if($elements->diabetes_history == 1){
      $value+=1.65297046804888;
      }

      //高脂血症病史
      if($elements->hyperlipidemia_history == 1){
      $value+=1.77543355005884;
      }
      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      } */

    //糖尿病
    /* private function Tangniaobing($elements)
      {
      $value = 0.0;
      $birthday = date_create($elements->age);
      $age = date_diff(date_create(), $birthday);
      //高血压病史
      $int_med_val_low = 0.569;
      $int_med_val_high = 1.409;
      $ceil = 13.133;

      if ($age->y <=40){
      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      }elseif($elements->hypertension_history == 1){
      $value+=0.887679869832715;
      }

      //BMI
      if($elements->weightLevel >= 28){
      $value+=1.9723991507431;
      }elseif ($elements->weightLevel <= 24&&$elements->weightLevel < 28){
      $value+=0.407459535538353;
      }

      //wc
      if($elements->wc > 100){
      $value+=1.40963855421687;
      }elseif($elements->wc >= 90&&$elements->wc <= 100){
      $value+=0.855123674911661;
      }elseif($elements->wc >= 85&&$elements->wc < 90){
      $value+=0.31578947368421;
      }

      //空腹血糖调节受损
      if($elements->blood_sugar == 1){
      $value+=2.06513409961686;
      }

      //糖尿病双亲一方
      if($elements->diabetes_family_history_single == 1){
      $value+=0.3671875;
      }

      //糖尿病双方
      if($elements->diabetes_family_history_double == 1){
      $value+=2.8;
      }

      //血甘油三酯＞1.7mmol/L
      //if($elements->blood_triglycerides == 1){
      //	$value+=0.569279054740146;
      //}

      //血胆固醇＞5.2mmol/L
      if($elements->blood_cholesterol == 1){
      $value+=0.363459291001169;
      }

      //吸烟
      if($elements->smoking == 1){
      $value+=0.140684410646388;
      }
      //蔬菜水果少
      if($elements->fruit == 1){
      $value+=0.140684410646388;
      }
      //体力活动不足
      if($elements->sports == 1){
      $value+=0.31578947368421;
      }
      if($elements->salt_food == 1){
      $value+=0.227799227799228;
      }
      //过量饮酒（＞4杯/d
      if($elements->drink == 1){
      $value+=0.441060620616774;
      }

      //摄入肉类
      if($elements->meat == 1){
      $value+=0.216089803554724;
      }

      //静坐生活方式
      if($elements->stillsit == 1){
      $value+=1.44360902255639;
      }
      return array('value' => $value, 'int_med_val_low' => $int_med_val_low, 'int_med_val_high' => $int_med_val_high, 'ceil' => $ceil);
      }
     */
}
