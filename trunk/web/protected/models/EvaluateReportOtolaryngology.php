<?php

/**
 * This is the model class for table "evaluate_report_otolaryngology".
 *
 * The followings are the available columns in table 'evaluate_report_otolaryngology':
 * @property integer $id
 * @property integer $uid
 * @property string $external_ear
 * @property string $canal
 * @property string $eardrum
 * @property string $nasal_cavity
 * @property string $nasal_septum
 * @property string $pharynx
 * @property string $tonsil
 * @property string $otolaryngology_other
 * @property string $hearing
 * @property string $nasopharyngoscopy
 * @property string $preliminary_opinion
 */
class EvaluateReportOtolaryngology extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportOtolaryngology the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_otolaryngology';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, external_ear, canal, eardrum, nasal_cavity, nasal_septum, pharynx, tonsil, otolaryngology_other, hearing, nasopharyngoscopy, preliminary_opinion', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('external_ear, canal, eardrum, nasal_cavity, nasal_septum, pharynx, tonsil, hearing, nasopharyngoscopy, preliminary_opinion', 'length', 'max'=>200),
			array('otolaryngology_other', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, external_ear, canal, eardrum, nasal_cavity, nasal_septum, pharynx, tonsil, otolaryngology_other, hearing, nasopharyngoscopy, preliminary_opinion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'external_ear' => 'External Ear',
			'canal' => 'Canal',
			'eardrum' => 'Eardrum',
			'nasal_cavity' => 'Nasal Cavity',
			'nasal_septum' => 'Nasal Septum',
			'pharynx' => 'Pharynx',
			'tonsil' => 'Tonsil',
			'otolaryngology_other' => 'Otolaryngology Other',
			'hearing' => 'Hearing',
			'nasopharyngoscopy' => 'Nasopharyngoscopy',
			'preliminary_opinion' => 'Preliminary Opinion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('external_ear',$this->external_ear,true);
		$criteria->compare('canal',$this->canal,true);
		$criteria->compare('eardrum',$this->eardrum,true);
		$criteria->compare('nasal_cavity',$this->nasal_cavity,true);
		$criteria->compare('nasal_septum',$this->nasal_septum,true);
		$criteria->compare('pharynx',$this->pharynx,true);
		$criteria->compare('tonsil',$this->tonsil,true);
		$criteria->compare('otolaryngology_other',$this->otolaryngology_other,true);
		$criteria->compare('hearing',$this->hearing,true);
		$criteria->compare('nasopharyngoscopy',$this->nasopharyngoscopy,true);
		$criteria->compare('preliminary_opinion',$this->preliminary_opinion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}