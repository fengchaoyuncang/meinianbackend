<?php

/**
 * This is the model class for table "evaluate_suggestion_nutrition".
 *
 * The followings are the available columns in table 'evaluate_suggestion_nutrition':
 * @property integer $id
 * @property string $item
 * @property string $goodtags
 * @property string $badtags
 */
class EvaluateSuggestionNutrition extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateSuggestionNutrition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_suggestion_nutrition';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'numerical', 'integerOnly'=>true),
			array('item', 'length', 'max'=>20),
			array('goodtags, badtags', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item, goodtags, badtags', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item' => 'Item',
			'goodtags' => 'Goodtags',
			'badtags' => 'Badtags',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item',$this->item,true);
		$criteria->compare('goodtags',$this->goodtags,true);
		$criteria->compare('badtags',$this->badtags,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	

	public function getHarmfulSug($userid, $item_arr){
		if (empty($item_arr)){
			return array();
		}
		$criteria = new CDbCriteria();
		$criteria->select = "item";
		foreach ($item_arr as $item){
			$criteria->compare('badtags', $item, true, "or");
		}
		return EvaluateSuggestionNutrition::model()->findAll($criteria);
	}

	public function getBenefitSug($userid, $item_arr){
		if (empty($item_arr)){
			return array();
		}
		$criteria = new CDbCriteria();
		$criteria->select = "item";
		foreach ($item_arr as $item){
			$criteria->compare('goodtags', $item, true, "or");
		}
		return EvaluateSuggestionNutrition::model()->findAll($criteria);
	}
}