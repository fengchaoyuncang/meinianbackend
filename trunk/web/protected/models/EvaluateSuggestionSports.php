<?php

/**
 * This is the model class for table "evaluate_suggestion_sports".
 *
 * The followings are the available columns in table 'evaluate_suggestion_sports':
 * @property integer $id
 * @property string $item
 * @property string $goodtags
 * @property string $badtags
 * @property string $recommend
 * @property string $motion
 * @property string $trainingsite
 * @property string $trainingstandard
 * @property string $resistance
 * @property string $slope
 * @property string $speed
 * @property string $times
 * @property string $heartrate
 * @property string $breathtype
 * @property string $exercisetype
 */
class EvaluateSuggestionSports extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateSuggestionSports the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_suggestion_sports';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('item', 'length', 'max'=>20),
		array('goodtags', 'length', 'max'=>200),
		array('badtags', 'length', 'max'=>255),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('id, item, goodtags, badtags, recommend, motion, trainingsite, trainingstandard, resistance, slope, speed, times, heartrate, breathtype, exercisetype', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item' => 'Item',
			'goodtags' => 'Goodtags',
			'badtags' => 'Badtags',
			'recommend' => 'Recommend',
			'motion' => 'Motion',
			'trainingsite' => 'Trainingsite',
			'trainingstandard' => 'Trainingstandard',
			'resistance' => 'Resistance',
			'slope' => 'Slope',
			'speed' => 'Speed',
			'times' => 'Times',
			'heartrate' => 'Heartrate',
			'breathtype' => 'Breathtype',
			'exercisetype' => 'Exercisetype',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item',$this->item,true);
		$criteria->compare('goodtags',$this->goodtags,true);
		$criteria->compare('badtags',$this->badtags,true);
		$criteria->compare('recommend',$this->recommend,true);
		$criteria->compare('motion',$this->motion,true);
		$criteria->compare('trainingsite',$this->trainingsite,true);
		$criteria->compare('trainingstandard',$this->trainingstandard,true);
		$criteria->compare('resistance',$this->resistance,true);
		$criteria->compare('slope',$this->slope,true);
		$criteria->compare('speed',$this->speed,true);
		$criteria->compare('times',$this->times,true);
		$criteria->compare('heartrate',$this->heartrate,true);
		$criteria->compare('breathtype',$this->breathtype,true);
		$criteria->compare('exercisetype',$this->exercisetype,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getHarmfulSug($userid, $item_arr){
		if (empty($item_arr)){
			return array();
		}
		$criteria = new CDbCriteria();
		//		$criteria->select = "item";
		foreach ($item_arr as $item){
			$criteria->compare('badtags', $item, true, "or");
		}
		return EvaluateSuggestionSports::model()->findAll($criteria);
	}

	public function getBenefitSug($userid, $item_arr){
		$criteria = new CDbCriteria();
		if (!empty($item_arr)){
			foreach ($item_arr as $item){
				$criteria->compare('goodtags', $item, true, "or");
			}
		}
		$criteria->limit = 5;
		$criteria->order = "recommend desc";
		return EvaluateSuggestionSports::model()->findAll($criteria);
	}
}