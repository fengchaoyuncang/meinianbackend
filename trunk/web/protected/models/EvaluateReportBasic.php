<?php

/**
 * This is the model class for table "evaluate_report_basic".
 *
 * The followings are the available columns in table 'evaluate_report_basic':
 * @property integer $id
 * @property integer $uid
 * @property string $name
 * @property string $sex
 * @property string $check_time
 * @property string $no
 * @property string $card_no
 * @property string $height
 * @property string $height_normal
 * @property string $height_unit
 * @property string $weight
 * @property string $weight_normal
 * @property string $weight_unit
 * @property string $weightLevel
 * @property string $weightLevel_issue
 * @property string $weightLevel_normal
 * @property string $weightLevel_unit
 * @property string $systolic_blood_pressure
 * @property string $systolic_blood_pressure_issue
 * @property string $systolic_blood_pressure_normal
 * @property string $systolic_blood_pressure_unit
 * @property string $diastolic_blood_pressure
 * @property string $diastolic_blood_pressure_issue
 * @property string $diastolic_blood_pressure_normal
 * @property string $diastolic_blood_pressure_unit
 */
class EvaluateReportBasic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportBasic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_basic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, name, sex, check_time, no, card_no, height, height_normal, height_unit, weight, weight_normal, weight_unit, weightLevel, weightLevel_issue, weightLevel_normal, weightLevel_unit, systolic_blood_pressure, systolic_blood_pressure_issue, systolic_blood_pressure_normal, systolic_blood_pressure_unit, diastolic_blood_pressure, diastolic_blood_pressure_issue, diastolic_blood_pressure_normal, diastolic_blood_pressure_unit', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('name, check_time, no, card_no, weightLevel_normal, systolic_blood_pressure_normal', 'length', 'max'=>200),
			array('sex, height, height_normal, height_unit, weight, weight_normal, weight_unit, weightLevel, weightLevel_issue, weightLevel_unit, systolic_blood_pressure, systolic_blood_pressure_issue, systolic_blood_pressure_unit, diastolic_blood_pressure, diastolic_blood_pressure_issue, diastolic_blood_pressure_normal, diastolic_blood_pressure_unit', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, name, sex, check_time, no, card_no, height, height_normal, height_unit, weight, weight_normal, weight_unit, weightLevel, weightLevel_issue, weightLevel_normal, weightLevel_unit, systolic_blood_pressure, systolic_blood_pressure_issue, systolic_blood_pressure_normal, systolic_blood_pressure_unit, diastolic_blood_pressure, diastolic_blood_pressure_issue, diastolic_blood_pressure_normal, diastolic_blood_pressure_unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'name' => 'Name',
			'sex' => 'Sex',
			'check_time' => 'Check Time',
			'no' => 'No',
			'card_no' => 'Card No',
			'height' => 'Height',
			'height_normal' => 'Height Normal',
			'height_unit' => 'Height Unit',
			'weight' => 'Weight',
			'weight_normal' => 'Weight Normal',
			'weight_unit' => 'Weight Unit',
			'weightLevel' => 'Weight Level',
			'weightLevel_issue' => 'Weight Level Issue',
			'weightLevel_normal' => 'Weight Level Normal',
			'weightLevel_unit' => 'Weight Level Unit',
			'systolic_blood_pressure' => 'Systolic Blood Pressure',
			'systolic_blood_pressure_issue' => 'Systolic Blood Pressure Issue',
			'systolic_blood_pressure_normal' => 'Systolic Blood Pressure Normal',
			'systolic_blood_pressure_unit' => 'Systolic Blood Pressure Unit',
			'diastolic_blood_pressure' => 'Diastolic Blood Pressure',
			'diastolic_blood_pressure_issue' => 'Diastolic Blood Pressure Issue',
			'diastolic_blood_pressure_normal' => 'Diastolic Blood Pressure Normal',
			'diastolic_blood_pressure_unit' => 'Diastolic Blood Pressure Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('check_time',$this->check_time,true);
		$criteria->compare('no',$this->no,true);
		$criteria->compare('card_no',$this->card_no,true);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('height_normal',$this->height_normal,true);
		$criteria->compare('height_unit',$this->height_unit,true);
		$criteria->compare('weight',$this->weight,true);
		$criteria->compare('weight_normal',$this->weight_normal,true);
		$criteria->compare('weight_unit',$this->weight_unit,true);
		$criteria->compare('weightLevel',$this->weightLevel,true);
		$criteria->compare('weightLevel_issue',$this->weightLevel_issue,true);
		$criteria->compare('weightLevel_normal',$this->weightLevel_normal,true);
		$criteria->compare('weightLevel_unit',$this->weightLevel_unit,true);
		$criteria->compare('systolic_blood_pressure',$this->systolic_blood_pressure,true);
		$criteria->compare('systolic_blood_pressure_issue',$this->systolic_blood_pressure_issue,true);
		$criteria->compare('systolic_blood_pressure_normal',$this->systolic_blood_pressure_normal,true);
		$criteria->compare('systolic_blood_pressure_unit',$this->systolic_blood_pressure_unit,true);
		$criteria->compare('diastolic_blood_pressure',$this->diastolic_blood_pressure,true);
		$criteria->compare('diastolic_blood_pressure_issue',$this->diastolic_blood_pressure_issue,true);
		$criteria->compare('diastolic_blood_pressure_normal',$this->diastolic_blood_pressure_normal,true);
		$criteria->compare('diastolic_blood_pressure_unit',$this->diastolic_blood_pressure_unit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}