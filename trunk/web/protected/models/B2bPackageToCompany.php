<?php

/**
 * This is the model class for table "b2b_package_to_company".
 *
 * The followings are the available columns in table 'b2b_package_to_company':
 * @property integer $id
 * @property integer $company_id
 * @property string $level
 * @property integer $gender
 * @property integer $married
 * @property integer $package_id
 */
class B2bPackageToCompany extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bPackageToCompany the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_package_to_company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
//		return array(
//			array('company_id, level, married, package_id', 'required'),
//			array('company_id, gender, married, package_id', 'numerical', 'integerOnly'=>true),
//			array('level', 'length', 'max'=>50),
//			// The following rule is used by search().
//			// Please remove those attributes that should not be searched.
//			array('id, company_id, level, gender, married, package_id', 'safe', 'on'=>'search'),
//		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'packages' => array(self::BELONGS_TO,'B2bPackage',array('package_id'=>'id')),
                    'centers'=>array(self::HAS_MANY,'B2bPackageToCenter',array('package_id'=>'package_id'))
		);
	}
        
        public function getPackageByCompany($company_id,$level){
            $criteria=new CDbCriteria;
            //$criteria->select='company_name,company_name';  // 只选择 'title' 列
         
            $criteria->condition='company_id=:company_id and level=:level';
            $criteria->params=array(':company_id'=>$company_id,':level'=>$level);
            $criteria->with = array('packages');
            
            $ret = $this->findAll($criteria);
            return $ret;
        }
	
}