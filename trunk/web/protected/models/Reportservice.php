<?php
class ReportService{
    public function getValueByEles($uid,$items)
    {
        if(!is_array($items)){
            return false;
        }
        //转换
        $name = array();
        $map = array();
        foreach ($items as $key => $it) {
            array_push($name, $it["name"]);
            $map[$it["name"]] = $it["field_name"];
        }
        
        $ret = EvaluateReportCache::model()->findByAttributes(array("uid"=>$uid),array("order"=>"id desc"));
        if($ret === null){
            return array();
        }
        $ret = EvaluateReportCache::model()->findAllByAttributes(
                array("no"=>$ret->no,"name"=>$name));
        $rets = array();
        foreach ($ret as $it) {
            $item["name"]=$it->name;
            $item["value"]=$it->value;
            $item["issue"]=$it->issue=="↑"?1:0;
            $item["normal"]=$it->normal;
            $item["unit"]=$it->unit;
            
            $rets[$map[$it->name]] = $item;
        }
        return $rets;
    }
    
	
}
?>