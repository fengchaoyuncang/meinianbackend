<?php

/**
 * This is the model class for table "app_tipscard".
 *
 * The followings are the available columns in table 'app_tipscard':
 * @property integer $tipid
 * @property string $titlebar
 * @property string $summary
 * @property string $weburl
 * @property string $addtime
 */
class AppTipscard extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AppTipscard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_tipscard';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titlebar, summary, weburl, addtime', 'required'),
			array('titlebar, summary, weburl', 'length', 'max'=>256),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tipid, titlebar, summary, weburl, addtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tipid' => 'Tipid',
			'titlebar' => 'Titlebar',
			'summary' => 'Summary',
			'weburl' => 'Weburl',
			'addtime' => 'Addtime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tipid',$this->tipid);
		$criteria->compare('titlebar',$this->titlebar,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('weburl',$this->weburl,true);
		$criteria->compare('addtime',$this->addtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}