<?php

/**
 * This is the model class for table "reservation".
 *
 * The followings are the available columns in table 'reservation':
 * @property string $id
 * @property string $city
 * @property string $location
 * @property string $name
 * @property string $sex
 * @property string $date
 * @property string $idno
 * @property string $phone
 * @property string $series
 * @property string $created_time
 * @property string $status
 */
class Reservation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Reservation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reservation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone', 'required', 'message'=>'请填写{attribute}'),
			array('phone', 'numerical', 'integerOnly'=>true),
			array('city, name, idno, phone, status', 'length', 'max'=>32),
			array('location', 'length', 'max'=>64),
			array('sex', 'length', 'max'=>8),
            array('series', 'length', 'max' => 256),
			array('date, created_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, city, location, name, sex, date, idno, phone, series, created_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city' => '选择城市',
			'location' => '预约体检地点',
			'name' => '姓名',
			'sex' => '性别',
			'date' => '预约时间',
			'idno' => '身份证号/护照',
			'phone' => '联系电话',
			'series' => '套餐',
            'created_time' => '申请时间',
            'status' => '状态',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('idno',$this->idno,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('series',$this->series,true);
        $criteria->compare('created_time',$this->created_time,true);
        $criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
