<?php

/**
 * This is the model class for table "b2b_order_pre".
 *
 * The followings are the available columns in table 'b2b_order_pre':
 * @property integer $id
 * @property integer $externalid
 * @property integer $userid
 * @property string $cardno
 * @property string $realname
 * @property integer $orderstatus
 * @property string $create_at
 * @property string $update_at
 */
class B2bOrderPre extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bOrderPre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_order_pre';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cardno, realname, orderstatus, update_at', 'required'),
			array('externalid, userid, orderstatus', 'numerical', 'integerOnly'=>true),
			array('cardno', 'length', 'max'=>50),
			array('realname', 'length', 'max'=>200),
			array('create_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, externalid, userid, cardno, realname, orderstatus, create_at, update_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'externalid' => 'Externalid',
			'userid' => 'Userid',
			'cardno' => 'Cardno',
			'realname' => 'Realname',
			'orderstatus' => 'Orderstatus',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('externalid',$this->externalid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('cardno',$this->cardno,true);
		$criteria->compare('realname',$this->realname,true);
		$criteria->compare('orderstatus',$this->orderstatus);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}