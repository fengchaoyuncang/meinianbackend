<?php
	
/*推送的表单信息*/	
class PushForm extends CFormModel
{
	public $paper_id;
	public $item_id;
	public $indicator_id;
	public $android;
	public $ios;
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// paper_id, indicator_id are required
			array('paper_id', 'validatePaperId'),
			array('item_id', 'validateItemIndcator'),
			array('indicator_id', 'validateItemIndcator'),
			array('android', 'validatePlatform'),
			array('ios', 'validatePlatform')
		);
	}
	
    public function validatePlatform($attribute,$params)
	{    
		if(strcmp($this->android, '1') and  strcmp($this->ios, '1'))
		{
			$this->addError('android', '手机平台至少选择其中一个');
			$this->addError('ios', '手机平台至少选择其中一个');
		}		
	}
    
	public function validateItemIndcator($attribute,$params)
	{
		if(!strcmp($this->item_id, 'choose') and  !strcmp($this->indicator_id, 'choose'))
		{
			$this->addError('item_id', '体检项目，体检指标至少选中其中一个');
			$this->addError('indicator_id', '体检项目，体检指标至少选中其中一个');
		}		
	}
	
	public function validatePaperId($attribute,$params)
	{
		if(strcmp($this->$attribute, 'choose')==0)
		{
			$this->addError($attribute, '必须选择一个文章');
		}
	}		
}
	