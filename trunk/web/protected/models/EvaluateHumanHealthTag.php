<?php

/**
 * This is the model class for table "evaluate_human_health_tag".
 *
 * The followings are the available columns in table 'evaluate_human_health_tag':
 * @property integer $id
 * @property integer $uid
 * @property string $badtags
 * @property string $teststatus
 */
class EvaluateHumanHealthTag extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateHumanHealthTag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_human_health_tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('uid', 'numerical', 'integerOnly'=>true),
		array('badtags, teststatus', 'length', 'max'=>255),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('id, uid, badtags, teststatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'badtags' => 'Badtags',
			'teststatus' => 'Teststatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('badtags',$this->badtags,true);
		$criteria->compare('teststatus',$this->teststatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function clearTabs($uid, $column){
		$criteria = new CDbCriteria();
		$humanHeal_ret = EvaluateHumanHealthTag::model()->findByAttributes(array("uid" => $uid));
		if (isset($humanHeal_ret->$column)){
			switch (gettype($humanHeal_ret->$column)){
				case 'integer':
					$humanHeal_ret->$column = 0;break;
				case 'string':
					$humanHeal_ret->$column = '';break;
				case 'boolean':;
			}
			$humanHeal_ret->update();
		}
	}
	
	public function achieveAInstance($uid){
		$hum_tags_ins = new EvaluateHumanHealthTag();
		$hum_tags_ins->uid = $uid;
		
		return $hum_tags_ins->save();
	}
}