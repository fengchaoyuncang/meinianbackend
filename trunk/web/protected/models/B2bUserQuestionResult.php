<?php

/**
 * This is the model class for table "b2b_user_question_result".
 *
 * The followings are the available columns in table 'b2b_user_question_result':
 * @property integer $id
 * @property integer $uid
 * @property string $type
 * @property string $value
 * @property string $create_at
 * @property string $update_at
 */
class B2bUserQuestionResult extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bUserQuestionResult the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_user_question_result';
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'type' => 'Type',
			'value' => 'Value',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
		);
	}
        /**
         * 
         */
        public function saveResult($uid,$items)
        {
            if(!is_array($items)){
                return false;
            }
            $keys = array();
            foreach ($items as $key => $it) {
                array_push($keys, $key);
            }
            //查找所有问卷对应的因素ID
//            $ret = EvaluateDiseaseElement::model()->findAllByAttributes(array("field_name"=>$keys),array("select"=>"id,field_name"));
//            if(count($ret)!=count($keys)){
//                $k = "";
//                foreach($ret as $it){
//                    $k .= $it->field_name.",";
//                }
//                throw new Exception("问卷保存找不到问题相关的诱因：".$k);
//            }
            //替换为所有的type id
//            $keys = array();
//            foreach($ret as $it){
//                $keys[] = $it->id;
//                $items[$it->id] = $items[$it->field_name];
//            }
//            
            $ret = $this->findAllByAttributes(array("uid"=>$uid,"type"=>$keys),array("select"=>"id,type,value"));
            
            
            $update = array();
            if(count($ret)){
                foreach ($ret as $it) {
                    $it->value = $items[$it->type];
                    $it->update_at = new CDbExpression('NOW()');
//                    $id = $it->id;
                    if(false === $it->update()){
                        return false;
                    }
                    array_push($update, $it->type);
                }
            }
            
            $news = array_diff($keys, $update);
            $sql="INSERT INTO b2b_user_question_result (`uid`, `type`,`value`,`create_at`,`update_at`) VALUES";
            foreach($news as $it){
                $value = $items[$it];
                $sql .= "('$uid','$it','$value',NOW(),NOW()),";
            }
            
            
            if(count($news)){
                $num=Yii::app()->db->createCommand(substr($sql,0,-1))->execute();
                if($num){
                    return true;
                }
            }

        }
	
}