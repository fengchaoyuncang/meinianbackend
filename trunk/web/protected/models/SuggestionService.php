<?php
class SuggestionService{
	public function getSportsBenefiteSuggestion(){

	}

	public function getSportsHarmfulSuggestion(){

	}

	/**
	 *
	 * Enter description here ...
	 */
	private function getHumanBadtags($uid){
		$basic_tag_arr = array("hypertension_history" => "高血压", "hypercholesterolemia_history" => "高血脂", "diabetes_history" => "糖尿病", "hyperlipidemia_history" => "高血脂", "coronary_history" => "心血管疾病",);
		$hum_basic_ret = EvaluateExaminationBasic::model()->findByAttributes(array("uid" => $uid));
		$hum_bad_tag_arr = array();

		if (!empty($hum_basic_ret)){
			foreach ($basic_tag_arr as $tag_name => $value){
				if ($hum_basic_ret->$tag_name && !in_array($value, $hum_bad_tag_arr)){
					array_push($hum_bad_tag_arr, $value);
				}
			}
			//			var_dump($hum_basic_ret->uid);exit();
			if ($hum_basic_ret->systolic_blood_pressure > 140 && $hum_basic_ret->diastolic_blood_pressure > 90 && !in_array("高血压", $hum_bad_tag_arr)){
				array_push($hum_bad_tag_arr, "高血压");
			}
			//			"weightLevel" => "肥胖",
			if ($hum_basic_ret->weightLevel > 24){
				array_push($hum_bad_tag_arr, "肥胖");
			}
				
		}else{
			return array();
		}
		//		var_dump($hum_bad_tag_arr);exit();
		return $hum_bad_tag_arr;

	}
	/**
	 *
	 * 1 get the sports benefits and harmful suggestion
	 * @param unknown_type $userid
	 * @param unknown_type $item_arr
	 */
	public function getSportsSuggestion($userid){
		$evalsug_ins = new EvaluateSuggestionSports();
		$safe_sug_array = array();
		$safe_sug_other_arr = array();
		$unsafe_sug_array = array();
		$sports_sug_arr = array();
		$item_arr = $this->getHumanBadtags($userid);
		//		var_dump($item_arr);exit();
		$unsafe_suggestion_ret = $evalsug_ins->getHarmfulSug($userid, $item_arr);
		foreach ($unsafe_suggestion_ret as $sug_ins){
			array_push($unsafe_sug_array, $sug_ins->item);
		}

		//		var_dump($item_arr);exit();
		$safe_suggestion_ret = $evalsug_ins->getBenefitSug($userid, $item_arr);
		//		foreach ($safe_suggestion_ret as $sug_ins){
		//			array_push($safe_sug_array, $sug_ins->item);
		//		}
		 
		if (sizeof($safe_suggestion_ret) > 2){
			for($i = 0; $i < 2; $i++){
				array_push($safe_sug_array, $safe_suggestion_ret[$i]);
			}
			for($i = 2; $i < sizeof($safe_suggestion_ret); $i++){
				array_push($safe_sug_other_arr, $safe_suggestion_ret[$i]->item);
			}
		}else{
			$safe_sug_array = $safe_suggestion_ret;
			$safe_sug_other_arr = array();
		}
		$safe_sug_other_str = implode(",", $safe_sug_other_arr);

		return array("benefit" => array("safe_sug_arr" => $safe_sug_array, "safe_sug_other_str" => $safe_sug_other_str), "harmful" => $unsafe_sug_array);
		//		return array("benefit" => $safe_sug_array);
	}

	public function getNutritionSuggestion($userid, $item_arr){
		$evalsug_ins = new EvaluateSuggestionNutrition();
		$safe_sug_array = array();
		$unsafe_sug_array = array();
		$sports_sug_arr = array();

		$unsafe_suggestion_ret = $evalsug_ins->getHarmfulSug($userid, $item_arr);
		foreach ($unsafe_suggestion_ret as $sug_ins){
			array_push($unsafe_sug_array, $sug_ins->item);
		}

		$safe_suggestion_ret = $evalsug_ins->getBenefitSug($userid, $item_arr);
		foreach ($safe_suggestion_ret as $sug_ins){
			array_push($safe_sug_array, $sug_ins->item);
		}

		return array("benefit" => $safe_sug_array, "harmful" => $unsafe_sug_array);
	}
	public function getTotalAdvice($userid){
		//		每次理论运动消耗热量={（x%×实际体重）÷(选择每周运动次数【选择数字为4/5/6】×4)}×7700（千卡）（x=1.0，1.1，1.2，1.3，1.4，1.5）

		$hum_basic_ret = EvaluateExaminationBasic::model()->findByAttributes(array("uid" => $userid));
		$energy = 0;
		//		var_dump($hum_basic_ret->weightLevel);exit();
		//		$hum_bag_tag_arr = $this->getHumanBadtags($userid);
		if (!empty($hum_basic_ret->weightLevel) && $hum_basic_ret->weightLevel > 24){
			$energy = (($hum_basic_ret->weight * 0.01) / (7 * 4)) * 7700;
			//一步是0.7卡
			$steps = (int)(($energy / 10) * 1000);
		}else{
			$steps = "根据饮食量控制运动";
		}
		return $steps;
	}
}
?>