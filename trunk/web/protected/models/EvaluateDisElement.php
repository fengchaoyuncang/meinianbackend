<?php

/**
 * This is the model class for table "evaluate_diselement".
 *
 * The followings are the available columns in table 'evaluate_diselement':
 * @property integer $id
 * @property string $field_name
 * @property string $description
 * @property string $name
 * @property string $suggest
 */
class EvaluateDisElement extends CActiveRecord
{


    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateDisElement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_diselement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('field_name, description, name, suggest', 'required'),
			array('field_name', 'length', 'max'=>50),
			array('description', 'length', 'max'=>200),
			array('name', 'length', 'max'=>30),
			array('suggest', 'length', 'max'=>300),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, field_name, description, name, suggest', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'field_name' => 'Field Name',
			'description' => 'Description',
			'name' => 'Name',
			'suggest' => 'Suggest',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('field_name',$this->field_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('suggest',$this->suggest,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}