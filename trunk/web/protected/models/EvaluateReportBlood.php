<?php

/**
 * This is the model class for table "evaluate_report_blood".
 *
 * The followings are the available columns in table 'evaluate_report_blood':
 * @property integer $id
 * @property integer $uid
 * @property string $RBC
 * @property string $RBC_normal
 * @property string $RBC_unit
 * @property string $HGB
 * @property string $HGB_normal
 * @property string $HGB_unit
 * @property string $HCT
 * @property string $HCT_normal
 * @property string $HCT_unit
 * @property string $MCV
 * @property string $MCV_normal
 * @property string $MCV_unit
 * @property string $MCH
 * @property string $MCH_normal
 * @property string $MCH_unit
 * @property string $MCHC
 * @property string $MCHC_normal
 * @property string $MCHC_unit
 * @property string $RDW_CV
 * @property string $RDW_CV_normal
 * @property string $RDW_CV_unit
 * @property string $WBC
 * @property string $WBC_normal
 * @property string $WBC_unit
 * @property string $GRA_PERCENT
 * @property string $GRA_PERCENT_normal
 * @property string $GRA_PERCENT_unit
 * @property string $LYM_PERCENT
 * @property string $LYM_PERCENT_normal
 * @property string $LYM_PERCENT_unit
 * @property string $MON_PERCENT
 * @property string $MON_PERCENT_normal
 * @property string $MON_PERCENT_unit
 * @property string $GRA_ABSOLUTE
 * @property string $GRA_ABSOLUTE_normal
 * @property string $GRA_ABSOLUTE_unit
 * @property string $LYM_ABSOLUTE
 * @property string $LYM_ABSOLUTE_normal
 * @property string $LYM_ABSOLUTE_unit
 * @property string $MON_ABSOLUTE
 * @property string $MON_ABSOLUTE_normal
 * @property string $MON_ABSOLUTE_unit
 * @property string $PLT
 * @property string $PLT_normal
 * @property string $PLT_unit
 * @property string $MPV
 * @property string $MPV_normal
 * @property string $MPV_unit
 * @property string $PDW
 * @property string $PDW_normal
 * @property string $PDW_unit
 */
class EvaluateReportBlood extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportBlood the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_blood';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, RBC, RBC_normal, RBC_unit, HGB, HGB_normal, HGB_unit, HCT, HCT_normal, HCT_unit, MCV, MCV_normal, MCV_unit, MCH, MCH_normal, MCH_unit, MCHC, MCHC_normal, MCHC_unit, RDW_CV, RDW_CV_normal, RDW_CV_unit, WBC, WBC_normal, WBC_unit, GRA_PERCENT, GRA_PERCENT_normal, GRA_PERCENT_unit, LYM_PERCENT, LYM_PERCENT_normal, LYM_PERCENT_unit, MON_PERCENT, MON_PERCENT_normal, MON_PERCENT_unit, GRA_ABSOLUTE, GRA_ABSOLUTE_normal, GRA_ABSOLUTE_unit, LYM_ABSOLUTE, LYM_ABSOLUTE_normal, LYM_ABSOLUTE_unit, MON_ABSOLUTE, MON_ABSOLUTE_normal, MON_ABSOLUTE_unit, PLT, PLT_normal, PLT_unit, MPV, MPV_normal, MPV_unit, PDW, PDW_normal, PDW_unit', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('RBC, HGB, HGB_unit, HCT, HCT_unit, MCV, MCV_unit, MCH, MCH_unit, MCHC, MCHC_unit, RDW_CV, RDW_CV_normal, RDW_CV_unit, WBC, GRA_PERCENT, GRA_PERCENT_normal, GRA_PERCENT_unit, LYM_PERCENT, LYM_PERCENT_normal, LYM_PERCENT_unit, MON_PERCENT, MON_PERCENT_normal, MON_PERCENT_unit, GRA_ABSOLUTE, LYM_ABSOLUTE, MON_ABSOLUTE, PLT, MPV, MPV_unit, PDW, PDW_normal, PDW_unit', 'length', 'max'=>20),
			array('RBC_normal, RBC_unit, HGB_normal, HCT_normal, MCV_normal, MCH_normal, MCHC_normal, WBC_normal, WBC_unit, GRA_ABSOLUTE_normal, GRA_ABSOLUTE_unit, LYM_ABSOLUTE_normal, LYM_ABSOLUTE_unit, MON_ABSOLUTE_normal, MON_ABSOLUTE_unit, PLT_normal, PLT_unit, MPV_normal', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, RBC, RBC_normal, RBC_unit, HGB, HGB_normal, HGB_unit, HCT, HCT_normal, HCT_unit, MCV, MCV_normal, MCV_unit, MCH, MCH_normal, MCH_unit, MCHC, MCHC_normal, MCHC_unit, RDW_CV, RDW_CV_normal, RDW_CV_unit, WBC, WBC_normal, WBC_unit, GRA_PERCENT, GRA_PERCENT_normal, GRA_PERCENT_unit, LYM_PERCENT, LYM_PERCENT_normal, LYM_PERCENT_unit, MON_PERCENT, MON_PERCENT_normal, MON_PERCENT_unit, GRA_ABSOLUTE, GRA_ABSOLUTE_normal, GRA_ABSOLUTE_unit, LYM_ABSOLUTE, LYM_ABSOLUTE_normal, LYM_ABSOLUTE_unit, MON_ABSOLUTE, MON_ABSOLUTE_normal, MON_ABSOLUTE_unit, PLT, PLT_normal, PLT_unit, MPV, MPV_normal, MPV_unit, PDW, PDW_normal, PDW_unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'RBC' => 'Rbc',
			'RBC_normal' => 'Rbc Normal',
			'RBC_unit' => 'Rbc Unit',
			'HGB' => 'Hgb',
			'HGB_normal' => 'Hgb Normal',
			'HGB_unit' => 'Hgb Unit',
			'HCT' => 'Hct',
			'HCT_normal' => 'Hct Normal',
			'HCT_unit' => 'Hct Unit',
			'MCV' => 'Mcv',
			'MCV_normal' => 'Mcv Normal',
			'MCV_unit' => 'Mcv Unit',
			'MCH' => 'Mch',
			'MCH_normal' => 'Mch Normal',
			'MCH_unit' => 'Mch Unit',
			'MCHC' => 'Mchc',
			'MCHC_normal' => 'Mchc Normal',
			'MCHC_unit' => 'Mchc Unit',
			'RDW_CV' => 'Rdw Cv',
			'RDW_CV_normal' => 'Rdw Cv Normal',
			'RDW_CV_unit' => 'Rdw Cv Unit',
			'WBC' => 'Wbc',
			'WBC_normal' => 'Wbc Normal',
			'WBC_unit' => 'Wbc Unit',
			'GRA_PERCENT' => 'Gra Percent',
			'GRA_PERCENT_normal' => 'Gra Percent Normal',
			'GRA_PERCENT_unit' => 'Gra Percent Unit',
			'LYM_PERCENT' => 'Lym Percent',
			'LYM_PERCENT_normal' => 'Lym Percent Normal',
			'LYM_PERCENT_unit' => 'Lym Percent Unit',
			'MON_PERCENT' => 'Mon Percent',
			'MON_PERCENT_normal' => 'Mon Percent Normal',
			'MON_PERCENT_unit' => 'Mon Percent Unit',
			'GRA_ABSOLUTE' => 'Gra Absolute',
			'GRA_ABSOLUTE_normal' => 'Gra Absolute Normal',
			'GRA_ABSOLUTE_unit' => 'Gra Absolute Unit',
			'LYM_ABSOLUTE' => 'Lym Absolute',
			'LYM_ABSOLUTE_normal' => 'Lym Absolute Normal',
			'LYM_ABSOLUTE_unit' => 'Lym Absolute Unit',
			'MON_ABSOLUTE' => 'Mon Absolute',
			'MON_ABSOLUTE_normal' => 'Mon Absolute Normal',
			'MON_ABSOLUTE_unit' => 'Mon Absolute Unit',
			'PLT' => 'Plt',
			'PLT_normal' => 'Plt Normal',
			'PLT_unit' => 'Plt Unit',
			'MPV' => 'Mpv',
			'MPV_normal' => 'Mpv Normal',
			'MPV_unit' => 'Mpv Unit',
			'PDW' => 'Pdw',
			'PDW_normal' => 'Pdw Normal',
			'PDW_unit' => 'Pdw Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('RBC',$this->RBC,true);
		$criteria->compare('RBC_normal',$this->RBC_normal,true);
		$criteria->compare('RBC_unit',$this->RBC_unit,true);
		$criteria->compare('HGB',$this->HGB,true);
		$criteria->compare('HGB_normal',$this->HGB_normal,true);
		$criteria->compare('HGB_unit',$this->HGB_unit,true);
		$criteria->compare('HCT',$this->HCT,true);
		$criteria->compare('HCT_normal',$this->HCT_normal,true);
		$criteria->compare('HCT_unit',$this->HCT_unit,true);
		$criteria->compare('MCV',$this->MCV,true);
		$criteria->compare('MCV_normal',$this->MCV_normal,true);
		$criteria->compare('MCV_unit',$this->MCV_unit,true);
		$criteria->compare('MCH',$this->MCH,true);
		$criteria->compare('MCH_normal',$this->MCH_normal,true);
		$criteria->compare('MCH_unit',$this->MCH_unit,true);
		$criteria->compare('MCHC',$this->MCHC,true);
		$criteria->compare('MCHC_normal',$this->MCHC_normal,true);
		$criteria->compare('MCHC_unit',$this->MCHC_unit,true);
		$criteria->compare('RDW_CV',$this->RDW_CV,true);
		$criteria->compare('RDW_CV_normal',$this->RDW_CV_normal,true);
		$criteria->compare('RDW_CV_unit',$this->RDW_CV_unit,true);
		$criteria->compare('WBC',$this->WBC,true);
		$criteria->compare('WBC_normal',$this->WBC_normal,true);
		$criteria->compare('WBC_unit',$this->WBC_unit,true);
		$criteria->compare('GRA_PERCENT',$this->GRA_PERCENT,true);
		$criteria->compare('GRA_PERCENT_normal',$this->GRA_PERCENT_normal,true);
		$criteria->compare('GRA_PERCENT_unit',$this->GRA_PERCENT_unit,true);
		$criteria->compare('LYM_PERCENT',$this->LYM_PERCENT,true);
		$criteria->compare('LYM_PERCENT_normal',$this->LYM_PERCENT_normal,true);
		$criteria->compare('LYM_PERCENT_unit',$this->LYM_PERCENT_unit,true);
		$criteria->compare('MON_PERCENT',$this->MON_PERCENT,true);
		$criteria->compare('MON_PERCENT_normal',$this->MON_PERCENT_normal,true);
		$criteria->compare('MON_PERCENT_unit',$this->MON_PERCENT_unit,true);
		$criteria->compare('GRA_ABSOLUTE',$this->GRA_ABSOLUTE,true);
		$criteria->compare('GRA_ABSOLUTE_normal',$this->GRA_ABSOLUTE_normal,true);
		$criteria->compare('GRA_ABSOLUTE_unit',$this->GRA_ABSOLUTE_unit,true);
		$criteria->compare('LYM_ABSOLUTE',$this->LYM_ABSOLUTE,true);
		$criteria->compare('LYM_ABSOLUTE_normal',$this->LYM_ABSOLUTE_normal,true);
		$criteria->compare('LYM_ABSOLUTE_unit',$this->LYM_ABSOLUTE_unit,true);
		$criteria->compare('MON_ABSOLUTE',$this->MON_ABSOLUTE,true);
		$criteria->compare('MON_ABSOLUTE_normal',$this->MON_ABSOLUTE_normal,true);
		$criteria->compare('MON_ABSOLUTE_unit',$this->MON_ABSOLUTE_unit,true);
		$criteria->compare('PLT',$this->PLT,true);
		$criteria->compare('PLT_normal',$this->PLT_normal,true);
		$criteria->compare('PLT_unit',$this->PLT_unit,true);
		$criteria->compare('MPV',$this->MPV,true);
		$criteria->compare('MPV_normal',$this->MPV_normal,true);
		$criteria->compare('MPV_unit',$this->MPV_unit,true);
		$criteria->compare('PDW',$this->PDW,true);
		$criteria->compare('PDW_normal',$this->PDW_normal,true);
		$criteria->compare('PDW_unit',$this->PDW_unit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}