<?php

/**
 * This is the model class for table "meinian_disease_type"."evaluate_disease_type".
 *
 * The followings are the available columns in table 'meinian_disease_type':'evaluate_disease_type':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integerstring $element_id
 * * @property string $element_name
 * @property string $outline
 * @property string $elementmechanism
 * @property string $complication
 * @property string $symptom
 */
class EvaluateDiseaseType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateDiseaseType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_disease_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('id, name, description, element_id, element_name, outline, elementmechanism, complication', 'required'),
		array('id', 'numerical', 'integerOnly'=>true),
		array('name', 'length', 'max'=>30),
		array('description', 'length', 'max'=>200),
		array('element_id', 'length', 'max'=>50),
		array('element_name', 'length', 'max'=>1000),
		//
		array('outline, elementmechanism, complication, symptom', 'length', 'max'=>3000),
		// The following rule is used by search().
		//
		// Please remove those attributes that should not be searched.
		array('id, name, description, element_id,element_name, outline, elementmechanism, complication, symptom', 'safe', 'on'=>'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'elements' => array(self::BELONGS_TO, 'MeinianDiseaseElement', array('element_id' => 'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'element_id' => 'Element',
	'element_name' => 'Element Name',
			'outline' => 'Outline',
			'elementmechanism' => 'Elementmechanism',
			'complication' => 'Complication',
			'symptom' => 'Symptom',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('element_id',$this->element_id);		$criteria->compare('element_id',$this->element_id,true);
		$criteria->compare('element_name',$this->element_name,true);
		$criteria->compare('outline',$this->outline,true);
		$criteria->compare('elementmechanism',$this->elementmechanism,true);
		$criteria->compare('complication',$this->complication,true);
		$criteria->compare('symptom',$this->symptom,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * 通过疾病ID找到疾病诱因
	 * Enter description here ...
	 */
	/*	public function getElementOfDisease($disease){
		$criteria = new CDbCriteria();
		$criteria->select = 'element_id';
		$criteria->compare('id', $disease);
		$result = EvaluateDiseaseType::model()->findAll($criteria);

		return $result;
		}*/

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $element_exits_array
	 */
	public function getTypeOfDisease($element_exits_array){
		$criteria = new CDbCriteria();

		$criteria->addNotInCondition('id', $element_exits_array);

		$result = EvaluateDiseaseType::model()->findAll($criteria);

		return $result;
	}

	/**
	 * 得到相应体检报告对应疾病测评是否已经进行过
	 * Enter description here ...
	 * @param unknown_type $diseaseExmID
	 * @param unknown_type $disease
	 */
	public function getAllOfDiseasetemp($diseaseExmID, $disease){
		$criteria = new CDbCriteria();
		$criteria->compare('uid', $diseaseExmID);
		$criteria->compare('type_id', $disease);

		$result = EvaluateDiseaseType::model()->findAll($criteria);
		return $result;
	}

	/**
	 * find elements
	 * @param unknown_type $diseaseid
	 */
	public function getElementById($diseaseid){
		$criteria = new CDbCriteria();
		//$criteria->select = 'element_id';
		$criteria->compare('id', $diseaseid);
		$result = EvaluateDiseaseType::model()->findAll($criteria);
		return $result;
	}

	/**
	 * 得到疾病id, name
	 * 返回一个
	 */
	public function getDiseaseIdName(){
		$criteria = new CDbCriteria();
		$criteria->select = 'id, name';
		return EvaluateDiseaseType::model()->findAll($criteria);
	}

	/**
	 * save the element_name column changed
	 * Enter description here ...
	 */
	public function updateElementName($disid, $ele_new_str){
		if (isset($disid)&&($ele_new_str)){
			//$sql_up = "update evaluate_disease_type set element_name='$ele_new_str' where id=$disid";
			$dis_type = new EvaluateDiseaseType();
				
			$result = $dis_type->updateByPk($disid,array("element_name" => $ele_new_str));
				
			// return $result ? 1:0;
			if ($result) {
				return 1;
			}else{
				return 0;
			}
		}
		return 0;
	}

	/**
	 * delete the element_name which want to delete
	 * Enter description here ...
	 */
	public function deleteElementName(){

	}
}