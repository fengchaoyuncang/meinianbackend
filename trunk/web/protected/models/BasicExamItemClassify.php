<?php

/**
 * This is the model class for table "basic_exam_item_classify".
 *
 * The followings are the available columns in table 'basic_exam_item_classify':
 * @property integer $id
 * @property string $item_name
 * @property string $description
 * @property integer $type
 * @property string $content
 * @property string $diagnosis
 * @property string $objective
 * @property string $analysis
 * @property string $suggestion
 */
class BasicExamItemClassify extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasicExamItemClassify the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'basic_exam_item_classify';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type', 'numerical', 'integerOnly'=>true),
			array('item_name', 'length', 'max'=>255),
			array('description, content, diagnosis, objective, analysis, suggestion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_name, description, type, content, diagnosis, objective, analysis, suggestion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_name' => 'Item Name',
			'description' => 'Description',
			'type' => 'Type',
			'content' => 'Content',
			'diagnosis' => 'Diagnosis',
			'objective' => 'Objective',
			'analysis' => 'Analysis',
			'suggestion' => 'Suggestion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('diagnosis',$this->diagnosis,true);
		$criteria->compare('objective',$this->objective,true);
		$criteria->compare('analysis',$this->analysis,true);
		$criteria->compare('suggestion',$this->suggestion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>90,),
		));
	}
}