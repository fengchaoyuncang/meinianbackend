<?php

class User extends CActiveRecord {
    
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'b2b_user';
    }

    public function getAll($center_id){
        if($center_id === null){
            return false;
        }
        $criteria=new CDbCriteria;
        if($center_id !== 0){
            $criteria->condition='center_id=:center_id';
            $criteria->params=array(':center_id'=>$center_id);
        }
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>20,
           ),
        ));
    }
}
