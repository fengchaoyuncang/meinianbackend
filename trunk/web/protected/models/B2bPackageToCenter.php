<?php

/**
 * This is the model class for table "b2b_package_to_center".
 *
 * The followings are the available columns in table 'b2b_package_to_center':
 * @property integer $id
 * @property integer $package_id
 * @property integer $center_id
 * @property integer $sub_center_id
 * @property string $status
 * @property string $create_at
 * @property string $update_at
 */
class B2bPackageToCenter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bPackageToCenter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_package_to_center';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, package_id, brand_id, sub_center_id, status, update_at', 'required'),
			array('id, package_id, brand_id, sub_center_id', 'numerical', 'integerOnly'=>true),
			array('create_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, package_id, brand_id, sub_center_id, status, create_at, update_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'center' => array(self::BELONGS_TO,'B2bSubCenter',array('sub_center_id'=>'id')),
                    'package'=>array(self::BELONGS_TO,'B2bPackage',array('package_id'=>'id')),
                    'parent_center'=>array(self::BELONGS_TO,'B2bCenter',array('brand_id'=>'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'package_id' => 'Package',
			'brand_id' => 'Center',
			'sub_center_id' => 'Sub Center',
			'status' => 'Status',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('package_id',$this->package_id);
		$criteria->compare('brand_id',$this->brand_id);
		$criteria->compare('sub_center_id',$this->sub_center_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getPaInfo($centerid){
		$criteria = new CDbCriteria();
		
		$criteria->condition = "sub_center_id=:centerid";
		$criteria->group = "package_id";
		$criteria->params = array(":centerid" => $centerid);
		
		return B2bPackageToCenter::model()->findAll($criteria);
	}
        public function bindcenters($packageid,$ids){
            //首先要过滤掉已经关联的体检中心
            $ret = B2bPackageToCenter::model()->findAllByAttributes(array("package_id"=>$packageid,"sub_center_id"=>$ids));
            foreach ($ret as $i){
                if(($k=array_search($i->id, $ids))!==false){
                    unset($ids[$k]);                    
                }
            }
            $ids = array_unique($ids);
            $sql = "insert into `b2b_package_to_center` (`package_id`,`brand_id`,`sub_center_id`) values (:package_id,:brand_id,:sub_center_id)";
            $ret = B2bSubCenter::model()->findAllByPk($ids,array("select"=>"id,brandid"));
            $center = array();
            foreach ($ret as $i){
                $center[$i->id] = $i->brandid;
            }
            //准备插入数据
            try {
                $db = Yii::app()->db->createCommand($sql);
                foreach($ids as $id){
                    $params[":package_id"] = $packageid;
                    $params[":brand_id"] = isset($center[$id])?$center[$id]:0;
                    $params[":sub_center_id"] = $id;
                    $r = $db->execute($params);
                }
                return true;
            } catch (Exception $exc) {
                return $exc->getMessage();
            }

            
        }
}