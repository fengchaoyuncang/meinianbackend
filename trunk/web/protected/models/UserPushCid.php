<?php

/**
 * This is the model class for table "user_push_cid".
 *
 * The followings are the available columns in table 'user_push_cid':
 * @property integer $id
 * @property string $app_id
 * @property string $push_cid
 * @property string $update_time
 */
class UserPushCid extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserPushCid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_push_cid';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('update_time', 'required'),
			array('app_id, push_cid', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, app_id, push_cid, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_id' => 'App',
			'push_cid' => 'Push Cid',
			'update_time' => 'Update Time',
		);
	}

	public function saveInfo($uid, $push_id)
	{
		try
		{
			$connection = Yii::app()->db;
			$sql = "insert into user_push_cid(app_id, push_cid) values(:app_id, :push_id) on duplicate key update push_cid=:push_id";
			$command = $connection->createCommand($sql);
			$command->bindParam(":app_id", $uid, PDO::PARAM_STR);
			$command->bindParam(":push_id", $push_id, PDO::PARAM_STR);
			$command->execute();
			return 0;
		}
		catch(exception $e)
		{
			return 1;
		}	
	}
    
    public function getCid($uid)
    {
        $criteria = new CDbCriteria;
        $criteria->select="push_cid";
        $criteria->compare("app_id", $uid);
        $row = $this->find($criteria);
        return $row==null ? "" : $row->push_cid;
    }

	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('push_cid',$this->push_cid,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}