<?php

/**
 * This is the model class for table "evaluate_report_internal_medicine".
 *
 * The followings are the available columns in table 'evaluate_report_internal_medicine':
 * @property integer $id
 * @property integer $uid
 * @property string $medical_history
 * @property string $family_history
 * @property string $heart_rate
 * @property string $rhythm
 * @property string $heart_sounds
 * @property string $lung_auscultation
 * @property string $liver_palpation
 * @property string $spleen_palpation
 * @property string $kidney_percussion
 * @property string $internal_medicine_other
 * @property string $preliminary_opinion
 */
class EvaluateReportInternalMedicine extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportInternalMedicine the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_internal_medicine';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, medical_history, family_history, heart_rate, rhythm, heart_sounds, lung_auscultation, liver_palpation, spleen_palpation, kidney_percussion, internal_medicine_other, preliminary_opinion', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('medical_history, family_history, heart_sounds, lung_auscultation, liver_palpation, spleen_palpation, kidney_percussion, preliminary_opinion', 'length', 'max'=>200),
			array('heart_rate, rhythm, internal_medicine_other', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, medical_history, family_history, heart_rate, rhythm, heart_sounds, lung_auscultation, liver_palpation, spleen_palpation, kidney_percussion, internal_medicine_other, preliminary_opinion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'medical_history' => 'Medical History',
			'family_history' => 'Family History',
			'heart_rate' => 'Heart Rate',
			'rhythm' => 'Rhythm',
			'heart_sounds' => 'Heart Sounds',
			'lung_auscultation' => 'Lung Auscultation',
			'liver_palpation' => 'Liver Palpation',
			'spleen_palpation' => 'Spleen Palpation',
			'kidney_percussion' => 'Kidney Percussion',
			'internal_medicine_other' => 'Internal Medicine Other',
			'preliminary_opinion' => 'Preliminary Opinion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('medical_history',$this->medical_history,true);
		$criteria->compare('family_history',$this->family_history,true);
		$criteria->compare('heart_rate',$this->heart_rate,true);
		$criteria->compare('rhythm',$this->rhythm,true);
		$criteria->compare('heart_sounds',$this->heart_sounds,true);
		$criteria->compare('lung_auscultation',$this->lung_auscultation,true);
		$criteria->compare('liver_palpation',$this->liver_palpation,true);
		$criteria->compare('spleen_palpation',$this->spleen_palpation,true);
		$criteria->compare('kidney_percussion',$this->kidney_percussion,true);
		$criteria->compare('internal_medicine_other',$this->internal_medicine_other,true);
		$criteria->compare('preliminary_opinion',$this->preliminary_opinion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}