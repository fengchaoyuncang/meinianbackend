<?php

/**
 * This is the model class for table "evaluate_sports_list".
 *
 * The followings are the available columns in table 'evaluate_sports_list':
 * @property integer $id
 * @property string $title
 * @property integer $sportid
 * @property string $video_url
 * @property string $desc
 * @property string $create_at
 * @property string $update_at
 */
class EvaluateSportsList extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateSportsList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_sports_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, sportid, video_url, desc', 'required'),
			array('sportid', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>50),
			array('video_url', 'length', 'max'=>1024),
			array('desc', 'length', 'max'=>1000),
			array('create_at, update_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, sportid, video_url, desc, create_at, update_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'sportid' => 'Sportid',
			'video_url' => 'Video Url',
			'desc' => 'Desc',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('sportid',$this->sportid);
		$criteria->compare('video_url',$this->video_url,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}