<?php

/**
 * This is the model class for table "b2b_exam_item_to_subcenter".
 *
 * The followings are the available columns in table 'b2b_exam_item_to_subcenter':
 * @property integer $id
 * @property integer $center_id
 * @property integer $exam_item_id
 * @property string $create_at
 * @property string $update_at
 * @property integer $sale
 */
class B2bExamItemToSubcenter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bExamItemToSubcenter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_exam_item_to_subcenter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, subcenter_id, exam_item_id, sale', 'numerical', 'integerOnly'=>true),
			array('create_at, update_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, subcenter_id, exam_item_id, create_at, update_at, sale', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'center_id' => 'Center',
			'exam_item_id' => 'Exam Item',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
			'sale' => 'Sale',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subcenter_id',$this->subcenter_id);
		$criteria->compare('exam_item_id',$this->exam_item_id);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('sale',$this->sale);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}