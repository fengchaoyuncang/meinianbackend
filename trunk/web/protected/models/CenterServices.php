<?php

/*
 * 获取体检中心不同订单的分页结果
 * @param $status  子体检中心的状态
 * @param $view 渲染到的视图
 */

class CenterServices{
    //获取所有有体检中心的城市
    public function getCenterRegion()
    {
        try {
            $model = B2bSubCenter::model()->getCities();
        } catch (Exception $exc) {
            return JSONHelper::ERROR_SERVICE;
        }
        
        return $model;
    }
    //根据省或者市获取体检中心
    public function getCentersByCity($arr)
    {
        $province = null;
        $city = null;
        if(isset($arr["province"])){
            $province = trim($arr["province"]);
        }
        if(isset($arr["city"])){
            $city = trim($arr["city"]);
        }
        if($province === null && $city === null){
            return JSONHelper::ERROR_ILLEGAL_REQUEST;
        }
        $criteria = new CDbCriteria();
        if($province !== null){
            $criteria->addCondition("province = '$province'");
        }
        if($city !== null){
            $criteria->addCondition("city = '$city'");
        }
        $ret = B2bSubCenter::model()->findAll($criteria);
        $items = array();
        foreach($ret as $it){
            $item["name"] = $it->name;
            $item["province"] = $it->province;
            $item["city"] = $it->city;
            $item["address"] = $it->address;
            array_push($items, $item);
        }
        return $items;
    }
    public function getCentersByCityAndPage($arr,$page = 20)
    {
        $province = null;
        $city = null;
        if(isset($arr["province"])){
            $province = trim($arr["province"]);
        }
        if(isset($arr["city"])){
            $city = trim($arr["city"]);
        }
//        if($province === null && $city === null){
//            return JSONHelper::ERROR_ILLEGAL_REQUEST;
//        }
        $criteria = new CDbCriteria();
        if($province !== null){
            $criteria->addCondition("province = '$province'");
        }
        if($city !== null){
            $criteria->addCondition("city = '$city'");
        }
        $total = B2bSubCenter::model()->count($criteria);
        $pages= new CPagination($total);
        $pages->pageSize = $page;
        $pages->applyLimit($criteria);
        $ret = B2bSubCenter::model()->findAll($criteria);
        $items = array();
        foreach($ret as $it){
            $item["id"] = $it->id;
            $item["name"] = $it->name;
            $item["province"] = $it->province;
            $item["city"] = $it->city;
            $item["address"] = $it->address;
            $item["telephone"] = $it->telephone;
            array_push($items, $item);
        }
        return array("page"=>$pages,"items"=>$items);
    }
}   

?>
 