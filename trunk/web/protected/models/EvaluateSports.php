<?php

/**
 * This is the model class for table "evaluate_sports".
 *
 * The followings are the available columns in table 'evaluate_sports':
 * @property integer $id
 * @property string $title
 * @property double $fanying
 * @property double $liliang
 * @property double $naili
 * @property double $rouren
 * @property double $pingheng
 * @property string $image_url
 * @property string $desc
 * @property integer $type
 * @property string $create_at
 * @property string $update_at
 */
class EvaluateSports extends CActiveRecord {

    const YOUYANG = 1;
    const LILIANG = 2;
    const ROUREN = 3;
    
    const CONTENT_VIDEO = 1;
    const CONTENT_BOOK = 2;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EvaluateSports the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'evaluate_sports';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, fanying, liliang, naili, rouren, pingheng, image_url, desc, type', 'required'),
            array('type', 'numerical', 'integerOnly' => true),
            array('fanying, liliang, naili, rouren, pingheng', 'numerical'),
            array('title, desc', 'length', 'max' => 200),
            array('image_url', 'length', 'max' => 1024),
            array('create_at, update_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, fanying, liliang, naili, rouren, pingheng, image_url, desc, type, create_at, update_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'fanying' => 'Fanying',
            'liliang' => 'Liliang',
            'naili' => 'Naili',
            'rouren' => 'Rouren',
            'pingheng' => 'Pingheng',
            'image_url' => 'Image Url',
            'desc' => 'Desc',
            'type' => 'Type',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function getRecSports($sport = null){
        if($sport !== null){
            $sql="select * from (select * from `evaluate_sports` order by`$sport`) `temp` group by `type` order by `$sport`";
        }else{
            $sql = "select * from `evaluate_sports` group by `type`";
        }
        $command=Yii::app()->db->createCommand($sql);
        $rows=$command->queryAll();
        
        return $rows;
    }
    /**
     * 检索所有运动列表
     */
    public function getSportsList($sport = "fanying"){
        
        if($sport !== null){
            $sql="select * from `evaluate_sports` order by`$sport`";
        }else{
            $sql = "select * from `evaluate_sports`";
        }
        $command=Yii::app()->db->createCommand($sql);
        $rows=$command->queryAll();
        
        return $rows;
    }

    static function getSportTypeName($type) {
        switch ($type) {
            case EvaluateSports::YOUYANG:
                return "有氧运动";

                break;
            case EvaluateSports::LILIANG:
                return "力量运动";

                break;
            case EvaluateSports::ROUREN:
                return "柔韧运动";

                break;
            default:
                return false;
                break;
        }
    }

}
