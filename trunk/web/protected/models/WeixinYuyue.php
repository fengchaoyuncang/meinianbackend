<?php

/**
 * This is the model class for table "weixin_yuyue".
 *
 * The followings are the available columns in table 'weixin_yuyue':
 * @property integer $id
 * @property string $phone
 * @property string $realname
 * @property string $examno
 * @property string $examcenter
 * @property string $booktime
 * @property string $tjpackage
 * @property integer $status
 */
class WeixinYuyue extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WeixinYuyue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'weixin_yuyue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('phone, realname, examcenter, booktime, tjpackage', 'required'),
			array('id, status', 'numerical', 'integerOnly'=>true),
			array('phone, examno', 'length', 'max'=>15),
			array('identity_no','length','max'=>20),
			array('realname', 'length', 'max'=>30),
			array('examcenter, tjpackage', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, phone, realname, examno, examcenter, booktime, tjpackage, status,identity_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'phone' => '手机号',
			'realname' => '姓名',
			'examno' => '体检号',
			'identity_no'=>'身份证号',
			'examcenter' => '体检中心',
			'booktime' => '预约日期',
			'tjpackage' => '预约套餐',
			'status' => '状态',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order="t.id desc";
		
		$criteria->compare('id',$this->id);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('realname',$this->realname,true);
		$criteria->compare('examno',$this->examno,true);
		$criteria->compare('identity_no',$this->identity_no,true);
		$criteria->compare('examcenter',$this->examcenter,true);
		$criteria->compare('booktime',$this->booktime,true);
		$criteria->compare('tjpackage',$this->tjpackage,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}