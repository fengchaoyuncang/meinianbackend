<?php

/**
 * This is the model class for table "info_papers".
 *
 * The followings are the available columns in table 'info_papers':
 * @property integer $id
 * @property string $title
 * @property integer $type
 * @property integer $related_item_id
 * @property integer $related_indicator_id
 * @property string $abstract
 * @property string $content
 * @property string $symptom
 * @property string $examination
 * @property string $treatment
 * @property string $suggestion
 */
class InfoPapers extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return InfoPapers the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'info_papers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//			array('content', 'required'),
//			array('type, related_item_id, related_indicator_id', 'numerical', 'integerOnly'=>true),
//			array('title', 'length', 'max'=>255),
//			array('abstract, symptom, examination, treatment, suggestion', 'safe'),
//			// The following rule is used by search().
//			// Please remove those attributes that should not be searched.
//			array('id, title, type, related_item_id, related_indicator_id, abstract, content, symptom, examination, treatment, suggestion', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'type' => 'Type',
            'related_item_id' => 'Related Item',
            'related_indicator_id' => 'Related Indicator',
            'abstract' => 'Abstract',
            'content' => 'Content',
            'symptom' => 'Symptom',
            'examination' => 'Examination',
            'treatment' => 'Treatment',
            'suggestion' => 'Suggestion',
        );
    }

    public function getByPage() {
        $criteria = new CDbCriteria();
        $criteria->order = "id desc";

        return new CActiveDataProvider("InfoPapers", array("criteria" => $criteria,
            "pagination" => array("pagesize" => 10,),
        ));
    }

}
