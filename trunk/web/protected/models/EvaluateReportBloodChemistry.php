<?php

/**
 * This is the model class for table "evaluate_report_blood_chemistry".
 *
 * The followings are the available columns in table 'evaluate_report_blood_chemistry':
 * @property integer $id
 * @property integer $uid
 * @property string $ALT
 * @property string $ALT_normal
 * @property string $ALT_unit
 * @property string $AST
 * @property string $AST_normal
 * @property string $AST_unit
 * @property string $Cr
 * @property string $Cr_normal
 * @property string $Cr_unit
 * @property string $BUN
 * @property string $BUN_normal
 * @property string $BUN_unit
 * @property string $UA
 * @property string $UA_normal
 * @property string $UA_unit
 * @property string $FBG
 * @property string $FBG_issue
 * @property string $FBG_normal
 * @property string $FBG_unit
 * @property string $TC
 * @property string $TC_normal
 * @property string $TC_unit
 * @property string $TG
 * @property string $TG_issue
 * @property string $TG_normal
 * @property string $TG_unit
 * @property string $AFP
 * @property string $AFP_normal
 * @property string $AFP_unit
 * @property string $CEA
 * @property string $CEA_normal
 * @property string $CEA_unit
 */
class EvaluateReportBloodChemistry extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportBloodChemistry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_blood_chemistry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, ALT, ALT_normal, ALT_unit, AST, AST_normal, AST_unit, Cr, Cr_normal, Cr_unit, BUN, BUN_normal, BUN_unit, UA, UA_normal, UA_unit, FBG, FBG_issue, FBG_normal, FBG_unit, TC, TC_normal, TC_unit, TG, TG_issue, TG_normal, TG_unit, AFP, AFP_normal, AFP_unit, CEA, CEA_normal, CEA_unit', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('ALT, ALT_normal, ALT_unit, AST, AST_normal, AST_unit, Cr, BUN, UA, FBG, FBG_issue, TC, TG, TG_issue, AFP, AFP_normal, AFP_unit, CEA, CEA_normal, CEA_unit', 'length', 'max'=>20),
			array('Cr_normal, Cr_unit, BUN_normal, BUN_unit, UA_normal, UA_unit, FBG_normal, FBG_unit, TC_normal, TC_unit, TG_normal, TG_unit', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, ALT, ALT_normal, ALT_unit, AST, AST_normal, AST_unit, Cr, Cr_normal, Cr_unit, BUN, BUN_normal, BUN_unit, UA, UA_normal, UA_unit, FBG, FBG_issue, FBG_normal, FBG_unit, TC, TC_normal, TC_unit, TG, TG_issue, TG_normal, TG_unit, AFP, AFP_normal, AFP_unit, CEA, CEA_normal, CEA_unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'ALT' => 'Alt',
			'ALT_normal' => 'Alt Normal',
			'ALT_unit' => 'Alt Unit',
			'AST' => 'Ast',
			'AST_normal' => 'Ast Normal',
			'AST_unit' => 'Ast Unit',
			'Cr' => 'Cr',
			'Cr_normal' => 'Cr Normal',
			'Cr_unit' => 'Cr Unit',
			'BUN' => 'Bun',
			'BUN_normal' => 'Bun Normal',
			'BUN_unit' => 'Bun Unit',
			'UA' => 'Ua',
			'UA_normal' => 'Ua Normal',
			'UA_unit' => 'Ua Unit',
			'FBG' => 'Fbg',
			'FBG_issue' => 'Fbg Issue',
			'FBG_normal' => 'Fbg Normal',
			'FBG_unit' => 'Fbg Unit',
			'TC' => 'Tc',
			'TC_normal' => 'Tc Normal',
			'TC_unit' => 'Tc Unit',
			'TG' => 'Tg',
			'TG_issue' => 'Tg Issue',
			'TG_normal' => 'Tg Normal',
			'TG_unit' => 'Tg Unit',
			'AFP' => 'Afp',
			'AFP_normal' => 'Afp Normal',
			'AFP_unit' => 'Afp Unit',
			'CEA' => 'Cea',
			'CEA_normal' => 'Cea Normal',
			'CEA_unit' => 'Cea Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('ALT',$this->ALT,true);
		$criteria->compare('ALT_normal',$this->ALT_normal,true);
		$criteria->compare('ALT_unit',$this->ALT_unit,true);
		$criteria->compare('AST',$this->AST,true);
		$criteria->compare('AST_normal',$this->AST_normal,true);
		$criteria->compare('AST_unit',$this->AST_unit,true);
		$criteria->compare('Cr',$this->Cr,true);
		$criteria->compare('Cr_normal',$this->Cr_normal,true);
		$criteria->compare('Cr_unit',$this->Cr_unit,true);
		$criteria->compare('BUN',$this->BUN,true);
		$criteria->compare('BUN_normal',$this->BUN_normal,true);
		$criteria->compare('BUN_unit',$this->BUN_unit,true);
		$criteria->compare('UA',$this->UA,true);
		$criteria->compare('UA_normal',$this->UA_normal,true);
		$criteria->compare('UA_unit',$this->UA_unit,true);
		$criteria->compare('FBG',$this->FBG,true);
		$criteria->compare('FBG_issue',$this->FBG_issue,true);
		$criteria->compare('FBG_normal',$this->FBG_normal,true);
		$criteria->compare('FBG_unit',$this->FBG_unit,true);
		$criteria->compare('TC',$this->TC,true);
		$criteria->compare('TC_normal',$this->TC_normal,true);
		$criteria->compare('TC_unit',$this->TC_unit,true);
		$criteria->compare('TG',$this->TG,true);
		$criteria->compare('TG_issue',$this->TG_issue,true);
		$criteria->compare('TG_normal',$this->TG_normal,true);
		$criteria->compare('TG_unit',$this->TG_unit,true);
		$criteria->compare('AFP',$this->AFP,true);
		$criteria->compare('AFP_normal',$this->AFP_normal,true);
		$criteria->compare('AFP_unit',$this->AFP_unit,true);
		$criteria->compare('CEA',$this->CEA,true);
		$criteria->compare('CEA_normal',$this->CEA_normal,true);
		$criteria->compare('CEA_unit',$this->CEA_unit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}