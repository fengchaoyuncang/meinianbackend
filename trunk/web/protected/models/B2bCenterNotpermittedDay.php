<?php

/**
 * This is the model class for table "b2b_center_notpermitted_day".
 *
 * The followings are the available columns in table 'b2b_center_notpermitted_day':
 * @property string $id
 * @property integer $centerid
 * @property string $centername
 * @property string $notpermitteddate
 * @property integer $type
 */
class B2bCenterNotpermittedDay extends CActiveRecord
{
    const YOUXIAO = 1;
    const WUXIAO = 2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bCenterNotpermittedDay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_center_notpermitted_day';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('centerid, type', 'numerical', 'integerOnly'=>true),
			array('centername', 'length', 'max'=>20),
			array('notpermitteddate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, centerid, centername, notpermitteddate, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'centerid' => 'Centerid',
			'centername' => '体检中心',
			'notpermitteddate' => '不可预约日期',
			'type' => '状态',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('centerid',$this->centerid);
		$criteria->compare('centername',$this->centername,true);
		$criteria->compare('notpermitteddate',$this->notpermitteddate,true);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    static function getCodetypeDesc($codetype)
    {
        switch ($codetype) {
            case B2bCenterNotpermittedDay::YOUXIAO:
                $codetype = "有效";
                break;
            case B2bCenterNotpermittedDay::WUXIAO:
                $codetype = "无效";
                break;
        }
        return $codetype;
    }
}