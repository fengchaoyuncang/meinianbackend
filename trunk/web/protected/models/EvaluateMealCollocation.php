<?php

/**
 * This is the model class for table "evaluate_meal_collocation".
 *
 * The followings are the available columns in table 'evaluate_meal_collocation':
 * @property integer $id
 * @property string $name
 * @property string $mealtype
 * @property integer $energyperkilogram
 * @property double $coefficient
 */
class EvaluateMealCollocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateMealCollocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_meal_collocation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, energyperkilogram', 'numerical', 'integerOnly'=>true),
			array('coefficient', 'numerical'),
			array('name', 'length', 'max'=>10),
			array('mealtype', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, mealtype, energyperkilogram, coefficient, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'mealtype' => 'Mealtype',
			'energyperkilogram' => 'Energyperkilogram',
			'coefficient' => 'Coefficient',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mealtype',$this->mealtype,true);
		$criteria->compare('energyperkilogram',$this->energyperkilogram);
		$criteria->compare('coefficient',$this->coefficient);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}