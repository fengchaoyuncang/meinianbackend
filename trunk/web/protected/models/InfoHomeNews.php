<?php

/**
 * This is the model class for table "info_home_news".
 *
 * The followings are the available columns in table 'info_home_news':
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $uid
 * @property string $content
 * @property integer $center_id
 * @property string $create_at
 * @property string $update_at
 */
class InfoHomeNews extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfoHomeNews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'info_home_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('uid, content, update_at', 'required'),
            array('uid, center_id', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>200),
            array('image', 'length', 'max'=>500),
            array('create_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, image, uid, content, center_id, create_at, update_at', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'image' => 'Image',
			'uid' => 'Uid',
			'content' => 'Content',
			'center_id' => 'Center',
			'create_at' => 'Create At',
			'update_at' => 'Update At',
		);
	}
	
	/**
	 * 返回最新的三篇首页资讯
	 */
	public function getHomeNews($num = 3) {
		$criteria = new CDbCriteria ();
		$criteria->select = 'id,title,image';
		$criteria->order = 'create_at desc';
		$criteria->limit = $num;
		$ret = $this->findAll ( $criteria );
		$items = array ();
		foreach ( $ret as $it ) {
			$item ["id"] = $it->id;
			$item ["title"] = $it->title;
			$item ["image"] = Yii::app ()->baseUrl . $it->image;
			$item ["url"] = Yii::app ()->controller->createUrl ( "app/papers/view", array (
					"id" => $it->id 
			) );
			array_push ( $items, $item );
		}
		return $items;
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * 
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria = new CDbCriteria ();
		
		$criteria->compare ( 'id', $this->id );
		$criteria->compare ( 'title', $this->title, true );
		$criteria->compare ( 'image', $this->image, true );
		$criteria->compare ( 'uid', $this->uid );
		$criteria->compare ( 'content', $this->content, true );
		$criteria->compare ( 'center_id', $this->center_id );
		$criteria->compare ( 'create_at', $this->create_at, true );
		$criteria->compare ( 'update_at', $this->update_at, true );
		
		return new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
	}
}