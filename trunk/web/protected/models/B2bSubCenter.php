<?php

/**
 * This is the model class for table "b2b_sub_center".
 *
 * The followings are the available columns in table 'b2b_sub_center':
 * @property string $id
 * @property string $brandid
 * @property string $name
 * @property integer $country
 * @property integer $province
 * @property integer $city
 * @property string $address
 * @property string $telephone
 * @property string $description
 * @property integer $status
 * @property double $gps_lat
 * @property double $gps_lon
 * @property string $baidu_map_url
 * @property string $create_at
 * @property string $update_at
 */
class B2bSubCenter extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return B2bSubCenter the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'b2b_sub_center';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name,brandid, province, city,address, telephone,gps_lat, gps_lon', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('gps_lat, gps_lon', 'numerical'),
            array('brandid', 'length', 'max' => 11),
            array('name', 'length', 'max' => 255),
            array('address, description', 'length', 'max' => 500),
            array('telephone', 'length', 'max' => 32),
            array('baidu_map_url', 'length', 'max' => 1024),
            array('create_at, update_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('brandid, name,  province, city, address, telephone, description, status, gps_lat, gps_lon, baidu_map_url, create_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            "brand" => array(self::BELONGS_TO, 'B2bBrand', array("brandid" => "id"))
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'brandid' => '品牌ID',
            'name' => '名称',
            'country' => '国家',
            'province' => '省份',
            'city' => '城市',
            'address' => '详细地址',
            'telephone' => '联系方式',
            'description' => '描述',
            'status' => '状态',
            'gps_lat' => 'Gps Lat',
            'gps_lon' => 'Gps Lon',
            'baidu_map_url' => '百度地图URL',
            'create_at' => '记录创建日期',
            'update_at' => 'Update At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
		
        $criteria->order='t.id desc';
        	
        $criteria->compare('id', $this->id, true);
        $criteria->compare('brandid', $this->brandid, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('country', $this->country);
        $criteria->compare('province', $this->province);
        $criteria->compare('city', $this->city);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('gps_lat', $this->gps_lat);
        $criteria->compare('gps_lon', $this->gps_lon);
        $criteria->compare('baidu_map_url', $this->baidu_map_url, true);
        $criteria->compare('create_at', $this->create_at, true);
        $criteria->compare('update_at', $this->update_at, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
       		'pagination'=>array(
				'pageSize'=>20,
			),
        ));
    }

    /**
     * 根据uid获取用户的用户所预定的健身中心的经纬度
     */
    public function getOrderCenterGps($uid) {
        $criteria = new CDbCriteria;
        $criteria->condition = "customer_id=:customer_id";
        $criteria->params = array('customer_id' => $uid);
        $criteria->with = array('orderCenterGps');
        $ret = $this->findAll($criteria);
        if (empty($ret)) {
            return NULL;
        }
        return $ret;
    }

    /**
     *
     * @param type $gps_lat
     * @param type $gps_lon
     * @param type $radius
     * @param type $cn
     * @param type $centers
     * @return array
     */
    public function centerSearch($gps_lat, $gps_lon, $radius, $cn = "", $centers = array()) {
        $block = LBSHelper::getAround($gps_lat, $gps_lon, $radius);
        $ret = array();
        $center = implode(",", $centers);
        if (empty($center)) {
            return $ret;
        }

        $criteria = new CDbCriteria;
        $criteria->select = "*";
        $criteria->distinct = true;
        if (strlen($cn) > 0) {
            $criteria->condition = "gps_lat>=:minLat and gps_lat<=:maxLat and gps_lon>=:minLon and gps_lon<=:maxLon and brand_name=:brand_name and status=:status";
            $criteria->params = array(
                ":minLat" => $block["minLat"],
                ":maxLat" => $block["maxLat"],
                ":minLon" => $block["minLon"],
                ":maxLon" => $block["maxLon"],
                ":status" => 0,
                ":brand_name" => $cn);
        } else {
            $criteria->condition = "id in ($center) and gps_lat>=:minLat and gps_lat<=:maxLat and gps_lon>=:minLon and gps_lon<=:maxLon and status=:status";
            $criteria->params = array(
                ":minLat" => $block["minLat"],
                ":maxLat" => $block["maxLat"],
                ":minLon" => $block["minLon"],
                ":status" => 0,
                ":maxLon" => $block["maxLon"]);
        }
        $criteria->order = "gps_lat, gps_lon";
        $rows = $this->findAll($criteria);
        return $rows;
    }

    public function getPagementCenterList($pagecount, $offset) {
        $criteria = new CDbCriteria();

        $criteria->limit = $pagecount;
        $criteria->offset = $offset;

        $ret = B2bSubCenter::model()->findAll($criteria);

        return $ret;
    }

    public function getTotalCount() {
        $criteria = new CDbCriteria();

        $sql = "select count(id) as id from b2b_sub_center";
        $num = B2bSubCenter::model()->findBySql($sql);

        return $num['id'];
    }

    /*
     * 获取所有体检机构列表
     * @return 数据集
     */

    public function getSubcenterEachPage() {

        $criteria = new CDbCriteria();
//        $criteria->condition = "status=:status and sub_brandid=:sub_brandid";
//        $criteria->params = array(":status" => $status, ":sub_brandid" => $sub_brandid);
        return new CActiveDataProvider("B2bSubCenter", array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 10,
            ),
        ));
    }
    
    /*
     * 获取每页的体检套餐
     */
    public function getPackageEachPage(){
//        $brandid = b2bcenter
        $criteria = new CDbCriteria();
        $criteria ->condition="";
    }
    
    /*
     * 获取属于某个城市的体检中心
     * @param $city 城市名称
     * return  数据集
     */
    public function getSubcenterToCityEachPage(){
        if(isset($_GET['city'])){
            $city = $_GET['city'];
            if($city=="全部"){
               $criteria = new CDbCriteria();
            }else{
                 $criteria = new CDbCriteria();
                 $criteria->condition="city=:city";
                 $criteria->params=array(":city"=>$city);
            }             
        }
        return new CActiveDataProvider("B2bSubCenter",array(
            'criteria'=>$criteria,
            'pagination'=>array('pagesize'=>10),
        ));
    }
    
    /*
     * 获取所有城市
     */
    public function getCities(){
        $cities = B2bSubCenter::model()->findAll(
                array("select"=>"province,city,count(city) as num",
                        "distinct"=>true, 
                        "group"=>"city",
                        "order"=>"province"
                    ));
        $items = array();
        foreach($cities as $city){
            if(!isset($items[$city->province])){
                $items[$city->province] = array();
            }
            
            array_push($items[$city->province], $city->city);
        }  
        return $items;
    }
    
    /*
     * 获取全部体检品牌名称
     * @param 无
     * return array()
     */
    static function getBrandName(){
    	$model=B2bBrand::model()->findAll();
    	$items=array();
    	if($model!=null){
    		foreach ($model as $k=>$v){
    			$items["$v->id"]=$v->name;
    		} 
    	}
    	return $items;
    }
    
    
    
}