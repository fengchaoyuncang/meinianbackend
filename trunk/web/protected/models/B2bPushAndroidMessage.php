<?php

/**
 * This is the model class for table "b2b_push_android_message".
 *
 * The followings are the available columns in table 'b2b_push_android_message':
 * @property integer $id
 * @property integer $uid
 * @property string $messages
 * @property string $addtime
 * @property string $sendtime
 */
class B2bPushAndroidMessage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bPushAndroidMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_push_android_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, messages, addtime', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('messages', 'length', 'max'=>200),
			array('sendtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, messages, addtime, sendtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'messages' => 'Messages',
			'addtime' => 'Addtime',
			'sendtime' => 'Sendtime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('messages',$this->messages,true);
		$criteria->compare('addtime',$this->addtime,true);
		$criteria->compare('sendtime',$this->sendtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
//        public function pushMessageToUser
}