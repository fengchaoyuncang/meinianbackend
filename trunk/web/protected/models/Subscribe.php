<?php

/**
 * This is the model class for table "subscribe".
 *
 * The followings are the available columns in table 'subscribe':
 * @property integer $id
 * @property string $app_id
 * @property integer $indicator_id
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property Indicator $indicator
 * @property AppUser $app
 */
class Subscribe extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Subscribe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'info_subscribe';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('update_time', 'required'),
			array('indicator_id', 'numerical', 'integerOnly'=>true),
			array('app_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, app_id, indicator_id, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'indicator' => array(self::BELONGS_TO, 'Indicator', 'indicator_id'),
			'app' => array(self::BELONGS_TO, 'AppUser', 'app_id'),
		);
	}

    //获取用户订阅的一级一级二级类目,如果用户订阅了一级类目，那么该一级类目下所有的二级类目都要获取出来
    public function getSubsribedId($app_id)
    {
        //订阅的id
        $criteria=new CDbCriteria;
		$criteria->select="indicator_id";
        $criteria->condition = "app_id=:app_id";
        $criteria->params=array(':app_id'=>$app_id);
		$rows = $this->findAll($criteria);
        $sub_ids = array();
        foreach($rows as $row)
        {
            array_push($sub_ids, $row->indicator_id);
        }
        
        //扩展二级id
        $sqlCmd = Yii::app()->db->createCommand();
        $sqlCmd->selectDistinct('id');
        $sqlCmd->from('info_indicator');
        $sqlCmd->where(array('in', 'item_id', $sub_ids));
        $rows = $sqlCmd->queryAll();    
        foreach($rows as $row)
        {
            array_push($sub_ids, $row['id']);
        }
        
        //扩展一级id
        $sqlCmd = Yii::app()->db->createCommand();
        $sqlCmd->selectDistinct('item_id');
        $sqlCmd->from('info_indicator');
        $sqlCmd->where(array('in', 'id', $sub_ids));
        $rows = $sqlCmd->queryAll();    
        foreach($rows as $row)
        {
            array_push($sub_ids, $row['item_id']);
        }
                
        $extend_id = array_unique($sub_ids);
        
        return $extend_id;    
    }
    
    //获取订阅了指定列表的人
	public function getSubsciber($id_array)
	{
        $sqlCmd = Yii::app()->db->createCommand();
        $sqlCmd->selectDistinct('app_id');
        $sqlCmd->from($this->tableName());
        $sqlCmd->where(array('in', 'indicator_id', $id_array));
        $rows = $sqlCmd->queryAll();
		$users = array();
        foreach ($rows as $row)
        {
            array_push($users, $row['app_id']);
        }
		return $users;
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_id' => 'App',
			'indicator_id' => 'Indicator',
			'update_time' => 'Update Time',
		);
	}
	
	public function doInsert($appId, $indicatorId)
	{
		try
		{
			$connection = Yii::app()->db;
			$sql = "insert into subscribe(app_id, indicator_id) values(:appId, :indicatorId) on duplicate key update update_time=CURRENT_TIMESTAMP";
			$command = $connection->createCommand($sql);
			$command->bindParam(":appId", $appId, PDO::PARAM_STR);
			$command->bindParam(":indicatorId", $indicatorId, PDO::PARAM_INT);
			$command->execute();
			return 0;
		}
		catch(exception $e)
		{
			return 1;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('indicator_id',$this->indicator_id);
		$criteria->compare('update_time',$this->update_time,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}