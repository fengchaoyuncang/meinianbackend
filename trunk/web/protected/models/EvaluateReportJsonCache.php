<?php

/**
 * This is the model class for table "evaluate_report_json_cache".
 *
 * The followings are the available columns in table 'evaluate_report_json_cache':
 * @property integer $id
 * @property integer $uid
 * @property string $no
 * @property string $json_data
 * @property string $type
 */
class EvaluateReportJsonCache extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportJsonCache the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_json_cache';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, no, json_data, type', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('no', 'length', 'max'=>50),
			array('type', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, no, json_data, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'no' => 'No',
			'json_data' => 'Json Data',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('no',$this->no,true);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
     * set data from hash array intent to insert datas to database
     * @param type $data_arr (hash array) e.g. array('name'=>'admin','password'=>'123456')
     */
    public function setData($data_arr,$array=null){
        foreach ($data_arr as $key => $value) {
            $this->$key = $value;
        }
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->$key = $value;
			}
		}
    }
}