<?php

/**
 * This is the model class for table "b2b_package".
 *
 * The followings are the available columns in table 'b2b_package':
 * @property string $id
 * @property string $name
 * @property integer $org_price
 * @property integer $sale_price
 * @property integer $suitable_gender
 * @property integer $status
 * @property integer $type
 * @property string $description
 * @property string $create_at
 * @property string $update_at
 */
class B2bPackage extends CActiveRecord {
	
	//套餐适用的性别,0 通用；1 男；2 女
    const COMMEN_GENDER=0;
    const MALE=1;
    const FEMEAL=2;
    
    //套餐适用的婚姻状态,0,通用，1未婚，2已婚。
    const MARRIAGE_ALL=0;
    const MARRIAGE_SINGLE=1;
    const MARRIAGE_DOUBLE=2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return B2bPackage the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'b2b_package';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('org_price, sale_price, suitable_gender,suitable_married', 'required'),
            array('org_price, sale_price, suitable_gender, type,status', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('description', 'length', 'max' => 1000),
            array('introduce,examitems,create_at', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, osuitable_gender, suitable_married,status, type,description,examitems,introduce', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            "centers" => array(self::HAS_MANY, 'B2bPackageToCenter', array("package_id" => "id"))
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => '套餐名',
            'org_price' => '原价格(元)',
            'sale_price' => '销售价格(元)',
            'suitable_gender' => '性别',
            'suitable_married' => '婚否',
            'examitems' => '体检项目',
	        'introduce' => '公司详细介绍',
	        'status' => '状态',
            'description' => '描述',
            'create_at' => '创建日期',
            'update_at' => '更新时间',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
		$criteria->order='t.id desc';
        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('org_price', $this->org_price);
        $criteria->compare('sale_price', $this->sale_price);
        $criteria->compare('suitable_gender', $this->suitable_gender);
        $criteria->compare('suitable_married', $this->suitable_married);
        $criteria->compare('status', $this->status);
        $criteria->compare('type', $this->type);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('create_at', $this->create_at, true);
        $criteria->compare('update_at', $this->update_at, true);

        $ret= new CActiveDataProvider($this, array(
            'criteria' => $criteria,
       		'pagination'=>array(
				'pageSize'=>20,
			),
        ));
        
        foreach($ret->getData() as $item){
        	$item->suitable_gender=$this->getSuitableGender($item->suitable_gender);
        	$item->suitable_married=$this->getSuitableMarried($item->suitable_married);
//        	$item->org_price=$item->org_price/100;
//        	$item->sale_price=$item->sale_price/100;
        }
        return $ret;
    }

    /*
     * 获取套餐列表
     * @param $status 订单状态
     * @return array
     */

    public function getPackageEachPage() {
        $criteria = new CDbCriteria();
        return new CActiveDataProvider("B2bPackage", array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 2,
            ),
        ));
    }
    
/*
     * 获取适用性别  0 通用；1 男；2 女
     * @param $suitableGender 适用性别
     * @return $suitableGender  转换为相应的汉字
     */
    public function getSuitableGender($suitableGender){
    	switch($suitableGender){
    		case B2bPackage::COMMEN_GENDER:$suitableGender="通用";break;
    		case B2bPackage::MALE:$suitableGender="男";break;
    		case B2bPackage::FEMEAL:$suitableGender="女";break;
    	}
    	return $suitableGender;
    }
    
    /*
     * 获取套餐所适用的婚姻状况，0,通用，1未婚，2已婚。
     * @param $suitable_married
     * @return 婚姻状况所对应的中文
     */
    public function getSuitableMarried($suitableMarried){
    	switch($suitableMarried){
    		case B2bPackage::MARRIAGE_ALL:$suitableMarried="通用";break;
    		case B2bPackage::MARRIAGE_SINGLE:$suitableMarried="未婚";break;
    		case B2bPackage::MARRIAGE_DOUBLE:$suitableMarried="已婚";break;
    	}
    	return $suitableMarried;
    }
	
}