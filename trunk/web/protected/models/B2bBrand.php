<?php

/**
 * This is the model class for table "b2b_center".
 *
 * The followings are the available columns in table 'b2b_center':
 * @property string $id
 * @property string $name
 * @property integer $status
 * @property string $description
 * @property string $activkey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property integer $superuser
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $create_at
 * @property string $update_at
 */
class B2bBrand extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bBrand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_brand';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
                    array('name','required'),
                    array('description,icon,tags,createtime,email','safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => '品牌名',
			'status' => 'Status',
			'icon'=>'图标地址',
			'description' => '描述',
			'createtime' => '品牌创建日期',
			'createtime'=>'创办时间',
			'tags'=>'标签',
			'email' => '邮件',
			'create_at' => '记录创建日期',
			'update_at' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->order='t.id desc';
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('createtime',$this->createtime);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>20,
			),
		));
	}

	public function getPagementCenterList($pagesize, $offset){
		$criteria = new CDbCriteria();

		$criteria->limit = $pagesize;
		$criteria->offset = $offset;

		$ret = B2bBrand::model()->findAll($criteria);
		
		return $ret;
	}
	
	public function getTotalCount(){
		$criteria = new CDbCriteria();
		
		$sql = "select count(id) as id from b2b_brand";
		$num = B2bBrand::model()->findBySql($sql);
		
		return $num['id'];
	}
	
	public function getDistinctId(){
		return B2bBrand::model()->findAll(array("select" => "id,name","group" => "id"));
	}
}