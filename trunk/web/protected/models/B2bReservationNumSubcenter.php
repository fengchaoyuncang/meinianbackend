<?php

/**
 * This is the model class for table "b2b_reservation_num_subcenter".
 *
 * The followings are the available columns in table 'b2b_reservation_num_subcenter':
 * @property integer $int
 * @property integer $fk_subcenter
 * @property integer $num
 * @property string $telephone
 */
class B2bReservationNumSubcenter extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bReservationNumSubcenter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_reservation_num_subcenter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('int, fk_subcenter, num', 'numerical', 'integerOnly'=>true),
			array('telephone', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('int, fk_subcenter, num, telephone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'int' => 'Int',
			'fk_subcenter' => 'Fk Subcenter',
			'num' => 'Num',
			'telephone' => 'Telephone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('int',$this->int);
		$criteria->compare('fk_subcenter',$this->fk_subcenter);
		$criteria->compare('num',$this->num);
		$criteria->compare('telephone',$this->telephone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}