<?php

/**
 * This is the model class for table "evaluate_report_cache".
 *
 * The followings are the available columns in table 'evaluate_report_cache':
 * @property integer $id
 * @property integer $uid
 * @property string $no
 * @property string $parent_name
 * @property string $name
 * @property string $value
 * @property string $issue
 * @property string $normal
 * @property string $unit
 * @property string $type
 */
class EvaluateReportCache extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportCache the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_cache';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, no, parent_name, name, value, issue, normal, unit, type', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('no', 'length', 'max'=>50),
			array('parent_name, name, normal, unit', 'length', 'max'=>20),
			array('value', 'length', 'max'=>200),
			array('issue', 'length', 'max'=>10),
			array('type', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, no, parent_name, name, value, issue, normal, unit, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'indicator' => array(self::BELONGS_TO,'Indicator',array('name'=>'name'),"select"=>"indicator.id"),                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'no' => 'No',
			'parent_name' => 'Parent Name',
			'name' => 'Name',
			'value' => 'Value',
			'issue' => 'Issue',
			'normal' => 'Normal',
			'unit' => 'Unit',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('no',$this->no,true);
		$criteria->compare('parent_name',$this->parent_name,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('issue',$this->issue,true);
		$criteria->compare('normal',$this->normal,true);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
     * set data from hash array intent to insert datas to database
     * @param type $data_arr (hash array) e.g. array('name'=>'admin','password'=>'123456')
     */
    public function setData($data_arr,$array=null){
        foreach ($data_arr as $key => $value) {
            $this->$key = $value;
        }
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->$key = $value;
			}
		}
    }
}