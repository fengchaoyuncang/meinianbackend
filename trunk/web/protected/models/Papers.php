<?php

/**
 * This is the model class for table "papers".
 *
 * The followings are the available columns in table 'papers':
 * @property integer $id
 * @property string $title
 * @property string $abstract
 * @property string $content
 * @property integer $type
 * @property integer $related_item_id
 * @property integer $related_indicator_id
 * @property string $symptom
 * @property string $examination
 * @property string $treatment
 * @property string $suggestion
 *
 * The followings are the available model relations:
 * @property PapersIndicator[] $papersIndicators
 */
class Papers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Papers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'info_papers';
	}
	
    public function getPaperTitle($paperId)
    {
   		$criteria=new CDbCriteria;
		$criteria->select="title";
        $criteria->compare("id", $paperId);
        $row = $this->find($criteria);
        return $row ==null ? "" : $row->title;        
    }
    
	public function getPaperList()
	{
		$ret = array();
		$criteria=new CDbCriteria;
		$criteria->select="id, title, type";
		//$criteria->group="type";	
		$rows = $this->findAll($criteria);
		$ret = array();
		for($i = 0; $i < count($rows); $i++)
		{
			$row = $rows[$i];
			$type = $row->type;
			$id = $row->id;
			$title = $row->title;
			$ret[$id]=$title;
		}
		return $ret;
	}

	public function getBasicPapersListWithType()
	{
		$ret = array();
		$criteria=new CDbCriteria;
		$criteria->select="id, title, type,abstract";
		//$criteria->group="type";	
		$rows = $this->findAll($criteria);
		for($i = 0; $i < count($rows); $i++)
		{
			$row = $rows[$i];
			$type = $row->type;
			$id = $row->id;
			$title = $row->title;
                        if($type == 2){
                            $title=$title.":\n";//.
                            $title .= mb_substr($row->abstract, 0, 95, 'utf-8')."...";
                        }elseif ($type == 3){
                        	$title=$title.":\n";//.
                            $title .= mb_substr($row->abstract, 0, 40, 'utf-8')."...";
                        }
			$typeArray = array_key_exists($type, $ret)?$ret[$type]:array();
			//echo json_encode($typeArray);
			array_push($typeArray, array("pid"=>$id, "title"=>$title));
			$ret[$type] = $typeArray;
		}
		//echo json_encode($ret);
		return $ret;
	}
	
	public function getSubscribedPaperList($app_id)
	{
		$ret = array();
		$connection = Yii::app()->db;
        //获取该用户订阅的一级类目,二级类目
        $sub_ids = Subscribe::model()->getSubsribedId($app_id);
 
        $sqlCmd = Yii::app()->db->createCommand();
        $sqlCmd->selectDistinct('id as pid, title');
        $sqlCmd->from('info_papers');
        $sqlCmd->where(array('in', 'related_item_id', $sub_ids));
        $sqlCmd->orWhere(array('in', 'related_indicator_id', $sub_ids));        
        
        $rows = $sqlCmd->queryAll();    
		$type = Constants::SUBSCRIBE_TYPE;
		$typeArray = array();
		for($i = 0; $i < count($rows); $i++)
		{
			$row = $rows[$i];
	     	array_push($typeArray, $row);
		}
		$ret[$type] = $typeArray;
		
        
        return $ret;		
	}
	
	public function getAllTypePaperList($uid)
	{
		$ret=array();
		//
		$basicList = $this->getBasicPapersListWithType();
		foreach ($basicList as $k => $v)
		{
			$ret[$k] = $v;
		}
		//
		$subscribedList = $this->getSubscribedPaperList($uid);
		foreach ($subscribedList as $k => $v)
		{
			$ret[$k] = $v;
		}
		return $ret;
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content', 'required'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('related_item_id', 'numerical', 'integerOnly'=>true),
			array('related_indicator_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('abstract, symptom, examination, treatment, suggestion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, abstract, content, type, related_item_id,symptom,related_indicator_id, examination, treatment, suggestion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'papersIndicators' => array(self::HAS_MANY, 'PapersIndicator', 'paper_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => '标题',
			'abstract' => '摘要',
			'content' => '正文',
			'type' => '类型',
            'related_item_id' => '关联体检项',
            'related_indicator_id' => '关联体检指标',
            'symptom' => '症状',
            'examination' => '检查',
            'treatment' => '治疗',
            'suggestion' => '专家建议',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('abstract',$this->abstract,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('related_item_id',$this->related_item_id);
		$criteria->compare('related_indicator_id',$this->related_indicator_id);
        $criteria->compare('symptom',$this->symptom,true);
        $criteria->compare('examination',$this->examination,true);
        $criteria->compare('treatment',$this->treatment,true);
        $criteria->compare('suggestion',$this->suggestion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}