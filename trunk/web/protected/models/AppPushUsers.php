<?php

/**
 * This is the model class for table "app_push_users".
 *
 * The followings are the available columns in table 'app_push_users':
 * @property integer $mapid
 * @property integer $userid
 * @property string $device_token
 * @property integer $platform
 * @property string $addtime
 */
class AppPushUsers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AppPushUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_push_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, device_token, platform', 'required'),
			array('userid, platform', 'numerical', 'integerOnly'=>true),
			array('device_token', 'length', 'max'=>64),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mapid, userid, device_token, platform, addtime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mapid' => 'Mapid',
			'userid' => 'Userid',
			'device_token' => 'Device Token',
			'platform' => 'Platform',
			'addtime' => 'Addtime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mapid',$this->mapid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('device_token',$this->device_token,true);
		$criteria->compare('platform',$this->platform);
		$criteria->compare('addtime',$this->addtime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}