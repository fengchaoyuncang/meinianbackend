<?php

/**
 * This is the model class for table "b2c_purchase_records".
 *
 * The followings are the available columns in table 'b2c_purchase_records':
 * @property integer $id
 * @property string $uid
 * @property integer $unitprice
 * @property integer $totalprice
 * @property integer $packageid
 * @property string $activecodes
 * @property integer $num
 * @property integer $status
 * @property string $aliid
 * @property string $serialnumber
 * @property string $create_at
 * @property string $update_at
 */
class B2cPurchaseRecords extends CActiveRecord
{
	const NO_PAID=0;  //购买记录未付款
	const HAVE_PAID=1; //购买记录已付款
	public $author_search;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2cPurchaseRecords the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public $realName;
	public $packageName;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2c_purchase_records';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, unitprice, totalprice, packageid, num, status', 'required'),
			array('unitprice, totalprice, packageid, num, status', 'numerical', 'integerOnly'=>true),
			array('uid', 'length', 'max'=>30),
			array('activecodes', 'length', 'max'=>2048),
			array('aliid, serialnumber', 'length', 'max'=>50),
			array('create_at, update_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, unitprice, totalprice, packageid, activecodes, num, status, aliid, serialnumber, create_at, update_at,realName,packageName', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'username'=>array(self::BELONGS_TO,'B2bUser',array('uid'=>'id')),
//			'user'=>array(self::BELONGS_TO,'B2bUser',array('uid'=>'id')),
			'package'=>array(self::BELONGS_TO,'B2bPackage',array('packageid'=>'id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => '用户ID',
			'packageid' => '套餐ID',
			'realName' => '用户姓名',
			'unitprice' => '购买单价(元)',
			'totalprice' => '总价格(元)',
			'packageName' => '套餐名称',
			'activecodes' => '激活码',
			'num' => '数量',
			'status' => '状态',
			'aliid' => '支付宝单号',
			'serialnumber' => '系统内部单号',
			'create_at' => '创建时间',
			'update_at' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{ 
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order='t.id desc';
		$criteria->with=array('username','package');
		
		$criteria->compare('t.id',$this->id);
		
		$criteria->compare('username.realname',$this->realName,true);
		$criteria->compare('unitprice',$this->unitprice);
		$criteria->compare('totalprice',$this->totalprice);
		$criteria->compare('package.name',$this->packageName,true);
		$criteria->compare('activecodes',$this->activecodes,true);
		$criteria->compare('num',$this->num);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('aliid',$this->aliid,true);
		$criteria->compare('serialnumber',$this->serialnumber,true);
		$criteria->compare('t.create_at',$this->create_at,true);
		$criteria->compare('t.update_at',$this->update_at,true);
		
		$ret = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
		  	'pagination' => array(
           		'pageSize' => 20,
       		),
		));

		return $ret;
	}
	

}