<?php

class EvaluateUserDiselement extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateUserDiselement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_user_diselement';
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'coronary_family_history' => 'Coronary Family History',
			'stroke_family_history' => 'Stroke Family History',
			'uid' => 'Uid',
			'name' => 'Name',
			'sex' => 'Sex',
			'age' => 'Age',
			'check_time' => 'Check Time',
			'no' => 'No',
			'card_no' => 'Card No',
			'height' => 'Height',
			'weight' => 'Weight',
			'weightLevel' => 'Weight Level',
			'systolic_blood_pressure' => 'Systolic Blood Pressure',
			'diastolic_blood_pressure' => 'Diastolic Blood Pressure',
			'hypertension_family_history' => 'Hypertension Family History',
			'diabetes_family_history_single' => 'Diabetes Family History Single',
			'diabetes_family_history_double' => 'Diabetes Family History Double',
			'hypertension_history' => 'Hypertension History',
			'coronary_history' => 'Coronary History',
			'diabetes_history' => 'Diabetes History',
			'hypercholesterolemia_history' => 'Hypercholesterolemia History',
			'hyperlipidemia_history' => 'Hyperlipidemia History',
			'smoking' => 'Smoking',
			'smoked' => 'Smoked',
			'sports' => 'Sports',
			'salt_food' => 'Salt Food',
			'drink' => 'Drink',
			'fruit' => 'Fruit',
			'meat' => 'Meat',
			'contraceptive' => 'Contraceptive',
			'hulu' => 'Hulu',
			'stillsit' => 'Stillsit',
			'wc' => 'Wc',
			'blood_triglycerides' => 'Blood Triglycerides',
			'blood_cholesterol' => 'Blood Cholesterol',
			'blood_sugar' => 'Blood Sugar',
			'lack_activity' => 'Lack Activity',
		);
	}
        
}