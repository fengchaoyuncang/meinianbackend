<?php

/**
 * This is the model class for table "b2b_ticket".
 *
 * The followings are the available columns in table 'b2b_ticket':
 * @property integer $id
 * @property integer $codetype
 * @property string $activecode
 * @property integer $userid
 * @property integer $companyid
 * @property integer $groupid
 * @property string $cardno
 * @property integer centerid
 * @property string $realname
 * @property integer $orderstatus
 * @property string $examdate
 * @property string $startdate
 * @property string $enddate
 * @property string $create_at
 * @property string $update_at
 */
class B2bTicket extends CActiveRecord
{
    const UNACTIVATED = 1;
    const YUYUE_PROCESSING = 2;
    const YUYUE_CONFIRM = 3;
    const FINISHED = 4;
    const YUYUE_CANCEL = 5;

    const COMPANYUSER = 1;
    const GENERALUSER = 2;

    const SHOPINGCODERESERVATION = 1;
    const FCODERESERVATION = 2;
    const NOCODERESERVATION = 3;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return B2bTicket the static model class
     */
    public $companyName;
    public $realName;
    public $packageName;
    public $centerName;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('codetype,tstatus,examdate,startdate,enddate', 'safe'),
            //array('id,groupid,identity_no,realname,telephone,tstatus,packageid,centerid,examdate,startdate,enddate,create_at,update_at', 'safe'),
            array('id,codetype,activecode,uid,companyid,groupid,identity_no,realname,telephone,tstatus,packageid,centerid,examdate,startdate,enddate,create_at,update_at,companyName,packageName,centerName', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'b2b_ticket';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            "package" => array(self::BELONGS_TO, 'B2bPackage', array("packageid" => "id")),
            "center" => array(self::BELONGS_TO, 'B2bSubCenter', array("centerid" => "id")),
            "user" => array(self::BELONGS_TO, 'B2bUser', array("uid" => "id")),
            "company" => array(self::BELONGS_TO, 'B2bCompany', array("companyid" => 'id')),
        );
    }

    /**
     * $identity_no 证件号码
     * $realname 真实姓名
     *
     */
    public function relationUserByCardNoName($uid, $identity_no, $realname)
    {
        if (empty($identity_no) || empty($realname)) {
            return false;
        }

        $criteria = new CDbCriteria;
        $criteria->addCondition("identity_no = '$identity_no'");
        $criteria->addCondition("realname = '$realname'");
        $criteria->addCondition("uid = 0");
        $criteria->addCondition("companyid != 0");
        try {
            $ret = $this->updateAll(array("uid" => $uid), $criteria);
        } catch (Exception $exc) {
            echo $exc->getMessage();
            return false;
        }
        return $ret;
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'codetype' => '预约类型',
            'activecode' => '激活码',
            'uid' => '用户ID',
            'companyName' => '公司名称',
            'companyid' => '公司ID',
            'groupid' => '团单ID',
            'identity_no' => '身份证号码',
            'realname' => '真实姓名',
            'telephone' => '电话',
            'tstatus' => '订单状态',
            'packageid' => '套餐ID',
            'packageName' => '套餐名称',
            'centerid' => '体检中心ID',
            'centerName' => '体检中心名称',
            'center.name' => '体检中心名称',
            'examdate' => '预约体检日期',
            'startdate' => '有效期开始日期',
            'enddate' => '有效期截止日期',
            'create_at' => '创建日期',
            'update_at' => '更新日期',
        );
    }

    public function search()
    {
//        var_dump($this);exit;
        $criteria = new CDbCriteria;

        $criteria->with = array('user', 'company', 'package', 'company', 'center');
        $criteria->order = 't.id desc';
        $criteria->compare('t.id', $this->id, false);
        $criteria->compare('codetype', $this->codetype, true);
//            $criteria->addInCondition("codetype", array(B2bTicket::NOCODERESERVATION, B2bTicket::SHOPINGCODERESERVATION), true);
        $criteria->compare('uid', $this->uid);

        $criteria->compare('company.name', $this->companyName, true);
        $criteria->compare('groupid', $this->groupid);
        $criteria->compare('t.identity_no', $this->identity_no, true);
        $criteria->compare('t.realname', $this->realname, true);
        $criteria->compare('t.telephone', $this->telephone);
        $criteria->compare('tstatus', $this->tstatus, true);
        $criteria->compare('package.name', $this->packageName, true);
        $criteria->compare('center.name', $this->centerName, true);
        $criteria->compare('examdate', $this->examdate);
        $criteria->compare('startdate', $this->startdate);
        $criteria->compare('enddate', $this->enddate);
        $criteria->compare('create_at', $this->create_at);
        $criteria->compare('update_at', $this->update_at);


        $ret = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),

        ));
//		foreach ($ret->getData() as $item){
//			$item->tstatus = $this->getTicketStatus($item->tstatus);
//		}
        return $ret;
    }

    public function searchAdmin()
    {
//        var_dump($this);exit;
        $criteria = new CDbCriteria;

        $criteria->with = array('user', 'company', 'package', 'company', 'center');
        $criteria->order = 't.id desc';
        $criteria->compare('t.id', $this->id, false);
//            $criteria->compare('codetype', $this->codetype, true);
        $criteria->addInCondition("codetype", array(B2bTicket::NOCODERESERVATION, B2bTicket::SHOPINGCODERESERVATION), true);
        $criteria->compare('uid', $this->uid);
        $criteria->compare('company.name', $this->companyName, true);
        $criteria->compare('groupid', $this->groupid);
        $criteria->compare('t.identity_no', $this->identity_no, true);
        $criteria->compare('t.realname', $this->realname, true);
        $criteria->compare('t.telephone', $this->telephone);
        $criteria->compare('tstatus', $this->tstatus, true);
        $criteria->compare('package.name', $this->packageName, true);
        $criteria->compare('center.name', $this->centerName, true);
        $criteria->compare('examdate', $this->examdate);
        $criteria->compare('startdate', $this->startdate);
        $criteria->compare('enddate', $this->enddate);
        $criteria->compare('create_at', $this->create_at);
        $criteria->compare('update_at', $this->update_at);


        $ret = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),

        ));
//		foreach ($ret->getData() as $item){
//			$item->tstatus = $this->getTicketStatus($item->tstatus);
//		}
        return $ret;
    }


    /*
     * @param none
     * @return 套餐ID和套餐名称组成的数组
     */
    public function getPackages()
    {
        $rets = B2bPackage::model()->findAll();
        $packages = array();
        $packages[""] = "";
        if ($rets !== null) {
            foreach ($rets as $value) {
                $packages[$value->name] = $value->name;
            }
        }
        return $packages;
    }

    /*
     * 根据companyid获取名称
     * @param companyid
     * @return companyName
     */
    public function getCompanyName($id)
    {
        $rets = B2bTicket::model()->with(array('company', 'center'))->findByPk($id);
        return $rets;

    }

    /*
     * 转换订单的状态
     * @param $tstatus
     * return 状态描述
     */
    static function getStatusDesc($tstatus)
    {

        switch ($tstatus) {
            case $tstatus == B2bTicket::UNACTIVATED:
                $tstatus = "未激活";
                break;
            case $tstatus == B2bTicket::YUYUE_PROCESSING:
                $tstatus = "预约处理中，可取消";
                break;
            case $tstatus == B2bTicket::YUYUE_CONFIRM:
                $tstatus = "预约确认，电话取消";
                break;
            case $tstatus == B2bTicket::FINISHED:
                $tstatus = "体检完成";
                break;
            case $tstatus == B2bTicket::YUYUE_CANCEL:
                $tstatus = "体检完成";
                break;
        }
        return $tstatus;
    }

    static function getCodetypeDesc($codetype)
    {
        switch ($codetype) {
            case B2bTicket::SHOPINGCODERESERVATION:
                $codetype = "商城购买预约";
                break;
            case B2bTicket::FCODERESERVATION:
                $codetype = "F码预约";
                break;
            case B2bTicket::NOCODERESERVATION:
                $codetype = "直接预约";
                break;
        }
        return $codetype;
    }

    static function getActivecodeDesc($activecode)
    {
        if (strstr($activecode, " ")) {
            $activecode = "无激活码";
        }
        return $activecode;
    }

    public function findexceptfcode()
    {
        $criteria = new CDbCriteria();
        $criteria->addNotInCondition('codetype', array(B2bTicket::NOCODERESERVATION, B2bTicket::SHOPINGCODERESERVATION));

        $ticket_obj_arr = B2bTicket::model()->findAll($criteria);
        return $ticket_obj_arr;
    }
}