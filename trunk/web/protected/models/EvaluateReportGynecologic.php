<?php

/**
 * This is the model class for table "evaluate_report_gynecologic".
 *
 * The followings are the available columns in table 'evaluate_report_gynecologic':
 * @property integer $id
 * @property integer $uid
 * @property string $vulva
 * @property string $vagina
 * @property string $cervix
 * @property string $uterus
 * @property string $attachment
 * @property string $gynecologic_other
 * @property string $vaginal_cleanliness
 * @property string $rosary_like_fungus
 * @property string $trichomoniasis
 * @property string $leucorrhea
 * @property string $cervical_smears
 * @property string $preliminary_opinion
 */
class EvaluateReportGynecologic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EvaluateReportGynecologic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluate_report_gynecologic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, vulva, vagina, cervix, uterus, attachment, gynecologic_other, vaginal_cleanliness, rosary_like_fungus, trichomoniasis, leucorrhea, cervical_smears, preliminary_opinion', 'required'),
			array('uid', 'numerical', 'integerOnly'=>true),
			array('vulva, vagina, cervix, uterus, attachment, vaginal_cleanliness, cervical_smears, preliminary_opinion', 'length', 'max'=>200),
			array('gynecologic_other, rosary_like_fungus, trichomoniasis, leucorrhea', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, vulva, vagina, cervix, uterus, attachment, gynecologic_other, vaginal_cleanliness, rosary_like_fungus, trichomoniasis, leucorrhea, cervical_smears, preliminary_opinion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'vulva' => 'Vulva',
			'vagina' => 'Vagina',
			'cervix' => 'Cervix',
			'uterus' => 'Uterus',
			'attachment' => 'Attachment',
			'gynecologic_other' => 'Gynecologic Other',
			'vaginal_cleanliness' => 'Vaginal Cleanliness',
			'rosary_like_fungus' => 'Rosary Like Fungus',
			'trichomoniasis' => 'Trichomoniasis',
			'leucorrhea' => 'Leucorrhea',
			'cervical_smears' => 'Cervical Smears',
			'preliminary_opinion' => 'Preliminary Opinion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('vulva',$this->vulva,true);
		$criteria->compare('vagina',$this->vagina,true);
		$criteria->compare('cervix',$this->cervix,true);
		$criteria->compare('uterus',$this->uterus,true);
		$criteria->compare('attachment',$this->attachment,true);
		$criteria->compare('gynecologic_other',$this->gynecologic_other,true);
		$criteria->compare('vaginal_cleanliness',$this->vaginal_cleanliness,true);
		$criteria->compare('rosary_like_fungus',$this->rosary_like_fungus,true);
		$criteria->compare('trichomoniasis',$this->trichomoniasis,true);
		$criteria->compare('leucorrhea',$this->leucorrhea,true);
		$criteria->compare('cervical_smears',$this->cervical_smears,true);
		$criteria->compare('preliminary_opinion',$this->preliminary_opinion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}