<?php

/**
 * This is the model class for table "b2b_push_queue".
 *
 * The followings are the available columns in table 'b2b_push_queue':
 * @property integer $msgid
 * @property integer $userid
 * @property string $message
 * @property string $time_queued
 * @property string $time_sent
 */
class B2bPushQueue extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2bPushQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'b2b_push_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, message, time_queued', 'required'),
			array('userid', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>256),
			array('time_sent', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('msgid, userid, message, time_queued, time_sent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'msgid' => 'Msgid',
			'userid' => 'Userid',
			'message' => 'Message',
			'time_queued' => 'Time Queued',
			'time_sent' => 'Time Sent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('msgid',$this->msgid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('time_queued',$this->time_queued,true);
		$criteria->compare('time_sent',$this->time_sent,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}