<?php
 // Using Autoload all classes are loaded on-demand

class IosHelperNew
{
    private $push;

    public function begin()
    {
        // Adjust to your timezone
        date_default_timezone_set('Asia/Shanghai');

        // Report all PHP errors
        error_reporting(-1);

        // Instanciate a new ApnsPHP_Push object
        $this->push = new ApnsPHP_Push(
            ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
            dirname(__FILE__). '/../data/production_ck.pem',
            '123123'
        );
        
        //$this->push = new ApnsPHP_Push(
        //    ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
        //    dirname(__FILE__). '/../data/ck.pem',
        //    '123123'
        //);

        // Connect to the Apple Push Notification Service
        $this->push->connect();    
    }

    public function end()
    {
        // Send all messages in the message queue
        $this->push->send();

        // Disconnect from the Apple Push Notification Service
        $this->push->disconnect();
       // Examine the error message container
        $aErrorQueue = $this->push->getErrors();
        if (!empty($aErrorQueue)) 
        {
            var_dump($aErrorQueue);
        }
        return $aErrorQueue;    
    }
    
    
    public function doPush($cid, $title, $pid)
    {
        // Instantiate a new Message with a single recipient
        $message = new ApnsPHP_Message($cid);

        // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
        // over a ApnsPHP_Message object retrieved with the getErrors() message.
        $message->setCustomIdentifier("Message-Badge-3");

        // Set badge icon to "0"
        $message->setBadge(0);

        // Set a simple welcome text
        $message->setText($title);

        // Play the default sound
        $message->setSound();

        // Set a custom property
        $message->setCustomProperty('title', $title);

        // Set another custom property
        $message->setCustomProperty('pid', $pid);

        // Set the expiry value to 30 seconds
        $message->setExpiry(30);

        // Add the message to the message queue
        $this->push->add($message);
  }
  
}
