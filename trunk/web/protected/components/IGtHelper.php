<?php
require_once(dirname(__FILE__). '/' . 'IGt.Push.php');

//define('APPKEY','KOhbnIFb859ux7KUhosZU7');
//define('APPID','ErHekuMMWN6XrCxz9JT866');
//define('MASTERSECRET','efPjDynZ7P6IB3TSlF7Kf4');
//define('HOST','http://192.168.10.61:8006/apiex.htm');
//define('HOST','http://sdk.open.api.igexin.com/apiex.htm');

/*singleton class*/
class IGtHelper
{
    private $HOST;
    private $APPID;
    private $MASTERSECRET;
    private $APPKEY;
    private static $_phyexam_common ;

    private function __construct($host, $appid, $mastersecrete, $appkey)
    {
        $this->HOST = $host;
        $this->APPID = $appid;
        $this->MASTERSECRET = $mastersecrete;
        $this->APPKEY = $appkey;        
    }
    
    public static function getPhyexamCommon()
    {
        if(!(self::$_phyexam_common instanceof self))
        {
            self::$_phyexam_common = new self('http://sdk.open.api.igexin.com/apiex.htm','yDeXEwARwW9Dk9jbDi5or9', 'D4x7SeqQTZ6VQvnbj6dek8', 'OLRx0PtKYd8KtSkLJ3aWe3');
        }
        return self::$_phyexam_common;
    }
    
    public function pushMessageToSingleNote($trs_content, $title, $note_content, $clientId)
    {
        $igt = new IGeTui($this->HOST,$this->APPKEY,$this->MASTERSECRET);
        //消息类型 : 状态栏通知 点击通知启动应用
        $template =  new IGtNotificationTemplate(); 
        $template->set_appId($this->APPID);//应用appid
        $template->set_appkey($this->APPKEY);//应用appkey
        $template->set_transmissionType(2);//透传消息类型
        $template->set_transmissionContent($trs_content);//透传内容
        $template->set_title($title);//通知栏标题
        $template->set_text($note_content);//通知栏内容
        $template->set_logo("http://www.igetui.com/logo.png");//通知栏logo
        $template->set_isRing(true);//是否响铃
        $template->set_isVibrate(true);//是否震动
        $template->set_isClearable(true);//通知栏是否可清除
        //个推信息体
        $message = new IGtSingleMessage();
        $message->set_isOffline(true);//是否离线
        $message->set_offlineExpireTime(3600*12);//离线时间
        $message->set_data($template);//设置推送消息类型
        //接收方
        $target = new IGtTarget();
        $target->set_appId($this->APPID);
        $target->set_clientId($clientId);
        
        //推送
        $rep = $igt->pushMessageToSingle($message,$target);
        //var_dump($rep);
        return $rep;
    }
}
