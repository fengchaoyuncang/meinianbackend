<?php

class AppInterfaceHelper
{
	/*
	*	@paramter: status the result satus, $data array that contain all the data to return to app
	*	@return array that can be used to return to app as a json
	**/
	public static function createArrayResult($status, $data)
	{
		$ret = array('status'=>$status, 'info'=>$data);
		return $ret;
	}
}




