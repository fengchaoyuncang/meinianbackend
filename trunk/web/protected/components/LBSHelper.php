<?php

class LBSHelper
{
	/*
	*  功能：返回以某个lat/lon为正方形的中心，radius（单位为米）为边长的正方形的下对下对角和上对角。
	*	@参数: 当前的lat/lon, 半径radius
	*	@正方形的下对角lat/lon, 上对角lat/lon
	**/
	public static function getAround($gps_lat, $gps_lon, $radius)
	{
		$PI = 3.14159265;
		$latitude = $gps_lat;
		$longitude = $gps_lon;
		$degree = (24901*1609)/360.0;
		$raidusMile = $radius;
		$dpmLat = 1/$degree;
		$radiusLat = $dpmLat*$raidusMile;
		$minLat = $latitude - $radiusLat;
		$maxLat = $latitude + $radiusLat;
		$mpdLng = $degree*cos($latitude * ($PI/180));
		$dpmLng = 1 / $mpdLng;
		$radiusLng = $dpmLng*$raidusMile;
		$minLon = $longitude - $radiusLng;
		$maxLon = $longitude + $radiusLng;		
		return array("minLat"=>$minLat, "minLon"=>$minLon, "maxLat"=>$maxLat, "maxLon"=>$maxLon);
	}
}
