<?php

class PushCenter
{
	//@var $pushFormModel	
	public function doPush($pushFormModel)
	{
		//第一步，找到所有需要推送的用户，区分android和ios
        //获取与一级相关的所有二级的类目
        $push_ids = array();
        
        if($pushFormModel->item_id != -1)
        {
            $push_ids = Indicator::model()->getSubIndicatorOfItem($pushFormModel->item_id);
            array_push($push_ids, $pushFormModel->item_id);
        }
        if($pushFormModel->indicator_id != -1)
        {
            array_push($push_ids, $pushFormModel->indicator_id);
        }
        
        $users = Subscribe::model()->getSubsciber($push_ids);
        
		//$users1 = Subscribe::model()->getSubsciber($pushFormModel->item_id);
		//$users2 = Subscribe::model()->getSubsciber($pushFormModel->indicator_id);
		//$users = array_merge($users1, $users2); 
        //$users = $push_ids;
        
        //var_dump($users);
        
        echo "users: ", json_encode($users), "<br/>";
		
		//第二步，区分出ios和android用户
		$androidUsers = preg_grep("/^android.*$/", $users);
		$iosUsers = preg_grep("/^ios.*$/", $users);
        $userPushCid = UserPushCid::model();		
        
        //$trs_content, $title, $note_content, $clientId
        $title="掌上体检最新资讯";
        $paperId = $pushFormModel->paper_id;
        $note_content = Papers::model()->getPaperTitle($paperId);
        //透串内容，pid:title
        $trs_content = $paperId.":".$note_content;
        
        $androidSucCnt = 0;
        $androidFailCnt = 0;
        $isoSucCnt = 0;
        $isoFailCnt = 0;

		//第二步，推送 android
        $igtHelper = IGtHelper::getPhyexamCommon();
        if(strcmp($pushFormModel->android, "1") == 0)
		{
            foreach ($androidUsers as $user)
            {
                $cid = trim($userPushCid->getCid($user));
                if(strlen($cid) > 0)
                {
                    echo "push to android user ", $user, ", cid is ", $cid, "<br/>"; 
                    $ret = $igtHelper->pushMessageToSingleNote($trs_content, $title, $note_content, $cid);
                    if($ret['result']=='ok')
                    {
                        $androidSucCnt++;
                    }
                    else
                    {
                        $androidFailCnt++;                        
                    }
                }
            }
            echo "android:  suc cnt ",$androidSucCnt, ", fail cnt ", $androidFailCnt, "<br/>"; 
		}
		
		//第三步, 推送ios
        //$iosHelper = new IosHelper();
        $iosHelper = new IosHelperNew();
        $iosHelper->begin();    
		if(strcmp($pushFormModel->ios, "1") == 0)
		{
			foreach ($iosUsers as $user)
            {
                $cid = trim($userPushCid->getCid($user));
                if(strlen($cid) > 0)
                {
                    echo "push to ios user ", $user, ", cid is ", $cid, "<br/>";
                    $iosHelper->doPush($cid, $title, $paperId);
                   // $ret = $iosHelper->pushToSingle($cid, $title, $paperId);
                   // if($ret=="ok")
                   // {
                   //     $isoSucCnt++;
                   // }
                   // else
                   // {
                   //     $isoFailCnt++;    
                   // }
                }
            }
           // echo "ios:  suc cnt ",$isoSucCnt, ", fail cnt ", $isoFailCnt, "<br/>";
            $iosHelper->end();
		}		
		
		//第四步，返回推送状态
        echo "push over";
	}	
}