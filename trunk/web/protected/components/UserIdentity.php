<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    
    private $_centerId;
    
    private $_companyId;
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    /**
     * 
     * 普通用户验证
     */
    public function authenticate() {
        $user = User::model()->findByAttributes(array('username' => $this->username));
        if ($user === null) { // No user found!
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($user->password !== $this->password) { //Invalid password!
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else { // Okay!
            $this->errorCode = self::ERROR_NONE;
            // Store the role in a session:
            //$this->setState('role', $user->role);
//            $this->setState('centerId', $user->center_id);
//            Yii::app()->session['username'] = $this->username;
            $this->_id = $user->id;
            $this->_centerId = $user->center_id;
            //	Yii::app()->session['password'] = $password;
        }
        return !$this->errorCode;
    }
    /**
     * 企业管理员账户验证
     */
    public function company_admin_authenticate() {
        $user = AdminUser::model()->findByAttributes(array('username' => $this->username));
        if ($user === null||$user->company_id == 0) { // No user found!或者非企业管理员账户
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($user->password !== ($this->password)) { //Invalid password!
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else { // Okay!
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $user->id;
            //企业管理员登陆，保存下公司ID
            $this->_companyId = $user->company_id;
        }
        return !$this->errorCode;
    }
    
    /**
     * 体检中心管理员账户验证
     */
    public function center_admin_authenticate(){
        $user = AdminUser::model()->findByAttributes(array('username' => $this->username));
        if ($user === null||$user->center_id == 0) { // No user found!或者非体检中心管理员账户
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($user->password !== ($this->password)) { //Invalid password!
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else { // Okay!
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $user->id;
            //体检中心账户，保存下体检中心ID
            $this->_centerId = $user->center_id;
        }
        return $this->errorCode;
    }
    
    /**
     * 渠道管理员账户验证
     */
    public function channel_admin_authenticate(){
        $user = AdminUser::model()->findByAttributes(array('username' => $this->username));
        if ($user === null||$user->center_id != 0||$user->company_id != 0) { 
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($user->password !== ($this->password)) { //Invalid password!
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else { // Okay!
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $user->id;
        }
        return $this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
    public function getCenterId(){
        return $this->_centerId;
    }
    public function getCompanyId(){
        return $this->_companyId;
    }
    public  function setId($value){
        $this->_id = $value;
    }
}