<?php
class CObject
{
    private $_oj = null;
    function __construct($ar) {
        $this->_oj = $ar;
    }
    public function __get($name) {
        return isset($this->_oj[$name])?$this->_oj[$name]:null;
    }
    public function __set($name, $value) {
        $this->_oj[$name] = $value;
    }
}