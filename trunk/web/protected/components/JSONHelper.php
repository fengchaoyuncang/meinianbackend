<?php

class JSONHelper {
    /**
     *  retInfo:根据状态json化返回值
     *  @param status 执行的状态
     * 			info 返回的信息
     *  @return json字符串;
     */
    
    const MAN = 1;
    const WOMAN = 2;
    
    const ERROR = 1;
    const SUCCESS =0;
    //下面都是错误码
    const TYPE_JSON = 10;
    const ERROR_ILLEGAL_REQUEST = 400; //非法请求
    const ILLEGAL_TELEPHONE = 401; //手机号码非法
    const REPEAT_TELEPHONE = 402; //手机号码重复
    const ILLEGAL_EMAIL = 403; //邮箱非法
    const REPEAT_EMAIL = 404; //EMAIL重复
    const ILLEGAL_PASSWORD = 405; //密码格式错误
    const ILLEGAL_IDENTITY_NO = 406; //身份证号码不合法
    const ILLEGAL_LOGIN = 407; //登录用户名或者密码不正确
    
    const ERROR_ILLEGAL_UID = 408; //没有获取到uid
    const ERROR_USER_EXISTS = 409; //用户不存在
    const ERROR_RECORDS_EXISTS = 410; //没有可预约记录
    const ERROR_DATE_FORMAT = 411; //提交的时间格式不正确
    const ERROR_DATE = 412; //预约时间不正确
    const ERROR_CENTER_EXISTS = 413; //体检中心不存在

    const ERROR_SERVICE = 414; //数据库异常
    
    const ERROR_EMPTY_ORDER= 415; //用户订单为空
    const ERROR_STATUS= 416; //不可预约
    
    const ERROR_CENTER_SEARCH= 417; //获取体检中心错误
    const REPEAT_IDENTITY= 418; //身份证号码重复
    
    const ERROR_EMPTY_EMAIL= 419; //找到不到此邮箱
    const ERROR_FAILURE_EMAIL= 420; //发送邮件失败
    const ERROR_EXCEPT_EMAIL= 421; //发送邮件异常
    
    const ERROR_NOEXIST_ORDER = 425;//不存在订单
    const ERROR_ILLEGAL_ORDER = 426;//非法订单号，订单和用户不一致
    const ERROR_NOALLOW_ORDER = 427;//不允许取消订单
    
    const ERROR_NOEXIST_REPORT = 430;//电子版体检报告不存在
    
    const ERROR_ILLEGAL_SUB_CENTER_ID= 431; //sub_center_id不能为空
    const ERROR_ILLEGAL_STATUS= 432; //订单状态不能为空
    const ERROR_ILLEGAL_ORDER_ID= 433; //订单id为空
    
    const ERROR_ILLEGAL_DATA = 434;     //数据不存在
    const ERROR_NOTEXIST_ORDER = 435;      //没有订单信息
    const ERROR_NOTEXIST_REPORT = 436;      //没有体检信息
    const ERROR_PACKAGE_NOTWITHCENTERS = 437;      //体检套餐未关联体检中心
    const ERROR_PACKAGE = 438;      //体检套餐未关联体检中心
    
    const ERROR_USERINFO_INCOMPLETE = 440;// 用户信息不全
    const ERROR_ILLEGAL_TICKET = 441;// 此ticket不属于此人~攻击啊~或者bug
    const ERROR_TICKET_EXPIRE = 442;//ticket过期
    const ERROR_TICKET_COMPANY = 443;//ticket标示是企业用户，但是却没有导入身份证和姓名
    const ERROR_TICKET_INFO = 444;//ticket标示是企业用户，但是却没有导入身份证和姓名
    const ERROR_ILLEGAL_ACTIVECODE = 445;//激活码错误
    const ERROR_ACTIVECODE_USED = 446;//激活码错误
    const ERROR_NOEXIST_TICKET= 447;//找不到ticket
    const ERROR_TICKET_BIND= 448;//ticket已经绑定
    
    const ERROR_SMS_SERVICE = 460;//短信服务器异常
    const ERROR_SMS_FRUQUENCY = 461;//短信服务请求频繁
    const ERROR_SMS_VERIFY = 462;//校验失败
    
    const ERROR_EVALUATE_NOINFO = 600;//用户缺少问卷
    const ERROR_DISEASE_ELES = 601;//找不到疾病关联的诱因
    
    const ERROR_NOEXIST_SPORT = 701;//找不到疾病关联的诱因
    const ERROR_NO_SUBCENTER = 702;//没有这个体检中心
    
    
    
    
    static function retInfo($status, $info = null, $type = self::TYPE_JSON) {
        if ($type == self::TYPE_JSON) {
            header('Content-Type: application/json');
        }
        $ret = array();
        if ($status) {
            $ret['status'] = JSONHelper::SUCCESS;
        } else {
            $ret['status'] = JSONHelper::ERROR;
        }
        $ret['data'] = $info;
        return json_encode($ret);
    }
    
    static function echoJSON($status, $info = null, $type = self::TYPE_JSON) {
        echo self::retInfo($status, $info, $type);
        exit();
    }
    static function echoErrorCode($status, $info = null, $type = self::TYPE_JSON) {
        echo self::retInfo($status, self::getErrorStringByCode($info), $type);
        exit();
    }
    static function getErrorStringByCode($errno){
        switch ($errno){
            case 400:
                return "非法请求";
            case 401:
                return "手机号码非法";
            case 402:
                return "手机号码重复";
            case 403:
                return "邮箱非法";
            case 404:
                return "邮箱地址重复";
            case 405:
                return "密码格式错误";
            case 406:
                return "身份证号码不合法";
            case 407:
                return "登录用户名或者密码不正确";
            case 408:
                return "非法请求";
            case 409:
                return "非法请求";
            case 410:
                return "非法请求";
            case 411:
                return "非法请求";
            case 412:
                return "非法请求";
            case 413:
                return "非法请求";
            case 414:
                return "服务异常";
            case 415:
                return "非法请求";
            case 416:
                return "非法请求";
            case 417:
                return "非法请求";
            case 418:
                return "身份证号码重复";
            case 419:
                return "非法请求";
            case 420:
                return "非法请求";
            case 421:
                return "非法请求";
            case 422:
                return "非法请求";
            case 423:
                return "非法请求";
            default :
                return $errno;
        }
    }
}