<?php

class ToolsCommand extends CConsoleCommand {

    public function actionIndex($name,$tags) {
        $n = explode(";", $name);
        foreach($n as $it)
        {
            $model = new EvaluateFood();
            $model->name = $it;
            $model->tags = $tags;
            $model->create_at = new CDbExpression('NOW()');
            $model->save();
        }
    }
}
