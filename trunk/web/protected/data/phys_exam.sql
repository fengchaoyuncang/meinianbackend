﻿# MySQL-Front 5.0  (Build 1.0)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: 106.187.54.170    Database: phys_exam
# ------------------------------------------------------
# Server version 5.1.62-0ubuntu0.11.10.1-log

DROP DATABASE IF EXISTS `phys_exam`;
CREATE DATABASE `phys_exam` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `phys_exam`;

#
# Table structure for table app_user
#

CREATE TABLE `app_user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT '' COMMENT '用户id',
  `first_login_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '首次访问服务器时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table app_version
#

CREATE TABLE `app_version` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `version_num` varchar(255) NOT NULL DEFAULT '""' COMMENT '版本号',
  `platform` varchar(11) NOT NULL DEFAULT '"IOS"' COMMENT '平台',
  `version_name` varchar(255) NOT NULL DEFAULT '"最新版本"' COMMENT '新版本名字',
  `download_url` varchar(1024) NOT NULL DEFAULT '""' COMMENT '下载连接',
  `available_date` date NOT NULL DEFAULT '0000-00-00' COMMENT '上线时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `uniq` (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app版本控制表';
/*!40000 ALTER TABLE `app_version` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table exam_item
#

CREATE TABLE `exam_item` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) DEFAULT '' COMMENT '项目名',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '项目类型0为常规1为非常规',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `uniq_index` (`item_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='体检项目表';
INSERT INTO `exam_item` VALUES (1,'血压',0);
INSERT INTO `exam_item` VALUES (2,'血常规',0);
INSERT INTO `exam_item` VALUES (3,'微量元素',1);
/*!40000 ALTER TABLE `exam_item` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table indicator_spec
#

CREATE TABLE `indicator_spec` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `indicator_id` int(11) NOT NULL DEFAULT '0' COMMENT '指标id',
  `over_view` longtext NOT NULL COMMENT '概述',
  `under_reason` longtext NOT NULL COMMENT '偏低成因',
  `over_reason` longtext NOT NULL COMMENT '偏高成因',
  `treatment` longtext NOT NULL COMMENT '治疗保健建议',
  `baidu_baike` longtext NOT NULL COMMENT '百度百科知识',
  `suggestion` longtext NOT NULL COMMENT '专家建议',
  PRIMARY KEY (`Id`),
  KEY `indicator_foreign_key` (`indicator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='定量指标详细信息表';
INSERT INTO `indicator_spec` VALUES (1,1,'','','','','','如果您偶尔一次发现血压升高、不能确诊为高血压。建议非同日测量血压，多次达到或超过140/90mmHg，说明你已经有高血压了。高血压病是冠心病最主要的危险因素之一。防治高血压，对预防冠心病及减少冠心病死亡率具有重要意义。具体方法如下：\r\n1、定期测量血压是早期发现症状性高血压的有效方法。\r\n2、限盐。许多研究证明摄盐量与高血压发生率成正相关。终生低钠的人群，几乎不发生高血压。世界卫生组织规定，每人每天的食盐摄入量为３-５g，这对预防高血压有良好的作用。有高血压家族史的人，最好每天只吃２-３g盐。\r\n3、控制体重。超重给机体带来许多副作用。胖人高血压的患病率是体重正常者的２-６倍，而降低体重则可使血压正常化。降低体重还可明显减少降压药剂量。控制高糖．高脂食物，少食多餐，积极参加体育锻炼是减肥的重要方法。\r\n4、戒烟。吸烟可以使血压升高，心跳加快。尼古丁作用于血管运动中枢，同时还使肾上腺素分泌增加，引起小动脉收缩。长期在量吸烟，可使小动脉持续收缩，久之动脉壁变性、硬化、管腔变窄，形成持久性高血压。\r\n5、积极参加体育锻炼，放松紧张情绪。缺乏体育锻炼易使脂肪堆积，体重增加，血压升高。体育锻炼还可使紧张的精神放松，慢跑、散步、游泳等均对稳定血压有很大好处。');
/*!40000 ALTER TABLE `indicator_spec` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table item_indicator
#

CREATE TABLE `item_indicator` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL DEFAULT '0' COMMENT '体检项id',
  `indicator_name` varchar(255) NOT NULL DEFAULT '' COMMENT '指标名字',
  `is_quantitative` binary(1) NOT NULL DEFAULT '1' COMMENT '是否为定量指标，默认为是',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `uniq` (`item_id`,`indicator_name`),
  KEY `name_index` (`indicator_name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='项目指标表';
INSERT INTO `item_indicator` VALUES (1,1,'收缩压','1');
INSERT INTO `item_indicator` VALUES (2,1,'舒张压','1');
INSERT INTO `item_indicator` VALUES (3,2,'白细胞计数','1');
INSERT INTO `item_indicator` VALUES (4,2,'中性粒细胞百分率','1');
INSERT INTO `item_indicator` VALUES (5,2,'淋巴细胞百分率','1');
INSERT INTO `item_indicator` VALUES (6,2,'红细胞计数','1');
INSERT INTO `item_indicator` VALUES (7,2,'血红蛋白','1');
INSERT INTO `item_indicator` VALUES (8,2,'血小板计数','1');
INSERT INTO `item_indicator` VALUES (9,3,'铁测定','1');
INSERT INTO `item_indicator` VALUES (10,3,'钙测定','1');
INSERT INTO `item_indicator` VALUES (11,3,'镁测定','1');
INSERT INTO `item_indicator` VALUES (12,3,'锌测定','1');
INSERT INTO `item_indicator` VALUES (13,3,'铜测定','1');
/*!40000 ALTER TABLE `item_indicator` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table papers
#

CREATE TABLE `papers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `abstract` text COMMENT '摘要',
  `related_indicator_list` text NOT NULL COMMENT '关键字相关体检指标',
  `content` longtext NOT NULL COMMENT '正文内容',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='咨询文章表';
/*!40000 ALTER TABLE `papers` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table phys_exam_center
#

CREATE TABLE `phys_exam_center` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) NOT NULL DEFAULT '""' COMMENT '体检中心品牌名',
  `shop_name` varchar(255) NOT NULL DEFAULT '""' COMMENT '体检中心门店名',
  `prov` varchar(255) DEFAULT NULL COMMENT '所在省',
  `city` varchar(255) NOT NULL DEFAULT '""' COMMENT '市',
  `address_in_city` varchar(255) NOT NULL DEFAULT '""' COMMENT '室内的具体地址',
  `phone` varchar(255) DEFAULT '""' COMMENT '电话',
  `business_hour` varchar(255) DEFAULT NULL COMMENT '营业时间',
  `lat` double NOT NULL DEFAULT '0' COMMENT '维度',
  `lon` double NOT NULL DEFAULT '0' COMMENT '经度',
  PRIMARY KEY (`Id`),
  KEY `prov` (`brand_name`),
  KEY `city` (`city`),
  KEY `phone` (`phone`),
  KEY `lat` (`lat`),
  KEY `lon` (`lon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='体检中心表';
/*!40000 ALTER TABLE `phys_exam_center` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table quantitative_indicator_value
#

CREATE TABLE `quantitative_indicator_value` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `indicator_id` int(11) DEFAULT NULL COMMENT '指标id',
  `range_min_male` double DEFAULT NULL COMMENT '男性（默认）区间下限，闭区间',
  `range_max_male` double DEFAULT NULL COMMENT '男性（默认）区间上限，闭区间',
  `range_name` varchar(1024) DEFAULT NULL COMMENT '区间实际有意义的名称',
  `range_min_female` double DEFAULT NULL COMMENT '女性区间下限',
  `range_max_female` double DEFAULT NULL COMMENT '女性区间上限',
  PRIMARY KEY (`Id`),
  KEY `NewIndex` (`indicator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='定量指标值的参照表';
INSERT INTO `quantitative_indicator_value` VALUES (1,1,90,120,'理想血压',90,120);
INSERT INTO `quantitative_indicator_value` VALUES (2,1,90,130,'正常血压',90,130);
INSERT INTO `quantitative_indicator_value` VALUES (3,1,130,139,'正常高值',130,139);
INSERT INTO `quantitative_indicator_value` VALUES (4,1,140,NULL,'高血压',140,NULL);
INSERT INTO `quantitative_indicator_value` VALUES (5,2,60,80,'理想血压',60,80);
INSERT INTO `quantitative_indicator_value` VALUES (6,2,60,85,'正常血压',60,85);
INSERT INTO `quantitative_indicator_value` VALUES (7,2,85,89,'正常高值',85,89);
INSERT INTO `quantitative_indicator_value` VALUES (8,2,90,NULL,'高血压',90,NULL);
/*!40000 ALTER TABLE `quantitative_indicator_value` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table subsribe
#

CREATE TABLE `subsribe` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL DEFAULT '' COMMENT '用户id',
  `indicator_id` varchar(255) NOT NULL DEFAULT '' COMMENT '订阅的体检指标项id',
  PRIMARY KEY (`Id`),
  KEY `uid` (`user_id`),
  KEY `iid` (`indicator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订阅表';
/*!40000 ALTER TABLE `subsribe` ENABLE KEYS */;
UNLOCK TABLES;

#
# Table structure for table web_user
#

CREATE TABLE `web_user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `passwd` varchar(255) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `uniq` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台管理人员';
/*!40000 ALTER TABLE `web_user` ENABLE KEYS */;
UNLOCK TABLES;

#
#  Foreign keys for table indicator_spec
#

ALTER TABLE `indicator_spec`
ADD CONSTRAINT `indicator_foreign_key` FOREIGN KEY (`indicator_id`) REFERENCES `item_indicator` (`Id`);

#
#  Foreign keys for table item_indicator
#

ALTER TABLE `item_indicator`
ADD CONSTRAINT `item_foreigmn_key` FOREIGN KEY (`item_id`) REFERENCES `exam_item` (`Id`);

#
#  Foreign keys for table quantitative_indicator_value
#

ALTER TABLE `quantitative_indicator_value`
ADD CONSTRAINT `fk_indicator_id_value` FOREIGN KEY (`indicator_id`) REFERENCES `item_indicator` (`Id`);

#
#  Foreign keys for table subsribe
#

ALTER TABLE `subsribe`
ADD CONSTRAINT `fk_userid` FOREIGN KEY (`user_id`) REFERENCES `app_user` (`user_id`);


/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
