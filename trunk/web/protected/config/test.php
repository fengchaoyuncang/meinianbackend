<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
            
            /*
            'db'=>array(
                'connectionString' => 'mysql:host=1010.am;dbname=phyexam',
                'emulatePrepare' => true,
                'username' => 'phyexam',
                'password' => '98d9cf7fd2bbdd7357342859760fbb09',
                'charset' => 'utf8',
            ),
            */ 
		),

        'modules'=>array(
            'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'123456',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'=>array('127.0.0.1','::1','*.*.*.*'),
            ),
        ),
	)
);
