<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');
$host = $_SERVER["HTTP_HOST"];
$ret = explode(".", $host);
$channel = count($ret) > 2 ? $ret[0] : null;
//$theme = 'yjx_admin';
$theme = 'pocdoc';
if ($channel == "chuanqi") {
    $theme = 'chuanqi';
} elseif ($channel == "yjx_admin") {
    $theme = 'yjx_admin';
} elseif ($channel == 'yjx_front') {
    $theme = 'yjx_front';
} elseif ($channel == 'm') {
    $theme = 'mobile';
}

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '美年后台管理系统',
    // language
    'defaultController' => 'site',
    'homeUrl' => array('site/login'),
    'theme' => $theme,
    'sourceLanguage' => 'en',
    'language' => 'zh_cn',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        //    'application.modules.user.models.*',
        //    'application.modules.user.components.*',
    ),
    'modules' => array(
        'auth' => array(
            'strictMode' => true,
            'userClass' => 'AdminUser', // the name of the user model class.
            'userIdColumn' => 'id', // the name of the user id column.
            'userNameColumn' => 'username',
            'appLayout' => 'application.views.layouts.admin.column2',
            'viewDir' => null,
        ),
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123456',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'chuanqi' => array(),
        #... add user compent , added by 80x86. see http://www.yiiframework.com/extension/yii-user/
        #...
    ),
    // application components
    'components' => array(
        'bootstrap' => array( //添加一个新的bootstrap容器
            'class' => 'bootstrap.components.Bootstrap',
//执行Bootstrap.php文件
        ),
        'user' => array(
            'class' => 'auth.components.AuthWebUser',
            'allowAutoLogin' => true,
            'behaviors' => array(
                'auth' => array(
                    'class' => 'auth.components.AuthBehavior',
                    'admins' => array('admin', 'kou'),
                ),
            ),
            'loginUrl' => array('site/login'),
            'returnUrl' => 'admin/b2bTicket/admin',
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'behaviors' => array(
                'auth' => array(
                    'class' => 'auth.components.AuthBehavior',
                    'admins' => array('admin', 'kou'), // users with full access
                ),
            ),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(/* 'urlFormat'=>'path',
          'showScriptName'=>false,
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ), */
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=zstjmeinian',
//            'connectionString' => 'mysql:host=115.28.48.232;dbname=marvel',


//            'connectionString' => 'mysql:host=localhost;dbname=marvel',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'ins',
//          'password' => '',
            'charset' => 'utf8',
            'enableProfiling' => true, //设置是否在调试中打印sql
            'enableParamLogging' => true, //设置是否打印sql中所用的参数
        ),
//        'cache' => array(
//            'class'     => 'system.caching.CMemCache',
//            'servers' => array(
//                array('host' => '115.28.14.33', 'port' => 11211, 'weight' => 60),
//             ),
//        ),
        /*
          'db' => array(
          'connectionString' => 'mysql:host=localhost;dbname=marvel',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8',
          'tablePrefix' => '',
          'enableProfiling'=>true,//设置是否在调试中打印sql
          'enableParamLogging'=>true,//设置是否打印sql中所用的参数
          ), */
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
//            array( 
//            'class'=>'CWebLogRoute', 
//            'levels'=>'trace',     //级别为trace 
//            'categories'=>'system.db.*' //只显示关于数据库信息,包括数据库连接,数据库执行语句 
//        ), 
            ),
        ),
        'cache' => array(
            'class' => 'system.caching.CDbCache',
            'connectionID' => 'db',
            'cacheTableName' => 'tbl_yii_cache'
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
);
