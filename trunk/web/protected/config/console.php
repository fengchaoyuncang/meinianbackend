<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
        'import' => array(
            'application.models.*',
            'application.components.*',
        //    'application.modules.user.models.*',
        //    'application.modules.user.components.*',
        ),

	// application components
	'components'=>array(
		'db' => array(
                    'connectionString' => 'mysql:host=115.28.14.33;dbname=marvel',
        //            'connectionString' => 'mysql:host=115.28.48.232;dbname=marvel',
        //            'connectionString' => 'mysql:host=localhost;dbname=marvel',
                    'emulatePrepare' => true,
                    'username' => 'root',
                    'password' => 'ins',
        //          'password' => '',
                    'charset' => 'utf8',
                    'enableProfiling' => true, //设置是否在调试中打印sql
                    'enableParamLogging' => true, //设置是否打印sql中所用的参数
                ),
		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		*/
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);