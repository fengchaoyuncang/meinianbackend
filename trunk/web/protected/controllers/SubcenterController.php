<?php

class SubcenterController extends Controller
{
	public $layout='//layouts/mobile';
	public function actionIndex()
	{
		$this->render('index');
	}
	
	/*
	 * 体检中心城市名称列表
	 */
	
	public function actionCityList()
	{
		$criteria = new CDbCriteria();
		$criteria -> select = 'city';
		$criteria->distinct = true;//是否唯一查询
		$ret = B2bSubCenter::model()->findAll($criteria);
		$items = array();
		foreach($ret as $k=>$v){
			array_push($items, $v->city);
		}
		array_shift($items);
		$this->render('list',array("cities"=>$items));	
	}
	
	/*
	 * 体检中心详情
	 */
	public function actionCenterDetial(){
		$city = $_GET['city'];
		$ret = B2bSubCenter::model()->findAllByAttributes(array('city'=>$city));
		$items =array();
		foreach($ret as $k=>$v){
			$item['name'] = $v->name;
			$item['address'] = $v->address;
			$item['telephone'] = $v->telephone;
			$item['description'] = $v->description;
			$item['baidu_map_url'] = $v->baidu_map_url;
			array_push($items, $item);
		}
		$this->render("details",array('centers'=>$items));
		
	}

	/*
	 * 体检
	 */
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}