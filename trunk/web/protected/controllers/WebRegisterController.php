<?php

class WebRegisterController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'showPasswd', 'create'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = '//layouts/mobile';

        $model = new WebRegister;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['WebRegister'])) {
            $model->attributes = $_POST['WebRegister'];

            //随机生成一个随机数字，放倒licence表中
            $licence = new Licences;
            $licence->dist_name = "美年健康体检";
            $licence->licence = $this->create_password(6);

            if ($model->save() && $licence->save()) {
                $this->redirect(array('showPasswd', 'name' => $model->name, 'licence' => $licence->licence));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionShowPasswd($name, $licence) {
        //session_start();
        $_SESSION['name'] = $name;
        $_SESSION['licence'] = $licence;

        $this->layout = '//layouts/mobile';
        //$this->render('showPasswd', array('name'=>$name, 'licence'=>$licence));
        //$this->redirect(array('/css/statics/suc.php?name='.$name.'&licence='.$licence));
        $this->redirect(array('/css/statics/sucshow.php#wokoapple&'));
    }

    private function create_password($pw_length) {
        $randpwd = "";
        for ($i = 0; $i < $pw_length / 2; $i++) {
            $randpwd .= chr(mt_rand(97, 122));
        }
        for ($i = $pw_length / 2; $i < $pw_length; $i++) {
            $randpwd .= chr(mt_rand(48, 57));
        }


        return $randpwd;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['WebRegister'])) {
            $model->attributes = $_POST['WebRegister'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
//            $connectcin = new CDbConnection($dns,$username,$password);
          $connection = Yii::app()->db;
          if($connection){
              echo "连接成功";
              
          }
//        echo "你好";break;
//        $dataProvider = new CActiveDataProvider('WebRegister');
////        $dataProvider = new CActiveDataProvider('b2bUsers');
//        $this->render('index', array(
//            'dataProvider' => $dataProvider,
//        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new WebRegister('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['WebRegister']))
            $model->attributes = $_GET['WebRegister'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return WebRegister the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = WebRegister::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param WebRegister $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'web-register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
