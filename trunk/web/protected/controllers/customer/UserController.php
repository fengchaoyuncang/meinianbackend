<?php

class UserController extends Controller {

	//    public $layout = "//layouts/main-website-customer";
	public $layout = "//layouts/main-website";
	private $userService = null;

	function __construct($id) {
		parent::__construct($id);
		$this->userService = new UserService();
	}

	/**
	 *
	 *  登陆控制
	 */
	public function filters() {
		return array(
            'accessControl', // 控制器自身访问过滤器
            'rolescheck',
		);
	}

	public function filterRolescheck($filterChain) {
		$role = key(Yii::app()->authManager->getRoles(Yii::app()->user->getId()));
		if (empty($role))
		$filterChain->run();
		else {
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	// 该方法是定义：基于访问控制过滤器，允许所有用户访问的动作。
	public function accessRules() {
		return array(
		array('deny',
                'users' => array('?'),
		),
		);
	}

	public function actionIndex() {
		//显示基本信息
		$this->render("index");
	}

	public function actionYuyuelist() {
		//显示预约列表
		$user = Yii::app()->user->getUserInfo();
		$arr = array("uid" => $user['userid']);
		$user_ser_ins = new UserService();
		$reserved_package_arr = $user_ser_ins->getMyOrder($arr);

		$this->render('yuyuelist', array('reserved_package_arr' => $reserved_package_arr));
	}

	public function actionYuyueview() {
		//显示预约详情
		$this->render("yuyueview");
	}

	/**
	 * 问卷
	 */
	public function actionWenjuan() {
		if (isset($_GET['quesagain']) && $_GET['quesagain'] === "quesagain"){
			$this->questionAgain(Yii::app()->user->getId());
		}
		if(isset($_POST["Q"])){
			EvaluateQuestionnaire::model()->saveQAnswer(Yii::app()->user->getId(), $_POST["Q"]);
			//			$hum_consititution = EvaluateHumConstitution::model()->findByAttributes(array("uid" => Yii::app()->user->getId()));
			//			$this->render("index", array("hum_arr" => $hum_consititution));exit();
		}
		$ret = EvaluateDiseaseType::model()->findAll("",array("select"=>"element_name"));
		$items = array();
		foreach ($ret as $value) {
			$i = explode(",", $value->element_name);
			$items = array_merge($items, $i);
		}
		$items = array_unique($items);
		$userinfo = Disease::getUserInfo(Yii::app()->user->getId());
		$ques = array();
		foreach ($items as $key => $element_name) {
			$col = $element_name;
			//用户所有信息列默认为空，这里判断如果为空则表示信息不全，需要提供问题
			if ($userinfo->$col === NULL) {
				$ret = EvaluateQuestionElement::model()->find("element_name=:name", array(":name" => $element_name));
				if (!empty($ret)) {
					array_push($ques, $ret->question_id);
				}
			}
		}
		$hum_consititution = EvaluateHumConstitution::model()->findByAttributes(array("uid" => Yii::app()->user->getId()));
		if (empty($hum_consititution)){
			array_push($ques, "20");
		}
		//        var_dump(Yii::app()->user->getId());exit();
		$ret = EvaluateQuestionnaire::model()->findAllByAttributes(array("id"=>$ques),array("order"=>"questype,id"));
		$this->render("wenjuan",array("ques"=>$ret,"uid"=>Yii::app()->user->getId(),"dis"=> "all"));
	}

	public function actionBaogaolist() {
		//报告列表

		$result = $this->userService->getReportList(array("uid" => Yii::app()->user->getId()));
		if (is_int($result)) {
			JSONHelper::echoJSON(false, $result);
		}

		$this->render("baogaolist", array("list" => $result));
	}

	public function baogaoview($orderid){
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$dis_properties = $report_ins->getDiseaseProbabilities(Yii::app()->user->getId(), 10);
		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());
		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());
	}

	public function actionBaogaoview($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$dis_properties = $report_ins->getDiseaseProbabilities(Yii::app()->user->getId(), 10);
		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());
		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());

		$this->renderPartial("baogaoview",array("item"=>$item, "summary" => $summary_arr, "meal" => $meal_arr, "sports" => $sports_sug_arr, "dis_properties" => $dis_properties));
			
	}



	public function actionBaogaoview1($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$dis_properties = $report_ins->getDiseaseProbabilities(Yii::app()->user->getId(), 10);
		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());
		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());

		$this->renderPartial("baogaoview1",array("item"=>$item, "summary" => $summary_arr, "meal" => $meal_arr, "sports" => $sports_sug_arr, "dis_properties" => $dis_properties));
			
	}

	public function actionBaogaoview4($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$item_arr = array();

		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());

		$this->renderPartial("baogaoview4",array("meal" => $meal_arr));
	}


	public function actionBaogaoview3($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid)); 
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());

		$this->renderPartial("baogaoview3",array("summary" => $summary_arr,));
			
	}

	public function actionBaogaoview2($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$item_arr = array();

		$dis_properties = $report_ins->getDiseaseLevel(Yii::app()->user->getId(), 10);

		$this->renderPartial("baogaoview2",array("dis_properties" => $dis_properties));
			
			
	}

	public function actionBaogaoview5($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$advice = $sugserice_ins->getTotalAdvice(Yii::app()->user->getId());

		$this->renderPartial("baogaoview5",array("sports" => $sports_sug_arr,"advice" => $advice));
	}


	public function actionBaogaoview6($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$dis_properties = $report_ins->getDiseaseProbabilities(Yii::app()->user->getId(), 10);
		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());
		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());

		$this->renderPartial("baogaoview6",array("item"=>$item, "summary" => $summary_arr, "meal" => $meal_arr, "sports" => $sports_sug_arr, "dis_properties" => $dis_properties));
			
	}

	public function actionBaogaoview7($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$dis_properties = $report_ins->getDiseaseProbabilities(Yii::app()->user->getId(), 10);
		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());
		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());

		$this->renderPartial("baogaoview7",array("item"=>$item, "summary" => $summary_arr, "meal" => $meal_arr, "sports" => $sports_sug_arr, "dis_properties" => $dis_properties));
			
	}

	public function actionBaogaoview8($orderid) {
		//报告详情
		$item = $this->userService->getReportArrayInfo(array("uid" => Yii::app()->user->getId(),
            "orderid" => $orderid));
		if (is_int($item)) {
			$item = array();
		}

		$report_ins = new Reportservice();
		$sugserice_ins = new SuggestionService();
		$item_arr = array();

		$sports_sug_arr = $sugserice_ins->getSportsSuggestion(Yii::app()->user->getId());
		$dis_properties = $report_ins->getDiseaseProbabilities(Yii::app()->user->getId(), 10);
		$summary_arr = $report_ins->getSummary(Yii::app()->user->getId());
		$meal_arr = $report_ins->getMealCollocation(Yii::app()->user->getId());

		$this->renderPartial("baogaoview8",array("item"=>$item, "summary" => $summary_arr, "meal" => $meal_arr, "sports" => $sports_sug_arr, "dis_properties" => $dis_properties));
			
	}

	/*
	 *显示体检报告各个页面的链接
	 */
	public function actionBaogaoviewNav(){
		$this->render("BaogaoviewNav");
	}

	/**
	 * remove the old question answer,
	 * Enter description here ...
	 */
	public function questionAgain($userid) {
		for ($i = 1; $i <= 4; $i++){
			$ret = Disease::deleteUserInfo($userid, $i);
		}
		$eval_ret = EvaluateHumConstitution::model()->findByAttributes(array("uid" => $userid));
		if (isset($eval_ret)){
			$eval_ret->delete();
		}
		$hum_ret = new EvaluateHumanHealthTag();
		$hum_ret->clearTabs($userid, "badtags");
	}


}
