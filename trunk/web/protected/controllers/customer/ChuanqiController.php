<?php

class ChuanqiController extends Controller {

    public $layout = "//layouts/main-website";
    /**
     * 健康管理
     */
    public function actionHealthmanager()
    {
        $this->render("healthmanager");
    }
    /**
     * 预约就医
     */
    public function actionJiuyi()
    {
        $this->render("jiuyi");
    }
    /**
     * 预约私教
     */
    public function actionSijiao()
    {
        $this->render("sijiao");
    }
    /**
     * 口腔健康
     */
    public function actionKouqiang()
    {
        $this->render("kouqiang");
    }
    /**
     * 特色医疗
     */
    public function actionTeseyiliao()
    {
        $this->render("teseyiliao");
    }
    /**
     * 场馆预约
     */
    public function actionChangguanyuyue() {
        $this->render("changguanyuyue");
    }
    
    /**
     * 健康商城
     * */
    public function actionHealthstore(){
    	$this->render("healthstore");
    }
    
}

