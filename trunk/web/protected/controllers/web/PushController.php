<?php

/*APP推送的控制类*/
class PushController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
    
    public function accessRules()
	{
		return array(
			//array('allow',  // allow all users to perform 'index' and 'view' actions
			//	'actions'=>array('index','view', 'viewMobile'),
			//	'users'=>array('*'),
			//),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index',),
				'users'=>array('admin'),
			),
			//array('allow', // allow admin user to perform 'admin' and 'delete' actions
			//	'actions'=>array('admin','delete'),
			//	'users'=>array('admin'),
			//),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    

	public function actionIndex()
	{
		$model = new PushForm;
        $pushCenter = new PushCenter;
		//判断是否已经提交
		if (isset($_POST['PushForm']))
		{
			$model->attributes=$_POST['PushForm'];
			if($model->validate())
			{
				echo "paper_id: $model->paper_id<br/>";
				echo "item_id: $model->item_id<br/>";
				echo "indicator_id: $model->indicator_id<br/>";
				echo "android: $model->android<br/>";
				echo "ios: $model->ios<br/>";	
				//执行推送的动作
                $pushCenter->doPush($model);
                
				Yii::app()->end();
			}
		}
		
		//显示push界面
		$papers =  Papers::model()->getPaperList();
		$indicators = Indicator::model()->getAllIndicators();
		$items = ExamItem::model()->listAllNames();
		$this->render('view',array('model'=>$model, 'papers'=>$papers, 'indicators'=>$indicators, 'items'=>$items));
	}
	
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
}