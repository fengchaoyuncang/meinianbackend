<?php

class TipsCardController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionFetch()
	{
		
		$twoTips = AppTipscard::model()->findAll(array(
				'condition' => 'tipid > 0 ORDER BY tipid DESC LIMIT 2',
				'params' => array()
		));
		
		$data = array();
		foreach ($twoTips as $tip) {
			$oneTip["titlebar"] = $tip->titlebar;
			$oneTip["summary"] = $tip->summary;
			$oneTip["weburl"] = $tip->weburl;
			
			$data[] = $oneTip;
		}
				
		JSONHelper::echoJSON(true, $data);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}