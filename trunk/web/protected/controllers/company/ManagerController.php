<?php
class ManagerController extends Controller {
    public $layout = "//layouts/companyadmin";
    
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        Yii::app()->user->loginUrl=array("login/pageregister");
    }
    
    public function actionIndex() {
        $ret = AdminUser::model()->with(array("company"))->findByPk(Yii::app()->user->getId());
        //错误验证
        $users = new B2bExternalUser();
        $status = isset($_GET["statuscode"])?$_GET["statuscode"]:null;
        $ret = $users->getCompUser($ret->company_id,$status);
        $this->render("index",array("items"=>$ret));
    }
    /**
     * 团单预约管理
     */
    public function actionTuandan()
    {
        $items = Yii::app()->session['items'];
        $company_id = Yii::app()->user->getCompanyId();
        $items = B2bExternalUser::model()->findAllByAttributes(array("id"=>$items,"company_id"=>$company_id));
        if(isset($_POST["yuyue"])){            
            $order = new B2bOrder();
            $ret = $order->appointmentTuan($items,$_POST["yuyue"]);            
//            JSONHelper::echoJSON($ret);
            if($ret === true){
                Yii::app()->session['items'] = array();
                JSONHelper::echoJSON($ret);
            }else{
                JSONHelper::echoJSON(false, $ret);
            }
        }
        $ret = array();
        if (count($items) > 0) {
            foreach ($items as $key => $value) {
                if($value->statuscode!=0){
                    continue;
                }
                if (!isset($ret[$value->package_id])) {
                    $ret[$value->package_id]["people"] = array();
                    $package = B2bPackage::model()->findByPk($value->package_id);
                    $ret[$value->package_id]["package"] = $package;
                    $centers = B2bPackageToCenter::model()->with(array("center"))->findAllByAttributes(array("package_id" => $value->package_id));
                    $ret[$value->package_id]["centers"] = $centers;
                }
                array_push($ret[$value->package_id]["people"], $value);
            }
        }

        $this->render("tuandan",array("items"=>$ret));
    }
    /**
     * 清空所有的预约信息
     */
    public function actionCleantuan(){
        Yii::app()->session['items'] = array();
        $this->redirect(array("tuandan"));
    }
    /**
     * 预约团
     */
    public function actionYuyuetuan(){
        if(!isset($_POST["item"])){
            JSONHelper::echoJSON(false, "非法请求");
        }

        $items = Yii::app()->session['items']===null?array():Yii::app()->session['items'];
        $item = explode(".", $_POST["item"]);
        foreach($item as $it){
            if(empty($it)){
                continue;
            }
            if(in_array($it, $items)){
                continue;
            }
            array_push($items, $it);
        }
        Yii::app()->session['items'] = $items;
        JSONHelper::echoJSON(TRUE);
    }
    /**
     * 取消某个准备预约团单中的一人
     */
    public function actionCancelbyid(){
        if(!isset($_POST["id"])){
            JSONHelper::echoJSON(false);
        }
        $items = Yii::app()->session['items'];
        if($items === null){
            JSONHelper::echoJSON(TRUE);
        }
        if(($ret = array_search($_POST["id"], $items))!==false){
            unset($items[$ret]);
        }
        if(count($items) === 1){
            $items = array_values($items);
        }
        Yii::app()->session['items'] = $items;
        JSONHelper::echoJSON(TRUE);
    }
    /**
     * 确认预约
     */
    public function actionYuyueconfirm()
    {
        $items = Yii::app()->session['items'];
        $company_id = Yii::app()->user->getCompanyId();
        $this->render("yuyueconfirm",array("items"=>B2bExternalUser::model()->findAllByAttributes(array("id"=>$items,"company_id"=>$company_id)),"yuyue"=>$_POST["yuyue"]));
    
    }

    /**
     * 团单管理查询
     */
    public function actionTuandanlist(){
        $ret = B2bOrderTuan::model()->findAllByAttributes(array("company_id"=>Yii::app()->user->getCompanyId()),array("order"=>"order_time desc"));
        $this->render("tuandanlist",array("items"=>$ret));
    }
    /**
     * 团单详情管理
     */
    public function actionTuandanview($tuanid){
        $item = B2bOrderTuan::model()->findByAttributes(array("id"=>$tuanid,"company_id"=>Yii::app()->user->getCompanyId()));
        if($item === null){
            throw new CHttpException(404,'错误的请求');
        }
        $p=explode(",", $item->b2b_order_id);
        $people =  B2bOrder::model()->findAllByPk($p);
        $this->render("tuandanview",array("item"=>$item,"people"=>$people));
    }
    /**
     * 团单取消
     */
    public function actionTuandancancel(){
        if(!isset($_POST["id"])){
            JSONHelper::echoJSON(false, "非法请求");
        }
        
        $tuan = B2bOrderTuan::model()->findByAttributes(array("id"=>$_POST["id"],"company_id"=>Yii::app()->user->getCompanyId()));
        if(null === $tuan){
            JSONHelper::echoJSON(false, "错误请求");
        }
        //如果体检中心已经确认过，则不允许取消
        if($tuan->statuscode==2){
            JSONHelper::echoJSON(false, "此状态下不允许取消");
        }
        $b2bordertuan = new B2bOrderTuan();
        $ret = $b2bordertuan->cancel($tuan);
        if($ret === true){
            JSONHelper::echoJSON(true,"",0);
        }else{
            JSONHelper::echoJSON(false,"操作失败，请重试");
        }
    }
    /**
     * 团单再次预约
     */
    public function actionTuandanreyuyue(){
        if(!isset($_POST["id"])||!isset($_POST["time"])){
            JSONHelper::echoJSON(false, "非法请求");
        }
        $tuan = B2bOrderTuan::model()->findByAttributes(array("id"=>$_POST["id"],"company_id"=>Yii::app()->user->getCompanyId()));
        if(null === $tuan){
            JSONHelper::echoJSON(false, "错误请求");
        }
        //只在体检中心反馈后，才允许一次修改
        if($tuan->statuscode!=3){
            JSONHelper::echoJSON(false, "此状态下不允许再次预约");
        }
        $time = date_create($_POST["time"]);
        if($time==date_create($tuan->order_time)||$time<date_create()){
            JSONHelper::echoJSON(FALSE, "错误的预约时间");
        }
        $tuan->order_time = $time->format('Y-m-d');
        $tuan->statuscode = 4;
        if($tuan->save()===false){
            JSONHelper::echoJSON(false, "数据库异常");
        }
        JSONHelper::echoJSON(TRUE);
    }
    /**
     * 
     *  登陆控制
     */
    public function filters() {
        return array(
            array('auth.filters.AuthFilter'),
        );
    }
}

?>