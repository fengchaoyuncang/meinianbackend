<?php

class MainController extends Controller {

    public $layout = "//layouts/main-website";
    private $diseaseService = null;

    function __construct($id) {
        parent::__construct($id);
        $this->diseaseService = new DiseaseService();
    }

    public function actionIndex($status = false) {
        $session = Yii::app()->session;
        $session['item_id'] = '0';
        $user_info = Yii::app()->user->getUserInfo();
        if (isset($user_info)) {
            $status = true;
        }
        $subcenter_ins = new B2bSubCenter();
        $cities = $subcenter_ins->getCities();
        $titles = InfoPapers::model()->findAll(array("select" => "id,title", "order" => "id desc", "limit" => 7));

        $this->render("index", array("login_status" => $status, "titles" => $titles, "cities" => $cities, "userinfo" => $user_info));
    }

    public function actionCancellation() {
        Yii::app()->user->logout();
        $subcenter_ins = new B2bSubCenter();
        $cities = $subcenter_ins->getCities();
        $titles = InfoPapers::model()->findAll(array("select" => "id,title", "limit" => 5, "order" => "id desc"));

        $this->render("index", array("login_status" => false, "titles" => $titles, "cities" => $cities));
    }

    public function actionError() {
        $this->render("error");
    }

    /*
     * 列出体检机构列表
     * @param 无
     * return 数据集
     */

    public function actionCenterList() {
        $session = Yii::app()->session;
        $session['item_id'] = '2';
        $B2bSubCenter = new B2bSubCenter();
        //获取所有城市名称
        $cities = $B2bSubCenter->getCities();

        //获取所有城市数量
        $countAll = B2bSubCenter::model()->count();
        $all = array("city" => "全部", 'count' => $countAll);
        array_unshift($cities, $all);

        //获取所有体检机构        
        $rets = $B2bSubCenter->getSubcenterEachPage();
        $this->render('sub_center_list', array(
            'rets' => $rets,
            'cities' => $cities,
        ));
    }

    /*
     * 获取属于某个城市的体检中心
     * @param $city 城市
     * return 体检中心数据集
     */

    public function actionCenterToCity() {
        $cs = new CenterServices();

        //获取所有城市名称
        $cities = $cs->getCenterRegion();
        //获取所有城市数量
        $countAll = B2bSubCenter::model()->count();
//        $all = array("city" => "全部", 'count' => $countAll);
//        array_unshift($cities, $all);
        //获取属于某个城市的体检中心
        $items = $cs->getCentersByCityAndPage($_GET);

//        $rets = $B2bSubCenter->getSubcenterToCityEachPage();
        $this->render('sub_center_list', array(
            'cities' => $cities,
            'items' => $items,
        ));
    }

    /*
     * 列出套餐列表
     * @param 无
     * return 数据集
     */

    public function actionPackageList() {
        $session = Yii::app()->session;
        $session['item_id'] = '4';
        $b2bcenter_ins = new B2bBrand();
        $centerid_ret_arr = $b2bcenter_ins->getDistinctId();

        $ret_arr = array();
        foreach ($centerid_ret_arr as $centerid) {
            $packagecenter_ins = new B2bPackageToCenter();
            $package_ret_arr = $packagecenter_ins->getPaInfo($centerid->id);
            array_push($ret_arr, array("centerid" => $centerid, "package" => $package_ret_arr));
        }
        $this->render('package_list', array(
            'rets' => $ret_arr,
        ));
    }

    /**
     * 营养方案
     */
    public function actionNutritionlist() {
        $result = $this->diseaseService->getNutritionList($_GET);
        $this->render("nutrition_list", array("result" => $result));
    }

    /*
     * 体检须知
     */

    public function actionMedicalGuide() {
        $session = Yii::app()->session;
        $session['item_id'] = '3';
        $this->render("medical_guide");
    }

    /*
     * 体检中心详情
     */

    public function actionCenterDetails() {
        $cityid = isset($_GET['cid']) ? $_GET['cid'] : 1;
        $subcenter_ret = B2bSubCenter::model()->findByPk($cityid);
        $center_ret = B2bBrand::model()->findByPk($subcenter_ret->brandid);
        $this->render("center_details", array("subcenterinfo" => $subcenter_ret, "centerinfo" => $center_ret));
    }

    /*
     * 健康列表
     */

    public function actionHealthList() {
        $this->render('health_list');
    }

    /*
     * 下载手机客户端
     */

    public function actionMobileDownload() {
        $this->render("mobile_download");
    }

    /* 手机访问的页面 */

    public function actionAppDownload() {
        $this->layout = "//layouts/app-download";
        $this->render("app_download");
    }

    /* 敬请期待 */

    public function actionWait() {
        $this->render("wait");
    }

    /* 预约界面 */

    public function actionYuyue() {
        $this->render("yuyue");
    }

    public function actionAboutus() {
        $this->render("aboutus");
    }

    public function actionHealthcare() {
        $this->render("healthcare");
    }

}
