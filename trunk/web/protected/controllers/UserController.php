<?php

class UserController extends Controller {

    public $layout = '//layouts/user';
    
    public function actionIndex() {
//        var_dump($this->center_id);
//        exit();
        $user = new User();
        $dataProvider = $user->getAll(Yii::app()->user->getCenterId());
        if($dataProvider === false){
            //错误啊，不应该到达
            return;
        }

        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
    public function actionView($id){
        $id = Yii::app()->user->getId();
        var_dump($id);
    }
    
    public function actionCreate(){
        $roles = array(array("id"=>"center_admin","name"=>"管理员"),
            array("id"=>"center_work","name"=>"业务员"));
        if(Yii::app()->user->getIsAdmin()){
            $centers = Center::model()->findAll("", array('select'=>'id,name'));
        }else{
            $centers = Center::model()->findAll("id=".Yii::app()->user->getCenterId(),array('select'=>'id,name'));
        }
        $this->render('create',array("roles"=>$roles,"centers" => $centers));
    }
    public function actionUpdate($id){
        var_dump($id);
    }
    /**
     * 权限控制，对于指定用户只能操作自己体检中心的用户，否则拒绝
     * @param type $filterChain
     * @throws CHttpException
     * 
     */
    public function filterCheckCenter($filterChain) {
        if (Yii::app()->user->getCenterId() === 0) {
            $filterChain->run();
        }else{
            if(!isset($_GET['id'])){
                throw new CHttpException(200, 'Access denied.',401);
            }
            $user = User::model()->findByPk($_GET['id']);
            if(null !== $user){
                if(Yii::app()->user->getCenterId() == $user->center_id){
                    $filterChain->run();
                }
            }else{
                throw new CHttpException(200, 'Access denied.',401);
            }
        }        
    }

    public function filters() {
        return array(
            'checkCenter +view,update',
            array('auth.filters.AuthFilter'),
        );
    }
    
    
}