<?php

class B2bTicketController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'error'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $item = $this->loadModel($id);
        $item->tstatus = B2bTicket::model()->getStatusDesc($item->tstatus);
        $item->codetype = B2bTicket::model()->getCodetypeDesc($item->codetype);
        $item->activecode = B2bTicket::model()->getActivecodeDesc($item->activecode);
        $this->render('view', array(
            'model' => $item,
        ));
    }

    public function actionError($note)
    {
        $this->render('error', array(
            'note' => $note
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new B2bTicket;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['B2bTicket'])) {
            $model->attributes = $_POST['B2bTicket'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * 预约码状态未预约 确认预约 预约码类型 商城 F码
     *
     */
    public function actionUpdate($id)
    {
        $transaction = Yii::app()->db->beginTransaction();
        $model = $this->loadModel($id);
        $tstatus_pre = $model->tstatus;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);


        if (isset($_POST['B2bTicket'])) {
            $model->attributes = $_POST['B2bTicket'];
            //@liuchang
            //验证码，F码更改状态为预约成功的时候会关联预约一个体检账号密码
//            $subR_ret = $this->subReservationNum($model->centerid, $model->examdate);
//            if ($subR_ret === JSONHelper::ERROR_NO_SUBCENTER) {
//                $this->redirect(array('error', "note" => "此体检中心没有录入可用人数"));
//            }
            $ret_update = $model->update();

            if ($tstatus_pre == B2bTicket::YUYUE_PROCESSING && $model->tstatus == B2bTicket::YUYUE_CONFIRM && $model->codetype == B2bTicket::SHOPINGCODERESERVATION) { //} || $model->codetype == B2bTicket::FCODERESERVATION)) {
                $meinian_user_ob = B2bMeinianUser::model()->findByAttributes(array("fk_userid" => null));
                if ($meinian_user_ob == null) {
                    $this->redirect(array('error', 'id' => $model->id, "note" => "没有可用的账户"));
                }
                $meinian_user_ob->fk_userid = $model->uid;
                $ret_save = $meinian_user_ob->save();

                if (!$ret_save || !$ret_update) { //} || !$subR_ret) {
                    $transaction->rollback();
                    $sms = new SmsService();
                    $sms->send($model->telephone, "尊敬的用户,您预约每年体检日期:{$model->examdate}" .
                        ",由于网络原因预约失败，请您重新预约");
                } else {
                    $transaction->commit();
                    $sms = new SmsService();
                    $sms->send($model->telephone, "尊敬的用户,您预约每年体检日期:{$model->examdate}" .
                        "美年用户名:{$meinian_user_ob->username},密码:{$meinian_user_ob->password}");
                }
            } elseif ($model->codetype == B2bTicket::NOCODERESERVATION) {
                if (!$ret_update) { //} || !$subR_ret) {

                    $transaction->rollback();
                    $sms = new SmsService();
                    $sms->send($model->telephone, "尊敬的用户,您预约每年体检日期:{$model->examdate}" .
                        ",由于网络原因预约失败，请您重新预约");
                } else {
                    $transaction->commit();
                    $sms = new SmsService();
                    $sms->send($model->telephone, "尊敬的用户,您预约每年体检日期:{$model->examdate}" .
                        "");
                }
            }
            $this->redirect(array('view', 'id' => $model->id));
        }
//        $model->tstatus = B2bTicket::model()->getStatusDesc($model->tstatus);
//        $model->codetype = B2bTicket::model()->getCodetypeDesc($model->codetype);
        $model->activecode = B2bTicket::model()->getActivecodeDesc($model->activecode);

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public
    function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * Manages all models.
     */
    public
    function actionAdmin()
    {
        $model = new B2bTicket('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['B2bTicket'])) {
            $ticket = $_GET['B2bTicket'];
            if (isset($ticket['companyid'])) {
                $companyName = $ticket['companyid'];
                $ret = B2bCompany::model()->find("`name` like '%$companyName%'"); //高级搜索中，根据名称搜索内容

                if ($ret != null) {
                    $ticket["companyid"] = $ret->id;
                }
            }
            $model->attributes = $ticket;
        }

        if(isset($_GET['B2bTicket'])&&$_GET['B2bTicket']['codetype'] != ''){
            $searchmethod = $model->search();
        }else{
            $searchmethod = $model->searchAdmin();
        }
        //默认显示"预约处理中，可取消"状态的套餐
        if (!isset($_GET['B2bTicket'])) {
            $ticket['tstatus'] = 2;
            $model->attributes = $ticket;
        }
        //获取所有套餐的id和名称组成的数组
        $packages = B2bTicket::model()->getPackages();
        $this->render('admin', array(
            'searchmethod' => $searchmethod,
            'model' => $model,
            'packages' => $packages,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return B2bTicket the loaded model
     * @throws CHttpException
     */
    public
    function loadModel($id)
    {
        $model = B2bTicket::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param B2bTicket $model the model to be validated
     */
    protected
    function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'b2b-ticket-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @liuchang
     * 剩余预约名额-1
     */
    private
    function subReservationNum($subcenter, $date)
    {
//        //检测是否提前1天预约
//        $tomorrow = new DateTime('tomorrow');
//        if ($date < $tomorrow) {
//            return JSONHelper::ERROR_DATE;
//        }
//
//        //查找体检中心信息
//        $center = B2bSubCenter::model()->findByPk($subcenter);
//        if ($center === null) {
//            return JSONHelper::ERROR_CENTER_EXISTS;
//        }

        $b2bre_num_subpd_ob = B2bReservationSubCenterPd::model()->findByAttributes(array("fk_subcenter" => $subcenter, "date" => $date));
        if ($b2bre_num_subpd_ob === null) {
            $b2bre_num_subpd_ob = new B2bReservationSubCenterPd();
            $b2bre_num_subpd_ob->fk_subcenter = $subcenter;
            $b2bre_num_subpd_ob->date = $date;
            $b2bre_num_sub_ob = B2bReservationNumSubcenter::model()->findByAttributes(array("fk_subcenter" => $subcenter));
            if ($b2bre_num_sub_ob === null) {
                //@liuchang  这个bug不应该存在
                return JSONHelper::ERROR_NO_SUBCENTER;
            }
            $b2bre_num_subpd_ob->num = $b2bre_num_sub_ob->num;

            $b2bre_num_subpd_ob->telephone = $b2bre_num_sub_ob->telephone;
        }
        if ($b2bre_num_subpd_ob->num > 0) {
            $b2bre_num_subpd_ob->num = $b2bre_num_subpd_ob->num - 1;
            try {
                if ($b2bre_num_subpd_ob->save() == false) {
//                    var_dump($b2bre_num_subpd_ob->update()->errors);exit;
                    return JSONHelper::ERROR_SERVICE;
                }
            } catch (Exception $e) {
//                var_dump($b2bre_num_subpd_ob->update()->errors);exit;
                return JSONHelper::ERROR_SERVICE;
            }
        } else {
            return JSONHelper::ERROR_SUBCENTER_NUM_NOMORE;
        }
        return true;
    }

    /**
     * @liuchang  关联美年帐号
     * @param $uid
     * @return int
     */
    private
    function relationofmeinianuid($uid)
    {
        $meinian_user_ob = B2bMeinianUser::model()->findByAttributes(array("fk_userid" => null));
        if ($meinian_user_ob != null) {
            $meinian_user_ob->fk_userid = $uid;
            try {
                if ($meinian_user_ob->update() == false) {
                    return JSONHelper::ERROR_SERVICE;
                }
            } catch (Exception $ex) {
                return JSONHelper::ERROR_SERVICE;
            }
        } else {
            return JSONHelper::ERROR_MEINIAN_USER_NOMORE;
        }
    }
}
