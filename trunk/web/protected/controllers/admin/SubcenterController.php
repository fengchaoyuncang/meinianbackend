<?php

class SubcenterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		array('auth.filters.AuthFilter'),
		);
	}
	/**
	 * 辅助方法获取体检中心列表
	 */
	public function actionProvince(){
		$model = B2bSubCenter::model()->findAll(array("select"=>"province","group"=>"province"));
		$pro = array();
		foreach($model as $it){
			array_push($pro, $it->province);
		}
		JSONHelper::echoJSON(true,$pro);
	}
	public function actionCity($province){
		$model = B2bSubCenter::model()->findAll(array("condition"=>"`province`='$province'", "select"=>"city","group"=>"city"));
		$city = array();
		foreach($model as $it){
			array_push($city, $it->city);
		}
		JSONHelper::echoJSON(true,$city);
	}
	public function actionGetsubcenters($city = null,$name = null){
		$model = B2bSubCenter::model();

		$criteria = new CDbCriteria;

		$params = array();
		if(!empty($city)){
			$criteria->addCondition("city='$city'");
		}
		if(!empty($name)){
			//                $params["center_id"] = $brand;
			$criteria->compare("name", $name,true);
		}
		$ret = $model->findAll($criteria);
		$centers = array();
		foreach($ret as $it){
			$item["id"] = $it->id;
			$item["name"] = $it->name;
			$item["address"] = $it->address;
			array_push($centers, $item);
		}
		JSONHelper::echoJSON(true,$centers);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
//		$examitemsubcenter_ret = B2bExamItemToSubcenter::model()->findAllByAttributes(array("subcenter_id" => $id));
//		$examitemid_arr = array();
//
//		if (!empty($examitemsubcenter_ret)){
//			foreach ($examitemsubcenter_ret as $item){
//				array_push($examitemid_arr, $item->exam_item_id);
//			}
//		}
//		$examitemclassify_ret = BasicExamItemClassify::model()->findAll();
//		$itemclassify_arr = array();
//		foreach ($examitemclassify_ret as $item) {
//			$itemclassify_arr[$item->id] = $item->item_name;
//		}
//
//		$examitem_ret = BasicExamItem::model()->findAllByAttributes(array("id" => $examitemid_arr));
//		$item_arr = array();
//		foreach ($examitem_ret as $item) {
//			if (!isset($item_arr[$itemclassify_arr[$item->item_id]])){
//				$item_arr[$itemclassify_arr[$item->item_id]] = array();
//			}
//			array_push($item_arr[$itemclassify_arr[$item->item_id]], $item);
//		}
            $item_arr = array();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'examitem_arr'=>$item_arr,
		));
	}

	/**
	 * 帮顶体检项目
	 * Enter description here ...
	 * @param unknown_type $id
	 */
	public function actionBindexamitems($id = 0)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (isset($_POST['classify'])){
			$transaction = Yii::app()->db->beginTransaction();
			try{
				/*
				 * 1	wheather classify is empty
				 * 1.1	yes		do noting
				 * 1.2	no		save the item_id to the b2b_exam_item_to_package
				 * 2	if classify[-1] empty
				 * 2.1	yes		do nothing
				 * 2.2	no		save all the item_id which belong to the classify_id
				 */
				$old_arr = array();
				$old_itemtosubcenter_arr = B2bExamItemToSubcenter::model()->findAllByAttributes(array("subcenter_id" => $_POST['subcenter_id']));
				foreach ($old_itemtosubcenter_arr as $item){
					array_push($old_arr, $item->exam_item_id);
				}

				$update_arr = array();
				foreach ($_POST['classify'] as $key => $arr){
					foreach ($arr as $item){
						array_push($update_arr, $item);
					}
				}
				$dele_arr = array_diff($old_arr, $update_arr);
				$add_arr = array_diff($update_arr, $old_arr);
				$up_arr = array_intersect($old_arr, $update_arr);

				B2bExamItemToSubcenter::model()->deleteAllByAttributes(array("subcenter_id" => $_POST['subcenter_id'], "exam_item_id" => $dele_arr));
				foreach ($add_arr as $item){
					$item_package_ins = new B2bExamItemToSubcenter();
					$item_package_ins->exam_item_id = $item;
					$item_package_ins->subcenter_id = $_POST['subcenter_id'];
					$item_package_ins->create_at= date('Y-m-d H:i:s',time());
					$item_package_ins->update_at= date('Y-m-d H:i:s',time());
					$ret = $item_package_ins->save();
					//						var_dump($item_package_ins->errors);exit();
				}
				foreach ($up_arr as $item){
					$item_package_ins = B2bExamItemToSubcenter::model()->findByAttributes(array("subcenter_id" => $_POST['subcenter_id'], "exam_item_id" => $item));
					$item_package_ins->exam_item_id = $item;
					$item_package_ins->subcenter_id = $_POST['subcenter_id'];
					$item_package_ins->update_at= date('Y-m-d H:i:s',time());
					$item_package_ins->update();
				}
				$transaction->commit();
				$this->redirect(array('view','id'=>$model->id));
			}catch (Exception $e){
//				var_dump($e);exit();
				$transaction->rollback();
			}
		}

		//得到一种套餐对应的体检项目
		$examitempackage_ret = B2bExamItemToSubcenter::model()->findAllByAttributes(array("subcenter_id" => $id));
		$examitempackage_exit_arr = array();
		foreach ($examitempackage_ret as $examitempackage) {
			$examitempackage_exit_arr[$examitempackage->exam_item_id] = $examitempackage;
		}
		//得到体检项目大类
		$basicitemclassify_ret = BasicExamItemClassify::model()->findAll();
		//得到体检项目小项
		$basicitem_ret = BasicExamItem::model()->findAll();
		$basicitem_arr = array();
		/*
		 * array("大项目" => array(
		 * 							"小项目" => array(
		 * 											"flage" => true/false,
		 * 											"ins" => 小项目实体)))
		 */
		foreach ($basicitem_ret as $item) {
			if (isset($examitempackage_exit_arr[$item->id])){
				$basicitem_arr[$item->item_id][$item->id] = array("check"=>true, "ins" => $item);
			}else{
				$basicitem_arr[$item->item_id][$item->id] = array("check"=>false, "ins" => $item);
			}
		}
		$this->render('bindexamitems',array(
			'model'=>$model,
			'basicitemclassify'=>$basicitemclassify_ret,
			'basicitem'=>$basicitem_arr,
		));
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new B2bSubCenter;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['B2bSubCenter']))
		{
			$model->attributes=$_POST['B2bSubCenter'];
			if($model->save())
			$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['B2bSubCenter']))
		{
			$model->attributes=$_POST['B2bSubCenter'];
			if($model->save())
			$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('B2bSubCenter');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new B2bSubCenter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['B2bSubCenter']))
		$model->attributes=$_GET['B2bSubCenter'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return B2bSubCenter the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=B2bSubCenter::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param B2bSubCenter $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='b2b-sub-center-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
