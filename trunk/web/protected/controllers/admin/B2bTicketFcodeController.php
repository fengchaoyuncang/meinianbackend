<?php

class B2bTicketFcodeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = '//layouts/admin/column2';

    /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $item = $this->loadModel($id);
        $item->tstatus = B2bTicket::model()->getStatusDesc($item->tstatus);
        $item->codetype = B2bTicket::model()->getCodetypeDesc($item->codetype);
        $item->activecode = B2bTicket::model()->getActivecodeDesc($item->activecode);
		$this->render('view',array(
			'model'=>$item,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new B2bTicketFcode;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['B2bTicketFcode']))
		{
			$model->attributes=$_POST['B2bTicketFcode'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $transaction = Yii::app()->db->beginTransaction();
        $model = $this->loadModel($id);
        $tstatus_pre = $model->tstatus;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);


        if (isset($_POST['B2bTicketFcode'])) {
            $model->attributes = $_POST['B2bTicketFcode'];
            //@liuchang
            //验证码，F码更改状态为预约成功的时候会关联预约一个体检账号密码
//            $subR_ret = $this->subReservationNum($model->centerid, $model->examdate);
//            if ($subR_ret === JSONHelper::ERROR_NO_SUBCENTER) {
//                $this->redirect(array('error', "note" => "此体检中心没有录入可用人数"));
//            }
            $ret_update = $model->update();

            if ($tstatus_pre == B2bTicket::YUYUE_PROCESSING && $model->tstatus == B2bTicket::YUYUE_CONFIRM && $model->codetype == B2bTicket::FCODERESERVATION){//} || $model->codetype == B2bTicket::FCODERESERVATION)) {
                $meinian_user_ob = B2bMeinianUser::model()->findByAttributes(array("fk_userid" => null));
                if ($meinian_user_ob == null) {
                    $this->redirect(array('error', 'id' => $model->id, "note" => "没有可用的账户"));
                }
                $meinian_user_ob->fk_userid = $model->uid;
                $ret_save = $meinian_user_ob->save();

                if (!$ret_save || !$ret_update){//} || !$subR_ret) {
                    $transaction->rollback();
                    $sms = new SmsService();
                    $sms->send($model->telephone, "尊敬的用户,您预约每年体检日期:{$model->examdate}" .
                        ",由于网络原因预约失败，请您重新预约");
                } else {
                    $transaction->commit();
                    $sms = new SmsService();
                    $sms->send($model->telephone, "尊敬的用户,您预约每年体检日期:{$model->examdate}" .
                        "美年用户名:{$meinian_user_ob->username},密码:{$meinian_user_ob->password}");
                }
            }
            $this->redirect(array('view', 'id' => $model->id));
        }
//        $model->tstatus = B2bTicket::model()->getStatusDesc($model->tstatus);
//        $model->codetype = B2bTicket::model()->getCodetypeDesc($model->codetype);
        $model->activecode = B2bTicket::model()->getActivecodeDesc($model->activecode);

        $this->render('update', array(
            'model' => $model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('B2bTicketFcode');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
    public function actionAdmin()
    {
        $model = new B2bTicketFcode('search');
        $model->unsetAttributes(); // clear any default values

        $tuangoucompany = B2bTuangouCompany::model()->findAllByAttributes(array("type" => B2bTuangouCompany::TUANGOU_CANYU));
        $tuangoumap = array();
        foreach($tuangoucompany as $tuan){
            $tuangoumap[$tuan->id] = $tuan->name;
        }
        $tuangoumap[''] = "全部";

        if (isset($_GET['B2bTicketFcode'])) {
            $ticket = $_GET['B2bTicketFcode'];
            if (isset($ticket['companyid'])) {
                $companyName = $ticket['companyid'];
                $ret = B2bCompany::model()->find("`name` like '%$companyName%'"); //高级搜索中，根据名称搜索内容

                if ($ret != null) {
                    $ticket["companyid"] = $ret->id;
                }
            }

            $model->attributes = $ticket;
        }

        //默认显示"预约处理中，可取消"状态的套餐
        if (!isset($_GET['B2bTicketFcode'])) {
            $ret = B2bTicket::model()->find("`tstatus`=2");
            if ($ret != null) {
                $ticket['tstatus'] = $ret->tstatus;
            }
            $model->attributes = $ticket;
        }
        //获取所有套餐的id和名称组成的数组
        $packages = B2bTicket::model()->getPackages();


        $this->render('admin', array(
            'model' => $model,
            'packages' => $packages,
            "tuangoucompany" => $tuangoumap
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return B2bTicketFcode the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=B2bTicketFcode::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param B2bTicketFcode $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='b2b-ticket-fcode-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionError($note)
    {
        $this->layout = '//layouts/admin/column2';

        $this->render('error', array(
            'note' => $note
        ));
    }
}
