<?php

class OrderController extends Controller {

    private $orderServices = null;

    function __construct($id) {
        parent::__construct($id);
        Yii::app()->user->loginUrl=array("site/centerlogin");
        $this->orderServices = new orderServices();
    }

    public $layout = "//layouts/centeradmin";   //添加布局
    /**
     * 首页列表，新预约订单
     */
    public function actionIndex() {
        $items = array();
        $ret = B2bOrder::model()->findAllByAttributes(array("sub_center_id"=>Yii::app()->user->getCenterId(),"status"=>array(1,8)),
                                            array("order"=>"id desc"));
        
        $max["gedan"] = count($ret)>0?$ret[0]->id:0;
        foreach ($ret as $t) {
            $item["id"] = $t->id;
            $item["order_time"] = $t->order_time;
            $item["sub_center_name"] = $t->sub_center_name;
            $item["customer_company"] = $t->customer_company;
            $item["people_count"] = 1;
            $item["order_type"] = 1;//个单    
            array_push($items, $item);
        }
        $ret = B2bOrderTuan::model()->findAllByAttributes(array("sub_center_id"=>Yii::app()->user->getCenterId(),"statuscode"=>array(1,4)),
                                            array("order"=>"id desc"));
        $max["tuan"] = count($ret)>0?$ret[0]->id:0;
        foreach ($ret as $t) {
            $item["id"] = $t->id;
            $item["order_time"] = $t->order_time;
            $item["sub_center_name"] = $t->sub_center_name;
            $item["customer_company"] = $t->customer_company;
            $item["people_count"] = $t->order_count;
            $item["order_type"] = 2;//团单    
            array_push($items, $item);
        }
        $this->render('index',array("items"=>$items,"max"=>$max));
    }
    /**
     * 团单详情
     */
    public function actionTuandan(){
        if(!isset($_GET["id"])){
            JSONHelper::echoJSON(false,"非法请求");
        }
        $item = B2bOrderTuan::model()->findByAttributes(array("id"=>$_GET["id"],"sub_center_id"=>Yii::app()->user->getCenterId()));
        if($item === null){
            JSONHelper::echoJSON(false,"错误订单信息");
        }
        $ret = explode(",", $item->b2b_order_id);
        $people = B2bOrder::model()->findAllByPk($ret);
        $value = $this->renderPartial("tuandan",array("item"=>$item,"people"=>$people),true);
        JSONHelper::echoJSON(true, $value);
    }
    /**
     * 个单详情
     */
    public function actionGedan(){
        if(!isset($_GET["id"])){
            JSONHelper::echoJSON(false,"非法请求");
        }
        $item = B2bOrder::model()->findByAttributes(array("id"=>$_GET["id"],"sub_center_id"=>Yii::app()->user->getCenterId()));
        if($item === null){
            JSONHelper::echoJSON(false,"错误订单信息");
        }
        $value = $this->renderPartial("gedan",array("item"=>$item),true);
        JSONHelper::echoJSON(true, $value);
    }
    /**
     * 预约单处理
     */
    public function actionProc(){
        if(!isset($_POST["order"])){
            JSONHelper::echoJSON(false, "非法请求");
        }
        $order = $_POST["order"];
        if($order["type"] == 1)//个单
        {
            $model = new B2bOrder();            
            $geren = $model->findByAttributes(array("id"=>$order["id"],"sub_center_id"=>Yii::app()->user->getCenterId()));
            if(null === $geren){
                JSONHelper::echoJSON(false, "非法请求");
            }
            $ret = $model->proc($geren,$order);
            if(true === $ret){
                JSONHelper::echoJSON(true);
            }else{
                JSONHelper::echoJSON(FALSE, $ret);
            }
        }else{
            $model = new B2bOrderTuan();
            $tuan = $model->findByAttributes(array("id"=>$order["id"],"sub_center_id"=>Yii::app()->user->getCenterId()));
            if(null === $tuan){
                JSONHelper::echoJSON(false,"非法请求");
            }
            if($order["op"] == 0){//通过
                $ret = $model->procpass($tuan);
                if($ret === true){
                    JSONHelper::echoJSON(TRUE);
                }else{
                    JSONHelper::echoJSON(false, $ret);
                }
            }elseif($order["op"] == 1){//有反馈
                $t = date_create($order["time"]);
                if($t <  date_create()){
                    JSONHelper::echoJSON(false,"非法日期");
                }
                $tuan->etc = "体检中心的建议日期为：".($t->format("Y-m-d"));
                $tuan->statuscode = 3;
                if($tuan->update() === false){
                    JSONHelper::echoJSON(false,"数据库异常，请稍后再试");
                }
                JSONHelper::echoJSON(true);
            }else{//完全关闭，用户可再次预约
                $ret = $model->cancel($tuan);
                if(true === $ret){
                    JSONHelper::echoJSON(TRUE);
                }
                JSONHelper::echoJSON(FALSE, $ret);
            }
        }
    }

    /*
     * 获取新预约的订单，服务桌面提醒
     * @param sub_center_id 子体检中心id
     */

    public function actionGetneworder() {
        if(!isset($_POST["max"])){
            JSONHelper::echoJSON(FALSE, "非法请求");
        }
        $max = $_POST["max"];        
        $gedan = B2bOrder::model()->getneworder($max["gedan"], Yii::app()->user->getCenterId());
        $tuan = B2bOrderTuan::model()->getneworder($max["tuan"], Yii::app()->user->getCenterId());
        $items = array_merge($tuan,$gedan);
        if(!empty($items)){
            $ret["con"] = $this->renderPartial("getneworder",array("items"=>$items),TRUE);
            $ret["max"] = $max;
            JSONHelper::echoJSON(true,$ret);
        }else{
            JSONHelper::echoJSON(false);
        }
        
        
    }
    /**
     * 订单查询
     */
    public function actionList(){
        $type = isset($_GET["type"])?$_GET["type"]:1;//1为个单2为团单
        if($type == 1){//个单
            $model = new B2bOrder("search");
            if(isset($_GET["order"])){
                $model->unsetAttributes();
                $model->attributes = $_GET['order'];
            }
            $ret = $model->search(Yii::app()->user->getCenterId());
            $this->render("listgeren",array("data"=>$ret));
        }else{
            $model = new B2bOrderTuan("search");
            if(isset($_GET["order"])){
                $model->unsetAttributes();
                $model->statuscode = $_GET['order']["statuscode"];
                $model->customer_company = $_GET['order']["customer_company"];
            }
            $ret = $model->search(Yii::app()->user->getCenterId());
            $this->render("listtuan",array("data"=>$ret));
        }
    }
    /*
     * 更新个单、团单的状态
     * 支持已预约-》已体检
     *    已预约-》取消体检
     * @param $id 订单id
     */

    public function actionUpstatus() {
        if(!isset($_POST["order"])){
            JSONHelper::echoJSON(false,"非法请求");
        }
        $order = $_POST["order"];
        if($order['type'] == 1){//个单
            $ret = B2bOrder::model()->upstatus($order["id"],Yii::app()->user->getCenterID(),$order["status"]);
            if($ret === true){
                JSONHelper::echoJSON(true);
            }else{
                JSONHelper::echoJSON(false,$ret);
            }
        }else{
            $ret = B2bOrderTuan::model()->upstatus($order["id"],Yii::app()->user->getCenterID(),$order["status"]);
            if($ret === true){
                JSONHelper::echoJSON(true);
            }else{
                JSONHelper::echoJSON(false,$ret);
            }
        }
    }

    /**
     * 体检报告管理
     */
    public function actionReport(){
        $this->render("report");
    }
    
    /**
     * 
     *  登陆控制
     */
    public function filters() {
        return array(
            array('auth.filters.AuthFilter'),
        );
    }

}
?>







