<?php

/**
 * 
 * @author joy32812
 *
 * 此为iOS端推送相关代码.收集device token.
 */
class NotificationController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	
	public function actionJoin()
	{
		$data = $_POST;
		if (!isset($data["uid"]) || !isset($data["devicetoken"])) {
			JSONHelper::echoJSON(false, JSONHelper::ERROR_ILLEGAL_REQUEST);
		}
		
		$userid = trim($data["uid"]);
		$token = trim($data["devicetoken"]);
		
		$oldInfo = B2bPushUsers::model()->findByAttributes(array("userid" => $userid, "device_token" => $token));
		
		if ($oldInfo) { //如果该用户和deviceToken已经存在直接返回
			JSONHelper::echoJSON(true, "already there");
		}
		
		
		$pushUser = new B2bPushUsers();
		$pushUser->userid = $userid;
		$pushUser->device_token = $token;

		$ret = $pushUser->save();
		
		JSONHelper::echoJSON($ret, $ret);
	}
	
	public function actionLeave()
	{
		$data = $_POST;
		if (!isset($data["uid"]) || !isset($data["devicetoken"])) {
			JSONHelper::echoJSON(false, JSONHelper::ERROR_ILLEGAL_REQUEST);
		}
		
		$userid = trim($data["uid"]);
		$token = trim($data["devicetoken"]);
		
		$oldInfo = B2bPushUsers::model()->findByAttributes(array("userid" => $userid, "device_token" => $token));
		
		if ($oldInfo) { //如果该用户和deviceToken已经存在直接返回
			$oldInfo->delete();
		}
		
		JSONHelper::echoJSON(true, true);
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	
	
}