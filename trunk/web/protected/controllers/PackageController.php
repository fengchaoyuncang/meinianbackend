<?php

require_once (dirname(__FILE__) . '/../models/UserService.php');

class PackageController extends Controller {

    public $layout = "//layouts/main-website";

    //    public $layout = "//layouts/main-website-frameset";

    /**
     * get the reserved examination of one user
     * Enter description here ...
     */
    public function searchPackage() {
        $user = Yii::app()->user->getUserInfo();
        $arr = array("uid" => $user['userid']);
        $user_ser_ins = new UserService();
        $reserved_package_arr = $user_ser_ins->getMyOrder($arr);

        $this->render('reservelist', array('reserved_package_arr' => $reserved_package_arr));
    }

    public function actionYuyue() {
        $session = Yii::app()->session;
        $session['item_id'] = '1';
        $userSer = new UserService();
        if (isset($_POST["appoint"])) {
            $post = $_POST;
            //            $post["appoint"]["userID"] = Yii::app()->user->getId();
            $result = $userSer->appointment($post);
            if (is_int($result)) {
                if ($result == 400) {
                    $message = "错误，非法请求";
                }
                if ($result == 411) {
                    $message = "提交的时间格式不正确";
                }
                if ($result == 412) {
                    $message = "预约时间不正确";
                }
                if ($result == 413) {
                    $message = "体检中心不存在";
                }
                if ($result == 409) {
                    $message = "用户不存在";
                }
                if ($result == 416) {
                    $message = "不可预约";
                }
                if ($result == 414) {
                    $message = "数据库异常";
                }
                JSONHelper::echoJSON(false, $message);
            }
            JSONHelper::echoJSON(TRUE);
        }
        $arr["uid"] = Yii::app()->user->getId();
        $result = $userSer->getPackageAndCenter($arr);


        $this->render("yuyue", array("tickets" => $result, "userinfo" => Yii::app()->user->getUserInfo()));
    }
    public function actionCancelBooking() {
        $post = $_GET;
        $userSer = new UserService();
        $post["uid"] = Yii::app()->user->getId();
        $result = $userSer->cancelBooking($post);
        if (is_int($result)) {
            JSONHelper::echoErrorCode(false, $result);
        }
        JSONHelper::echoJSON(TRUE);
    }

    public function actionAppointment()
    {
        $post = $_POST;
        $userSer = new UserService();
        $post["uid"] = Yii::app()->user->getId();
            $result = $userSer->appointment($post);
            if (is_int($result)) {
                
                JSONHelper::echoJSON(false, $result);
            }
            JSONHelper::echoJSON(TRUE);
    }
    public function actionYuyuebyac()
    {
        $us = new UserService();
        if(isset($_POST["appoint"])){
            $arr = $_POST;
            $arr["uid"] = Yii::app()->user->getId();
            $ticket = $us->checkTicket($arr);
            if(is_int($ticket)){
                JSONHelper::echoJSON(false,$ticket);
            }
            JSONHelper::echoJSON(true,$ticket);
        }
        if(isset($_GET["ac"])){
            $ac = $_GET["ac"];
            $arr["uid"] = Yii::app()->user->getId();
            $arr["activecode"] = $ac;
            $ticket = $us->getTicketDetails($arr);
            if(is_int($ticket)){
                throw new CHttpException(200,"错误的请求:".$ticket);
            }
        }else{
            $ticket["type"]=B2bTicket::GENERALUSER;
            $ticket["realname"] = "";
            $ticket["identity_no"] = "";
        }
        
        
        $this->render("yuyuebyac",array("ticket"=>$ticket));
    }

    
    /**
     * cancel the package which you had reserved
     * get
     * Enter description here ...
     */
    public function actionPackageCancel() {
        $orderid = $_GET['orderid'];
        $user_ins = new UserService;
        $user_info = Yii::app()->user->getUserInfo();
        $uid = $user_info['userid'];
        $arr = array("uid" => $uid, "orderid" => $orderid);
        $ret = $user_ins->cancelBooking($arr);

        if ($ret == 400) {
            $message = "非法请求";
        }
        if ($ret == 425) {
            $message = "不存在订单";
        }
        if ($ret == 426) {
            $message = "非法订单号，订单和用户不一致";
        }
        if ($ret == 427) {
            $message = "不允许取消订单";
        }
        if ($ret == 414) {
            $message = "数据库异常";
        }
        if ($ret == true) {
            $message = "成功";
        }
        JSONHelper::echoJSON(true, $message);
    }

    /**
     * get the pakage of one unique person
     * Enter description here ...
     */
    public function getPackageByUid() {
        $user = Yii::app()->session['user_info'];
        $arr = array("uid" => $user['userid']);
        $user_ser_ins = new UserService();
        $package_center_arr = $user_ser_ins->getPackageAndCenter($arr);
        $this->render('yuyue', array('package_center_arr' => $package_center_arr));
    }

    /**
     * reversation frontpage
     * Enter description here ...
     */
    public function actionIndex() {
        $this->render("index");
    }

    /**
     *
     * Enter description here ...
     */
    public function actionReservedlist() {
        $this->searchPackage();
    }

    public function actionReservelist() {
        $uinfo = Yii::app()->user->getUserInfo();
        $uservice = new UserService();
        $ret = $uservice->getPackageAndCenter(array("uid" => $uinfo["uid"]));
        if (is_int($ret)) {
            throw new CHttpException(200, "错误的请求");
        }
        $this->render("reserverlist", array("items" => $ret));
    }

    public function actionView() {
        $this->render($_GET["pname"]);
    }
    
    public function filters() {
        return array(
            'accessControl',
        );
    }
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('yuyue', 'yuyuebyac','appointment','cancelBooking'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

}
