<?php
class InfoPapersController extends Controller{
	public $layout = "//layouts/main-website";
	public function actionGetContent(){
		$pk = $_GET['paperid'];
		$infopaper_content = InfoPapers::model()->findByPk($pk);
		//		$infopaper_content = InfoPapers::model()->findAll(array("order"=>"id desc",'limit'=>'1'));
		$infor_ret['title']=$infopaper_content->title;
		$infor_ret['content']=$infopaper_content->content;

		$infopaper_ins = new InfoPapers();
		$info_page_ret = $infopaper_ins->getByPage();

		//		$this->render("infopaperdetail",array("infopaper_list" => $info_page_ret, "infopaper_ret" => $infopaper_content));
		$this->render("infopaperdetail",array("infopaper_list" => $info_page_ret, "infopaper_ret" => $infor_ret));
	}

	public function actionTitleList(){
		$infopaper_ins = new InfoPapers();
		$info_page_ret = $infopaper_ins->getByPage();

		$this->render("titlelist", array("infopaper_ret" => $info_page_ret));
	}
}
?>