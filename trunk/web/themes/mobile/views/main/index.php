<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/index.css" />
<section id="ad">
  <img src="<?= Yii::app()->theme->baseUrl ?>/images/mobile/ad.png"/>
  <div id="close-ad">X</div>
</section>
<div id="frontPic">
  <ul id="adv">
    <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu1.jpg" /></li>
    <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu2.jpg" /></li>
    <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu3.jpg" /></li>
    <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu4.jpg" /></li>
    <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu5.jpg" /></li>
  </ul>
  <div id="frontPicSelector">
    <li select="true" class='frontPicSelectorCircle'><a href="#"></a></li>
    <li class='frontPicSelectorCircle'><a href="#"></a></li>
    <li class='frontPicSelectorCircle'><a href="#"></a></li>
    <li class='frontPicSelectorCircle'><a href="#"></a></li>
    <li class='frontPicSelectorCircle'><a href="#"></a></li>
  </div>
</div>
<section id="process" class="round-wrapper">
  <h1>预约体检流程</h1>
  <ul class="pic">
    <li>
      <img src="<?=Yii::app()->theme->baseUrl?>/images/mobile/following.png" alt=""/>
    </li>
    <li></li>
    <li>
      <img src="<?=Yii::app()->theme->baseUrl?>/images/mobile/category.png" alt=""/>
    </li>
    <li></li>
    <li>
      <img src="<?=Yii::app()->theme->baseUrl?>/images/mobile/write.png" alt=""/>
    </li>
    <li></li>
    <li>
      <img src="<?=Yii::app()->theme->baseUrl?>/images/mobile/serialise.png" alt=""/>
    </li>
    <li></li>
    <li>
      <img src="<?=Yii::app()->theme->baseUrl?>/images/mobile/phone.png" alt=""/>
    </li>
  </ul>
  <ul class="txt">
    <li>注册帐户</li>
    <li>选择体检机构</li>
    <li>填写列表提交订单</li>
    <li>持有效证件体检</li>
    <li>接受体检报告</li>
  </ul>
</section>
<section id="personality" class="clearfix">
  <h1>个性体检套餐</h1>
  <!-- <a class="more">更多 ></a> -->
  <div id="package-wrapper">
    <div class="package">
      <h1><a href="<?=$this->createUrl("package/view",array("pname"=>"suixinka"))?>">遂心卡</a></h1>
      <div class="favorable">优惠价<br>￥<span>260</span></div>
      <div class="reservation">门市价:￥377 <a href='<?=$this->createUrl("package/yuyue")?>'>预&nbsp;&nbsp;约</a></div>
    </div>
    <div class="package">
      <h1><a href="<?=$this->createUrl("package/view",array("pname"=>"shunxinka"))?>">顺心卡</a></h1>
      <div class="favorable">优惠价<br>￥<span>550</span></div>
      <div class="reservation">门市价:￥917 <a href='<?=$this->createUrl("package/yuyue")?>'>预&nbsp;&nbsp;约</a></div>
    </div>
    <div class="package">
      <h1><a href="<?=$this->createUrl("package/view",array("pname"=>"shuxinka"))?>">舒心卡</a></h1>
      <div class="favorable">优惠价<br>￥<span>360</span></div>
      <div class="reservation">门市价:￥562 <a href='<?=$this->createUrl("package/yuyue")?>'>预&nbsp;&nbsp;约</a></div>
    </div>
  </div>
</section>
<section id="other" class="clearfix">
  <h1>其他体检健康管理</h1>
  <a href="index.php?r=main/healthcare" class="more">更多 ></a>
  <div id="other-img-wrapper">
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/fxpg.png" /></a>
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/ydfa.png" /></a>
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/yyfa.png" /></a>
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/xlfa.png" /></a>
  </div>
</section>
<seciton id="city" class="clearfix">
  <h1>覆盖城市</h1>
  <a href="index.php?r=main/centerToCity" class="more">更多 ></a>
  <div id="city-wrapper" class="round-wrapper">
    <table>
      <tr>
        <td>北 京</td><td>河 南</td><td>上 海</td><td>广 东</td><td>内 蒙</td>
      </tr>
      <tr>
        <td>西 藏</td><td>四 川</td><td>哈尔滨</td><td>新 疆</td><td>山 西</td>
      </tr>
      <tr>
        <td>吉 林</td><td>河 北</td><td>湖 南</td><td>辽 宁</td><td>江 苏</td>
      </tr>
    </table>
  </div>
</seciton>
<section id="news" class="clearfix">
  <h1>健康咨讯</h1>
  <a href="index.php?r=infoPapers/titleList" class="more">更多 ></a>
  <div id="disease-wrapper" class="round-wrapper">
    <ul>
      <li>冠心病</li>
      <li>肝病患者的饮食</li>
      <li>脑卒中</li>
      <li>高血脂症</li>
      <li>乳腺自我检查</li>
      <li>远离肿瘤</li>
    </ul>
  </div>
</section>
<section id="league" class="clearfix">
  <h1>合作企业联盟</h1>
  <div id="coop-wrapper">
    <div class="cooperation">
      <div class="slide-prv"></div>
      <div class="slide-nxt"></div>
      <div id="slide-wrapper">
        <div class="cooperation-slide clearfix" style="left: -6px">
          <img src="<?= Yii::app()->baseUrl ?>/images/kehu/联通汽车集团.jpg" alt="联通汽车集团">
          <img src="<?= Yii::app()->baseUrl ?>/images/kehu/中国国际航空公司.jpg" alt="中国国际航空公司">
          <img src="<?= Yii::app()->baseUrl ?>/images/kehu/中英人寿.jpg" alt="中英人寿">
          <img src="<?= Yii::app()->baseUrl ?>/images/kehu/北检.jpg" alt="北检-新创源">
        </div>
      </div>
    </div>
  </div>
</section>

<script>
window.onload = function () {
  $('#ad').slideDown('slow');
}
$(function () {
  $('#close-ad').on('click', function (event) {
    $('#ad').slideUp('slow');
  });
})
var adv_index = 0;
var adv_task;
var adv_height = 130;
$(function(){
  $('#frontPicSelector li').mouseenter(function(){
    var index = $('#frontPicSelector li').index(this);
    showAdv(index);
  });
  showAdv(0);
  $('#frontPic').hover(function(){
    clearInterval(adv_task);
  },function(){
    adv_task = setInterval(function(){
      showAdv(adv_index);
    },2000);
  }).mouseleave();
});
function showAdv(index) {
  $('#adv').animate({'top':-1*adv_height*index - 1},800);
  $('#frontPicSelector li').attr('select','');
  $('#frontPicSelector li').eq(index).attr('select','true');
  adv_index = index + 1;
  if(adv_index == 5) adv_index = 0;
}
$(function () {
  $('.slide-nxt').on('touchstart', function() {
    $('.cooperation-slide').animate({left: '-=50px'}, 'slow', 'linear', arguments.callee);
  });
  $('.slide-nxt').on('touchend', function() {
    $('.cooperation-slide').stop();
  });
  $('.slide-prv').on('touchstart', function() {
    $('.cooperation-slide').animate({left: '+=50px'}, 'slow', 'linear', arguments.callee);
    if(parseInt($('div.cooperation-slide').css('left')) > 0) {
      $('div.cooperation-slide').stop();
    }
  });
  $('.slide-prv').on('touchend', function() {
    $('.cooperation-slide').stop();
  });
  $('#ad img').click(function () {
    window.location = 'http://app.pocdoc.cn';
  });
})
</script>
