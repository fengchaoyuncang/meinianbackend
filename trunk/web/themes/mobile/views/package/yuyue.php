<script src='<?= Yii::app()->baseUrl ?>/js/jquery/cookie/jquery.cookie.js'></script>
<link rel="stylesheet" href="<?= Yii::app()->baseUrl ?>/css/datepicker/glDatePicker.default.css" type="text/css" media="screen">
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/yuyue.css" media="screen" />
<style>
  .qx{
    margin-right: 85px;
    background-color: #fff;
    margin-top: -25px;
  }
  .qx a{
    text-decoration: none;
  }
</style>
<script>
function cancelBooking(tid){
  if(!confirm("确定要取消预约吗?")){
    return;
  }
  $.get('<?=$this->createUrl("package/cancelBooking")?>',
    {"tid":tid},function(ret){ 
      if(ret["status"]!=0){
        alert("预约取消失败，"+ret["data"]);
        return;
      }
      alert("取消成功");
      location.reload();
  });
}
</script>
<div>
  <div class="til">体检预约</div>
  <ul>
    <?php foreach ($tickets as $it):?>
    <li class="it">
      <span class="<?=$it["type"]==B2bTicket::COMPANYUSER?"qiye":""?>"></span>
      <?php if($it["status"] == B2bTicket::UNACTIVATED){?>
      <span class="yuyue">激活码:<br><span><?=$it["activecode"]?></span><span class="op"><a href="<?=$this->createUrl("yuyuebyac",array("ac"=>$it["activecode"]))?>">预&nbsp;约</a></span></span>
      <?php   
        }else{
      ?>
      <div class="yuyue_list">
        <div>套餐:<?=$it["packagename"]?></div>
        <div>体检中心:<?=$it["centers"]["name"]?></div>
        <div>体检人:<?=$it["realname"]?></div>
        <div>体检日期:<?=$it["centers"]["date"]?></div>       
        <?php        
          switch ($it["status"]) {
            case B2bTicket::YUYUE_PROCESSING:
              $tstatus =  "处理中";
              break;
            case B2bTicket::YUYUE_CONFIRM:
              $tstatus =  "预约完成";
              break;
            case B2bTicket::FINISHED:
              $tstatus =  "体检完成";
              break;
            default:
              $tstatus =  "状态异常";
              break;
          }
        ?>
        <div class="xq qx">
            <?=$it["status"]==B2bTicket::YUYUE_PROCESSING?"<a href='javascript:cancelBooking(".$it['tid'].")'>取消</a>":""?>
        </div>
        <div class="op xq">
            <?=$tstatus?>
        </div>
      </div>
      <?php   }?>
    </li>
    <?php endforeach ?>
  </ul>
  <div id="reservation-imme" class="clearfix">
    <h1>企业用户无激活码，或只有激活码的用户，请在此：</h1>
    <div class="op"><a href="<?=$this->createUrl("yuyuebyac")?>">直接预约</a></div>
  </div>
</div>

