<script type="text/javascript">
function reservecancel(orderidv){
//	index.php?r=package/packageCancel/orderid/
	$.post("<?=Yii::app()->createUrl('package/packageCancel/orderid')?>/"+orderidv,
			{},
			function(data){
				alert(data['data']);
				location.href="<?=Yii::app()->createUrl('package/reservedlist')?>";
			},
			"json"
			);
	
}
</script>
<div id="main">
    <div id="reserved_main">
        <?php if (is_array($reserved_package_arr)) { ?>
            <?php foreach ($reserved_package_arr as $it) { ?>
                <div class="reserved_list">
                    <table>
                        <tr>
                            <td><span class="reserved_title">地址：</span></td><td><span class="reserved_content"><?= $it['sub_center_name'] ?></span></td>    
                        </tr>
                        <tr>
                            <td><span class="reserved_title">姓名：</span></td><td><span class="reserved_content"> <?= $it['customer_name'] ?></span></td>
                        </tr>
                        <tr>
                            <td><span class="reserved_title">时间：</span></td><td><span class="reserved_content"> <?= $it['order_time'] ?></span></td>
                        </tr>
                        <tr>
                            <td> <span class="reserved_title">企业类型：</span></td>
                            <td>
                                <span class="reserved_content">
                                    <?php
                                    if ($it['order_type'] == 0) {
                                        echo "企业";
                                    } else {
                                        echo "散户";
                                    }
                                    ?>
                                </span>
                            </td>

                        </tr>
                        <tr>
                            <td><span class="reserved_title">预约状态：</span></td>
                            <td>
                                <span class="reserved_content">
                                    <?php
                                    switch ($it['status']) {
                                        case 1:
                                            echo "新预约";
                                            break;
                                        case 2:
                                            echo '<font color="#2365c7">预约成功</font>';
                                            break;
                                        case 3:
                                            echo '<font color="#2365c7">已体检</font>';
                                            break;
                                        case 4:
                                            echo '<font color="#ff0000">取消体检</font>';
                                            break;
                                        case 5:
                                            echo '<font color="#2365c7">已取得电子版体检报告</font>';
                                            break;
                                        case 6:
                                            echo '<font color="#ff0000">取消预约</font>';
                                            break;
                                        case 7:
                                            echo '<font color="#ff0000">取消预约，有消息反馈</font>';
                                            break;
                                        case 8:
                                            echo '<font color="#2365c7">再次预约</font>';
                                            break;
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php
                if ($it['status'] == 2||$it['status'] == 1) {
                    $str = "<div id='reserve_button'><a href='javascript:reservecancel(".$it['id'].");'>取消预约 </a></div>";
                    echo $str;
                }
                ?>            
    <?php } ?>
<?php } ?>
    </div>    
</div>
