<style>
    .tags{
        top: 130px; 
        overflow: hidden; 
        cursor: pointer; 
        left: 45%; 
        -webkit-transition: opacity; 
        transition: opacity; 
        position: fixed; 
        display: block; 
        opacity: 1;
        color: red;
        z-index: 1;
    }
    input:focus{
        border:1px solid red;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/login.css" media="screen" />
<div class="odd">
  <form id="register" name="enr_form">
  <dl class="odd-interior">
    <dt>Hello,欢迎注册郁金香云健康</dt>
    <dd>
      <input id="telephone" type="text" class="input-text" name="user[telephone]" placeholder="手机号码"  autocomplete="off" />
    </dd>
    <dd>
      <input id="password" type="password" class="input-text" name="user[password]" placeholder="密码" autocomplete="off" />
    </dd>
    <dd>
      <input id="repassword" type="password" class="input-text" name="user[repassword]" placeholder="确认密码" />
    </dd>
<!--    <hr  style="height:1px;border-width:0px;color:gray;background-color:gray;width: 610px;margin-left: 145px;margin-top: 25px;">-->
    <dd>
    <p class="notice">以下信息企业用户必填：</p>
    </dd>
    <dd>
        <input id="name" type="text" class="input-text" name="user[name]" placeholder="姓名" />
    </dd>
    <dd>
        <input id="idcard" type="text" class="input-text" name="user[idcard]" placeholder="身份证" />
    </dd>
    <dd>
        <a href="javascript:registermdencryption()"  class="input-submit">注 册 </a>
      <p>
        <span>已有账号？</span>
        <a href="<?=$this->createUrl("login/index")?>">立即登录</a>
      </p>
    </dd>
  </dl>
  </form>
</div>
<div class="tags"></div>
<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/md5.js"></script>
<script type="text/javascript">
function tags(text){
    $(".tags").text(text);
    setTimeout("$('.tags').text('')", 3000);
}
function registermdencryption(){
    var data = {};
  if($('#telephone').val().trim() == ''){
      tags("用户名不能为空");
      $('#telephone').focus();
      return;
  }
  var pass = $('#password').val().trim();
  var repass = $('#repassword').val().trim();
  if(pass == null||repass == ''){
      tags("密码不能为空");
      $('#password').focus();
      return;
  }
  if(pass != repass){
      tags("两次密码不一致");
      $('#password').focus();
      return;
  }
  
  if($('#name').val().trim() != ''&&$('#idcard').val().trim()!=''){
      if($('#idcard').val().trim().length != 18){
          tags("身份证号码有误");
          $('#idcard').focus();
          return;
      }
      data["user[idcard]"] = $('#idcard').val().trim();
      data["user[realname]"] = $('#name').val().trim();
  }else{
      $('#name').attr("disabled",true);
      $('#idcard').attr("disabled",true);
  }
  var passmd5 = hex_md5(pass);
  
  data["user[username]"] = $('#telephone').val().trim();
  data["user[password]"] = passmd5;
  
  $.post("<?= Yii::app()->baseUrl?>/index.php?r=/login/register",
    data,
    function(ret){
      if(ret['status']!=0){
          tags("注册发生错误："+ret['data']);
          return;
      }
      alert("注册成功");
      location.href="<?= $this->createUrl("login/index") ?>";
    },
    "json"
  );
  $('#name').attr("disabled",false);
  $('#idcard').attr("disabled",false);
  //pass.value = hash;
  //repass.value = rehash;
  //register();
}
function register(){
  $.post("<?=Yii::app()->baseUrl?>/index.php?r=/login/register",
    $("#register").serialize(),
    function(data){
      alert(data['data']);
      location.href="<?=$this->createUrl("main/index")?>";
    },
    "json"
    );
}
function verifyRegister(registerItem){
  document.getElementById("registeritem").value = registerItem;
  $.post("<?=Yii::app()->baseUrl?>/index.php?r=/login/registerVerify",
    $("#register").serialize(),
    function(data){
      if(data != null){
        alert(data['result']);
      }
    },
    "json");
}
function pawregex(str){
  var pattern = "^[a-zA-Z][0-9a-zA-Z]{5,7}$";
  var password = document.getElementById("password");
  var regex = new RegExp(pattern);
  if (regex.test(password.value)){
    verifyRegister(str);
  }else{
    alert("密码格式错误");
  }
}
</script>
