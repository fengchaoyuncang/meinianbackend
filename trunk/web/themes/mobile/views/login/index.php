<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/login.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/typography.css" media="screen" />
<style>
  .h{display:none}
  .breadcrums{float: left;}
</style>
<div class="odd">
  <form id="loginform" name="loginform">
    <dl class="odd-interior">
      <dt>
      Hello,欢迎回到郁金香云健康
      </dt>
      <dd>
        <input id="username" class="input-text" type="text" name="user[username]" placeholder="手机号码" autocomplete="off" />
        <p></p>
      </dd>
      <dd>
        <input id="password" class="input-text" type="password" name="user[password]" placeholder="密码" autocomplete="off" />
      </dd>
      <dd>
        <span class="ind-submit-radio-user left">
          <input type="radio" name="user[usertype]" value="0" checked="checked" />
          <span>企业用户</span>
        </span>
        <span class="ind-submit-radio-manage left">
          <input type="radio" name="user[usertype]" value="1" />
          <span>企业管理员</span>
        </span>
      </dd>
      <dd>
        <a href="javascript:mdencryption();" class="input-submit">登     录</a>
        <p>
          <span>没有账号？</span>
          <a href="<?=$this->createUrl("login/register")?>">立即注册</a>
        </p>
      </dd>
    </dl>
  </form>
</div>

<script type="text/javascript" src="<?= Yii::app()->baseUrl ?>/js/front/md5.js"></script>

<script type="text/javascript">
  function mdencryption() {
    var pass = $('#password').val();
    $('#password').val(hex_md5(pass));
    login_md5();
    $('#password').val(pass);
  }
  function login_md5() {
    var logform = $("#loginform");
    $.post("<?= $this->createUrl('login/verify') ?>",
      $("#loginform").serialize(),
      function(data) {
        if (data["status"] == 0) {
          if (data["data"]["usertype"] == 0) {//普通用户
            location.href = "<?= $this->createUrl('main/index') ?>";
          } else {
            location.href = "<?= $this->createUrl('company/manager/index') ?>";
          }
        } else {
          alert("密码错误");
        }
      }, "json");
  }

  function pawregex(str){
    var pattern = "^[a-zA-Z][a-zA-Z0-9]{5,7}$";
    var regex = new RegExp(pattern);
    return regex.test(str);
  }
</script>