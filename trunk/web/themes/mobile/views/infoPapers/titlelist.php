<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/infopaperdetail.css" media="screen" />
<div id="info-list-article">
  <h1>健康咨询</h1>
  <ul id="infolist">
    <li class="info-list-first"></li>
    <?php
    foreach ($infopaper_ret->getData() as $infopaper){
      if (!empty($infopaper)){
    ?>
      <li>
        <div class="content-wrapper">
          <a href="index.php?r=infoPapers/getContent/paperid/<?=$infopaper->id?>"><?=$infopaper->title?></a>
          <div class="summary">
            <?=$infopaper->abstract?>...
          </div>
        </div>
      </li>
    <?php 
    }}
    ?>
  </ul>
</div>


<?php
  $this->widget('CLinkPager', array(
 'header' => '',
 'firstPageLabel' => '首页',
 'lastPageLabel' => '末页',
 'prevPageLabel' => '上一页',
 'nextPageLabel' => '下一页',
 'pages' => $infopaper_ret->getPagination(),
 'maxButtonCount' => 10
    )
  );
?>
