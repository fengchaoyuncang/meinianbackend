<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8" />
<title>郁金香云健康</title>
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/cssReset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/typography.css" media="screen" />

<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl ?>/js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl ?>/js/jquery/cookie/jquery.cookie.js"></script>
</head>
<body>
<header>
  <div id="nav-drop-down-wrapper">
    <button></button>
  </div>
  <nav>
    <ul>
      <li><a href="index.php?r=main/index">首&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;页</a></li>
      <li><a href="index.php?r=package/yuyue">体检预约</a></li>
      <li><a href="index.php?r=main/centerToCity">体检机构</a></li>
      <li><a href="index.php?r=main/medicalguide">体检须知</a></li>
      <li><a href="index.php?r=main/packagelist">体检套餐</a></li>
      <li><a href="index.php?r=main/aboutus">关于我们</a></li>
    </ul>
  </nav>
  <div id="logo">
    郁金香云健康<br>TULIP e-Healthcare
  </div>
  <div id="log-drop-down-wrapper">
    <button></button>
  </div>
  <div id="log-reg">
    <?php if (Yii::app()->user->getIsGuest() === true) { ?>
      <a href="index.php?r=login/index">登录</a>
      <a href="index.php?r=login/register">注册</a>
    <?php } else { ?>
      <a class="a-quit" href="index.php?r=login/logout">退出登录</a>
    <?php } ?>
  </div>
</header>

<section class="content"><?php echo $content; ?></section>

<footer>
  <a class="a-img" href="<?=$this->createUrl("main/centerToCity")?>">合作体检联盟</a><br/>
  <img src="<?= Yii::app()->theme->baseUrl ?>/images/huiming.png" alt="慧铭体检">
  <img src="<?= Yii::app()->theme->baseUrl ?>/images/aikang.png" alt="爱康国宾">
  <img src="<?= Yii::app()->theme->baseUrl ?>/images/meinian.png" alt="美年大健康">
  <span><a href="<?=$this->createUrl("aboutus")?>">联系我们</a> | <a>商务合作</a></span>
  <span>版权所有：www.pocdoc.cn</span>
  <span>公司地址：北京市海淀区理工科技大厦</span>
</footer>

<script>
$(function () {
  var navDropToggle = 0, logDropToggle = 0;
  $('#nav-drop-down-wrapper button').on('click', function (event) {
    if(1 === logDropToggle) {  //如果log展开了
      $('#log-drop-down-wrapper').toggleClass('clicked');
      $('#log-reg').slideToggle('fast');  //将其关闭
      logDropToggle = 0;  //修改状态
    }
    $('#nav-drop-down-wrapper').toggleClass('clicked');
    $('header nav ul').slideToggle('fast');
    if(0 === navDropToggle) {
      navDropToggle = 1;
    } else {
      navDropToggle = 0;
    }
  });
  $('#log-drop-down-wrapper button').on('click', function (event) {
    if(1 === navDropToggle) {  //如果nav展开了
      $('#nav-drop-down-wrapper').toggleClass('clicked');
      $('header nav ul').slideToggle('fast');  //将其关闭
      navDropToggle = 0;  //修改状态
    }
    $('#log-drop-down-wrapper').toggleClass('clicked');
    $('#log-reg').slideToggle('fast');
    if(0 === logDropToggle) {
      logDropToggle = 1;
    } else {
      logDropToggle = 0;
    }
  })
})
</script>
</body>
</html>