function login($scope){
	$scope.btn = {
		color: '#00aeff',
		open: 'open()',
		display: 'none'
	}

	$scope.open = function(){
		var open = $scope.btn.open=='open()'?1:0;
		$scope.btn = {
			color: open?'#008eff':'#00aeff',
			open: open?'close()':'open()',
			display: open?'block':'none'
		}
	}
}

function plans($scope){
	$scope.plan = {
		a: {
			name: '遂心卡',
			price: '260',
			marketPrice: '377'
		},
		b: {
			name: '顺心卡',
			price: '550',
			marketPrice: '917'
		},
		c: {
			name: '舒心卡',
			price: '360',
			marketPrice: '562'
		}
	}
}

function city($scope){
	$scope.cities = [
		['北　京','西　藏','吉　林'],
		['河　南','四　川','河　北'],
		['上　海','哈尔滨','河　南'],
		['广　东','新　疆','辽　宁'],
		['内　蒙','山　西','江　苏']
	]
}

//function news($scope){
//	$scope.news = [
//		'冠心病',
//		'肝病患者的饮食禁忌',
//		'脑卒中',
//		'高血脂症',
//		'乳腺自我检查',
//		'远离肿瘤'
//	]
//}
function news($scope){
	$scope.news = [
	               {
	                   "id": "4", 
	                   "title": "冠心病"
	               }, 
	               {
	                   "id": "5", 
	                   "title": "脑卒中"
	               }, 
	               {
	                   "id": "6", 
	                   "title": "乳腺自我检查"
	               }, 
	               {
	                   "id": "7", 
	                   "title": "肝病患者的饮食禁忌"
	               }, 
	               {
	                   "id": "8", 
	                   "title": "高血脂症"
	               }
	           ]
}

//function news($scope){
//	$scope.news = <?=$titles?>
//}
function cooperation($scope){
	$scope.list = [
		'images/guohang.png',
		'images/huawei.png',
		'images/baidu.png'
	]
}

function ally($scope){
	$scope.list = [
		'images/huiming.png',
		'images/aikang.png',
		'images/meinian.png'
	]
}

function downloadApp($scope){
	$scope.display = 'block';
	$scope.close = function (){
		$scope.display = 'none';
	}
}