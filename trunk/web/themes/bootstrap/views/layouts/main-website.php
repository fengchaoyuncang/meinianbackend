<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>郁金香云健康</title>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->theme->baseUrl?>/css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->theme->baseUrl?>/css/index.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->theme->baseUrl?>/css/sub_page.css" media="screen" />
<script src="<?=Yii::app()->theme->baseUrl?>/js/angular.min.js"></script>
<script src="<?=Yii::app()->theme->baseUrl?>/js/handle.js"></script>
<script type="text/javascript" src="<?=Yii::app()->theme->baseUrl?>/js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->theme->baseUrl?>/js/jquery/cookie/jquery.cookie.js"></script>
<script>
	$(document).ready(function(){
		$("#headLineMenu a").each(function(index){
		    $(this).attr('id',"MainMenu"+index);
		});
		if(!$.cookie("menu_index")){
			$.cookie("menu_index","MainMenu0");
		}
		$("#headLineMenu a").each(function(){
			
			 if($(this).attr('id')==$.cookie("menu_index")){
			    $(this).attr("select",'true');
			 }else{
			    $(this).attr("select",'false');
			 }
	    });
		$("#headLineMenu a").click(function(){
             $.cookie("menu_index",$(this).attr('id'));
		});
	});
</script>
</head>
<body>

<div class="main">

<div id="headLine">
<div class="mobile">
	<div id="headLineTitle">郁金香云健康<br/>TULIP e-Healthcare</div>
	<div id="headLineDaohang"></div>
	<div id="headLineDenglu" ng-controller="login" style="background-color:{{btn.color}}" ng-click="open()">
	<div id="loginBtn" style="display:{{btn.display}}">
	<div>登录</div>
	<div>注册</div>
	</div>
	</div>
</div>
<div class="desktop">
	<div id="headLineMenu">
		<a href="index.php?r=main/index" select="true">首　　页</a>
		<a href="<?php 
										if(Yii::app()->user->getIsGuest() === true){
											echo "index.php?r=login/pageregister";
										}else{
											echo "index.php?r=package/yuyue";
										}?>">体验预约</a>
		<a href="index.php?r=main/centerlist">体验机构</a>
		<a href="index.php?r=main/medicalguide">体验须知</a>
		<a href="index.php?r=main/packagelist">体验套餐</a>
		<a href="#">关于我们</a>
	</div>
	<div id="headLogin">
		<?php if(Yii::app()->user->getIsGuest() === true){?>
	<a href="index.php?r=login/pageregister">登陆</a> 
	 <a href="index.php?r=login/pageenroll">注册</a>
	<?php }else{?>
		<a href="index.php?r=main/cancellation">退出登录</a>
	<?php }?>
	</div>
</div>
</div>

<div id='header_bottom'></div>

	 <?php echo $content; ?>
	 



<div class="footer">
<div id="footerInner">
	<div class="ally" ng-controller="ally">
	<img ng-repeat="img in list" ng-src="<?=Yii::app()->theme->baseUrl?>/{{img}}" />
	</div>
	<div>联系我们&nbsp;<span>|</span>&nbsp;商务合作</div>
	<div>版权所有：www.pocdoc.cn</div>
	<div>公司地址：北京市海淀区理工科技大厦</div>
</div>
</div>
</body>
</html>