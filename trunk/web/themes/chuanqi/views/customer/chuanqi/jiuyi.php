<div class="sub_content">
	<div class="sub_content_title"> 就医预约 </div>
	<!--专家坐诊_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>专家坐诊</div>
		<div class='kq_item_left'>
			<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/jiuyi1.jpg'>
		</div>
		<div class='kq_item_right'>
			<span>
				专家坐诊就是由专家获得卫生部资质审核通过在册的专家（一般是副主任医师以上的医生）坐诊的门诊。
专家门诊在挂号后不能进行专科处理，只能做退号处理。
一般医院的主任或者副主任未获得“专家”称号，也有按照专家（实际是按职称）来坐诊。
专家出诊和专家坐诊不同的是专家出诊是预约挂号，根据预约来安排出诊日期，而坐诊一般是上班时间内限制一定的诊断人数的。
			</span>
		
		</div>
	</div>
	<!--专家坐诊_end  -->
	<!--专家会诊_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>口腔体检</div>
		<div class='kq_item_left'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/jiuyi2.jpg'>
		</div>
		<div class='kq_item_right'>
			<span>
				专家管理是专家会诊工作的基础。在认真总结经验和查找不足的基础上，北京市安监局本着“节约、规范、高效”的原则，年初出台了《安全生产专家组内部管理制度》，进一步细化和完善了安全生产专家组工作运行机制。制定了严格的专家食宿标准和专家招待、使用预批制度，并严肃了专家会诊纪律，规定专家一律不准私自接受企业吃请、不准在工作时间饮酒等，切实解决了以往专家使用随意性大、专家招待审批不严和个别专家自律意识差等问题，有效降低了行政成本，切实提高了工作效率，在实现规范化运作的同时，塑造了良好的工作形象。
			</span>
			
		
		</div>
	</div>
	<!--专家会诊_end  -->
		<!--绿色就医通道_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>绿色就医通道</div>
		<div class='kq_item_left'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/jiuyi3.jpg'>
			
		</div>
		<div class='kq_item_right'>
			<span>1． 申请绿色通道服务必须至少提前一天服务预约，并且
			缴纳标准收费的30%作为订金 </span>
			<span>2． 如果因为健康管理中心由于各种原因不能按客户要求
			安排就医绿色通道服务，通知客户并退还订金。否则，订
			金不退。 </span>
			<span>3． 健康管理中心为客户做好服务安排并通知客户后，客
			户应交全额费用，然后开始服务。 </span>
			<span>4． 健康管理中心保证服务质量，如果客户对服务质量不
			满意，经调查确实是服务不周的原因，健康管理中心承诺
			服务退款。</span>
		
		</div>
	</div>
	<!--绿色就医通道_end  -->
	
	
</div>