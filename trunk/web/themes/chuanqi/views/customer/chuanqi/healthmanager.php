<div class="sub_content">
	<div class="sub_content_title"> 口腔健康 </div>
	<!--口腔健康服务_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>口腔保健服务</div>
		<div class='kq_item_left'>
			<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/kouqiang1.jpg'>
		</div>
		<div class='kq_item_right'>
			<span>
				最新时尚杂志公布了女性美丽的三要素：健康美丽的肌肤、丰满匀称的身材、洁白闪亮的牙齿。当你开心地笑时，灿烂的笑容绝不能因牙齿而枯萎。所以，一口洁白漂亮的牙齿必不可少。牙
 齿健美的标准是：无齿病、整齐、洁白、无牙周病，口中无异味，能进行正常咀嚼。你或许已有一项或几项不达标，而导致不能开怀大笑、缺少自信。其实，避免牙病的关键在于平常的保健
			</span>
			<span>
时尚理由：电视广告不断地宣称：拥有一口洁白整齐的牙齿，不仅是身体健康的基础，更能使人在社交场合充满自信。吃完工作餐，当别人忙着打“拖拉机”或者结伴逛街的时候，你从容不迫地拿出牙刷来。够时尚、够另类！
			</span>
		
		</div>
	</div>
	<!--口腔健康服务_end  -->
	<!--口腔体检_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>口腔体检</div>
		<div class='kq_item_left'>
			<span>
				第三次口腔流行病学调查显示，我国口腔病患病率达97.6%。专家表示，虽然患病率很高的龋齿已经得到社会的普遍重视，但是牙周病等高发疾病仍容易被忽视。上海市口腔病防治院的李存荣医生指出，解决口腔健康问题的关键在于预防和早期治疗。市口腔病防治院新开了口腔预防“体检”门诊，建议市民：也要定期给牙齿做“体检”。
			</span>
		</div>
		<div class='kq_item_right'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/kouqiang2.jpg'>
			
		
		</div>
	</div>
	<!--口腔体检_end  -->
		<!--洁牙美白_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>洁牙美白</div>
		<div class='kq_item_left'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/kouqiang3.jpg'>
			
		</div>
		<div class='kq_item_right'>
		
			<span>
如今，用超声波洁牙同样受到很多人的关注，许多医学专家都对其交口称赞，超声波洁牙好不好？下文，爱齿尔专家为我们回答这个问题！			
			</span>
			<span>
超声波洁牙好不好？好在哪里？超声波洁牙术就是通过超声波的高频震荡作用，去除牙齿上的牙结石、烟渍和茶斑，可以防止牙龈出血或者牙齿松动的情况，帮助治疗牙周炎等口腔疾病，而且对牙面的损害非常小，洗牙的时间也很短，治疗方便，效果明显，不会伤害到口腔健康。
			</span>
		
		</div>
	</div>
	<!--洁牙美白_end  -->
	<!--牙齿矫正_start  -->
		<div class='kq_item'>
		<div class='kq_item_title'>牙齿矫正</div>
		<div class='kq_item_left'>
			<span>
				牙齿矫正学名叫口腔正畸。英文名orthodontics。
需要矫正的牙齿
需要矫正的牙齿
它通过各种矫正装置来调整颜面骨、牙齿、颌面神经肌肉三者之间的平衡和协调。就是说矫正是调整上下颌骨之间，上下牙齿之间、牙齿与颌骨之间和联系它们的神经肌肉之间不正常的关系。
矫正有时能改善发音，治疗牙周病，骨折，颞下颌关节病，有时还能为缺牙修复，种植牙等其它牙科治疗创造条件。
			</span>
			<span>
齿矫正是通过正畸或外科手术等方法治疗错颌畸形，错颌畸形是指儿童生长发育过程中，由先天因素或后天因素导致的牙齿、颌骨、颅面的畸形，错颌畸形与龋齿、牙周病一起被列为口腔三大常见病，其患病率高达人口的50%以上。
			</span>
		</div>
		<div class='kq_item_right'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/kouqiang4.jpg'>
			
		
		</div>
	</div>
	<!--牙齿矫正_end  -->
			<!--种牙_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>洁牙美白</div>
		<div class='kq_item_left'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/kouqiang5.jpg'>
			
		</div>
		<div class='kq_item_right'>
		
			<span>
种牙疼吗？据植得口腔牙齿种植专家刘明洲介绍。其实，种植牙不算大手术，只要没有严重的心脏病、高血压、糖尿病或其它血液病，能接受拔牙
种牙模型图
种牙模型图[1]
这样的小手术就可以做。疼痛比拔牙要好一点，种好后两三天内会有胀胀的感觉，适应了就好了。 种牙是将人工牙根植入缺牙部位的牙床内，当牙根与牙床长牢后，再在牙根上接一颗逼真的瓷牙，这样，种好的人工牙既牢固又美观，而且结实耐用，被誉为人类的第三副牙齿。
　		
			</span>
			
		
		</div>
	</div>
	<!--种牙_end  -->
	
	
</div>