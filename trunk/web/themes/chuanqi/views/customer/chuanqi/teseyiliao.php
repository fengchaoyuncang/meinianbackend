<div class="sub_content">
	<div class="sub_content_title"> 特色疗养 </div>
	<!--  慢病分类疗养（自选全国各地特色疗养院）_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>慢病分类疗养</div>
		<div class='kq_item_left'>
			<span>
				 充分调动主观能动性，树立革命乐观主义精神与疾病作斗争。坚持功能康复锻炼，可能时参与适量体育锻炼。
纠正不合理的生活方式，依靠自我保健恢复健康。
全面了解和掌握病情，抓住主要矛盾，权衡利弊缓急，制订个例疗养方案，实施有步骤的治疗，康复。
树立整体观念，躯体治疗与心理治疗并重；药物治疗同时，酌情采用理疗、体疗，还可以中西药结合综合治疗。
			</span>
		</div>
		<div class='kq_item_right'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/liaoyang1.jpg'>
			
		
		</div>
	</div>
	<!--  慢病分类疗养（自选全国各地特色疗养院）_end  -->
		<!--康复疗养_start  -->
	<div class='kq_item'>
		<div class='kq_item_title'>康复疗养</div>
		<div class='kq_item_left'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/liaoyang2.jpg'>
			
		</div>
		<div class='kq_item_right'>
		
			<span>
康复医疗的主要目的    延长或恢复老年人日常生活能力，减少卧床不起的患者人数，以减少老年人对家庭和社会的压力。
   老年康复医疗的主要方法   针对功能康复，有医疗体育、物理疗法、作业疗法三大类。		
			</span>
			<span>
康复疗养主要是凭借疗养地所拥有的特殊自然资源条件，先进或传统的医疗保健技艺，优越的设施，将休息度假、健身治病与结合起来的专项活动。具体包括为治疗和康复而进行的气功、针灸、按 摩、矿泉浴、日光浴、森林浴、中草药药疗等多种形式的，以及高山气候疗养、海滨、湖滨度假等。
			</span>
		
		</div>
	</div>
	<!--康复疗养_end  -->
	<!--休闲养生疗养_start  -->
		<div class='kq_item'>
		<div class='kq_item_title'>休闲养生疗养</div>
		<div class='kq_item_left'>
			<span>
				通过怡休闲的方式达到养生的目的，又称为情趣养生，是一种以休闲来调节身心健康、陶冶情操，从而达到益寿延年的养生方法，
一些百岁寿星在日常生活中， 其休闲活动的内容颇为丰富，他们的休闲活动为其长寿打下了坚实的基础。
			</span>
			
		</div>
		<div class='kq_item_right'>
		<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/chuanqi/liaoyang3.jpg'>
			
		
		</div>
	</div>
	<!--休闲养生疗养_end  -->
	
	
</div>