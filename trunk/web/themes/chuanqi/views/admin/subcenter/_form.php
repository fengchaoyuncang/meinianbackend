<?php
/* @var $this SubcenterController */
/* @var $model B2bSubCenter */
/* @var $form CActiveForm */
?>
<style type="text/css">
#allmap {
	width: 80%;
	height: 400px;
	overflow: hidden;
	margin: 0;
}
</style>
<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=74002d29d88880bf997915616e34283b"></script>
<div class="form">

<?php

$form = $this->beginWidget ( 'CActiveForm', array (
		'id' => 'b2b-sub-center-form',
		'enableAjaxValidation' => false 
) );
?>

	<p class="note">
		带有 <span class="required">*</span>项目为必填项
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'center_id'); ?>
		<?php echo $form->textField($model,'center_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'center_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'province'); ?>
		<?php echo $form->textField($model,'province',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'province'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'telephone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gps_lat'); ?>
		<?php echo $form->textField($model,'gps_lat'); ?>
		<?php echo $form->error($model,'gps_lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gps_lon'); ?>
		<?php echo $form->textField($model,'gps_lon'); ?>
		<?php echo $form->error($model,'gps_lon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'baidu_map_url'); ?>
		<?php echo $form->textField($model,'baidu_map_url',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'baidu_map_url'); ?>
	</div>


	用来搜索坐标的体检中心详细地址名称 <input type="text" id="nn" size="50" value=""><br> 
	<input name="搜索经纬度" value="搜索经纬度" type="button"
		onClick="javascript:searchgeo();" style="background-color: #FFC">
	<div id="rr"></div>
	<div id="allmap"></div>
	<script type="text/javascript">

// 百度地图API功能
var map = new BMap.Map("allmap");
var point = new BMap.Point(116.331398,39.897445);
map.centerAndZoom(point,12);
if (document.getElementById("B2bSubCenter_gps_lon").value != "") {
	point = new BMap.Point(document.getElementById("B2bSubCenter_gps_lon").value,
						   document.getElementById("B2bSubCenter_gps_lat").value);
	map.centerAndZoom(point,16);
	map.addOverlay(new BMap.Marker(point));
}
map.centerAndZoom(point,16);
// 创建地址解析器实例
var myGeo = new BMap.Geocoder();
function searchgeo() {
  var nn = document.getElementById("nn").value;
  if (nn=="") {
    nn = document.getElementById("B2bSubCenter_address").value;
    document.getElementById("nn").value = nn;
  }
  // 将地址解析结果显示在地图上,并调整地图视野
  myGeo.getPoint(nn, function(point){
  if (point) {
    map.centerAndZoom(point, 16);
    map.addOverlay(new BMap.Marker(point));
	var res;
	res = document.getElementById("rr");
	// res.value = point.lat + "," + point.lng;
	res.innerHTML = "请在地图中检查坐标是否正确，若有问题，请尝试填写更详细的地址来搜索<br>" +
					"搜索成功后，上面的经纬度信息会被自动添加";
	// 记录GPS B2bSubCenter_gps_lat B2bSubCenter_gps_lon
	document.getElementById("B2bSubCenter_gps_lat").value = point.lat;
	document.getElementById("B2bSubCenter_gps_lon").value = point.lng;
  } else {
	  document.getElementById("rr").innerHTML = "我无法用您提供的名称和城市找到位置坐标，请尝试换个更具体的位置";
  }
}, document.getElementById("B2bSubCenter_city").value);
}

</script>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>
<?php $this->endWidget(); ?>

</div>
<!-- form -->