<?php
/* @var $this SubcenterController */
/* @var $model B2bSubCenter */

$this->breadcrumbs=array(
	'B2b Sub Centers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List B2bSubCenter', 'url'=>array('index')),
	array('label'=>'Manage B2bSubCenter', 'url'=>array('admin')),
);
?>

<h1>Create B2bSubCenter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>