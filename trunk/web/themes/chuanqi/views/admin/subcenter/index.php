<?php
/* @var $this SubcenterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'子体检中心管理',
);

$this->menu=array(
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>子体检中心列表</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
