<?php
/* @var $this BrandController */
/* @var $model B2bBrand */

$this->breadcrumbs=array(
	'品牌管理'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'更新',
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h3>品牌<?php echo $model->name; ?>更新</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>