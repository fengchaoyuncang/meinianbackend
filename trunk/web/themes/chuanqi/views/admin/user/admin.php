<?php
/* @var $this UserController */
/* @var $model AdminUser */

$this->breadcrumbs = array(
    '账号管理' => array('index'),
    '管理',
);

$this->menu = array(
    array('label' => '列表', 'url' => array('index')),
    array('label' => '创建', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#admin-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>账号管理</h3>

<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'admin-user-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'username',
        'alias',
//		'password',
//		'center_id',
//		array("name"=>'center_id',
//                    "value"=>'"($data->center)->name;"'),
        array(
            'name' => 'center_id',
            'value' => '$data->center!=null?$data->center->name:""',
            'sortable' => false,
            'filter' => false,
        ),
//                array(
//                    'name'=>"center_id",
//                    'value'=>'\$data->center_id;',
//                ),
        array("name" => "company_id",
            "value" => '$data->company!=null?$data->company->name:""',
            'sortable' => false,
            'filter' => false,
        ),
        /*
          'email',
          'telphone',
         */
        array(
            'class' => 'CButtonColumn',
            "htmlOptions" => array("style"=>"width:80px;"),
        ),
    ),
));
?>
