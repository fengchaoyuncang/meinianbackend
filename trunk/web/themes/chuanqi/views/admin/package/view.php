<?php
/* @var $this PackageController */
/* @var $model B2bPackage */

$this->breadcrumbs=array(
	'套餐管理'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'您确定要删除此条记录?')),
	array('label'=>'管理', 'url'=>array('admin')),
	array('label'=>'绑定体检中心', 'url'=>array('bindcenters',"id"=>$model->id)),
);
?>

<h3>查看套餐<?php echo $model->name; ?>详情</h3>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'org_price',
		'sale_price',
		'suitable_gender',
		'suitable_married',
		'status',
		'description',
	),
)); ?>
