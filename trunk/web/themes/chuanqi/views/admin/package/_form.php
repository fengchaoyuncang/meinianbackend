<?php
/* @var $this PackageController */
/* @var $model B2bPackage */
/* @var $form CActiveForm */
?>
<style>
input { display: inline; margin-right: 10px; }
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-package-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有 <span class="required">*</span> 项为必填项</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_price'); ?>
		<?php echo $form->textField($model,'org_price'); ?>
		<?php echo $form->error($model,'org_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price'); ?>
		<?php echo $form->error($model,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'suitable_gender'); ?>
		<?php echo $form->radioButtonList($model,'suitable_gender',array("1"=>"男","2"=>"女"),
                        array('style'=>'display:inline;width:50px;',"separator"=>" ",
                            "labelOptions"=>array("style"=>"display:inline;"))); ?>
		<?php echo $form->error($model,'suitable_gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'suitable_married'); ?>
		<?php echo $form->radioButtonList($model,'suitable_married',array("0"=>"通用","1"=>"未婚","2"=>"已婚"),
                        array('style'=>'display:inline;width:50px;',"separator"=>" ",
                            "labelOptions"=>array("style"=>"display:inline;"))); ?>
		<?php echo $form->error($model,'suitable_married'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->