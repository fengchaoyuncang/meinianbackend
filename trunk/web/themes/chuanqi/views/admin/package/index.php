<?php
/* @var $this PackageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'套餐管理',
);

$this->menu=array(
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>套餐列表</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
