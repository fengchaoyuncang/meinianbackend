<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="zh-CN" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <?php Yii::app()->bootstrap->register(); ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
        <style>
            .dropdown-menu{background-color: orange;}
        </style>
    </head>

    <body>

        <div class="body">
        <div class="container" id="page">

            <div class="header">
        	<!--Logo-->
        	<div class="logo">
        		<h1><a href="index.html" title="传奇体育">传奇体育</a></h1>
            </div>
        </div>

            <div id="mainmenu" >
                <?php
                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'pills',
                    'items' => array(
                        array('label'=>'体检中心管理',  'items'=>array(
                            array('label'=>'品牌管理', 'url'=>array('admin/center/index')),
                            array('label'=>'子体检中心管理', 'url'=>array('admin/subcenter/index')),
                        )),
                        array('label' => '套餐管理', 'url' => array('admin/package/index')),
                        array('label' => '公司管理', 'url' => array('admin/company/index')),
                        array('label' => '账户管理', 'items' => array(
                            array('label'=>'管理员账号', 'url'=>array('admin/user/index')),
                            array('label'=>'企业员工导入', 'url'=>array('admin/extuser/import')),
                            array('label'=>'企业员工管理', 'url'=>array('admin/extuser/index')),
                        )),
                        array('label' => '登录', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => '退出 (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                    ),
                ));
                ?>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'homeLink' => CHtml::link('首页', Yii::app()->homeUrl),
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

          

        </div><!-- page -->
        </div>

    </body>
</html>
