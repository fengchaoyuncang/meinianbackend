<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>传奇体育</title>
	<link rel="stylesheet" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/chaunqi_style.css" />
	<!--[if lt IE 9]>  
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>  
	<![endif]--> 
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/css_reset.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/stand_module.css">	
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/index.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/package.css">
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/center.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/image_slider.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/medical_guide.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/center_details.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/appstyle.css">
        <link rel="stylesheet" type="text/css" href="<?=Yii::app()->baseUrl?>/themes/chuanqi/css/wait.css">
     
        <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/themes/chuanqi/js/jquery.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/themes/chuanqi/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/themes/chuanqi/js/input_nature.js"></script>
    <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/themes/chuanqi/js/bxCarousel.js"> </script> 
    <script type="text/javascript" src="<?=Yii::app()->baseUrl?>/themes/chuanqi/js/arrow27.js"></script>
        <?php // Yii::app()->clientScript->registerCoreScript('jquery');?>
    <script>
	$(document).ready(function(){
           
		$(".cq_mainmenu_ul li").each(function(index){
		    $(this).attr('id',"MainMenu"+index);
		});
		if(!$.cookie("menu_index")){
			$.cookie("menu_index","MainMenu0");
		}
		$(".cq_mainmenu_ul li a").each(function(){
			
			 if($(this).attr('id')==$.cookie("menu_index")){
			    $(this).addClass("on");
			 }else{
			    $(this).removeClass("on");
			 }
	    });
		$(".ncq_mainmenu_ul li").click(function(){
             $.cookie("menu_index",$(this).attr('id'));
		});
            /**
             * 菜单栏点击切换效果
             * @author snail
             * @time 2013/11/18 17:30:23
             * 
             */
            /*
            $('li').live('click', function(){
                var item_id = $(this).attr("id");
                var head = 'MainMenu';
                var temp = null;
                for(var i = 0; i < 7; i++){
                    temp = head + i;
                    if(temp == item_id){
                        $("#"+temp).addClass("on");
                    }else{
                        $("#"+temp).removeClass("on");
                    }
                }
            });
            //*/
     
	});
	   //下拉菜单效果

</script>
</head>
<body>
<?php
    $session = Yii::app()->session;
    if(!isset($session['item_id'])){
        $session['item_id'] = '0';
    }
//    echo $session['item_id'];
?>
	<div id='cq_wapper'>
	<div class='mainBox'>
		<div id="cq_main">
		<div id="cq_logo">
			<h1>
				<a title="传奇体育" href="index.php?r=main/index">传奇体育</a>
			</h1>
		</div>
		<div id='cq_mainmenu_login'>
			<ul class="cq_mainmenu_login" id='mainmenu_logined'>
		
			<li class="navbar-line navbar-line-bottom">
				<div class="item_bg">
					<a href="index.php?r=main/index">首页</a>
				</div>
			</li>
			<li class="navbar-line navbar-line-bottom">
				<div class="item_bg">
					<a href="">体检套餐</a>
				</div>
			</li>
        	<li class="navbar-line">
        		<div class="item_bg">
        			<a href="<?=$this->createUrl("yuyuelist")?>">预约管理</a>
        		</div>
        	</li>
       
        
        	<li class="navbar-line">
        		<div class="item_bg">
        			<a href="<?=$this->createUrl("wenjuan")?>">问卷调查</a>
        		</div>	
        	</li>
       
        	<li class="navbar-line">
        		 <div class="item_bg">
        			<a href="<?=$this->createUrl("baogaolist")?>">报告管理</a>
        		</div>
        	</li>
		
</ul>
	
	<div class="landing right right_login" id='zhuce'>
	<?php if(Yii::app()->user->getIsGuest() === true){?>
	<a href="index.php?r=login/pageregister">登陆</a> 
	<span class="landing-border left"></span> <a href="index.php?r=login/pageenroll">注册</a></div>
	<?php }else{?>
		<a href="<?=$this->createUrl("customer/user/baogaolist")?>">个人中心</a><a href="<?=$this->createUrl("main/cancellation")?>">（退出登录）</a></div>
	<?php }?>
		</div>		  
		</div>
		<div id='cq_main_content'>
		<?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'homeLink' => CHtml::link('首页', Yii::app()->homeUrl),
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>
		 <?php echo $content; ?>
		 </div>
	</div>
		<div id='cq_footer'>
			<div id='cq_footer_title'>
				合作体检联盟
			</div>	
			<div id='cq_footer_content'>
				<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/ciming.png'>
				<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/aikang.png'>
				<img src='<?=Yii::app()->baseUrl?>/themes/chuanqi/images/meinian.png'>
			</div>
			<div id='cq_footer_bottom'>
				联系我们|版权声明|隐私条款|常见问题|意见反馈|网站地图
			</div>	
			<div id='cq_footer_en'>
				Copyright © 2011 Air China, All Right Reserved
			</div>	
		</div>
	</div>
</body>
