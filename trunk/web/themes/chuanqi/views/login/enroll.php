		<div class="odd">
			<form id="register" name="enr_form" method="POST" action="index.php?r=login/register">
			<dl class="odd-interior">
				<dt>Hello,欢迎注册郁金香云健康</dt>
				<dd>
					<input id="name" type="text" class="input-text" name="user[name]" value="姓名" onfocus="cls(id)" onblur="res(id),verifyRegister('name')" />
					<p>请输入您的真实姓名</p>
				</dd>
				<dd>
					<input id="idcard" type="text" class="input-text" name="user[idcard]" value="身份证" onfocus="cls(id)" onblur="res(id),verifyRegister('idcard')" />
					<p>以此来预约并获取自己的体检报告</p>
				</dd>
				<dd>
					<input id="telephone" type="text" class="input-text" name="user[telephone]" value="手机号码" onfocus="cls(id)" onblur="res(id),verifyRegister('telephone')" />
					<p>用于每次登录时的用户名</p>
				</dd>
				<dd>
					<input id="password" type="password" class="input-text" name="user[password]" value="密码" onfocus="cls(id)" onblur="res(id),pawregex('password')" />
					<p>请输入以字母开头的6~8位字符</p>
				</dd>
				<dd>
					<input id="repassword" type="password" class="input-text" name="user[repassword]" value="密码" onfocus="cls(id)" onblur="res(id),verifyRegister('repassword')" />
					<p>请再一次输入上面的密码，以此确保密码的正确性</p>
				</dd>
				<dd>
					<input id="email" type="text" class="input-text" name="user[email]" value="邮箱" onfocus="cls(id)" onblur="res(id),verifyRegister('email')" />
					<p class="odd-prompt">请正确常用邮箱地址，以此方便找回密码<br />且体体检报告等资料将默认发到该邮箱</p>
				</dd>
				<input id="registeritem" type="hidden" name="registeritem" value="" />
				<dd>
					<a href="#"  onclick="registermdencryption();" class="input-submit">注	册 </a>
					<p>
						<span>已有账号？</span>
						<a href="index.php?r=login/pageregister">立即登录</a>
					</p>
				</dd>
			</dl>
			</form>
		</div>
<script type="text/javascript" src="<?=Yii::app()->baseUrl?>/js/front/md5.js"></script>
<script type="text/javascript">
function registermdencryption(){
	$pass = document.getElementById('password');
	$repass = document.getElementById('repassword');
 	$hash = hex_md5($pass.value);
 	$rehash = hex_md5($repass.value);
 	$pass.value = $hash;
 	$repass.value = $rehash;

 	register();
}

function register(){
	$.post("<?=Yii::app()->baseUrl?>/index.php?r=/login/register",
			$("#register").serialize(),
			function(data){
				alert(data['data']);
				location.href="<?=$this->createUrl("main/index")?>";
			},
			"json"
			);}
function verifyRegister(registerItem){
	document.getElementById("registeritem").value = registerItem;
	$.post("<?=Yii::app()->baseUrl?>/index.php?r=/login/registerVerify",
			$("#register").serialize(),
			function(data){
				if(data != null){
					alert(data['result']);
				}
			},
			"json");
}
function pawregex(str){
	var pattern = "^[a-zA-Z][0-9a-zA-Z]{5,7}$";

	var password = document.getElementById("password");
	var regex = new RegExp(pattern);
	if (regex.test(password.value)){
		verifyRegister(str);
	}else{
		alert("密码格式错误");
	}
}
</script>