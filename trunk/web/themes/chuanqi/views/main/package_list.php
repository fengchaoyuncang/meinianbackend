<div id="package_banner">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/themes/chuanqi/images/front/package_banner.png"/> 
</div>
<div id="main">   
            <?php        
            foreach ($rets as $center) {
                echo "<div class='package'>
                         <div class='package_list_top'>
                            <div class='package_name_image'></div> 
                            <div class='center_name'>".$center['centerid']->name."</div>
                         </div>";
                foreach ($center['package'] as $package){
                	if (!empty($package->package->name)){
		                echo "<div class='package_list'>
		                        <ul>
		                            <li class='package_list_title'>".$package->package->name."</li>
		                            <li class='package_list_price'>优惠价：<p class='price_yh'>￥".$package->package->sale_price."</p></li>    
		                            <li class='package_list_price'>门市价：￥".$package->package->org_price."</li>    
		                        </ul>
		                      </div>";
		                   
		                }

                }
                echo "</div>";
            }?>
</div>
<div id="footer">
    <?php
    $this->widget('CLinkPager', array(
        'header' => '',
        'firstPageLabel' => '首页',
        'lastPageLabel' => '末页',
        'prevPageLabel' => '上一页',
        'nextPageLabel' => '下一页',
//        'pages' => $rets->getPagination(),
        'maxButtonCount' => 10
            )
    );
    ?>
</div>