function login($scope){
  $scope.btn = {
    color: '#00aeff',
    open: 'open()',
    display: 'none'
  }

  $scope.open = function(){
    var open = $scope.btn.open=='open()'?1:0;
    $scope.btn = {
      color: open?'#008eff':'#00aeff',
      open: open?'close()':'open()',
      display: open?'block':'none'
    }
  }
}

function downloadApp($scope){
  $scope.close = function (){
    $scope.display = 'none';
  }
}