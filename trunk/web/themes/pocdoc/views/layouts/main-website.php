<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8" />
<title>郁金香云健康</title>
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/cssReset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/typography.css" media="screen" />
<!--[if lt IE 9]>
  <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl ?>/js/pie/PIE_IE678.js"></script>
<![endif]-->
<!--[if IE 9]>
  <script type="text/javascript" src="<?= Yii::app()->theme->baseUrl ?>/js/pie/PIE_IE9.js"></script>
<![endif]-->
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl ?>/js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?= Yii::app()->theme->baseUrl ?>/js/jquery/cookie/jquery.cookie.js"></script>
</head>
<body>
<script>
    browser={
        versions:function(){
            var u = navigator.userAgent, app = navigator.appVersion;
            return {         //移动终端浏览器版本信息
                 trident: u.indexOf('Trident') > -1, //IE内核
                presto: u.indexOf('Presto') > -1, //opera内核
                webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
                iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
                iPad: u.indexOf('iPad') > -1, //是否iPad
                webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
            };
         }(),
         language:(navigator.browserLanguage || navigator.language).toLowerCase()
    }
    if (browser.versions.mobile){
            location.href="http://m.pocdoc.cn/index.php";
    }
$(function() {
        
    
  if (window.PIE) {
    $('.rounded').each(function() {
      PIE.attach(this);
    });
  }
});
</script>
<div class="main">
  <div id="headLine">
    <div class="mobile">
      <div id="headLineTitle">郁金香云健康<br/>TULIP e-Healthcare</div>
      <div id="headLineDaohang"></div>
      <div id="headLineDenglu" ng-controller="login" style="background-color:{{btn.color}}" ng-click="open()">
        <div id="loginBtn" style="display:{{btn.display}}">
          <div>登录</div>
          <div>注册</div>
        </div>
      </div>
    </div>
    <div class="desktop">
      <div id="headLineMenu">
        <a href="index.php?r=main/index"><img id="logo" src="<?= Yii::app()->theme->baseUrl ?>/images/logo.png" alt="郁金香云健康"></a>
        <a class="navA" href="index.php?r=main/index" select="true">首　　页</a>
        <a class="navA" href="index.php?r=package/yuyue">体检预约</a>
        <a class="navA" href="index.php?r=main/centerToCity">体检机构</a>
        <a class="navA" href="index.php?r=main/medicalguide">体检须知</a>
        <a class="navA" href="index.php?r=main/packagelist">体检套餐</a>
        <a class="navA" href="index.php?r=main/aboutus">关于我们</a>
      </div>
      <div id="headLogin">
        <?php if (Yii::app()->user->getIsGuest() === true) { ?>
          <a class="a-first" href="index.php?r=login/index">登录</a> 
          <a href="index.php?r=login/register">注册</a>
        <?php } else { ?>
          <a class="a-quit" href="index.php?r=login/logout">退出登录</a>
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="content"><?php echo $content; ?></div>
  
  <div class="footer">
    <div id="footerInner">
      <a class="ally-before" href="<?=$this->createUrl("main/centerToCity")?>">合作体检联盟</a>
      <div class="ally">
        <img src="<?= Yii::app()->theme->baseUrl ?>/images/huiming.png" alt="">
        <img src="<?= Yii::app()->theme->baseUrl ?>/images/aikang.png" alt="">
        <img src="<?= Yii::app()->theme->baseUrl ?>/images/meinian.png" alt="">
      </div>
      <div class="about-us">
        <div><a href="<?=$this->createUrl("aboutus")?>">关于我们</a>&nbsp;&nbsp;Copyright©2014 北京郁香尚医软件技术有限公司版权所有 京ICP备13027130号</div>
      <div>公司地址：北京市海淀区理工科技大厦811室</div>
    </div>
  </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $("#headLineMenu .navA").each(function(index) {
    $(this).attr('id', "MainMenu" + index);
  });
  if (!$.cookie("menu_index")) {
    $.cookie("menu_index", "MainMenu0");
  }
  $("#headLineMenu .navA").each(function() {
    if ($(this).attr('id') == $.cookie("menu_index")) {
      $(this).attr("select", 'true');
    } else {
      $(this).attr("select", 'false');
    }
  });
  $("#headLineMenu .navA").click(function() {
    $.cookie("menu_index", $(this).attr('id'));
  });
  $('#logo').on('click', function (event) {
    $.cookie('menu_index', '');
  });
});
</script>
</body>
</html>