<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/yuyue.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/glDatePicker.css" media="screen" />
<script src='<?= Yii::app()->baseUrl ?>/js/jquery/datapicker/glDatePicker.min.js'></script>
<h3 class="til">预约体检</h3>
<form action="POST" id="appoint">
<table id="user-info">
  <?php
    if($ticket["type"]==B2bTicket::COMPANYUSER){
  ?>
  <tr>
    <td>姓名</td>
    <input class="realname" name="appoint[realname]" value="<?=$ticket["realname"]?>" type="hidden">
    <td><?=$ticket["realname"]?></td>
  </tr>
  <tr>
    <td>证件号码</td>
    <input class="identity_no" name="appoint[identity_no]" value="<?=$ticket["identity_no"]?>" type="hidden">
    <td><?=$ticket["identity_no"]?></td>
  </tr>
  <?php
    }else{
  ?>        
  <tr>
    <td>姓名</td>
    <td><input class="realname" name="appoint[realname]" type="text"></td>
  </tr>
  <tr>
    <td>证件号码</td>
    <td><input class="identity_no" name="appoint[identity_no]" type="text"></td>
  </tr>   
  <?php
  }
  ?>
  <tr>
    <td>手机号码</td>
    <td><input class="telephone" name="appoint[telephone]" type="text"></td>
  </tr> 
  <tr id="appendinfo">
    <td>激活码</td>
    <td><input name="activecode" value="<?=isset($_GET["ac"])?$_GET["ac"]:""?>" type="text"
     <?=isset($_GET["ac"])?"readonly='readonly'":""?>></td>
  </tr>
</table> 

<table id="slecenter" class="clearfix">
    <input id="tid" name="tid" value="" type="hidden" disabled="disabled">
  <tr>
    <td>选择体检套餐</td>
    <td>
      <select name="appoint[packageid]" value="">
      </select>
    </td>
  </tr>
  <tr>
    <td>选择体检中心</td>
    <td>
      <select id="center-selector" name="appoint[subcenterID]">
      
      </select>
    </td>
  </tr>
  <tr>
    <td>地点</td>
    <td id="address">
      
    </td>
  </tr>
  <tr>
    <td>预约日期</td>
    <td>
      <input id="dating-date" name="appoint[date]" value="" type="text">
    </td>
  </tr>
</table>
</form>    
<input id="nxt-btn" class="op nxt" name="activecode" value="下一步" type="button">
<input type="button" name="submit" class="op nxt" value="提&nbsp;交" id="submit-btn" style="display: none">
<script>
$('#nxt-btn').on('click', function(event) {
  if((0 === $('.realname').eq(0).val().length) || (0 === $('.identity_no').eq(0).val().length) || (0 === $('.telephone').eq(0).val().length)) {
    alert('请输入完整信息！');
  } else {
    getPackageAndCenter();
  }
});
function getPackageAndCenter()
{
  var infocheck = false;
  $.post("<?=$this->createUrl("yuyuebyac")?>",$("#appoint").serialize(),function(data){
    if(data["status"] != 0){
      switch(data["status"]){
        case 400:
          alert("非法请求");
          break;
        default:
          alert("错误的信息");
          break;
      }
      // window.location.href = '<?=$this->createUrl("yuyue")?>';
      return;
    }
    var c = $("#slecenter").find("select[name='appoint[subcenterID]']");
    window.tinfo = data["data"]["ticketinfo"];
    
    for(var t = 0; t < tinfo.length; t++) { //初始化下拉菜单
      if(0 === t) {                
        for(var i = 0; i < tinfo[t]['centers'].length; i++) { //添加第一个套餐的中心名
          c.append('<option value="' + tinfo[t]['centers'][i]['id'] + '">' + tinfo[t]['centers'][i]['name'] + '</option>');
          if(0 === i) {
            $('#address').html(tinfo[t]['centers'][i]['address']);
          }
        }
      }
      $('#tid').val(data["data"]["tid"]);
      var p = $("#slecenter").find("select[name='appoint[packageid]']");
      p.append('<option value="' + tinfo[t]['packageid'] + '">' + tinfo[t]['packagename'] + '</option>');
        $("#tid").attr('disabled', false);
    $("#nxt-btn").css('display', 'none');
    $("#submit-btn").css('display', 'block');

    }
  });
}
  // if(false === infocheck) {
  //   alert('无可用套餐，点击返回！');
  //   window.location.href = '<?=$this->createUrl("yuyue")?>';
  // }

$(function () {
  $('#dating-date').glDatePicker({
      onClick: function(target, cell, date, data) {
        target.val(date.getFullYear() + '-' +
                    (date.getMonth()+1) + '-' +
                    date.getDate());

        if(data != null) {
            alert(data.message + '\n' + date);
        }
    }
  });
  $("#center-selector").on('change', function () {
        for(var i = 0; i < tinfo[0]['centers'].length; i++) {
          if(tinfo[0]['centers'][i]['id'] == this.value) {
            $('#address').html(tinfo[0]['centers'][i]['address']);
          }
        }
    });
 $('#submit-btn').on('click', function(event) {
  $.post("<?=$this->createUrl("appointment")?>",$("#appoint").serialize(),function(data){
    if(data["status"] != 0){
        alert("发生错误，错误代码:"+data["data"]);
        return;
      }
      alert("预约成功");
      location.href="<?=$this->createUrl('main/medicalguide')?>";
      return;
  });
});   

})
</script>