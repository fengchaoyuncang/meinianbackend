<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/index.css" media="screen" />
<div id="topLine">
  <div id="downloadApp" ng-controller="downloadApp" style="display:block">
    <div>
      <a href='http://app.pocdoc.cn'><img src='<?=Yii::app()->theme->baseUrl?>/images/xiazaiapp.jpg'></a>
    </div>
    <a href='http://app.pocdoc.cn'><img src='<?=Yii::app()->theme->baseUrl?>/images/download.jpg'></a>
  </div>
  <script>
    /*首页大图翻转效果*/
    //平台、设备和操作系统
    var system ={
    win : false,
    mac : false,
    xll : false
    };
    //检测平台
    var p = navigator.platform;
    system.win = p.indexOf("Win") == 0;
    system.mac = p.indexOf("Mac") == 0;
    system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
    //跳转语句
    if(system.win||system.mac||system.xll){
      var adv_height=230;
      //alert("PC访问");
    }else{
      var adv_height=130;
      // alert("非PC访问");
    }

    var adv_index=0;
    var adv_task;
    $(function(){   
      $('#frontPicSelector li').mouseenter(function(){
        var index = $('#frontPicSelector li').index(this);
        showAdv(index);
      });
      showAdv(0);
      $('#frontPic').hover(function(){
        clearInterval(adv_task);
      },function(){
        adv_task = setInterval(function(){
          showAdv(adv_index); 
        },2000);
      }).mouseleave();
    });
    function showAdv(index) {
      $('#adv').animate({'top':-1*adv_height*index - 1},800);
      $('#frontPicSelector li').attr('select','');
      $('#frontPicSelector li').eq(index).attr('select','true');
      adv_index = index + 1;
      if(adv_index == 5) adv_index = 0;
    }
  </script>
  <div id="frontPic">
    <ul id="adv">
      <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu1.jpg" /></li>
      <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu2.jpg" /></li>
      <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu3.jpg" /></li>
      <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu4.jpg" /></li>
      <li><img class="desktop" src="<?=Yii::app()->theme->baseUrl?>/images/toutu5.jpg" /></li>
    </ul>
  <div id="frontPicSelector">
  <li select="true" class='frontPicSelectorCircle'><a href="#"></a></li>
  <li class='frontPicSelectorCircle'><a href="#"></a></li>
  <li class='frontPicSelectorCircle'><a href="#"></a></li>
  <li class='frontPicSelectorCircle'><a href="#"></a></li>
  <li class='frontPicSelectorCircle'><a href="#"></a></li>
  </div>
</div>
</div>
<div id="oppointment">
<div id="oppointmentTitle">预约体验流程</div>
  <div class="oppointmentProcessOuter">
    <div id="oppointmentFollowing" class="oppointmentProcess">注册<br/>账户</div>
    <div id="oppointmentCategory" class="oppointmentProcess">选择体<br/>检机构</div>
    <div id="oppointmentWrite" class="oppointmentProcess">填写列表<br/>提交订单</div>
    <div id="oppointmentSerialise" class="oppointmentProcess">持有效<br/>证件体检</div>
    <div id="oppointmentPhone" class="oppointmentProcess">接收体<br/>检报告</div>
  </div>
</div>
<div id="mainContent">
<div class="itemTitle">个性体验套餐<!--<a class="more" href="#">更多›</a>--></div>
<div class="plans">
  <div class="plan" color="green">
    <div class="topCube">
      <div class="name"><a class="a-green" href="<?=$this->createUrl("package/view",array("pname"=>"suixinka"))?>">遂心卡</a></div>
    </div>
    <div class="bottomCube">
      <div class="marketPrice">门市价：￥377</div>
      <div class="oppointment">
        <a href='<?=$this->createUrl("package/yuyue")?>'>预 约</a>
      </div>
    </div>
    <div class="price">￥260</div>
  </div>
  <div class="plan" color="blue">
    <div class="topCube">
      <div class="name"><a class="a-blue" href="<?=$this->createUrl("package/view",array("pname"=>"shunxinka"))?>">顺心卡</a></div>
    </div>
    <div class="bottomCube">
    <div class="marketPrice">门市价：￥917</div>
      <div class="oppointment">
        <a href='<?=$this->createUrl("package/yuyue")?>'>预 约</a>
      </div>
    </div>
    <div class="price">￥550</div>
  </div>
  <div class="plan" color="green">
    <div class="topCube">
      <div class="name"><a class="a-green" href="<?=$this->createUrl("package/view",array("pname"=>"shuxinka"))?>">舒心卡</a></div>
    </div>
    <div class="bottomCube">
      <div class="marketPrice">门市价：￥562</div>
      <div class="oppointment">
        <a href='<?=$this->createUrl("package/yuyue")?>'>预 约</a>
      </div>
    </div>
    <div class="price">￥360</div>
  </div>
</div>
<div id="rightSide">
  <div class="itemTitle">其他体检健康管理<a class="more" href="index.php?r=main/healthcare">更多›</a></div>
  <div class="healManage clearfix">
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/fxpg.png" /></a>
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/ydfa.png" /></a>
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/yyfa.png" /></a>
    <a href='index.php?r=main/wait'><img src="<?=Yii::app()->theme->baseUrl?>/images/xlfa.png" /></a>
  </div>
  <div class="itemTitle">覆盖城市<a class="more" href="index.php?r=main/centerToCity">更多›</a></div>
  <div class="city">
    <div class="cityList">
      <div>北　京</div>
      <div>西　藏</div>
      <div>吉　林</div>
    </div>
    <div class="cityList">
      <div>河　南</div>
      <div>四　川</div>
      <div>河　北</div>
    </div>
    <div class="cityList">
      <div>上　海</div>
      <div>哈尔滨</div>
      <div>河　南</div>
    </div>
    <div class="cityList">
      <div>广　东</div>
      <div>新　疆</div>
      <div>辽　宁</div>
    </div>
    <div class="cityList">
      <div>内　蒙</div>
      <div>山　西</div>
      <div>江　苏</div>
    </div>
  </div>
</div>
<div id="leftSide">
  <div class="itemTitle">健康资讯<a class="more" href="<?=$this->createUrl("infoPapers/titleList")?>">更多›</a></div>
  <div class="healNews">
    <?php foreach($titles as $title):?>
    <div ><a href='<?=$this->createUrl("infoPapers/getContent",array("paperid"=>$title->id))?>'><?=  mb_substr($title->title, 0, 10,"utf8")?></a></div>
    <?php endforeach;?>
  </div>
</div>
<div class="itemTitle">我们的客户</div>
  <div class="cooperation">
    <div class="slide-prv"></div>
    <div class="slide-nxt"></div>
    <div id="slide-wrapper">
      <div class="cooperation-slide clearfix" style="left: -6px">
        <img src="<?= Yii::app()->baseUrl ?>/images/kehu/联通汽车集团.jpg" alt="联通汽车集团">
        <img src="<?= Yii::app()->baseUrl ?>/images/kehu/中国国际航空公司.jpg" alt="中国国际航空公司">
        <img src="<?= Yii::app()->baseUrl ?>/images/kehu/中英人寿.jpg" alt="中英人寿">
        <img src="<?= Yii::app()->baseUrl ?>/images/kehu/北检.jpg" alt="北检-新创源">        
      </div>
    </div>
  </div>
</div>
<script>
$(function () {
  $('.slide-nxt').on('mousedown', function() {
    $('.cooperation-slide').animate({left: '-=50px'}, 'slow', 'linear', arguments.callee);
  });
  $('.slide-nxt').on('mouseup', function() {
    $('.cooperation-slide').stop();
  });
  $('.slide-prv').on('mousedown', function() {
    $('.cooperation-slide').animate({left: '+=50px'}, 'slow', 'linear', arguments.callee);
    if(parseInt($('div.cooperation-slide').css('left')) > 0) {
      $('div.cooperation-slide').stop();
    }
  });
  $('.slide-prv').on('mouseup', function() {
    $('.cooperation-slide').stop();
  });
})
</script>

