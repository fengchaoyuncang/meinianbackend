<link rel="stylesheet" type="text/css" href="<?= Yii::app()->theme->baseUrl ?>/css/medical_guide.css" media="screen" />
<div id="medicalguide_main">
  <div id="medical_top"> 
    <div id="guide_left">
      <img src="<?= Yii::app()->baseUrl ?>/images/front/doctor.jpg">
    </div>
    <div id="medical_right">
      <div class="right_item">
        <h2 class="title"><a href="#">体检前</a></h2>     
        <div class="decorate">
          <div class="onset"></div>
          <div class="pregnancy"></div>
        </div>
        <div class="item_main">
          体检前一天请勿进食油腻食物和大量饮酒，体检当日请您禁食禁水，否则将影响体检结果的准确性。
        </div>
      </div>
      <div class="right_item">
        <h2 class="title"><a href="#">体检中</a></h2>     
        <div class="decorate">
          <div class="onset"></div>
          <div class="pregnancy"></div>
        </div>
        <div class="item_main">
          体检当天请携带身份证件；请您根据体检表内容进行各科检查，不要漏检，以免影响对某些疾病的早期发现；若体检者拒绝检查某一项目而造成漏检，责任由体检者本人承担（体检者在该项目栏内签字）；体检当天请不要携带贵重物品及金属物品；
        </div>
      </div>
      <div class="right_item">
        <h2 class="title"><a href="#">体检后</a></h2>     
        <div class="decorate">
          <div class="onset"></div>
          <div class="pregnancy"></div>
        </div>
        <div class="item_main">
          全部检查完毕后，请您将体检单交给服务台方可离开。
        </div>
      </div>
      <div id="right_bottom">
        <div id="right_bottom_top">
          <div id="warning"></div>
          <h2 id="title"><a href="#">特别提示</a></h2>
        </div>
        <div class="item_main">
          糖尿病、高血压、心脏病、哮喘等慢性病患者，请携带平时药物备用，受检日建议不要停药。应主动告知医生身体上的异常情况或所患疾病，这样有助于主检医师对体检结果做出综合判断； 
          女性受检者请注意：妇科常规检查前需排空小便，女性月经期不宜做妇科检查，可在经期干净3天后再做妇科检查。准备近期生育的先生、女士及未成年人，不做X线检查。怀孕或可能已受孕的女士，还应避免妇科及阴式超声检查。做宫颈TCT的女士，体检前不能阴道冲洗，用药，不能过性生活；
          做妇科超声、膀胱超声的客人需憋足尿，做前列腺超声检查的客人请少量憋尿；
        </div>
      </div>
    </div>
  </div>
  <div id="medical_bottom">
    <div class="title_bottom">
      什么是疾病检测？ 
    </div>   
    <div class="bottom_item_main">
      是通过专业医师临床检查、利用先进的仪器设备及实验室检查等方法，及时发现疾病的萌芽状态，进行疾病预警，最大限度地减少疾病造成的危害。
    </div>
    <div class="title_bottom">
      为什么要体检？ 
    </div>   
    <div class="bottom_item_main">
      随着生活水平的提高，我国恶性肿瘤、糖尿病、高血压、动脉硬化、骨质疏松症等慢性疾病患病率迅速上升，已经成为影响我国居民健康的主要问题。世界卫生组织预测，到2015年，2/3的肿瘤都会发生在发展中国家。这种趋势在中国已经开始显现。目前，恶性肿瘤已成为我国居民第一死因，据2010年卫生部统计，我国年均癌症发病人数约180万~200万、死亡人数约为140万~150万，如不加以防控，今后20年中死亡人数将翻一番。心脑血管病、糖尿病等慢性病的患病率迅速增长，有面临失控的危险。我国每年新发脑血管病约有200万例，且以每年8.7%的速率迅速增长；我国糖尿病患病人数已达9240万，糖尿病前期的患病人数高达1.48亿，且明显呈年轻化趋势……
    </div>
  </div>
  </div>
</div>