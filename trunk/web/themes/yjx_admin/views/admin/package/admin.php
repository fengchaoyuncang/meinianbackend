<?php
/* @var $this PackageController */
/* @var $model B2bPackage */

$this->breadcrumbs=array(
	'套餐管理'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#b2b-package-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>套餐管理</h3>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'b2b-package-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'org_price',
		'sale_price',
		'suitable_gender',
		'suitable_married',
		/*
		'status',
		'description',
		'create_at',
		'update_at',
		*/
		array(
			'class'=>'CButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 75px'),
		),
	),
)); ?>
