<?php
/* @var $this SubcenterController */
/* @var $model B2bSubCenter */

$this->breadcrumbs=array(
	'子体检中心管理'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'确定要删除此体检中心？')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h3><?php echo $model->name; ?> 详情</h3>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'center_id',
		'name',
		'country',
		'province',
		'city',
		'address',
		'telephone',
		'description',
		'status',
		'gps_lat',
		'gps_lon',
		'baidu_map_url',
		'create_at',
	),
)); ?>
