<?php
/* @var $this ExtuserController */
/* @var $model B2bExternalUser */

$this->breadcrumbs=array(
	'企业用户管理'=>array('index'),
	'创建',
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>创建用户</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>