<?php
/* @var $this ExtuserController */
/* @var $model B2bExternalUser */

$this->breadcrumbs=array(
	'企业用户管理'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'更新',
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'查看', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>更新<?php echo $model->realname; ?>信息</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>