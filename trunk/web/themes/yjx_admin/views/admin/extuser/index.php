<?php
/* @var $this ExtuserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'企业用户管理',
);

$this->menu=array(
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h3>企业用户</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
