<?php
/* @var $this CompanyController */
/* @var $model B2bCompany */

$this->breadcrumbs=array(
	'公司管理'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'您确定要删除此项?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h1>查看<?php echo $model->name; ?>详情</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'descript',
	),
)); ?>
