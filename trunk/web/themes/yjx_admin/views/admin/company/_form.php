<?php
/* @var $this CompanyController */
/* @var $model B2bCompany */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2b-company-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有 <span class="required">*</span> 项为必填</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descript'); ?>
		<?php echo $form->textField($model,'descript',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'descript'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->