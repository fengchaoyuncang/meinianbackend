<?php
/* @var $this BrandController */
/* @var $model B2bBrand */

$this->breadcrumbs=array(
	'品牌管理'=>array('index'),
	'管理',
);

$this->menu=array(
	array('label'=>'品牌列表', 'url'=>array('index')),
	array('label'=>'新建品牌', 'url'=>array('create')),
);

//Yii::app()->clientScript->registerScript('search', "
//$('.search-button').click(function(){
//	$('.search-form').toggle();
//	return false;
//});
//$('.search-form form').submit(function(){
//	$('#b2b-center-grid').yiiGridView('update', {
//		data: $(this).serialize()
//	});
//	return false;
//});
//");
?>

<h1>品牌管理</h1>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'b2b-center-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'description',
		'create_at',
		/*
		'lastvisit',
		'superuser',
		'username',
		'password',
		'email',
		'update_at',
		*/
		array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'htmlOptions'=>array('style'=>'width: 75px'),
		),
	),
)); ?>
