<?php
/* @var $this UserController */
/* @var $data AdminUser */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username."(".($data->center_id!=0?"体检中心管理员":($data->company_id!=0?"企业管理员":"郁金香系统管理员")).")"); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('center_id')); ?>:</b>
	<?php echo CHtml::encode($data->center_id==0?"":$data->center->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_id')); ?>:</b>
	<?php echo CHtml::encode($data->company_id==0?"":$data->company->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telphone')); ?>:</b>
	<?php echo CHtml::encode($data->telphone); ?>
	<br />

	*/ ?>

</div>