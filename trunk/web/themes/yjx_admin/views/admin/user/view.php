<?php
/* @var $this UserController */
/* @var $model AdminUser */

$this->breadcrumbs=array(
	'账号管理'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'列表', 'url'=>array('index')),
	array('label'=>'创建', 'url'=>array('create')),
	array('label'=>'更新', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'确定要删除此用户?')),
	array('label'=>'管理', 'url'=>array('admin')),
);
?>

<h3>用户<?php echo $model->username; ?>详情</h3>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'alias',
		'center_id',
		'company_id',
		'email',
		'telphone',
	),
)); ?>
