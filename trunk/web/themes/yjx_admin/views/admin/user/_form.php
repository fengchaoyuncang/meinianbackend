<?php
/* @var $this UserController */
/* @var $model AdminUser */
/* @var $form CActiveForm */
?>
<script>
    function searchaddr(){
        $.getJSON("<?= $this->createUrl("admin/subcenter/getsubcenters") ?>", {"city":$("#city").find("option:selected").text(), },
                        function(data) {
                            if (data["status"] == 0) {
                                $("#center").empty();
                                $("<option value=0>" + "请选择"  + "</option>").appendTo($("#center"));
                                for (var i = 0; i < data["data"].length; i++) {
                                    $("<option value="+data["data"][i]["id"]+">" + data["data"][i]["name"] + "</option>").appendTo($("#center"));
                                }
                            }
                        });
            }
        function getcity(){
        $.getJSON("<?= $this->createUrl("admin/subcenter/city") ?>", {"province":$("#province").find("option:selected").text()}, function(data) {
                    if (data["status"] == 0) {
                        $("#city").empty();
                        $("<option value=0>" + "请选择"  + "</option>").appendTo($("#city"));
                        for (var i = 0; i < data["data"].length; i++) {
                            $("<option>" + data["data"][i] + "</option>").appendTo($("#city"));
                        }
                    }
                });
            }
    $(function(){
            $.getJSON("<?=$this->createUrl("admin/subcenter/province")?>", function(data){
                if(data["status"]==0){
                    $("#province").empty();
                    for(var i=0;i<data["data"].length;i++){
                        $("<option>"+data["data"][i]+"</option>").appendTo($("#province"));
                    }
                }
            });
    });
</script>
<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'admin-user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">带有 <span class="required">*</span> 项为必填项</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'center_id'); ?>
		<select id="province" onchange="getcity();" style="width:80px"></select>
		<select id="city" onchange="searchaddr();" style="width:80px"></select>
		<select id="center" name="AdminUser[center_id]"></select>
		<?php echo $form->error($model,'center_id'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'company_id'); ?>
		<?php echo $form->dropDownListRow($model, 'company_id', array(0=>"请选择")+B2bCompany::model()->getAll()); ?>
		<?php echo $form->error($model,'company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telphone'); ?>
		<?php echo $form->textField($model,'telphone',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telphone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->