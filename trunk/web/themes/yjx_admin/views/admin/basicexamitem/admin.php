<?php
/* @var $this BasicexamitemController */
/* @var $model BasicExamItem */

//$this->breadcrumbs=array(
//	'Basic Exam Items(首页)'=>array('index'),
//	'Manage',
//);

$this->menu=array(
//	array('label'=>'列出子项目', 'url'=>array('index')),
	array('label'=>'创建子项目', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#basic-exam-item-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h1>Manage Basic Exam Items</h1>-->
<!---->
<!--<p>-->
<!--You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>-->
<!--or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.-->
<!--</p>-->

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'basic-exam-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
//		'item_id',
		array(
            'name' => 'item_name',
            'value' => '$data->basicexamitemclassify!=null?$data->basicexamitemclassify->item_name:""',
            'sortable' => false,
            'filter' => false,
        ),
        
//		'over_view',
//		'over_reason',
//		'under_reason',
		/*
		'cure_care',
		'expert_advice',
		'value_ranges',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
