<?php
/* @var $this BasicexamitemController */
/* @var $model BasicExamItem */

//$this->breadcrumbs=array(
//	'Basic Exam Items(首页)'=>array('index'),
//	$model->name,
//);

$this->menu=array(
	array('label'=>'列出子项目', 'url'=>array('index')),
	array('label'=>'创建子项目', 'url'=>array('create')),
	array('label'=>'修改子项目', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'删除子项目', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'管理子项目', 'url'=>array('admin')),
);
?>

<!--<h1>View BasicExamItem #<?php echo $model->id; ?></h1>-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'item_id',
//		'over_view',
//		'over_reason',
//		'under_reason',
//		'cure_care',
//		'expert_advice',
//		'value_ranges',
	),
)); ?>
