<?php 
$items[] = array(
    "title"=>"体检中心管理",
    "items"=>array(
        array(
            'title'=>"品牌管理",
            'href'=>$this->createUrl("admin/center/index")
        ),
        array(
            'title'=>"子体检中心管理",
            'href'=>$this->createUrl("admin/subcenter/index")
        ),
        
    )
);
$items[] = array(
    "title"=>"套餐管理",
    "href"=>'admin/package/index'
);
$items[] = array(
    "title"=>"体检项目管理",
    "items"=>array(
        array(
            'title'=>"大项目管理",
            'href'=>$this->createUrl("admin/basicexamitemclassify")
        ),
        array(
            'title'=>"小项目管理",
            'href'=>$this->createUrl("admin/basicexamitem")
        ),
        
    )
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" ng-app=Yjx>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="zh-CN" />

        <!-- blueprint CSS framework
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
         -->
         <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->
        
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="http://code.angularjs.org/1.2.1/angular.min.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/jQMenu.js"></script>
<!--	<script src="<?php echo Yii::app()->theme->baseUrl;?>/js/main.js"></script> -->
        
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/admin/base.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/admin/main.css" />
        
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <script>
        var admin = angular.module('Yjx', []);
 
        admin.controller('Admin', function ($scope) {
          $scope.menus = <?=  json_encode($items)?>;
          $scope.click = function(){
              if(this.menu.href.length > 0){
                  location=this.menu.href;
              }
          };
        });
        
        $(function(){
            angular.element("#nav .menu").powerSwitch();
        });
        </script>
    </head>

    <body>

       <body ng-controller="Admin">
	<div id="container">
		<div id="head">
			
		</div>
		<div id="nav">
			<div class="menu" ng-repeat="menu in menus">
                            <h4 class="item" title="收起" data-rel="navvUl1" ng-click="click()">{{menu.title}}<span class="nav_cor">▶</span></h4>
				<ul>
					<li ng-repeat="item in menu.items"><a href="{{item.href}}">{{item.title}}</a></li>
				</ul>
			</div>
		</div>
		<div id="content">
			测试内容
		</div>
	</div>
</body>
</html>
