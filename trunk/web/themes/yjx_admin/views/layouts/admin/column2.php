<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>
<div class="span-23">
	<div id="content">
		<?php  echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-4 last">
	<div id="sidebar">
	<?php
//	echo "h";exit();
    $this->beginWidget('zii.widgets.CPortlet', array(
    'title'=>'操作',
    ));
    $this->widget('bootstrap.widgets.TbMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
    ));
    $this->endWidget();
    ?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>