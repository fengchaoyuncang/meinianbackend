// 创建一个闭包    
(function($) {    
  // 插件的定义    
  var jMenu = {
  	powerSwitch:function(){
  			var items = $(this).find("li");
  			for(var i=0;i<items.length;i++){
  				items.eq(i).attr("jMenu-item",i);
  			}

  			var item,reg=new RegExp("(?:^| )jMenu-item=([^;]*)(?:;|$)","gi");
    		item=(reg.exec(document.cookie))?(unescape(RegExp["$1"])):null;

    		if(item!=null&&typeof item !== undefined){
    			var ul = $(this).find("li[jMenu-item="+item+"]").parent();//parent.show('slow/400/fast');
    			ul.show('slow/400/fast');
    			ul.prev().find("span").html("▼");
    		}

  		
  		$(this).find("a").click(function(event) {
  			var t = $(this).parent("li").attr("jMenu-item");
  			document.cookie="jMenu-item" + "=" + t;
  			return true;
  		});

  		$(this).find(".item").click(function(event) {
  			/* Act on the event */
                        ;
                        if($(this).parent("div").find("li").length == 0)
                            return;
                        
  			var hidden = $(this).next().is(":hidden");

  			if(hidden){
  				$(this).next().show('slow/400/fast');
  			}else{
  				$(this).next().hide('slow/400/fast');
  			}
  			$(this).find("span").html(!hidden? "▶": "▼");
  		});
  	}
  };
  $.fn.extend(jMenu);

// 闭包结束    
})(jQuery); 


