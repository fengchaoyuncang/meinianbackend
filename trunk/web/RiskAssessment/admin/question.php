<?php
	@header("Content-Type: text/html; charset=utf-8");
	require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if(!($_SESSION['ADMIN'] != "" && $_SESSION['ADMIN'] != '0') ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once '../script/db.class.php';
	$db = new db();
	require_once 'script/functions.php';
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}
	require_once 'script/page.class.php';
//	print_r($_REQUEST);
	
	//添加信息
	$types = intval($_REQUEST['type']);
	$add = $_POST['add'];
	if($add && $_POST['flag'] == '1'){
		$name = $_POST['name'];
		$type = intval($_POST['type']);
		if($name != '' && $type > 0){
			$qid = $db->insert('meinian_questionnaire',array('TITLE'=>$name,'TYPE'=>$type));
			if($qid > 0){
				$msg = "添加问卷成功";
				if($type == 3){//填空
					for($index = 1; $index <= 4; $index++){
						$min_key = 'min'.$index;
						$max_key = 'max'.$index;
						if($_REQUEST[$min_key] != '' && $_REQUEST[$max_key] != ''){
							@$db->insert('meinian_question_options',array('question_id'=>$qid,'min'=>$_REQUEST[$min_key],'max'=>$_REQUEST[$max_key]));
						}
					}
				}else if($types == 1 || $types == 2){
					for($index = 1; $index <= 4; $index++){
						$name_key = 'oname'.$index;
						$des_key = 'des'.$index;
						if($_REQUEST[$name_key] != ''){
							@$db->insert('meinian_question_options',array('question_id'=>$qid,'name'=>$_REQUEST[$name_key],'description'=>$_REQUEST[$des_key]));
						}
					}
				}
				$types = -1;
			}else{
				$msg = "添加问卷失败";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//修改信息
	$update = $_POST['update'];
	if($update && $_POST['flag'] == '1'){
		$id = intval($_POST['id']);
		$name = $_POST['name'];
		$type = intval($_POST['type']);
		if($name != '' && $type != '' && $id > 0){
			@$db->update('meinian_questionnaire',array('TITLE'=>$name,'TYPE'=>$type),array('ID'=>$id));
			$msg = "修改信息成功";
				if($type == 3){//填空
					for($index = 1; $index <= 4; $index++){
						$id_key = 'oid'.$index;
						$min_key = 'min'.$index;
						$max_key = 'max'.$index;
						$oid = intval($_REQUEST[$id_key]);
						if($oid > 0){//更新or删除
							if($_REQUEST[$min_key] != '' && $_REQUEST[$max_key] != ''){
								@$db->update('meinian_question_options',array('min'=>$_REQUEST[$min_key],'max'=>$_REQUEST[$max_key]),array('id'=>$oid));
							}else{
								@$db->del('meinian_question_options',array('id'=>$oid));
							}
						}else{//插入数据or
							if($_REQUEST[$min_key] != '' && $_REQUEST[$max_key] != ''){
								@$db->insert('meinian_question_options',array('question_id'=>$id,'min'=>$_REQUEST[$min_key],'max'=>$_REQUEST[$max_key]));
							}
						}
					}
				}else if($types == 1 || $types == 2){
					$debug = false;
					for($index = 1; $index <= 6; $index++){
						$id_key = 'oid'.$index;
						$oid = intval($_REQUEST[$id_key]);
						$name_key = 'oname'.$index;
						$des_key = 'des'.$index;
						if($oid > 0){
							if($_REQUEST[$name_key] != '' && $oid > 0){
								if($_REQUEST[$name_key] != '' ){
									@$db->update('meinian_question_options',array('name'=>$_REQUEST[$name_key],'description'=>$_REQUEST[$des_key]),array('id'=>$oid),$debug);
								}else{
									@$db->del('meinian_question_options',array('id'=>$oid),$debug);
								}
							}
						}else{
							if($_REQUEST[$name_key] != ''){
								@$db->insert('meinian_question_options',array('question_id'=>$id,'name'=>$_REQUEST[$name_key],'description'=>$_REQUEST[$des_key]),$debug);
							}
						}
					}
				}
			$flag = true;
			$types = -1;
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//点击修改按扭时获取当前信息
	$action = $_REQUEST['action'];
	if($action == 'update'){
		$id = $_REQUEST['id'];
		if($id){
			$row = $db->fetchOne('meinian_questionnaire',array('ID'=>$id));
			if($_REQUEST['flag'] && $types >= 0){
				$types = $row['TYPE'];
			}
			$rows = $db->fetch('meinian_question_options',array('QUESTION_ID'=>$id));
//			print_r($rows);
		}
	//点击删除按扭时执行删除
	}else if($action == 'delete'){
//		$id = $_REQUEST['id'];
//		if($id){
//			if($db->del('meinian_disease_element',array('ID'=>$id))){
//				$msg = "删除信息成功";
//			}
//		}
	}else if($action == 'sub'){//删除子项
		$id = intval($_REQUEST['id']);
		if($id > 0){
//			@$db->del('meinian_element_percentage',array('option_id'=>$id));
//			@$db->del('meinian_question_options',array('id'=>$id));
//			$msg = '操作成功...';
		}
	}

	//修改信息
	$cancel = $_POST['cancel'];
	if($cancel){
		$action = '';
		$id = '';
		$name = '';
		$types = 0;
	}
	if( $types > 0){
		if(!is_array($row)){
			$id = $_REQUEST['id'];
			if($id){
				$row = $db->fetchOne('meinian_questionnaire',array('ID'=>$id));
//				$types = $row['TYPE'];
				$rows = $db->fetch('meinian_question_options',array('QUESTION_ID'=>$id));
	//			print_r($rows);
			}
		}
	}
	if($flag){
		$action = '';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">
<?php 
	if($action == 'adds'){//添加诱因
		$oid = intval($_REQUEST['oid']);
		echo $oid;
		$con = '';
		if($_POST['go']){
			if($oid>0){
				for($i = 0; $i < 100; $i++){
					$tid_key = 'tid'.$i;
					$percent_key = 'percent'.$i;
					$toid_key = 'toid'.$i;
					if($_REQUEST[$toid_key] !== ''){//update or delete
						if($_REQUEST[$tid_key] != '' && $_REQUEST[$percent_key] != ''){//update
							@$db->update('meinian_element_percentage',array('type_id'=>intval($_REQUEST[$tid_key]),'option_id'=>$oid,'percent'=>$_REQUEST[$percent_key]),array('id'=>intval($_REQUEST[$toid_key])));
						}else{
							@$db->update('meinian_element_percentage',array('type_id'=>intval($_REQUEST[$tid_key]),'option_id'=>$oid,'percent'=>'0'),array('id'=>intval($_REQUEST[$toid_key])));
						}
					}else{//insert or nothing
						if($_REQUEST[$tid_key] != '' && $_REQUEST[$percent_key] != ''){//update
							@$db->insert('meinian_element_percentage',array('type_id'=>intval($_REQUEST[$tid_key]),'option_id'=>$oid,'percent'=>$_REQUEST[$percent_key]));
						}else{
							break;
						}
					}
				}
			}
		}
?>
<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">编辑问卷选项对应疾病的诱发风险<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:">
			<td>
				<table width="100%" cellpadding="1" cellspacing="1">
					<tr>
						<td height="30" width="45%" align="right" style="padding-right:10px;color:red;">选择一个问卷选项：</td>
						<td height="30">
						<select name="oid" onchange="this.form.submit();">
							<option value="0">请选择问卷选项：</option>
							<?php
								foreach ($db->doSql('SELECT a.TITLE AS NAMEA,a.ID as IDA,a.TYPE AS TYPE,b.ID as IDB,b.NAME AS NAMEB,b.MIN AS MIN ,b.MAX AS MAX FROM meinian_questionnaire as a,meinian_question_options as b WHERE b.question_id = a.id GROUP BY b.ID DESC ') as $arr){
								?>
								<option value="<?php echo $arr['IDB'];?>" 
								<?php 
								if($oid == $arr['IDB']){
									echo 'selected';
								}
								?>
								><?php 
									$temp = ($arr['TYPE'] == 3)? $arr['MIN'].'-'.$arr['MAX'] : $arr['NAMEB'];
									echo '"'.$arr['NAMEA'].'":'.$temp;
								?>
								</option>
							<?php
//									if($con == ''){
//										$con .= ' element_id like \'%'.','.$arr['IDC'].',%\' ';
//									}else{
//										$con .= ' or element_id like \'%'.','.$arr['IDC'].',%\' ';
//									}
								}
							?>
						</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?php
	if($con == ''){
		$con = '1';
	}
	$sql = 'SELECT A.ID,A.NAME FROM meinian_disease_type AS A where '.$con.' ORDER BY ID DESC ';
//	echo $sql;
	$i = 0;
	?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;text-align:center">
		<tr bgcolor="#ffffff" >
			<td colspan="3" height="40" style="font-size:14px;font-weight:bold;padding-left:5px;">可引起的疾病列表(如果没有疾病，请先关联)</td>
			<td colspan="2" height="40" style="font-size:14px;font-weight:bold;padding-left:5px;"></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th colspan="3" height="40">疾病名称</th>
			<th colspan="2" height="40">风险（%）</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->doSql($sql) as $temp_arrs){
		?>
		<tr bgcolor="<?php if($color){echo '#FFFFFF';}else{echo '#FFFFEE';}?>" >
			<td colspan="3" height="40"><?php echo $temp_arrs['NAME'];?></td>
			<td colspan="2" height="40">
			<?php 
				$temp = '';
				$temp = $db->doSql('SELECT ID,PERCENT FROM meinian_element_percentage WHERE TYPE_ID=\''.$temp_arrs['ID'].'\' AND option_id = '."'$oid'");
			?>
			<input style="width:80px;" name="percent<?php echo $i;?>" value="<?php echo $temp[0]['PERCENT'];?>" />
			<input type="hidden" style="width:80px;" name="tid<?php echo $i;?>" value="<?php echo $temp_arrs['ID']?>"/>
			<input type="hidden" style="width:80px;" name="toid<?php echo $i;?>" value="<?php echo $temp[0]['ID'];?>"/>
			</td>
		</tr>
		<?php
		$color = !$color;
		$i++;
		}
		?>
	</table><br/>
	<div style="text-align:center;">
		<input type="hidden" name="action" value="adds"/>
	<!--	<input type="hidden" name="pid" value="<?php echo $pid;?>" />-->
		<input type="submit" name="go" value="提交" style="background-color:green;cursor:pointer;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="cancel" value="取消" style="background-color:green;cursor:pointer;" />
	</div>
</form>
<br/>
<?php 
	}else{//正常页面
?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				<?php if($action != "update"){?>添加问卷信息<?php }else{?>修改问卷信息<?php }?>
				<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" >
			<td>
				<form id="upload" name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">请先选择问卷类型：</td>
							<td height="30">
<!--								<select name="type" onchange="window.location.href='<?php echo $_SERVER['PHP_SELF'];?>?type='+this.value;">-->
								<select name="type" onchange="document.getElementById('flag').value='0';this.form.submit();">
									<option value="0" >请选择类型</option>
									<option value="1" <?php if($types == 1){echo 'selected';}?> >多选</option>
									<option value="2" <?php if($types == 2){echo 'selected';}?> >单选</option>
									<option value="3" <?php if($types == 3){echo 'selected';}?> >填空</option>
								</select>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">问卷题目：</td>
							<td height="30"><input type="text" name="name" value="<?php if($row['TITLE']){echo $row['TITLE'];}else{echo $_REQUEST['name'];}?>" style="width:250px;"/></td>
						</tr>
						<tbody style="display:<?php if($types==3){echo '';}else{echo 'none';}?>">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">参考范围一：</td>
							<td height="30">
								<input type="hidden" name="oid1" value="<?php if($rows[0]['ID']){ echo $rows[0]['ID'];}else{ echo $_REQUEST['oid1'] ? $_REQUEST['oid1']:0;}?>" ></input>
								<input type="text" name="min1" value="<?php if($rows[0]['MIN']){ echo $rows[0]['MIN'];}else{ echo $_REQUEST['min1'] ? $_REQUEST['min1']:0;}?>" style="width:100px;"/>&nbsp;-&nbsp;
								<input type="text" name="max1" value="<?php if($rows[0]['MAX']){ echo $rows[0]['MAX'];}else{ echo $_REQUEST['max1'];}?>" style="width:100px;"/>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">参考范围二：</td>
							<td height="30">
								<input type="hidden" name="oid2" value="<?php if($rows[1]['ID']){ echo $rows[1]['ID'];}else{ echo $_REQUEST['oid2'] ? $_REQUEST['oid2']:0;}?>" ></input>
								<input type="text" name="min2" value="<?php if($rows[1]['MIN']){ echo $rows[1]['MIN'];}else{ echo $_REQUEST['min2'];}?>" style="width:100px;"/>&nbsp;-&nbsp;
								<input type="text" name="max2" value="<?php if($rows[1]['MAX']){ echo $rows[1]['MAX'];}else{ echo $_REQUEST['max2'];}?>" style="width:100px;"/>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">参考范围三：</td>
							<td height="30">
								<input type="hidden" name="oid3" value="<?php if($rows[2]['ID']){ echo $rows[2]['ID'];}else{ echo $_REQUEST['oid3'] ? $_REQUEST['oid3']:0;}?>" ></input>
								<input type="text" name="min3" value="<?php if($rows[2]['MIN']){ echo $rows[2]['MIN'];}else{ echo $_REQUEST['min3'];}?>" style="width:100px;"/>&nbsp;-&nbsp;
								<input type="text" name="max3" value="<?php if($rows[2]['MAX']){ echo $rows[2]['MAX'];}else{ echo $_REQUEST['max3'];}?>" style="width:100px;"/>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">参考范围四：</td>
							<td height="30">
								<input type="hidden" name="oid4" value="<?php if($rows[3]['ID']){ echo $rows[3]['ID'];}else{ echo $_REQUEST['oid4'] ? $_REQUEST['oid4']:0;}?>" ></input>
								<input type="text" name="min4" value="<?php if($rows[3]['MIN']){ echo $rows[3]['MIN'];}else{ echo $_REQUEST['min4'];}?>" style="width:100px;"/>&nbsp;-&nbsp;
								<input type="text" name="max4" value="<?php if($rows[3]['MAX']){ echo $rows[3]['MAX'];}else{ echo $_REQUEST['max4'];}?>" style="width:100px;"/>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">参考范围五：</td>
							<td height="30">
								<input type="hidden" name="oid5" value="<?php if($rows[4]['ID']){ echo $rows[4]['ID'];}else{ echo $_REQUEST['oid5'] ? $_REQUEST['oid5']:0;}?>" ></input>
								<input type="text" name="min5" value="<?php if($rows[4]['MIN']){ echo $rows[4]['MIN'];}else{ echo $_REQUEST['min5'];}?>" style="width:100px;"/>&nbsp;-&nbsp;
								<input type="text" name="max5" value="<?php if($rows[4]['MAX']){ echo $rows[4]['MAX'];}else{ echo $_REQUEST['max5'];}?>" style="width:100px;"/>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">参考范围六：</td>
							<td height="30">
								<input type="hidden" name="oid6" value="<?php if($rows[5]['ID']){ echo $rows[5]['ID'];}else{ echo $_REQUEST['oid6'] ? $_REQUEST['oid6']:0;}?>" ></input>
								<input type="text" name="min6" value="<?php if($rows[5]['MIN']){ echo $rows[5]['MIN'];}else{ echo $_REQUEST['min6'];}?>" style="width:100px;"/>&nbsp;-&nbsp;
								<input type="text" name="max6" value="<?php if($rows[5]['MAX']){ echo $rows[5]['MAX'];}else{ echo $_REQUEST['max6'];}?>" style="width:100px;"/>
							</td>
						</tr>
						</tbody>
						<tbody style="display:<?php if($types == 1 || $types == 2){echo '';}else{echo 'none';}?>">
						
						<tr ><td><span style="margin-left:200px;">选项一：</span></td></tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项名称</td>
							<td height="30">
								<input type="text" name="oname1" value="<?php if($rows[0]['NAME']){ echo $rows[0]['NAME'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项描述</td>
							<td height="30">
								<input type="text" name="des1" value="<?php if($rows[0]['DESCRIPTION']){ echo $rows[0]['DESCRIPTION'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr ><td><span style="margin-left:200px;">选项二：</span></td></tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项名称</td>
							<td height="30">
								<input type="text" name="oname2" value="<?php if($rows[1]['NAME']){ echo $rows[1]['NAME'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项描述</td>
							<td height="30">
								<input type="text" name="des2" value="<?php if($rows[1]['DESCRIPTION']){ echo $rows[1]['DESCRIPTION'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						
						<tr ><td><span style="margin-left:200px;">选项三：</span></td></tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项名称</td>
							<td height="30">
								<input type="text" name="oname3" value="<?php if($rows[2]['NAME']){ echo $rows[2]['NAME'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项描述</td>
							<td height="30">
								<input type="text" name="des3" value="<?php if($rows[2]['DESCRIPTION']){ echo $rows[2]['DESCRIPTION'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						
						<tr ><td><span style="margin-left:200px;">选项四：</span></td></tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项名称</td>
							<td height="30">
								<input type="text" name="oname4" value="<?php if($rows[3]['NAME']){ echo $rows[3]['NAME'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项描述</td>
							<td height="30">
								<input type="text" name="des4" value="<?php if($rows[3]['DESCRIPTION']){ echo $rows[3]['DESCRIPTION'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						
						<tr ><td><span style="margin-left:200px;">选项五：</span></td></tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项名称</td>
							<td height="30">
								<input type="text" name="oname5" value="<?php if($rows[4]['NAME']){ echo $rows[4]['NAME'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项描述</td>
							<td height="30">
								<input type="text" name="des5" value="<?php if($rows[4]['DESCRIPTION']){ echo $rows[4]['DESCRIPTION'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						
						<tr ><td><span style="margin-left:200px;">选项六：</span></td></tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项名称</td>
							<td height="30">
								<input type="text" name="oname6" value="<?php if($rows[5]['NAME']){ echo $rows[5]['NAME'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">选项描述</td>
							<td height="30">
								<input type="text" name="des6" value="<?php if($rows[5]['DESCRIPTION']){ echo $rows[5]['DESCRIPTION'];}?>" style="width:250px;" ></input>
							</td>
						</tr>
						</tbody>
						<tr>
							<td colspan="2" height="30" align="center">
							<input name="id" value="<?php echo $id;?>" type="hidden"></input>
							<input type="hidden" name="action" value="<?php echo $action;?>"/>
							<?php if($action == "update"){?>
								<input type="hidden" id="flag" name="flag" value="1"/>
								<input type="hidden" name="id" value="<?php echo $id;?>"/>
								<input type="submit" name="update" value="修改" style="background-color:#ffffee;cursor:pointer;" onclick="document.getElementById('flag').value='1';return true;"/>
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							<?php }else{?>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;"  onclick="document.getElementById('flag').value='1';return true;"/>
							<?php }?>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
	<?php 
		$count_sql = "SELECT COUNT(ID) AS NUM FROM meinian_questionnaire ";
		foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
		@$pg = $_REQUEST['pg'];//当前页数
		@$showrows = $_REQUEST['showrows'];//当前每页显示行数
		$show = ($showrows != "" && $showrows != 0) ? $showrows : 20;//如没有当前显示行数则默认成配置文件中设置的行数
		$page = new page($temp['NUM'],$show,$pg,$param);
		$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	?>
	<div class="pages" style="float:right;"><?php $page->showPage();//显示分页信息?></div>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;">
		<tr bgcolor="#ffffff" >
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">诱因信息列表</td>
			<td colspan="5" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;" ></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th height="30" width="30">ID</th>
			<th>问卷名称</th>
			<th>问卷类型</th>
			<th colspan="2">操作</th>
		</tr>
		<?php
		$color = flase;
	
		foreach ($db->fetch('meinian_questionnaire','','ID DESC'.$limit) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo $arr['ID'];?></td>
			<td onclick="setDisplay('<?php echo 'row'.$arr['ID'];?>')"><?php echo $arr['TITLE'];?></td>
			<td ><?php echo getQuestionType($arr['TYPE']);?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=update&id=<?php echo $arr['ID'];?>&flag=1">修改</a></td>
<!--			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=add&eid=<?php echo $arr['ID'];?>">添加问卷</a></td>-->
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=delete&id=<?php echo $arr['ID'];?>" onclick="return cfmDel();">删除</a></td>
		</tr>
		<tbody id="row<?php echo $arr['ID'];?>" style="display:none;">
		<?php 
			$i = 1;
			foreach ($db->doSql('SELECT a.* FROM meinian_question_options as a WHERE a.question_id = \''.$arr['ID'].'\'') as $arr1){
				if($arr['TYPE'] == 3){//填空
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo '&nbsp;&nbsp'.$arr1['ID'];?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;范围<?php echo ' '.$i++?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $arr1['MIN'].' - '.$arr1['MAX'];?></td>
<!--			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $arr1['MAX'];?></td>-->
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=adds&qid=<?php echo $arr['ID'];?>&oid=<?php echo $arr1['ID'];?>">诱发几率</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=sub&qid=<?php echo $arr['ID'];?>&oid=<?php echo $arr1['ID'];?>" onclick="return cfmSub('<?php echo $arr['NAME'];?>','<?php echo $arr1['NAME'];?>');">删除</a></td>
		</tr>
		<?php 
				}else{//选择
					?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo '&nbsp;&nbsp'.$arr1['ID'];?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;选项<?php echo ' '.$i++?></td>
			<td title="<?php echo $arr1['DESCRIPTION'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $arr1['NAME'];?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=adds&qid=<?php echo $arr['ID'];?>&oid=<?php echo $arr1['ID'];?>">诱发几率</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=sub&qid=<?php echo $arr['ID'];?>&oid=<?php echo $arr1['ID'];?>" onclick="return cfmSub('<?php echo $arr['NAME'];?>','<?php echo $arr1['NAME'];?>');">删除</a></td>
		</tr>
					<?php 
				}
			}
		?>
		</tbody>
		<?php
		$color = !$color;
		}
		?>
	</table>
<?php }?>
</div>
</div>
</div>
<?php require_once('footer.php');?>

</body>
</html>


