<?php
@header("Content-Type: text/html; charset=utf-8");
require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if(!($_SESSION['ADMIN'] != "" && $_SESSION['ADMIN'] != '0') ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once 'script/page.class.php';
	require_once '../script/db.class.php';
	$db = new db();
	require_once 'script/functions.php';
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}
	//添加信息
function gets ($temp ,$id){
	$flag = false;
	if(is_array($temp)){
		foreach($temp as $key){
			//echo $key['ROLE_ID']." = ".$id;
			if($key['PERMISSION_ID'] == $id){
				$flag = true;
				break;
			}
		}
	}
	return $flag;
}
	$add = $_POST['add'];
	if($add){
		$name = $_POST['name'];
		$comments = $_POST['comments'];
		if($name != ""){
			if($db->insert('meinian_admin_roles',array('NAME'=>$name,'COMMENTS'=>$comments))){
				$msg = "添加信息成功";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//修改信息
	$update = $_POST['update'];
	if($update){
		$id = $_POST['id'];
		$name = $_POST['name'];
		$comments = $_POST['comments'];
		if($name != ""){
			if($db->update('meinian_admin_roles',array('NAME'=>$name,'COMMENTS'=>$comments),array('ID'=>$id))){
				$msg = "修改信息成功";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//点击修改按扭时获取当前信息
	$action = $_REQUEST['action'];
	if($action == "update"){
		$id = $_REQUEST['id'];
		if($id){
			$row = $db->fetchOne('meinian_admin_roles',array('ID'=>$id));
		}
	//点击删除按扭时执行删除
	}else if($action == "delete"){
		$id = $_REQUEST['id'];
		if($id){
			$temp_num = $db->doSql("SELECT COUNT(ID) AS NUM FROM meinian_admin_user_role WHERE ROLE_ID = '$id'");
			if($temp_num['NUM'] > 0){
				if($db->del('meinian_admin_roles',array('ID'=>$id))){
					$msg = "删除信息成功";
				}
			}else{
				$msg = "该角色正在被用户使用，不能删除...";
			}
		}
	}

	//修改信息
	$cancel = $_POST['cancel'];
	if($cancel){
		$action = '';
		$id = '';
		$name = '';
		$parent = '';
	}
	if($_POST['edit']){
		$id = intval($_POST['id']);
		$option = $_POST['option'];
		@$db->del('meinian_admin_role_permission',array('ROLE_ID'=>$id));
		if(is_array($option)){
			foreach($option as $key){
				@$db->insert('meinian_admin_role_permission',array('ROLE_ID'=>$id,'PERMISSION_ID'=>$key));
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">
<?php
	if($action == "edit"){
?>
		<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				修改权限
				<!--
					<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php //echo $msg;?></span>
				 -->
			</td>
		</tr>
		<tr id="ibody" >
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" >
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">角色名称：</td>
							<td height="30"><span style="width:250px;" ><?php echo $_REQUEST["name"];?></span></td>
						</tr>
						<!--  -->
						<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;text-align:center;" >
							<tr bgcolor="#ffffff">
								<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">权限列表</td>
								<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;"></td>
							</tr>
							<tr bgcolor="#ffffee">
								<th height="30" style="width:5px;" ><input name="all" id="all" type="checkbox" value="yes" style="width:10px;" title="全选" onclick= "checkAll()" ></input></th>
								<th >ID</th>
								<th>权限名称</th>
								<th>URL</th>
							</tr>
							<?php
							$temps = $db->doSql("SELECT PERMISSION_ID FROM meinian_admin_role_permission WHERE ROLE_ID = '".$_REQUEST['id']."'");
							//print_r($temps);
							$color = flase;
							$sql = "SELECT * FROM meinian_admin_permission WHERE PARENT > '0' ORDER BY ID DESC ";
							foreach ($db->doSql($sql) as $arr){
							?>
							<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
								<td height="25" style="width:5px;" ><input name="option[]" type="checkbox" value="<?php echo $arr['ID'];?>" style="width:10px;" <?php if(gets($temps ,$arr['ID'])){ echo "checked='checked'";} ?> /></td>
								<td><?php echo $arr['ID'];?></td>
								<td><?php echo $arr['NAME'];?></td>
								<td><?php echo $arr['URL'];?></td>
							</tr>
							<?php
							$color = !$color;
							}
							?>
						</table>
						<!--  -->
						<tr>
							<td colspan="2" height="30" align="center">
								<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>"/>
								<input type="hidden" name="pg" value="<?php echo $_REQUEST['pg'];?>"/>
								<input type="hidden" name="showrows" value="<?php echo $_REQUEST['showrows'];?>"/>
								<input type="submit" name="edit" value="提交" style="background-color:#ffffee;cursor:pointer;" />
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
		</table><br/>
<?php
	}else{
?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				<?php if($action != "update"){?>添加角色信息<?php }else{?>修改角色信息<?php }?>
				<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:">
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">角色名称：</td>
							<td height="30"><input type="text" name="name" value="<?php echo $row['NAME'];?>" style="width:250px;"/></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">角色描述：</td>
							<td height="30"><input type="text" name="comments" value="<?php echo $row['COMMENTS'];?>" style="width:250px;"/></td>
						</tr>
						<tr>
							<td colspan="2" height="30" align="center">
							<?php if($action == "update"){?>
								<input type="hidden" name="id" value="<?php echo $id;?>"/>
								<input type="submit" name="update" value="修改" style="background-color:#ffffee;cursor:pointer;" />
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							<?php }else{?>
								<input type="hidden" name="action" value="<?php echo $action;?>"/>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;" />
							<?php }?>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>

	<?php
	}
	$count_sql = "SELECT COUNT(ID) AS NUM FROM meinian_admin_roles ";
	foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
	@$pg = $_REQUEST['pg'];//当前页数
	@$showrows = $_REQUEST['showrows'];//当前每页显示行数
	$show = ($showrows != "" && $showrows != 0) ? $showrows : 30;//如没有当前显示行数则默认成配置文件中设置的行数
	$page = new page($temp['NUM'],$show,$pg,$param);
	$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	$sql = "SELECT * FROM meinian_admin_roles ORDER BY ID DESC ".$limit;
	?>
	<div class="pages" style="float:right;"><?php $page->showPage();//显示分页信息?></div>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;text-align:center">
		<tr bgcolor="#ffffff" >
			<td colspan="4" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">角色信息列表</td>
			<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;"></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th height="30">ID</th>
			<th>名称</th>
			<th>描述</th>
			<th>权限</th>
			<th colspan="3" >操作</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->doSql($sql) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo '#FFFFFF';}else{echo '#FFFFEE';}?>">
			<td height="25"><?php echo $arr['ID'];?></td>
			<td><?php echo $arr['NAME'];?></td>
			<td><?php echo $arr['COMMENTS'];?></td>
			<?php 
				$str = '';
				$str_con = '';
				$i = 0 ;
				foreach($db->doSql("SELECT A.NAME FROM meinian_admin_permission AS A ,meinian_admin_role_permission AS B WHERE B.ROLE_ID = '".$arr['ID']."' AND B.PERMISSION_ID = A.ID ") as $temp_role){
					if($str != ''){
						$str .= ','.$temp_role['NAME'];
					}else{ 
						$str = $temp_role['NAME'];
					} 
					if($i++ < 5){
						if($str_con != ''){
							$str_con .= ','.$temp_role['NAME'];
						}else{ 
							$str_con = $temp_role['NAME'];
						} 
					}
				}
			?>
			<td title="<?php if($str != ''){ echo $str; }else{ echo '无';}?>"><?php if($str_con != ''){ echo $str_con; }else{ echo '无';}?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=update&id=<?php echo $arr['ID'];?>">修改</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=delete&id=<?php echo $arr['ID'];?>" onclick="return cfmDel();">删除</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=edit&id=<?php echo $arr['ID'];?>&name=<?php echo $arr['NAME'];?>" >编辑权限</a></td>
		</tr>
		<?php
		$color = !$color;
		}
		?>
	</table>
</div>
</div>
</div>
<?php require_once('footer.php');?>

</body>
</html>




