<?php
	@header("Content-Type: text/html; charset=utf-8");
	require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if(!($_SESSION['ADMIN'] != "" && $_SESSION['ADMIN'] != '0') ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once '../script/db.class.php';
	$db = new db();
//	require_once 'script/functions.php';
	//echo getStatus($db,$_SESSION['ADMIN'],$now_page_uri);
	//*
	require_once 'script/functions.php';
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}
	//*/
	//print_r($_REQUEST);
	//添加信息
	$add = $_POST['add'];
	if($add){
		$name = $_POST['name'];
		$parent = $_POST['parent'];
		$url = $_POST['url'];
		if($name != ""){
			if($db->insert('meinian_admin_permission',array('NAME'=>$name,'PARENT'=>$parent,'URL'=>$url))){
				$msg = '添加信息成功';
			}
		}else{
			$msg = '请将信息填写完整';
		}
	}

	//修改信息
	$update = $_POST['update'];
	if($update){
		$id = intval($_POST['id']);
		$name = $_POST['name'];
		$parent = $_POST['parent'];
		$url = $_POST['url'];
		if($name != ""){
			if($db->update('meinian_admin_permission',array('NAME'=>$name,'PARENT'=>$parent,'URL'=>$url),array('ID'=>$id))){
				$msg = "修改信息成功";
			}
		}else{
			$msg = '请将信息填写完整';
		}
	}

	//点击修改按扭时获取当前信息
	$action = $_REQUEST['action'];
	if($action == "update"){
		$id = intval($_REQUEST['id']);
		if($id){
			$row = $db->fetchOne('meinian_admin_permission',array('ID'=>$id));
		}
	//点击删除按扭时执行删除
	}else if($action == "delete"){
		$id = intval($_REQUEST['id']);
		if($id){
			$num = $db->counts('meinian_admin_permission',array('PARENT'=>$id));
			if($num == 0){
				if($db->del('meinian_admin_permission',array('ID'=>$id))){
					$msg = '删除信息成功';
				}else{
					$msg = '删除信息失败';
				}
			}else{
				$msg = '操作请求被拒绝';
			}
		}
	}

	//修改信息
	$cancel = $_POST['cancel'];
	if($cancel){
		$action = '';
		$id = '';
		$name = '';
		$parent = '';
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				<?php if($action != "update"){?>添加系统权限<?php }else{?>修改系统权限<?php }?>
				<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:">
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">系统权限分类：</td>
							<td height="30">
								<select name="parent">
									<option value="0">分类菜单</option>
									<?php
										foreach ($db->fetch('meinian_admin_permission',array('PARENT'=>'0'),'ID ASC','','ID,NAME') as $arr){
									?>
									<option value="<?php echo $arr['ID'];?>" <?php if($row['PARENT'] == $arr['ID']) echo "selected";?>><?php echo $arr['NAME'];?></option>
									<?php
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">系统权限名称：</td>
							<td height="30"><input type="text" name="name" value="<?php echo $row['NAME'];?>" style="width:250px;"/></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">URL地址：</td>
							<td height="30"><input type="text" name="url" value="<?php echo $row['URL'];?>" style="width:250px;"/></td>
						</tr>
						<tr>
							<td colspan="2" height="30" align="center">
							<?php if($action == "update"){?>
								<input type="hidden" name="id" value="<?php echo $id;?>"/>
								<input type="submit" name="update" value="修改" style="background-color:#ffffee;cursor:pointer;" />
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							<?php }else{?>
								<input type="hidden" name="action" value="<?php echo $action;?>"/>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;" />
							<?php }?>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;">
		<tr bgcolor="#ffffff">
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">系统权限信息列表</td>
			<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;"></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th height="30">ID</th>
			<th>权限名称</th>
			<th>URL地址</th>
			<th colspan="2">操作</th>
		</tr>
		<?php
		foreach ($db->fetch('meinian_admin_permission',array('PARENT'=>'0'),'ID ASC','','ID,NAME') as $arr){
		?>
		<tr bgcolor="#ffffff" >
			<td height="25"><?php echo $arr['ID'];?></td>
			<td colspan="2" onclick="setDisplay('<?php echo 'row'.$arr['ID'];?>')"><?php echo $arr['NAME'];?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=update&id=<?php echo $arr['ID'];?>">修改</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=delete&id=<?php echo $arr['ID'];?>" onclick="return cfmDel();">删除</a></td>
		</tr>
		<tbody id="row<?php echo $arr['ID'];?>">
		<?php
		foreach ($db->fetch('meinian_admin_permission',array('PARENT'=>$arr['ID']),'ID ASC','','ID,NAME,URL') as $arr1){
		?>
		<tr bgcolor="#ffffee">
			<td height="25"><?php echo $arr1['ID'];?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $arr1['NAME'];?></td>
			<td><a href="<?php echo WEBSITE_URL.$arr1['URL'];?>" target="_blank" ><?php echo $arr1['URL'];?></a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=update&id=<?php echo $arr1['ID'];?>">修改</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=delete&id=<?php echo $arr1['ID'];?>" onclick="return cfmDel();">删除</a></td>
		</tr>
		<?php
			}
		?>
		</tbody>
		<?php 
		}
		?>
	</table>
</div>
</div>
</div>
<?php require_once('footer.php');?>

</body>
</html>

