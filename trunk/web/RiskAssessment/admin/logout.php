<?php
	@header("Content-Type: text/html; charset=utf-8");
	@require_once '../script/session_config.php';
	if($_SESSION['ADMIN']){//退出登录
		@require_once '../script/common.php';
		@require_once DOCUMENT_ROOT.'/script/db.class.php';
		@$db = new db();
		@$db->insert('DOG_LOGS' ,array('USER_ID'=>$_SESSION['ADMIN'],'IP'=>$_SERVER['REMOTE_ADDR'],'ACTION'=>'logout'));
		@session_destroy();
	}
	header("Location:index.php");
?>