<?php
	@header("Content-Type: text/html; charset=utf-8");
	require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if(!($_SESSION['ADMIN'] != "" && $_SESSION['ADMIN'] != '0') ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once '../script/db.class.php';
	$db = new db();
	require_once 'script/functions.php';
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}
	require_once 'script/page.class.php';
//	print_r($_REQUEST);
	//添加信息
	$add = $_POST['add'];
	if($add){
		$name = $_POST['name'];
		$para = $_POST['description'];
		if($name != "" && $para != ""){
			if($db->insert('meinian_disease_type',array('name'=>$name,'description'=>$para))){
				$msg = "添加信息成功";
			}else{
				$msg = "添加信息失败";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//修改信息
	$update = $_POST['update'];
	if($update){
		$id = intval($_POST['id']);
		$name = $_POST['name'];
		$para = $_POST['description'];
		if($name != '' && $para != ''){
			if($db->update('meinian_disease_type',array('name'=>$name,'description'=>$para),array('ID'=>$id))){
				$msg = "修改信息成功";
			}else{
				$msg = "添加信息失败";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//点击修改按扭时获取当前信息
	$action = $_REQUEST['action'];
	if($action == 'update'){
		$id = $_REQUEST['id'];
		if($id){
			$row = $db->fetchOne('meinian_disease_type',array('ID'=>$id));
		}
	//点击删除按扭时执行删除
	}else if($action == 'delete'){
//		$id = $_REQUEST['id'];
//		if($id){
//			if($db->del('meinian_disease_type',array('ID'=>$id))){
//				$msg = "删除信息成功";
//			}
//		}
	}else if($action == 'adds'){
		$pid = intval($_REQUEST['pid']);
		if($_POST['go']){//提交修改;
			$option = $_POST['options'];
			if(is_array($option)){
				$result_str = '';
				$i = 0;
				foreach($option as $keys){
					if($keys){
						if($i == 0){
							$result_str = $keys;
						}else{
							$result_str .= ','.$keys;
						}
						$i++;
					}
				}
				if(@$db->update('meinian_disease_type',array('element_id'=>$result_str),array('id'=>$pid))){
					$msg = '修改成功...';
				}else{
					$msg = '修改失败，请不要重复提交...';
				}
				
			}else{
				$msg = "请至少选择一个诱因...";
			}
		}
	}else if($action == 'sub'){
		$id = intval($_REQUEST['id']);
		$pid = intval($_REQUEST['pid']);
		
		if($id > 0 && $pid > 0){
			$temp_result = $db->doSql('SELECT element_id FROM meinian_disease_type WHERE ID = ' . "'$pid'");
			if($temp_result[0]['ELEMENT_ID'] != ''){
				$temp_arr = explode(',',$temp_result[0]['ELEMENT_ID']);
				$result_str = '';
				$i = 0;
				foreach($temp_arr as $keys){
					if($id != $keys){
						if($i == 0){
							$result_str = $keys;
						}else{
							$result_str .= ','.$keys;
						}
						$i++;
					}
				}
				@$db->update('meinian_disease_type',array('element_id'=>$result_str),array('id'=>$pid));
			}
		}
	}

	//修改信息
	$cancel = $_POST['cancel'];
	if($cancel){
		$action = '';
		$id = '';
		$name = '';
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">
<?php 
	if($action == 'adds'){
?>
<form id="addform" name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">关联诱因到指定的疾病<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:">
			<td>
				<table width="100%" cellpadding="1" cellspacing="1">
					<tr>
						<td height="30" width="45%" align="right" style="padding-right:10px;color:red;">选择一个疾病类型名称：</td>
						<td height="30">
						<select name="pid" onchange="this.form.submit();" >
							<option value="0">疾病类型：</option>
							<?php
								foreach ($db->doSql('SELECT NAME,ID,ELEMENT_ID FROM meinian_disease_type WHERE 1 ORDER BY ID DESC ') as $arr){
							?>
							<option value="<?php echo $arr['ID'];?>" <?php if($pid == $arr['ID']){echo 'selected';$elements = ','.$arr['ELEMENT_ID'].',';}?>><?php echo $arr['NAME'];?></option>
							<?php
								}
							?>
						</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td>
		<div style="text-align:center;">
	<input type="hidden" name="action" value="adds"/>
<!--	<input type="hidden" name="pid" value="<?php echo $pid;?>" />-->
	<input type="submit" name="go" value="提交" style="background-color:green;cursor:pointer;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" name="cancel" value="取消" style="background-color:green;cursor:pointer;" />
	<br/>
	</div>
		</td>
		</tr>
		
	</table>
	<?php
	$count_sql = "SELECT COUNT(ID) AS NUM FROM meinian_disease_element ";
	foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
	@$pg = $_REQUEST['pg'];//当前页数
	@$showrows = $_REQUEST['showrows'];//当前每页显示行数
	$show = ($showrows != "" && $showrows != 0) ? $showrows : 20;//如没有当前显示行数则默认成配置文件中设置的行数
	$page = new page($temp['NUM'],$show,$pg,$param);
	$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	$sql = "SELECT * FROM meinian_disease_element ORDER BY ID ASC ".$limit;
	//echo $sql;
	if($temp['NUM'] > 0){
	?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;text-align:center">
		<tr bgcolor="#ffffff" >
			<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">疾病诱因信息列表</td>
			<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;"></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th style="width:5px;" height="30"><input name="all" id="all" type="checkbox" value="yes" style="width:10px;" /></th>
			<th height="30">ID</th>
			<th>诱因名称</th>
			<th>备注</th>
			<th>超标建议</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->doSql($sql) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo '#FFFFFF';}else{echo '#FFFFEE';}?>" <?php if($arr['MENUPAGE_ID'] > 0){echo 'style=\'color:orange;\'';}?>>
			<td height="25"><input name="options[]"  type="checkbox" value="<?php echo  $arr['ID'];?>" <?php if(stristr($elements,','.$arr['ID'].',')){echo 'checked';}?>/></td>
			<td ><?php echo $arr['ID'];?></td>
			<td><?php echo $arr['NAME'];?></td>
			<td><?php echo $arr['DESCRIPTION'];?></td>
			<td><?php echo $arr['SUGESST'];?></td>
		</tr>
		<?php
		$color = !$color;
		}
		?>
	</table>
<?php }?>
</form>
<div class="pages" style="float:right;"><?php $page->showPage();//显示分页信息?></div>
<br/><br/>
<?php		
	}else{
?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				<?php if($action != "update"){?>添加疾病类型信息<?php }else{?>修改疾病类型信息<?php }?>
				<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:">
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">疾病类型名称：</td>
							<td height="30"><input type="text" name="name" value="<?php echo $row['NAME'];?>" style="width:250px;"/></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">疾病类型备注：</td>
							<td height="30"><input type="text" name="description" value="<?php if($row['DESCRIPTION']){ echo $row['DESCRIPTION'];}else{echo '无';}?>" style="width:250px;" /></td>
						</tr>
						<tr>
							<td colspan="2" height="30" align="center">
							<?php if($action == "update"){?>
								<input type="hidden" name="id" value="<?php echo $id;?>"/>
								<input type="submit" name="update" value="修改" style="background-color:#ffffee;cursor:pointer;" />
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							<?php }else{?>
								<input type="hidden" name="action" value="<?php echo $action;?>"/>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;" />
							<?php }?>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;">
		<tr bgcolor="#ffffff" >
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">疾病类型信息列表</td>
			<td colspan="4" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;" ></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th height="30" width="30">ID</th>
			<th>类型名称</th>
			<th>备注</th>
			<th colspan="3">操作</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->fetch('meinian_disease_type','','ID ASC') as $arr){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>" >
			<td height="25" ><?php echo $arr['ID'];?></td>
			<td onclick="setDisplay('<?php echo 'row'.$arr['ID'];?>')" ><?php echo $arr['NAME'];?></td>
			<td title="<?php echo $arr['DESCRIPTION'];?>"><?php echo utf8_substr($arr['DESCRIPTION'],0,30);?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=update&id=<?php echo $arr['ID'];?>">修改</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=adds&pid=<?php echo $arr['ID'];?>">添加诱因</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=delete&id=<?php echo $arr['ID'];?>" onclick="return cfmDel();">删除</a></td>
		</tr>
		<tbody id="row<?php echo $arr['ID'];?>" style="display:none;">
		<?php 
			foreach ($db->doSql('SELECT * FROM meinian_disease_element WHERE ID IN ('.$arr['ELEMENT_ID'].')') as $arr1){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo '&nbsp;&nbsp'.$arr1['ID'];?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $arr1['NAME'];?></td>
			<td colspan="2" title="<?php echo $arr1['DESCRIPTION'];?>"><?php echo $arr1['DESCRIPTION'];?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=adds&pid=<?php echo $arr['ID'];?>">添加</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=sub&pid=<?php echo $arr['ID'];?>&id=<?php echo $arr1['ID'];?>" onclick="return cfmSub('<?php echo $arr['NAME'];?>','<?php echo $arr1['NAME'];?>');">删除</a></td>
		</tr>
		<?php
			}
		?>
		</tbody>
		<?php
		$color = !$color;
		}
		?>
	</table>
<?php
	}
?>
</div>
</div>
</div>
<?php require_once('footer.php');?>

</body>
</html>


