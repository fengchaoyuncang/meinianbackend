<?php
	@header("Content-Type: text/html; charset=utf-8");
	require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if(!($_SESSION['ADMIN'] != '' && $_SESSION['ADMIN'] != '0') ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once '../script/db.class.php';
	require_once 'script/page.class.php';
	$db = new db();
	require_once 'script/functions.php';
	//echo getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI']);
	//*
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}
	//*/
	//print_r($_REQUEST);
function gets ($temp ,$id){
	$flag = false;
	if(is_array($temp)){
		foreach($temp as $key){
			//echo $key['ROLE_ID']." = ".$id;
			if($key['ROLE_ID'] == $id){
				$flag = true;
				break;
			}
		}
	}
	return $flag;
}
	$action = $_REQUEST['action'];
	if($action == 'cancel'){
		$id = intval($_REQUEST['id']);
		if($id > 0 && $_SESSION['ADMIN'] > 0){
			if($id != $_SESSION['ADMIN']){
				$sql = "SELECT STATU FROM meinian_admin WHERE ID='$id'";
				foreach($db->doSql($sql) as $temps);
				if($temps['STATU'] == '1' ){
					if($db->update('meinian_admin',array('STATU'=>'0'),array('ID'=>$id))){
						$msg = '操作成功';
					}else{
						$msg = '操作失败';
					}
				}else{
					$msg = '操作失败';
				}
			}else{
				$msg = '不能对自己的权限操作';
			}
		}
	}
	if($action == 'uncancel'){
		$id = intval($_REQUEST['id']);
		if($id > 0 && $_SESSION['ADMIN'] > 0){
			if($id != $_SESSION['ADMIN']){
				$sql = "SELECT STATU FROM meinian_admin WHERE ID='$id'";
				foreach($db->doSql($sql) as $temps);
				if($temps['STATU'] == '0' ){
					if($db->update('meinian_admin',array('STATU'=>'1'),array('ID'=>$id))){
						$msg = '操作成功';
					}else{
						$msg = '操作失败';
					}
				}else{
					$msg = '操作失败';
				}
			}else{
				$msg = '不能对自己的权限操作';
			}
		}
	}
	if($_POST["add"]){
		$id = intval($_POST["id"]);
		$option = $_POST["option"];
		@$db->del('meinian_admin_user_role',array('USER_ID'=>$id));
		if(is_array($option)){
			foreach($option as $key){
				@$db->insert('meinian_admin_user_role',array('USER_ID'=>$id,'ROLE_ID'=>$key));
			}
		}
	}
	if($_POST["addss"]){
		//print_r($_REQUEST);
		$username = $_POST['username'];
		$pass1 = $_POST['password1'];
		$pass2 = $_POST['password2'];
		$time = getDateTime();
		if($pass1 == $pass2 && $username != ""){
			$pass = md5($pass1);
			$user = $db->getQuery($username);
			$sql = "SELECT COUNT(ID) AS NUM FROM meinian_admin WHERE USER=$user ";
			foreach( $db->doSql($sql) as $usertemp);
			if($usertemp['NUM'] == 0){
				@$db->insert('meinian_admin',array('USER'=>$username,'PASSWD'=>$pass));
				$msg = "添加成功";
			}else{
				$msg = "添加失败...";
			}
		}else{
			$msg = "添加失败...";
		}
	}
	$cancel = $_POST['cancel'];
	if($cancel){
		$action = '';
		$id = '';
		$option = '';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
function checkAll(){
	var option = document.getElementsByName("option[]");
	var status_a = document.getElementById("all");
	if(status_a.checked == true){
		for (var i = 0; i < option.length; i++){
			option[i].checked = true ;
		}
	}else{
		for (var i = 0; i < option.length; i++){
			option[i].checked = false ;
		}
	}
}
function check(){
	if($("#statu").val() == '0' ){
		$('#user_error').html('');
	}
	$('#pass1_error').html('');
	$('#pass2_error').html('');
	var username = $('#username').val();
	var pass1 = $('#password1').val();
	var pass2 = $('#password2').val();
	if(username == ''){
		$('#user_error').html('请填写一个登录用户名...');
		return false;
	}else if($("#statu").val() == '0'){
		$('#user_error').html('请填写一个可用的登录用户名...');
		return false;
	}else if(pass1 == ''){
		$('#pass1_error').html('请填写一个登录密码...');
		return false;
	}else if(pass2 == ''){
		$('#pass2_error').html('请再次确认登录密码...');
		return false;
	}else if(pass1 != pass2){
		$('#password2').val('');
		$('#pass2_error').html('两次密码不一致...');
		return false;
	}else{
		return true;
	}
}
$(document).ready(function(){
	$("#username").blur(function(){
		//alert("1");
		$.ajax({
			type: "post",
			url : "script/api.php",
			dataType:'json',
			data:  'action=checkName&name='+$("#username").val(),
			success: function(json){
				//alert(json.result);
				   if(json.result == 0){
					   $("#user_error").removeClass("error");
					   $("#user_error").addClass("rights");
				       $("#user_error").html("用户名可用");
				       $("#statu").val('1');
				   }else if(json.result == 9){
					   $("#user_error").addClass("error");
					   $("#user_error").removeClass("rights");
					   $("#user_error").html("请填写一个登录用户名...");
					   $("#statu").val('0');
				   }else if(json.result == 1){
					   $("#user_error").addClass("error");
					   $("#user_error").removeClass("rights");
					   $("#user_error").html("用户名已被占用");
					   $("#statu").val('0');
				   }

			}
		});
	});
});
</script>
<style type="text/css">
.error{
	color:red;
	font-size:12px;
}
.rights{
	color:green;
	font-size:12px;
}
</style>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">
<?php
	if($action == "setrole"){
?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				修改角色
				<!--
					<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php //echo $msg;?></span>
				 -->
			</td>
		</tr>
		<tr id="ibody" >
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">登录用户名：</td>
							<td height="30"><span style="width:250px;" ><?php echo $_REQUEST["name"];?></span></td>
						</tr>
						<!--  -->
						<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;text-align:center;">
							<tr bgcolor="#ffffff" >
								<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">角色列表</td>
								<td colspan="5" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;" ></td>
							</tr>
							<tr bgcolor="#ffffee">
								<th height="30" style="width:5px;" ><input name="all" id="all" type="checkbox" value="yes" style="width:10px;" title="全选" onclick= "checkAll()" ></input></th>
								<th >ID</th>
								<th>角色名称</th>
								<th>描述</th>
							</tr>
							<?php
							$temps = $db->doSql("SELECT ROLE_ID FROM meinian_admin_user_role WHERE USER_ID = '".$_REQUEST['id']."'");
						//	print_r($temps);
							$color = flase;
							$sql = 'SELECT * FROM meinian_admin_roles ORDER BY ID DESC ';
							foreach ($db->doSql($sql) as $arr){
							?>
							<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
								<td height="25" style="width:5px;" ><input name="option[]" type="checkbox" value="<?php echo $arr['ID'];?>" style="width:10px;" <?php if(gets($temps ,$arr['ID'])){ echo "checked='checked'";} ?> /></td>
								<td><?php echo $arr['ID'];?></td>
								<td><?php echo $arr['NAME'];?></td>
								<td><?php echo $arr['COMMENTS'];?></td>
							</tr>
							<?php
							$color = !$color;
							}
							?>
						</table>
						<!--  -->
						<tr>
							<td colspan="2" height="30" align="center">
								<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>"/>
								<input type="hidden" name="pg" value="<?php echo $_REQUEST['pg'];?>"/>
								<input type="hidden" name="showrows" value="<?php echo $_REQUEST['showrows'];?>"/>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;" />
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
<?php
	}else{?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody1');">
				添加管理员帐号
				<!--
					<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php //echo $msg;?></span>
				 -->
			</td>
		</tr>
		<tr id="ibody1" style="display:none;">
			<td>
				<form name="ibody1form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">登录帐号：</td>
							<td height="30"><input type="text" id="username" name="username" value="" style="width:150px;"/>&nbsp;&nbsp;&nbsp;<label id="user_error" class="error" style="font-size:11px;"></label></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">设置密码：</td>
							<td height="30"><input type="password" id="password1" name="password1" value="" style="width:150px;"/>&nbsp;&nbsp;&nbsp;<label id="pass1_error" style="color:red;font-size:11px;"></label></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">重复密码：</td>
							<td height="30"><input type="password" id="password2" name="password2" value="" style="width:150px;"/>&nbsp;&nbsp;&nbsp;<label id="pass2_error" style="color:red;font-size:11px;"></label></td>
						</tr>
						<tr>
							<td colspan="2" height="30" align="center">
								<input type="hidden" id="statu" value="0" readonly/>
								<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>"/>
								<input type="hidden" name="pg" value="<?php echo $_REQUEST['pg'];?>"/>
								<input type="hidden" name="showrows" value="<?php echo $_REQUEST['showrows'];?>"/>
								<input type="hidden" name="action" value="<?php echo $action;?>"/>
								<input type="submit" name="addss" value="提交" onclick="return check();" style="background-color:#ffffee;cursor:pointer;" />
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
<?php
	}
?>
	<?php
	$count_sql = 'SELECT COUNT(ID) AS NUM FROM meinian_admin WHERE 1';
	foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
	@$pg = $_REQUEST['pg'];//当前页数
	@$showrows = $_REQUEST['showrows'];//当前每页显示行数
	$show = ($showrows != "" && $showrows != 0) ? $showrows : 30;//如没有当前显示行数则默认成配置文件中设置的行数
	$page = new page($temp['NUM'],$show,$pg);
	$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	$sql = "SELECT * FROM meinian_admin WHERE 1 ORDER BY ID DESC ".$limit;
	?>
	<div class="pages" style="float:right;"><?php $page->showPage();//显示分页信息?></div>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;text-align:center">
		<tr bgcolor="#ffffff">
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">用户信息列表&nbsp;<span style="color:red"><?php echo $msg;?></span></td>
			<td colspan="4" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;" ></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th height="30">ID</th>
			<th>用户名</th>
			<th>注册时间</th>
			<th>状态</th>
			<th>角色</th>
			<th colspan="2" >操作</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->doSql($sql) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo $arr['ID'];?></td>
			<td><?php echo $arr['USER'];?></td>
			<td><?php echo $arr['CTIME'];?></td>
			<td><?php if($arr['STATU'] == '1'){
				echo '<span style="color:green;">已启用</span>';
			}else{
				echo '<span style="color:red;">已禁用</span>';
			}?></td>
			<td><?php $str = ""; foreach($db->doSql("SELECT A.NAME FROM meinian_admin_roles AS A ,meinian_admin_user_role AS B WHERE B.USER_ID = '".$arr['ID']."' AND B.ROLE_ID = A.ID ") as $temp_role){ if($str != ""){$str .= ','.$temp_role['NAME'];}else{ $str = $temp_role['NAME'];} } if($str != ""){ echo $str; }else{ echo "无"; }?></td>
			<td>
				<?php 
					if($arr['STATU'] == '1'){
						?>
						<a onclick="return confirm('确定要禁用该账号吗？')" href="<?php echo $_SERVER['PHP_SELF'];?>?action=cancel&id=<?php echo $arr['ID'];?>&pg=<?php echo $pg;?>&showrows=<?php echo $showrows;?>" >禁用账号</a>
						<?php 
					}else{
						?>
						<a onclick="return confirm('确定要启用该账号吗？')" href="<?php echo $_SERVER['PHP_SELF'];?>?action=uncancel&id=<?php echo $arr['ID'];?>&pg=<?php echo $pg;?>&showrows=<?php echo $showrows;?>" >启用账号</a>
						<?php 
					}
				?>
				
				 | <a href="<?php echo $_SERVER['PHP_SELF'];?>?action=setrole&name=<?php echo $arr['USER'];?>&id=<?php echo $arr['ID'];?>&pg=<?php echo $pg;?>&showrows=<?php echo $showrows;?>" >设置角色</a>
			</td>
		</tr>
		<?php
		$color = !$color;
		}
		?>
	</table>
</div>
</div>
</div>
<?php require_once('footer.php');?>

</body>
</html>



