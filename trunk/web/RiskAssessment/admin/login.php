<?php
@header("Content-Type: text/html; charset=utf-8");
require_once '../script/session_config.php';
require_once '../script/common.php';
$r = urldecode($_REQUEST ['r']); //returnurl
$r = str_replace('http://localhost/RiskAssessment/', WEBSITE_URL, $r);
//'http://115.28.48.232:8080/RiskAssessment/';
//print_r($_SESSION);
if(isset($_SESSION['ADMIN'])){
	if($_SESSION['ADMIN'] != '' && $_SESSION['ADMIN'] != '0' ){
		if($r != ""){
			header("Location:".$r);
		}else{
			header("Location:index.php");
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/md5.js"></script>
<script type="text/javascript"><!--
$(document).ready(function(){
	$("#submit").click(function(){
		//$("#submit").hide();
		//$("#submit1").show();
		$("#error_tr").hide();
		$("#error").html("");
		var name = $.trim($("#name").val());
		var pass = $.trim($("#pass").val());
		if(name){
			$("#name_error").html("");
		}else{
			$("#name_error").html("用户名为空");
			return;
		}
		if(pass){//不为空
			pass = hex_md5(pass); 	//md5加密
			$("#pass_error").html("");
		}else{
			$("#pass_error").html("密码为空");
			return;
		}
		//alert('action=adminLogin&name='+$("#name").val()+'&pass='+$("#pass").val());
		$.ajax({
			type: "post",
			url : "script/api.php",
			dataType:'json',
			data:'action=adminLogin&name='+name+'&pass='+pass,
			success: function(json){
//				alert(json.result);
				if(json.result == 0 ){
					var url = $("#r").val();
					if(url){
						window.location.href=url;
					}else{
						window.location.href="index.php";
					}
				}else{
					if(json.result == 1){
						$("#error_tr").show();
						$("#error").html("用户名或密码错误！");
					}else{
						$("#error_tr").show();
						$("#error").html("登录失败!错误代码："+json.result);
					}
				}
				$("#submit1").hide();
				$("#submit").show();
			}
		});
		$("#submit1").hide();
		$("#submit").show();
	});
});
--></script>
</head>

<body>
<?php
require_once ('header.php');
?>

<div class="content">
	<div class="zhuce right">

		<div class="cpCon">
			<div class="zhuceCon">
				<div class="zcxx"><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
					<table width="369" border="0" align="center" cellpadding="0"
						cellspacing="0">
						<tr>
							<td height="40" colspan="2"><span
								style="font-size: 22px; color: #993300">管理员登录</span></td>
						</tr>
						<tr>
							<td width="80" height="40">管理员帐号：</td>
							<td width="290" height="40"><input type="text" name="name" id="name"
								style="width: 155px;border:1px orange solid;" maxlength="20" />&nbsp;<label id="name_error"
								style="color: red;"></label></td>
						</tr>
						<tr>
							<td height="40">密 码：</td>
							<td height="40"><input type="password" name="pass" id="pass"
								style="width: 155px;border:1px orange solid;" maxlength="20" />&nbsp;<label id="pass_error"
								style="color: red;"></label></td>
						</tr>
						<tr id="error_tr" style="display: none;">
							<td height="40"></td>
							<td height="40"><label id="error" style="color: red;"></label></td>
						</tr>
						<tr>
							<td height="40">&nbsp;</td>
							<td height="40"><input style="width: 93px; height: 34px; border: 0"
								type="image" name="submit" id="submit" src="images/zhucesa_07.jpg" />
							<input style="display: none; width: 93px; height: 34px; border: 0"
								type="image" name="submit1" id="submit1" src="images/zhucesa_07.jpg" />
							</td>
						</tr>
					</table><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
					<input type="hidden" id="r" value="<?php echo $r; ?>" />
				</div>
			</div>
		</div>
	</div>
</div>
<?php
require_once ('footer.php');
?>

</body>
</html>
