<?php
	@header("Content-Type: text/html; charset=utf-8");
	@require_once '../script/session_config.php';
	require_once '../script/common.php';
	//*
	if(!isset($_SESSION['ADMIN'])){
		if(!($_SESSION['ADMIN'] != '' && $_SESSION['ADMIN'] != '0') ){
			header("Location:login.php?r=".urlencode($now_page_uri));
		}
	}
	//*/
	@require_once DOCUMENT_ROOT.'/script/db.class.php';
	@$db = new db();
	$update = $_POST['update'];
	if($update){
		$oldpass = $_POST['oldpass'];
		$newpass = $_POST['newpass'];
		$newpasss = $_POST['newpasss'];
		if($newpasss != '' && $newpass != '' && $oldpass != ''){
			if($newpasss == $newpass){
				if($oldpass == $newpasss){
					$msg = '新密码与旧密码不能相同！';
				}else{
					$result = $db->doSql('SELECT PASSWD FROM meinian_admin WHERE ID=' . "'".$_SESSION['ADMIN']."'");
					if(isset($result)){
						if($result[0]['PASSWD'] == md5($oldpass)){
							if($db->update('meinian_admin',array('PASSWD'=>md5($newpass)),array('ID'=>$_SESSION['ADMIN']))){
								$msg = '修改成功！';
								@$db->insert('meinian_admin_logs' ,array('USER_ID'=>$_SESSION['ADMIN'],'IP'=>$_SERVER['REMOTE_ADDR'],'ACTION'=>'changePWD'));
							}else{
								$msg = '修改失败，请稍候再试！';
							}
						}
					}else{
						$msg = '操作错误！';
					}
				}
			}else{
				$msg = '新密码不一致！';
			}
		}else{
			$msg = '操作错误！';
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>
function showThis(){
	$('#changePass').show();
}
function hideThis(){
	$('#oldpass').val('');
	$('#newpass').val('');
	$('#newpasss').val('');
	$('#changePass').hide();
	return false;
}
function check(){
	var oldpass = $('#oldpass').val();
	if(oldpass == ''){
		$('#error').html('请填写原密码！');
		return false;
	}
	var newpass = $('#newpass').val();
	if(newpass == ''){
		$('#error').html('请填写新密码！');
		return false;
	}
	var newpasss = $('#newpasss').val();
	if(newpasss == ''){
		$('#error').html('请再次填写新密码！');
		return false;
	}
	if(newpasss != newpass){
		$('#error').html('新密码不一致！');
		$('#newpass').val('');
		$('#newpasss').val('');
		return false;
	}
	return true;
}
</script>
<style>
tr{
	height:32px;
}
input{
	border:1px orange solid;
}
</style>
</head>

<body>
<?php require_once('header.php');?>



<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>

<div class="caozuo">
  <div class="czTitle">
    <h2>系统基本信息</h2>
  </div>
  <div style="height:40px;">
  <p>您的级别：<?php $roles = '' ; foreach($db->doSql("SELECT B.NAME AS NAME FROM meinian_admin_user_role AS A ,meinian_admin_roles AS B WHERE USER_ID='".$_SESSION['ADMIN']."' AND B.ID=A.ROLE_ID" ) as $temp){ $roles .= ($roles == '')? $temp['NAME'] : ','.$temp['NAME'];} if($roles != ''){ echo $roles;}else{ echo "您当前没有任何权限，如有疑问请联系超级管理员...";}?>[<a href="javascript:void(0);" onclick="showThis()">修改密码</a>]</p>
	<br/>
</div>
	<div id="changePass" style="border:1px solid orange;padding-top:5px;<?php if(!$update){?>display:none;<?php }?>">
	<form action="index.php" method="post" >
		<table width="100%" cellpadding="1" cellspacing="1">
			<tr>
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%">&nbsp;</td>
			</tr>
			<tr>
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%">&nbsp;</td>
			</tr>
			<tr>
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%">&nbsp;</td>
			</tr>

			<tr>
				<td align="right" width="40%" >原密码:</td>
				<td width="60%"><input id="oldpass" name="oldpass" style="width:150px;" type="password"></input></td>
			</tr>
			<tr>
				<td align="right">新密码:</td>
				<td><input id="newpass" name="newpass" style="width:150px;" type="password"></input></td>
			</tr>
			<tr>
				<td align="right" >确认新密码:</td>
				<td><input id="newpasss" name="newpasss" style="width:150px;" type="password"></input></td>
			</tr>
			<tr style="height:24px;">
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%"><label id="error" style="color:orange;"><?php echo $msg;?></label></td>
			</tr>
			<tr>
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%">
				<input type="submit" name="update" value="修改" style="background-color:#ffffee;cursor:pointer;" onclick="return check();" />
				<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" onclick="return hideThis();" />
				</td>
			</tr>
			<tr>
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%">&nbsp;</td>
			</tr>
			<tr>
				<td align="right" width="40%" >&nbsp;</td>
				<td width="60%">&nbsp;</td>
			</tr>
		</table>
		</form>
	</div>
</div>
</div>




</div>
<?php require_once('footer.php');?>

</body>
</html>
