<?php
@header("Content-Type: text/html; charset=utf-8");
require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if( $_SESSION['ADMIN'] == '' || $_SESSION['ADMIN'] == '0' ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once '../script/db.class.php';
	require_once 'script/page.class.php';
	$db = new db();
	require_once 'script/functions.php';
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}

	//修改信息

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">

<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966">
		<tr bgcolor="#FFFFEE">
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				快速查询
				<!--
					<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php //echo $msg;?></span>
				 -->

			</td>
		</tr>
		<tr id="ibody" style="<?php if($_REQUEST["key"] == ""){echo "display:none";}?>" >
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="get">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">请输入关键词：</td>
							<td height="30"><input type="text" name="key" value="<?php echo $_REQUEST["key"];?>" style="width:250px;" title="关键词可以是邮箱、用户名中的任意连续部分..." /></td>
						</tr>
						<tr>
							<td colspan="2" height="30" align="center">

								<input type="hidden" name="pg" value="<?php echo $_REQUEST['pg'];?>"/>
								<input type="hidden" name="showrows" value="<?php echo $_REQUEST['showrows'];?>"/>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;" />

							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
	<?php
	$count_sql = "SELECT COUNT(ID) AS NUM FROM meinian_admin_logs";
	if($_REQUEST["key"] != ""){
		$count_sql = "SELECT COUNT(A.ID) AS NUM FROM meinian_admin_logs AS A,meinian_admin AS B WHERE A.USER_ID=B.ID AND (A.IP like '"."%".$_REQUEST["key"]."%"."' OR A.LTIME like '"."%".$_REQUEST["key"]."%"."' OR B.USER like '"."%".$_REQUEST["key"]."%"."' )";
	}
	foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
	$param = "key=".$_REQUEST["key"];
	@$pg = $_REQUEST['pg'];//当前页数
	@$showrows = $_REQUEST['showrows'];//当前每页显示行数a
	$show = ($showrows != "" && $showrows != 0) ? $showrows : 20;//如没有当前显示行数则默认成配置文件中设置的行数
	$page = new page($temp['NUM'],$show,$pg,$param);
	$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	$sql = "SELECT A.* ,B.USER FROM meinian_admin_logs AS A,meinian_admin AS B WHERE A.USER_ID=B.ID ORDER BY ID DESC ".$limit;
	if($_REQUEST["key"] != ""){
		$sql = "SELECT A.* ,B.USER FROM meinian_admin_logs AS A,meinian_admin AS B WHERE A.USER_ID=B.ID AND (A.IP like '"."%".$_REQUEST["key"]."%"."' OR A.LTIME like '"."%".$_REQUEST["key"]."%"."' OR B.USER like '"."%".$_REQUEST["key"]."%"."' ) ORDER BY ID DESC ".$limit;
	}
	?>
	<div class="pages" style="float:right;bgcolor:#FFFFEE"><?php $page->showPage();//显示分页信息?></div>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #FFFFEE;text-align:center;">
		<tr bgcolor="#ffffff">
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">用户登录信息列表</td>
			<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;" ></td>
		</tr>
		<tr bgcolor="#FFFFEE">
			<th height="30">ID</th>
			<th>用户名</th>
			<th>登录IP</th>
			<th>时间</th>
			<th>操作</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->doSql($sql) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo $arr['ID'];?></td>
			<td><?php echo $arr['USER'];?></td>
			<td><?php echo $arr['IP'];?></td>
			<td><?php echo $arr['LTIME'];?></td>
			<td><?php echo $arr['ACTION'];?></td>
		</tr>
		<?php
			$color = !$color;
		}
		?>
	</table>
</div>
</div>
</div>
<?php require_once('footer.php');?>
</body>
</html>



