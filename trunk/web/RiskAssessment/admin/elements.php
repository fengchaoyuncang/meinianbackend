<?php
	@header("Content-Type: text/html; charset=utf-8");
	require_once '../script/session_config.php';
	require_once '../script/common.php';
	if(!isset($_SESSION['ADMIN'])){//登录检测
		if(!($_SESSION['ADMIN'] != "" && $_SESSION['ADMIN'] != '0') ){
			header("Location:".WEBSITE_URL."admin/login.php?r=".urlencode($now_page_uri));
		}
	}
	require_once '../script/db.class.php';
	$db = new db();
	require_once 'script/functions.php';
	if(getStatus($db,$_SESSION['ADMIN'],$_SERVER['REQUEST_URI'])){
		header("Location:".WEBSITE_URL."admin/comfirm.php");
	}
	require_once 'script/page.class.php';
	//添加信息
	$add = $_POST['add'];
	if($add){
		$name = $_POST['name'];
		$description = $_POST['description'];
		$did = intval($_POST['did']);
		$sugesst = $_POST['sugesst'];
		if($name != '' && $did != '' && $sugesst != ''){
			if($db->insert('meinian_disease_element',array('NAME'=>$name,'DESCRIPTION'=>$description,'SUGESST'=>$sugesst,'DEPARTMENT_ID'=>$did))){
				$msg = "添加信息成功";
			}else{
				$msg = "添加信息失败";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//修改信息
	$update = $_POST['update'];
	if($update){
		$id = intval($_POST['id']);
		$name = $_POST['name'];
		$description = $_POST['description'];
		$did = intval($_POST['did']);
		$sugesst = $_POST['sugesst'];
		if($name != '' && $description != '' && $id > 0){
			if($db->update('meinian_disease_element',array('NAME'=>$name,'DESCRIPTION'=>$description,'SUGESST'=>$sugesst,'DEPARTMENT_ID'=>$did),array('ID'=>$id))){
				$msg = "修改信息成功";
			}else{
				$msg = "修改信息失败";
			}
		}else{
			$msg = "请将信息填写完整";
		}
	}

	//点击修改按扭时获取当前信息
	$action = $_REQUEST['action'];
	if($action == 'update'){
		$id = $_REQUEST['id'];
		if($id){
			$row = $db->fetchOne('meinian_disease_element',array('ID'=>$id));
		}
	//点击删除按扭时执行删除
	}else if($action == 'delete'){
//		$id = $_REQUEST['id'];
//		if($id){
//			if($db->del('meinian_disease_element',array('ID'=>$id))){
//				$msg = "删除信息成功";
//			}
//		}
	}else if($action == 'sub'){
		$id = intval($_REQUEST['id']);
		if($id > 0 ){
			@$db->del('meinian_question_element',array('id'=>$id));
		}
	}else if($action == 'adds'){
		$eid = intval($_REQUEST['eid']);
		if($_POST['go']){//提交修改;
			$option = $_POST['options'];
			if(is_array($option)){
				@$db->del('meinian_question_element',array('element_id'=>$eid));
				foreach($option as $keys){
					if($keys){
						@$db->insert('meinian_question_element',array('element_id'=>$eid,'question_id'=>$keys));
					}
				}
				$msg = '修改成功...';
			}else{
				$msg = "请至少选择一个问卷...";
			}
		}
	}

	//修改信息
	$cancel = $_POST['cancel'];
	if($cancel){
		$action = '';
		$id = '';
		$name = '';
	}
function getQuestion($have){
	$result_str = '';
	if(is_array($have)){
		foreach($have as $temps){
			if($temps['QUESTION_ID']){
				$result_str .= ','.$temps['QUESTION_ID'];
			}
		}
		if($result_str != ''){
			$result_str .= ',';
		}
	}
	return $result_str;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>风险评估子系统-后台管理系统</title>
<link href="css/houtai.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<?php require_once('header.php');?>

<div class="content">
<?php require_once('leftmenu.php');?>




<div class="conRight">
<div class="djTupian"><a href="javascript:void(0);"><img src="images/zljlw_48.jpg" width="8" height="80" alt="img" id="img_id" onclick="changeDisplay();" /></a>
<input type="hidden" id="web_url" value="<?php echo "48";?>" ></input>
</div>
<div class="caozuo">
<?php 
	if($action == 'adds'){
?>
<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">关联诱因的调查问卷<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:">
			<td>
				<table width="100%" cellpadding="1" cellspacing="1">
					<tr>
						<td height="30" width="45%" align="right" style="padding-right:10px;color:red;">选择一个诱因名称：</td>
						<td height="30">
						<select name="eid" onchange="this.form.submit();">
							<option value="0">请选择诱因名称：</option>
							<?php
								foreach ($db->doSql('SELECT NAME,ID FROM meinian_disease_element WHERE 1 ORDER BY ID DESC ') as $arr){
							?>
							<option value="<?php echo $arr['ID'];?>" <?php if($eid == $arr['ID']){echo 'selected';$result_str = getQuestion($db->fetch('meinian_question_element',array('element_id'=>$eid)));}?>><?php echo $arr['NAME'];?></option>
							<?php
								}
							?>
						</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td>
		<div style="text-align:center;">
		<input type="hidden" name="action" value="adds"/>
	<!--	<input type="hidden" name="pid" value="<?php echo $pid;?>" />-->
		<input type="submit" name="go" value="提交" style="background-color:green;cursor:pointer;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="cancel" value="取消" style="background-color:green;cursor:pointer;" />
	</div>
		</td></tr>
	</table>
	<?php
	$count_sql = "SELECT COUNT(ID) AS NUM FROM meinian_questionnaire ";
	foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
	@$pg = $_REQUEST['pg'];//当前页数
	@$showrows = $_REQUEST['showrows'];//当前每页显示行数
	$show = ($showrows != "" && $showrows != 0) ? $showrows : 20;//如没有当前显示行数则默认成配置文件中设置的行数
	$page = new page($temp['NUM'],$show,$pg,$param);
	$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	$sql = "SELECT * FROM meinian_questionnaire ORDER BY ID ASC ".$limit;
	//echo $sql;
	if($temp['NUM'] > 0){
	?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;text-align:center">
		<tr bgcolor="#ffffff" >
			<td colspan="3" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">问卷信息列表</td>
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;"></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th style="width:5px;" height="30"><input name="all" id="all" type="checkbox" value="yes" style="width:10px;" title="自动全选本页所有符合的卡券" onclick= "checkAll()"/></th>
			<th height="30">ID</th>
			<th>问卷题目</th>
			<th >类型</th>
		</tr>
		<?php
		$color = flase;
		foreach ($db->doSql($sql) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo '#FFFFFF';}else{echo '#FFFFEE';}?>" <?php if($arr['MENUPAGE_ID'] > 0){echo 'style=\'color:orange;\'';}?>>
			<td height="25"><input name="options[]"  type="checkbox" value="<?php echo  $arr['ID'];?>" <?php if(stristr($result_str,','.$arr['ID'].',')){echo 'checked';}?>/></td>
			<td ><?php echo $arr['ID'];?></td>
			<td><?php echo $arr['TITLE'];?></td>
			<td><?php echo getQuestionType($arr['TYPE']);;?></td>
		</tr>
		<?php
		$color = !$color;
		}
		?>
	</table>
</form>
<?php }?>
<div class="pages" style="float:right;"><?php $page->showPage();//显示分页信息?></div>
<br/>
<?php		
	}else{
?>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CC9966;">
		<tr>
			<td height="30" style="cursor:pointer;font-size:14px;font-weight:bold;background-color:#ffffee;padding-left:5px;" onclick="setStyle('ibody');">
				<?php if($action != "update"){?>添加诱因信息<?php }else{?>修改诱因信息<?php }?>
				<span style="font-size:14px;font-weight:bold;color:#FF0000;padding-left:10px;"><?php echo $msg;?></span>
			</td>
		</tr>
		<tr id="ibody" style="display:<?php if($action == 'update'){echo '';}else{ echo 'none';}?>">
			<td>
				<form name="ibodyform" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
					<table width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">科室：</td>
							<td height="30">
								<select name="did" >
									<option value="0">请选择科室：</option>
									<?php
										foreach ($db->doSql('SELECT NAME,ID FROM meinian_disease_department WHERE 1 ORDER BY ID DESC ') as $arr){
									?>
									<option value="<?php echo $arr['ID'];?>" <?php if($row['ID'] == $arr['ID']){echo 'selected';}?>><?php echo $arr['NAME'];?></option>
									<?php
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">诱因名称：</td>
							<td height="30"><input type="text" name="name" value="<?php echo $row['NAME'];?>" style="width:250px;"/></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">备注：</td>
							<td height="30"><input type="text" name="description" value="<?php if($row['DESCRIPTION']){ echo $row['DESCRIPTION'];}else{echo '无';}?>" style="width:250px;" /></td>
						</tr>
						<tr>
							<td height="30" width="45%" align="right" style="padding-right:10px;">超标建议：</td>
							<td height="30"><textarea name="sugesst"><?php echo $row['SUGESST'];?></textarea></td>
				
						</tr>
						<tr>
							<td colspan="2" height="30" align="center">
							<?php if($action == "update"){?>
								<input type="hidden" name="id" value="<?php echo $id;?>"/>
								<input type="submit" name="update" value="修改" style="background-color:#ffffee;cursor:pointer;" />
								<input type="submit" name="cancel" value="取消" style="background-color:#ffffee;cursor:pointer;" />
							<?php }else{?>
								<input type="hidden" name="action" value="<?php echo $action;?>"/>
								<input type="submit" name="add" value="提交" style="background-color:#ffffee;cursor:pointer;" />
							<?php }?>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table><br/>
	<?php 
		$count_sql = "SELECT COUNT(ID) AS NUM FROM meinian_disease_element ";
		foreach($db->doSql($count_sql) as $temp);//获取检索到的总行数
		@$pg = $_REQUEST['pg'];//当前页数
		@$showrows = $_REQUEST['showrows'];//当前每页显示行数
		$show = ($showrows != "" && $showrows != 0) ? $showrows : 20;//如没有当前显示行数则默认成配置文件中设置的行数
		$page = new page($temp['NUM'],$show,$pg,$param);
		$limit = " LIMIT ".$page->getStart().",".$page->getShowrows();
	?>
	<div class="pages" style="float:right;"><?php $page->showPage();//显示分页信息?></div>
	<table width="100%" cellpadding="1" cellspacing="1" style="border:1px solid #CCCCCC;">
		<tr bgcolor="#ffffff" >
			<td colspan="2" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;">诱因信息列表</td>
			<td colspan="5" height="30" style="font-size:14px;font-weight:bold;padding-left:5px;" ></td>
		</tr>
		<tr bgcolor="#ffffee">
			<th height="30" width="30">ID</th>
			<th>诱因名称</th>
			<th>备注</th>
			<th>建议</th>
			<th colspan="3">操作</th>
		</tr>
		<?php
		$color = flase;
	
		foreach ($db->fetch('meinian_disease_element','','ID ASC'.$limit) as $arr){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo $arr['ID'];?></td>
			<td onclick="setDisplay('<?php echo 'row'.$arr['ID'];?>')"><?php echo $arr['NAME'];?></td>
			<td title="<?php echo $arr['DESCRIPTION'];?>"><?php echo utf8_substr($arr['DESCRIPTION'],0,20);?></td>
			<td title="<?php echo $arr['SUGESST'];?>"><?php echo utf8_substr($arr['SUGESST'],0,20);?></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=update&id=<?php echo $arr['ID'];?>">修改</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=adds&eid=<?php echo $arr['ID'];?>">添加问卷</a></td>
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=delete&id=<?php echo $arr['ID'];?>" onclick="return cfmDel();">删除</a></td>
		</tr>
		<tbody id="row<?php echo $arr['ID'];?>" style="display:none;">
		<?php 
			foreach ($db->doSql('SELECT a.* FROM meinian_questionnaire as a,meinian_question_element as b WHERE b.ELEMENT_ID = \''.$arr['ID'].'\'AND b.QUESTION_ID = a.ID ') as $arr1){
		?>
		<tr bgcolor="<?php if($color){echo "#FFFFFF";}else{echo "#FFFFEE";}?>">
			<td height="25"><?php echo '&nbsp;&nbsp'.$arr1['ID'];?></td>
			<td colspan="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $arr1['TITLE'].'('.getQuestionType($arr1['TYPE']).')';?></td>
<!--			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=adds&pid=<?php echo $arr['ID'];?>">添加</a></td>-->
			<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?action=sub&eid=<?php echo $arr['ID'];?>&id=<?php echo $arr1['ID'];?>" onclick="return cfmSub('<?php echo $arr['NAME'];?>','<?php echo $arr1['TITLE'];?>');">删除</a></td>
		</tr>
		<?php
			}
		?>
		</tbody>
		<?php
		$color = !$color;
		}
		?>
	</table>
<?php 
	}
?>
</div>
</div>
</div>
<?php require_once('footer.php');?>

</body>
</html>


