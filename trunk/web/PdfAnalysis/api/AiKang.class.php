<?php
require_once './script/config.inc';
class AiKang{
	private $pdfFile = '';						//pdf文件路劲
	private $information = array();				//pdf提取数据
	private $stream = array();					//内容分层数组
	private $flag = true;						//true => 调试 ; false => 非调试
	private $information_flag = array();
	function __construct($file,$flag=false){
		$this->pdfFile = DOCUMENT_ROOT . '/' . $file;
		if(!$flag){//开启调试
			$this->flag = false;
		}
		$this->loadPdf();
		$this->Analysis();
	}
	/**
	 * String getStreams( String $str)
	 * 返回数据流，过滤掉杂项
	 * @param String $str
	 * @return String 
	 */
	function getStreams($str){
		$result = '';
		$arr = explode('stream',$str);
		return (count($arr) >= 2) ? $this->replaceBlank($arr[1]) : '';
	}
	/**
	 * void loadPdf()
	 * 载入pdf文件
	 */
	function loadPdf(){
		if(file_exists($this->pdfFile)){
			$content = file_get_contents($this->pdfFile);
			$this->stream = explode('版权所有www.ikang.com', $content);
			unset($content);
		}
	}
	/**
	 * void Analysis()
	 * 分析字符串，并提取信息
	 */
	function Analysis(){
		if(count($this->stream) >= 1){
			foreach($this->stream as $subStr){
//				echo $subStr;
				$this->pickUp($subStr);
			}
		}
	}
	/**
	 * void pickUp(String $str)
	 * 从$str中提取相关信息
	 * @param $str
	 * @return null
	 */
	function pickUp($str){
		if($str != ''){
//			$decoded = $this->encode_conver($str,2);
			$decoded = $str;
//			echo $decoded;
			if(!isset($this->information_flag['basic']['flag'])){
				$this->collectBasicInformation($decoded,'用户姓名：','性别','Name');
				$this->collectBasicInformation($decoded,'性别：','体检日期','Sex');
				$this->collectBasicInformation($decoded,'体检日期：','健检编号','Time');
				$this->collectBasicInformation($decoded,'健检编号：','爱康卡号','No');
				$this->collectBasicInformation($decoded,'爱康卡号：','(c)2004-2009','CardNo');
				//一般检查信息
				$this->collectExaminationInfo($decoded,'身高','Height','basic',3);
				$this->collectExaminationInfo($decoded,'体重','Weight','basic',3);
				$this->collectExaminationInfo($decoded,'体重指数','WeightLevel','basic',5);
				$this->collectExaminationInfo($decoded,'收缩压','SystolicBloodPressure','basic',4);
				$this->collectExaminationInfo($decoded,'舒张压','DiastolicBloodPressure','basic',4);
				if($this->information['basic']['DiastolicBloodPressure'] != ''){
					$this->information_flag['basic']['flag'] = true;
				}
			}
//			//内科
			if(!isset($this->information_flag['InternalMedicine']['flag'])){
				$this->getSpecialData($decoded,'检查结果','心率','InternalMedicine',array('MedicalHistory','FamilyHistory'));
				
//				$this->collectExaminationInfo($decoded,'病史','MedicalHistory','InternalMedicine',20);
//				$this->collectExaminationInfo($decoded,'家族史','FamilyHistory','InternalMedicine',20);
				$this->collectInfoByTag($decoded,'心率','心律','HeartRate','InternalMedicine',20);
				$this->collectInfoByTag($decoded,'心律','心音','Rhythm','InternalMedicine');
				$this->collectInfoByTag($decoded,'心音','肺部听诊','HeartSounds','InternalMedicine');
				$this->collectInfoByTag($decoded,'肺部听诊','肝脏触诊','LungAuscultation','InternalMedicine');
				$this->collectInfoByTag($decoded,'肝脏触诊','脾脏触诊','LiverPalpation','InternalMedicine');
				$this->collectInfoByTag($decoded,'脾脏触诊','肾脏叩诊','SpleenPalpation','InternalMedicine');
				$this->collectInfoByTag($decoded,'肾脏叩诊','神经反射：膝反射','KidneyPercussion','InternalMedicine');
				
				$this->getSpecialData($decoded,'内科其它','初步意见','InternalMedicine',array('KneeReflex','InternalMedicineOther'));
				
//				$this->collectInfoByTag($decoded,'神经反射：膝反射','内科其它','KneeReflex','InternalMedicine');
//				$this->collectExaminationInfo($decoded,'内科其它','InternalMedicineOther','InternalMedicine');
				if($this->information['InternalMedicine']['InternalMedicineOther'] != ''){
					$this->information_flag['InternalMedicine']['flag'] = true;
				}
			}
			
//			//外科
			if(!isset($this->information_flag['Surgica']['flag'])){
				$temp_str = $this->getTable($decoded,'内科其它','眼科');
				$this->getSpecialData($temp_str,'检查结果','乳房','Surgica',array('Skin','SuperficialLymphNodes','Thyroid'));
				$this->collectInfoByTag($temp_str,'乳房','脊柱','Breast','Surgica');
				$this->collectInfoByTag($temp_str,'脊柱','四肢关节','Spine','Surgica');
				$this->collectInfoByTag($temp_str,'四肢关节','肛门、直肠指诊','Limbs','Surgica');
				$this->collectInfoByTag($temp_str,'肛门、直肠指诊','外科其它','AnusAndRectum','Surgica');
				$this->collectInfoByTagDesc($temp_str,'外科其它','初步意见','SurgicalOther','Surgica');
				if($this->information['Surgica']['SurgicalOther'] != ''){
					$this->information_flag['Surgica']['flag'] = true;
				}
			}
			
//			//眼科
			if(!isset($this->information_flag['eye']['flag'])){
				$this->collectInfoByTag($decoded,'裸视力(右)','裸视力(左)','VisionUncorrectionRight','eye');
				$this->collectInfoByTag($decoded,'裸视力(左)','矫正视力(右)','VisionUncorrectionLeft','eye');
				$this->collectInfoByTag($decoded,'矫正视力(右)','矫正视力(左)','VisionCorrectionRight','eye',50);
				$this->collectInfoByTag($decoded,'矫正视力(左)','色觉','VisionCorrectionLeft','eye',50);
				$this->collectInfoByTag($decoded,'色觉','外眼','ColorVision','eye');
				$this->collectInfoByTag($decoded,'外眼','眼科其它','Extraocular','eye');
				$this->collectInfoByTag($decoded,'眼科其它','眼底镜检查','EyeOther','eye');
				$this->collectInfoByTagDesc($decoded,'眼底镜检查','初步意见','Ophthalmoscopy','eye',200);
				if($this->information['eye']['Ophthalmoscopy'] != ''){
					$this->information_flag['eye']['flag'] = true;
				}
			}
//			//耳鼻喉科
			if(!isset($this->information_flag['Otolaryngology']['flag'])){
//				echo $decoded ;
//				$temp_str = $this->getTable($decoded,'耳鼻咽喉科','口腔科');
//				echo $temp_str.'<br/><br/><br/><br/><br/>';
				$this->collectInfoByTag($decoded,'外耳','外耳道','ExternalEar','Otolaryngology');
				$this->collectInfoByTag($decoded,'外耳道','鼓膜','Canal','Otolaryngology');
				$this->collectInfoByTag($decoded,'鼓膜','鼻腔','Eardrum','Otolaryngology');
				$this->collectInfoByTag($decoded,'鼻腔','鼻中隔','NasalCavity','Otolaryngology');
				$this->collectInfoByTag($decoded,'鼻中隔','咽','NasalSeptum','Otolaryngology');
				$this->collectInfoByTag($decoded,'咽','扁桃体','Pharynx','Otolaryngology',-1);
				$this->collectInfoByTag($decoded,'扁桃体','耳鼻咽喉科其它','Tonsil','Otolaryngology');
				$this->collectInfoByTag($decoded,'耳鼻咽喉科其它','听力(128HZ音叉)','OtolaryngologyOther','Otolaryngology');
				$this->collectInfoByTag($decoded,'听力(128HZ音叉)','鼻咽镜检查','Hearing','Otolaryngology');
				$this->collectInfoByTagDesc($decoded,'鼻咽镜检查','初步意见','Nasopharyngoscopy','Otolaryngology');
				if($this->information['Otolaryngology']['Nasopharyngoscopy'] != ''){
					$this->information_flag['Otolaryngology']['flag'] = true;
				}
			}
//			//口腔科
			if(!isset($this->information_flag['Dental']['flag'])){
				$this->collectInfoByTag($decoded,'唇','口腔粘膜','Lip','Dental');
				$this->collectInfoByTag($decoded,'口腔粘膜','牙齿','OralMucosa','Dental');
				$this->collectInfoByTag($decoded,'牙齿','牙周','Tooth','Dental');
				$this->collectInfoByTag($decoded,'牙周','舌','Periodontal','Dental');
				$this->collectInfoByTag($decoded,'舌','腭','Tongue','Dental');
				$this->collectInfoByTag($decoded,'腭','腮腺','Palate','Dental');
				$this->collectInfoByTag($decoded,'腮腺','颌下腺','Parotid','Dental');
				$this->collectInfoByTag($decoded,'颌下腺','舌下腺','Submandibular','Dental');
				$this->collectInfoByTag($decoded,'舌下腺','颞下颌关节','SublingualGland','Dental');
				$this->collectInfoByTag($decoded,'颞下颌关节','口腔科其它','TMJ','Dental');
				$this->collectInfoByTagDesc($decoded,'口腔科其它','初步意见','DentalOther','Dental');
				if($this->information['Dental']['DentalOther'] != ''){
					$this->information_flag['Dental']['flag'] = true;
				}
			}
//			//妇科
			if(!isset($this->information_flag['Gynecologic']['flag'])){
				$this->collectInfoByTag($decoded,'外阴','阴道','Vulva','Gynecologic');
				$this->collectInfoByTag($decoded,'阴道','宫颈','Vagina','Gynecologic');
				$this->collectInfoByTag($decoded,'宫颈','子宫','Cervix','Gynecologic',-1);
				$this->collectInfoByTag($decoded,'子宫','附件','Uterus','Gynecologic');
				$this->collectInfoByTag($decoded,'附件','妇科其它','Attachment','Gynecologic');
				
				$this->getSpecialData($decoded,'妇科其它','念珠样菌','Gynecologic',array('GynecologicOther','VaginalCleanliness'),false,1);
				
				$this->collectInfoByTag($decoded,'念珠样菌','滴虫','RosaryLikeFungus','Gynecologic');
				$this->collectInfoByTag($decoded,'滴虫','其它（白带）','Trichomoniasis','Gynecologic');
				$this->collectInfoByTag($decoded,'其它（白带）','宫颈刮片','Leucorrhea','Gynecologic');
				$this->collectInfoByTagDesc($decoded,'宫颈刮片','初步意见','CervicalSmears','Gynecologic',140);
				if($this->information['Gynecologic']['CervicalSmears'] != ''){
					$this->information_flag['Gynecologic']['flag'] = true;
				}
			}
//			//血常规
			if(!isset($this->information_flag['Blood']['flag'])){
				$this->getSpecialData($decoded,'RBC','HGB','Blood',array('RBC','RBC_normal','RBC_unit'),true);
				$this->getSpecialData($decoded,'HGB','HCT','Blood',array('HGB','HGB_normal','HGB_unit'),true);
				$this->getSpecialData($decoded,'HCT','MCV','Blood',array('HCT','HCT_normal','HCT_unit'),true);
				$this->getSpecialData($decoded,'MCV','MCH','Blood',array('MCV','MCV_normal','MCV_unit'),true);
				
				$this->getSpecialData($decoded,'MCH','MCHC','Blood',array('MCH','MCH_normal','MCH_unit'),true);
				$this->getSpecialData($decoded,'MCHC','RDW-CV','Blood',array('MCHC','MCHC_normal','MCHC_unit'),true);
				$this->getSpecialData($decoded,'RDW-CV','WBC','Blood',array('RDWCV','RDWCV_normal','RDWCV_unit'),true);
				$this->getSpecialData($decoded,'WBC','GRA%','Blood',array('WBC','WBC_normal','WBC_unit'),true);
				
				$this->getSpecialData($decoded,'GRA%','LYM%','Blood',array('GRAPercentage','GRAPercentage_normal','GRAPercentage_unit'),true);
				$this->getSpecialData($decoded,'LYM%','MON%','Blood',array('LYMPercentage','LYMPercentage_normal','LYMPercentage_unit'),true);
				$this->getSpecialData($decoded,'MON%','GRA#','Blood',array('MONPercentage','MONPercentage_normal','MONPercentage_unit'),true);
				$this->getSpecialData($decoded,'GRA#','LYM#','Blood',array('GRAAbsolute','GRAAbsolute_normal','GRAAbsolute_unit'),true);
				
				$this->getSpecialData($decoded,'LYM#','MON#','Blood',array('LYMAbsolute','LYMAbsolute_normal','LYMAbsolute_unit'),true);
				$this->getSpecialData($decoded,'MON#','PLT','Blood',array('MONAbsolute','MONAbsolute_normal','MONAbsolute_unit'),true);
				$this->getSpecialData($decoded,'PLT','MPV','Blood',array('PLT','PLT_normal','PLT_unit'),true);
				$this->getSpecialData($decoded,'MPV','PDW','Blood',array('MPV','MPV_normal','MPV_unit'),true);
				
				$this->getSpecialData($decoded,'PDW','丙氨酸氨基转移酶','Blood',array('PDW','PDW_normal','PDW_unit'),true);
				$this->getSpecialData($decoded,'丙氨酸氨基转移酶','AST','Blood',array('ALT','ALT_normal','ALT_unit'),true);
				$this->getSpecialData($decoded,'AST','Cr','Blood',array('AST','AST_normal','AST_unit'),true);
				$this->getSpecialData($decoded,'Cr','BUN','Blood',array('Cr','Cr_normal','Cr_unit'),true);
				
				$this->getSpecialData($decoded,'BUN','UA','Blood',array('BUN','BUN_normal','BUN_unit'),true);
				$this->getSpecialData($decoded,'UA','FBG','Blood',array('UA','UA_normal','UA_unit'),true);
				$this->getSpecialData($decoded,'FBG','TC','Blood',array('FBG','FBG_normal','FBG_unit'),true);
				$this->getSpecialData($decoded,'TC','TG','Blood',array('TC','TC_normal','TC_unit'),true);
				
				$this->getSpecialData($decoded,'TG','AFP','Blood',array('TG','TG_normal','TG_unit'),true);
				$this->getSpecialData($decoded,'AFP','CEA','Blood',array('AFP','AFP_normal','AFP_unit'),true);
				$this->getSpecialData($decoded,'CEA','尿常规','Blood',array('CEA','CEA_normal','CEA_unit'),true);
				
				if($this->information['Blood']['CEA'] != ''){
					$this->information_flag['Blood']['flag'] = true;
				}
			}
//			//尿常规Urine
			if(!isset($this->information_flag['Urine']['flag'])){
				$arrs = explode('尿常规',$decoded);
				if(count($arrs) > 1){
					$temp_str = $arrs[1];
					$this->getSpecialData($temp_str,'PH','SG','Urine',array('PH'),true);
					$this->getSpecialData($temp_str,'SG','GLU','Urine',array('SG'),true);
					$this->getSpecialData($temp_str,'GLU','LEU','Urine',array('GLU'));
					$this->getSpecialData($temp_str,'LEU','NIT','Urine',array('LEU'));
					
					$this->getSpecialData($temp_str,'NIT','PRO','Urine',array('NIT'));
					$this->getSpecialData($temp_str,'PRO','BLD','Urine',array('PRO'));
					$this->getSpecialData($temp_str,'BLD','KET','Urine',array('BLD'));
					$this->getSpecialData($temp_str,'KET','BIL','Urine',array('KET'));
					
					$this->getSpecialData($temp_str,'BIL','URO','Urine',array('BIL'));
					$this->getSpecialData($temp_str,'URO','心电图','Urine',array('URO'));
					
					if($this->information['Urine']['URO'] != ''){
						$this->information_flag['Urine']['flag'] = true;
					}
				}
				
			}
			//其他检查
			if(!isset($this->information_flag['OtherTests']['flag'])){
				$arrs = explode('尿常规',$decoded);
				if(count($arrs) > 1){
					$temp_str = $arrs[1];
					$this->getSpecialData($temp_str,'医技检查','超声检查','OtherTests',array('SinusRhythm'),false,4);
//					$this->collectInfoByTag($temp_str,'窦性心律','超声检查','SinusRhythm','OtherTests');
					$temp_arrs = explode('超声检查',$temp_str);
					$temp_str = $temp_arrs[1];
//					echo $temp_str;
					$this->collectInfoByTag($temp_str,'肝','胆','Liver','OtherTests');
//					$arr_temp = explode('肝',$temp_str);
//					$arr_temp = explode('脾',$arr_temp[1]);
					$this->collectInfoByTag($temp_str,'胆','胰','Gallbladder','OtherTests');
					
					$this->collectInfoByTag($temp_str,'胰','脾','Pancreas','OtherTests');
					$this->getSpecialData($temp_str,'脾','双肾','OtherTests',array('Spleen'));
					
					$this->getSpecialData($temp_str,'双肾',' ','OtherTests',array('Kidneys'));
//					if($this->information['OtherTests']['Kidneys'] != ''){
//						$this->information_flag['Urine']['flag'] = true;
//					}
				}
				$this->collectInfoByTag($decoded,'胸部正位','颈椎侧位','AnteroposteriorChest','OtherTests');
				$this->collectInfoByTag($decoded,'颈椎侧位','初步意见','LateralCervicalSpine','OtherTests');
				$this->getSpecialData($decoded,'红外线检查','特殊检查','OtherTests',array('MilkThrough'),false,3);
				
				$this->getSpecialData($decoded,'特殊检查','健康专家指导','OtherTests',array('Self'),false,7);
//				$this->collectInfoByTag($decoded,'乳透','经颅多普勒','MilkThrough','OtherTests');
//				$this->collectInfoByTag($decoded,'经颅多普勒','颈椎侧位','AnteroposteriorChest','OtherTests');
				if($this->information['OtherTests']['MilkThrough'] != ''){
					$this->information_flag['OtherTests']['flag'] = true;
				}
			}
		}
	}
	/**
	 * void getSpecialData(String $str,String $start,String $end,String $name,String[] $arr,bool $flag=false,int $offset)
	 * 提取特殊表格信息
	 * @param unknown_type $str				信息源
	 * @param unknown_type $start			提取信息开始标识
	 * @param unknown_type $end				提取信息结束标识
	 * @param unknown_type $name			信息所属科室
	 * @param unknown_type $arr				信息所在数据库中的字段名称集合
	 * @param unknown_type $flag			是否过滤中文字符 true => 过滤中文; false => 不过滤;默认false
	 * @param int $offset					偏移
	 */
	function getSpecialData($str,$start,$end,$name,$arr,$flag=false,$offset=0){
		$temp_str = $this->getTable($str,$start,$end);
		if($flag === true){//过滤中文字符
			$temp_str = @preg_replace ('/[\\x80-\\xff]/', '', $temp_str);
		}
		$temp_arr = $this->getArray($temp_str);
		$i = 1;
		$counts = count($arr);
		foreach($temp_arr as $arr_key){
			if($counts < $i){
				break;
			}
			if($arr_key != ''){
				foreach($arr as $key){
					if($this->information[$name][$key] == ''){
						if($offset <= 0){
							$this->information[$name][$key] = $arr_key;
							$i++;
							break;
						}else{
							$offset--;
//							echo $arr_key;
							break;
						}
						
					}
				}
			}
		}
	}
	/**
	 * String[] getArray(String $str)
	 * 按照换行符分割字符串
	 * @param String $str 源字符串
	 * @return String[] 返回被分割的字符串集合
	 */
	function getArray($str){
		$arr = explode("\r\n",$str);
		if(count($arr) > 1){
			return $arr;		
		}else{
			$arr = explode("\r",$str);
			if(count($arr) > 1){
				return $arr;	
			}else{
				$arr = explode("\n",$str);
				if(count($arr) > 1){
					return $arr;	
				}else{
					return $str;
				}
			}
		}
	}
	/**
	 * Sting getTable(Sting $content , String $start_tag ,String $end_tag)
	 * 返回两个标记字符串之间的字符串
	 * @param $content
	 * @param $start_tag
	 * @param $end_tag
	 * @return String 
	 */
	function getTable($content , $start_tag ,$end_tag,$length=1){
		//*
		$arr = explode($end_tag,$content);
		$temp = explode($start_tag,$arr[0]);
		if($length == -100){
			print_r($arr);
			print_r($temp);
		}
		if($length == -1){
			return $temp[count($temp)-1];
		}else{
			return $temp[1];
		}
		//*/
	}
	function getTableDesc($content , $start_tag ,$end_tag){
		//*
		$arr = explode($start_tag,$content);
		if(count($arr) > 1){
			$temp = explode($end_tag,$arr[1]);
		}
		return $temp[0];
		//*/
	}
	/**
	 * String collectBasicInformation(String $decoded,String $name,String $column,int $length=20)
	 * 提取基本信息
	 * @param $decoded		待截取字符串
	 * @param $name			名称项
	 * @param $column		数据库字段
	 * @param $length		截取长度
	 * @return String 		返回截取字符串
	 */
	public function collectBasicInformation($decoded,$start,$offset,$column,$length=20){
		$this->collectInfoByTag($decoded,$start,$offset,$column,'basic');
	}
	/**
	 * 
	 * @param $decoded
	 * @param $name
	 * @param $offset
	 * @param $column
	 * @param $length
	 */
	public function collectInfoByTag($decoded,$start,$offset,$column,$name,$length=20){
		$result = $this->getTable($decoded,$start,$offset,$length);
		if($length == -100){
			echo $result;
		}
		if($result != ''){
			$this->information[$name][$column] = $this->replaceBlank($result);
			if($this->flag){
				echo $start . $this->replaceBlank($result).'<br/>';
			}
		}
	}
	/**
	 * 
	 * @param unknown_type $decoded
	 * @param unknown_type $start
	 * @param unknown_type $offset
	 * @param unknown_type $column
	 * @param unknown_type $name
	 * @param unknown_type $length
	 */
	public function collectInfoByTagDesc($decoded,$start,$offset,$column,$name,$length=20){
		$result = $this->getTableDesc($decoded,$start,$offset);
		if($result != ''){
			$this->information[$name][$column] = $this->replaceBlank($result);
			if($this->flag){
				echo $start . $this->replaceBlank($result).'<br/>';
			}
		}
	}
	/**
	 * Stirng collectExaminationInfo(String $decoded,String $name,String $column,int $length=100)
	 * 提取体检信息
	 * @param $decoded		待截取字符串
	 * @param $name			名称项
	 * @param $column		数据库字段
	 * @param $alias		hash数组子key
	 * @param $length		截取长度
	 * @return String 		返回截取字符串
	 */
	public function collectExaminationInfo($decoded,$name,$column,$alias,$length=100){
//		echo $decoded;
		$start =  strpos($decoded,$name);
//		echo '<br/>' .$start;
		$result = '';
		if(!$start === false){
//			$start += (mb_strlen(trim($name),'UTF8') + 1)*3;
			$offset = (mb_strlen(trim($name),'UTF8') + 1)*3;
			$result = substr($decoded,$start,$length+$offset);
//			echo $result;
			$result = $this->getTable($result,$name,'\n');
		}
		if($result != ''){
			$this->information[$alias][$column] = $this->replaceBlank($result);
			if($this->flag){
				echo $name . '：' . $this->replaceBlank($result).'<br/>';
			}
		}
	}
	/**
	 * int getSize()
	 * 返回当前提取到数据项的数量
	 * @return int 
	 */
	function getSize(){
		return count($this->information);
	}
	/**
	 * String replaceBlank(String document) 
	 * 去掉字符串中的空白字符
	 * @param String $document 待处理的字符串
	 * @return String 返回处理过的字符串
	 */
	function replaceBlank($document)
	{
		$document = trim($document);
		if (strlen($document) <= 0) {
			return $document;
		}
		$search = array (
		"'([\r\n])[\s]+'",			// 去掉空白字符
		"'&(nbsp|#160);'i"
		);							// 作为 PHP 代码运行
		$replace = array (
		'',
		''
		);
		return @preg_replace ($search, $replace, $document);
	}
	/**
	 * String encode_conver(String $subject,int $function)
	 * GB2312编码转UTF-8
	 * @param $subject
	 * @param $function
	 * @return String 
	 */
	public function encode_conver($subject,$function){
		switch ($function){
			case 1:
				$subject=iconv("GB2312","UTF-8",$subject);break;
			case 2:
				$subject=mb_convert_encoding($subject, "UTF-8", "GB2312");break;
//			case 3:     
//				$subject=gb2utf8($subject);break;
		}
		return $subject;
	}
	/**
	 * String getStrings(String $str)
	 * 返回需要提取的字符串
	 * @param String $str
	 * @return String 
	 */
	function getStrings($str){
		$tempArr = explode(')',$str);
		return $tempArr[0];
	}
	/**
	 * Hash String array getInfo()
	 * 返回提取到的数据数组
	 * @return 返回Hash数组
	 */
	function getInfo(){
		return $this->information;
	}
}
?>