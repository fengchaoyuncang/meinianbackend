﻿var $ = function $(id){return document.getElementById(id)};
function carry(color1,color2){
	var roll = $('roll'),
		roll_interior = $('roll_interior'),
		roll_figure = $('roll_figure'),
		roll_interiors = roll_interior.getElementsByTagName('li'),
		roll_figures = roll_figure.getElementsByTagName('li');
	var width_ = roll.clientWidth;
	function carry_style(){
		var height_ = roll.clientHeight;
		roll_interior.style.width = width_*roll_interiors.length + 'px';
		roll_interior.style.height = height_ + 'px';
		for (var i = 0; i < roll_interiors.length; i++) {
			roll_interiors[i].style.width = width_ + 'px';
			roll_interiors[i].style.height = height_ + 'px';
		};
	};
	carry_style();
	var index = 0;
	roll_figures[0].style.color = color1;
	for (var i = 0; i < roll_figures.length; i++) {
		roll_figures[i].onmouseover = function(){
			for (var i = 0; i < roll_figures.length; i++) {
				if (roll_figures[i] == this) {
					index = i;
					roll_interior.style.left = -(width_*index) + 'px';
					roll_figures[i].style.color = color1;
				}else{
					roll_figures[i].style.color = color2;
				}
			};
		}
	}
	function carry_roll(){
		index += 1;
		if (index >= roll_interiors.length) {
			index = 0;
		};
		roll_interior.style.left = -(width_*index) + 'px';
		for (var i = 0; i < roll_figures.length; i++) {
			if (index == i) {
				roll_figures[i].style.color = color1;
			}else{
				roll_figures[i].style.color = color2;
			};
			roll_figures[i].onmouseover = function(){
				for (var i = 0; i < roll_figures.length; i++) {
					if (roll_figures[i] == this) {
						index = i;
						roll_interior.style.left = -(width_*index) + 'px';
						roll_figures[i].style.color = color1;
					}else{
						roll_figures[i].style.color = color2;
					}
				};
			}
		};
	}
	var time = setInterval(carry_roll,3000);
	roll.onmouseover = function (){
		clearInterval(time);
	}
	roll.onmouseout = function (){
		time = setInterval(carry_roll,3000);
	}
}
function carry_1(){
	var roll_img_carry = $('roll_img_carry'),
		roll_img_carry_interior = $('roll_img_carry_interior'),
		roll_img_carry_interiors = roll_img_carry_interior.getElementsByTagName('li');
	roll_img_carry_interior.innerHTML += roll_img_carry_interior.innerHTML;
	var index = 0;
	var left = $('left');
	var right = $('right');
	left.onclick = function(){
		index--;
		if (index <0 ) {
			index = 0;
		}
		startMove(roll_img_carry_interior,"left",-index*130);
	}
	right.onclick = function(){
		index++;
		if (index >= roll_img_carry_interiors.length-3) {
			index = 0;
		}
		startMove(roll_img_carry_interior,"left",-index*130);
	}
	var t;
	function automove() {
		clearInterval(t);
		t = setInterval(function(){
			index++;
			if (index >= roll_img_carry_interiors.length/2) {
				index = 0;
			}
			startMove(roll_img_carry_interior,"left",-index*130);
		},3000)	
	}
	automove();
	left.onmouseover = function(){
		clearInterval(t);
	}
	left.onmouseout = function(){
		automove();
	}
	right.onmouseover = function(){
		clearInterval(t);
	}
	right.onmouseout = function(){
		automove();
	}
	roll_img_carry.onmouseover = function(){
		clearInterval(t);
	}
	roll_img_carry.onmouseout = function(){
		automove();
	}
}