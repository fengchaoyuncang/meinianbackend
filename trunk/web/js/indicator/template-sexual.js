/**
 * Created with JetBrains PhpStorm.
 * User: tq02ksu
 * Date: 3/26/13
 * Time: 10:03 PM
 * To change this template use File | Settings | File Templates.
 */
(function($) {
    $(function() {
        var $sex_toggles = $('.sex-toggle');
        $('.sex-selector').change(function() {
            $sex_toggles.toggle();
        });


        // female:
        var $f_range = $('input[name=curr_f_value]'),
            $f_wrapper = $('div.range-wrapper'),
            $f_min = $f_wrapper.find('input[name=f_min]'),
            f_min = parseFloat($f_min.val()),
            $f_max = $f_wrapper.find('input[name=f_max]'),
            f_max = parseFloat($f_max.val()),
            $under = $('div.under_reason'),
            $over = $('div.over_reason');

        $f_range.on('slidestop', function() {
            var f_val = parseFloat($(this).val());

            if (f_val <= f_min) {
                $under.trigger('expand');
            } else if (f_val >= f_max) {
                $over.trigger('expand');
            } else {
                $under.trigger('collapse');
                $over.trigger('collapse')
            }
        });

        var $m_range = $('input[name=curr_m_value]'),
            $m_wrapper = $f_wrapper,
            $m_min = $m_wrapper.find('input[name=m_min]'),
            m_min = parseFloat($m_min.val()),
            $m_max = $m_wrapper.find('input[name=m_max]'),
            m_max = parseFloat($m_max.val());

        $m_range.on('slidestop', function() {
            var m_val = parseFloat($(this).val());

            if (m_val <= m_min) {
                $under.trigger('expand');
            } else if (m_val >= m_max) {
                $over.trigger('expand');
            } else {
                $under.trigger('collapse');
                $over.trigger('collapse')
            }
        });
    });
})(jQuery);