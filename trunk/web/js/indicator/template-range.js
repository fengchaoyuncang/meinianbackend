/**
 * Created with JetBrains PhpStorm.
 * User: tq02ksu
 * Date: 3/26/13
 * Time: 10:03 PM
 * To change this template use File | Settings | File Templates.
 */
(function($) {
    $(function() {
        var $range = $('input[name=curr_value]'),
            $wrapper = $range.parentsUntil('div.range-wrapper'),
            $min = $wrapper.find('input[name=min]'),
            min = parseFloat($min.val()),
            $max = $wrapper.find('input[name=max]'),
            max = parseFloat($max.val()),
            $under = $('div.under_reason'),
            $over = $('div.over_reason');

        $range.on('slidestop', function() {
            var val = parseFloat($(this).val());

            if (val <= min) {
                $under.trigger('expand');
            } else if (val >= max) {
                $over.trigger('expand');
            } else {
                $under.trigger('collapse');
                $over.trigger('collapse');
            }
        });
    });
})(jQuery);
