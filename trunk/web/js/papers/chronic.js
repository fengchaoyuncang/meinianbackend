(function($) {
    $(function() {
        // enable jquery texteditor
        $('textarea.editor').jqte();

        var select = $('select[name="Papers[type]"]'),
            chronics = $('.chronic');

        select.change(function() {
            if (select.val() == '2') {
                chronics.fadeIn();
            } else {
                chronics.fadeOut();
            }
        }).change();
    });
})(jQuery);