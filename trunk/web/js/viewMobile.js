$(function () {
  $('ul li h2').click(function () {
    $(this).next('div').slideToggle('normal');
  })
  var t;
  $('#text_value').val($('#text_range').val());
  $('#text_range').mouseover(function () {
    function carr () {
      $('#text_value').val($('#text_range').val());
    }
    t = setInterval(carr, 10);
  })
  $('#text_range').mouseout(function () {
    clearInterval(t);
  })
  var num = 0, g, l, k, m = 0;
  $('#text_value_two').val(returnFloat1($('#text_range_f').val()));
  m++;
  if(1 === m) {
    function e () {
      $('#text_value_two').val(returnFloat1($('#text_range_f').val()));
      clearTimeout(l);
      k = setTimeout(e, 10);
    }
    e();
  }
  $('#gender').click(function () {
    num++;
    if(0 === (num % 2)) {
      manz();
    } else {
      womanz();
    }
  })
  function manz () {
    $('#zman').attr('class', 'zman');
    $('#woman').attr('class', 'woman');
    $('#among').attr('class', 'among');
    function h () {
      $('#text_value_two').val(returnFloat1($('#text_range_f').val()));
      clearTimeout(l);
      g = setTimeout(h, 10);
    }
    h();
    $('#text_range_m').css('display', 'none');
    $('#text_range_f').css('display', 'block');
    $('#text_text_m').css('display', 'none');
    $('#text_text_f').css('display', 'block');
    $('#zman').html('男');
  }
  function womanz () {
    $('#zman').attr('class', 'zman_last');
    $('#woman').attr('class', 'woman_last');
    $('#among').attr('class', 'among_last');
    function n () {
      $('#text_value_two').val(returnFloat1($('#text_range_m').val()));
      clearTimeout(k);
      clearTimeout(g);
      l = setTimeout(n, 10);
    }
    n();
    $('#text_range_m').css('display', 'block');
    $('#text_range_f').css('display', 'none');
    $('#text_text_m').css('display', 'block');
    $('#text_text_f').css('display', 'none');
    $('#zman').html('');
  }
});
function returnFloat1 (value) {
  value = Math.round(parseFloat(value) * 10) / 10;
  if (value.toString().indexOf('.') < 0) {
    value = value.toString() + '.0';
  };
  return value;
}
