/*
 * 全选订单
 * @param 无
 * return 选中订单前面的复选框
 */
function checkboxSelect() {
    //全选订单
    $("#checkAll").click(function() {
        $('input[name="subBox"]').attr("checked", this.checked);
    });
    var $subBox = $("input[name='subBox']");
    $subBox.click(function() {
        $("#checkAll").attr("checked", $subBox.length == $("input[name='subBox']:checked").length ? true : false);
    });
}

/*
 * 批量更改订单状态
 * @param url 提交地址
 * @param status 订单状态
 * return 在界面上删除处理过的订单
 */
function handleOrder(url, status) {
    checkedValue = [];
    userIds = {};
    $("input[name='subBox']:checkbox:checked").each(function() {
        checkedValue.push($(this).val());
        userIds = JSON.stringify(checkedValue);
    });

    $.ajax({
        type: 'POST',
        url: url,
        data: {userIds: userIds, status: status},
        success: function(data) {
            if (data != 0) {
                $("input[name='subBox']:checkbox").each(function() {
                    if (this.checked) {
                        $('#table_list input[type="checkbox"]:checked').parent().parent().remove();
                    }
                });

            }
        }
    });
}

